use util::{FxHashMap as HashMap, FxHashSet as HashSet};
use chrono::prelude::*;
use kitchen_sink::{CrateOwner, DependerChanges, KitchenSink, MiniDate, Origin, OwnerKind, StatsHistogram};
use libflate::gzip::Decoder;
use rayon::prelude::*;
use reqwest::header;
use serde::{Deserialize, Deserializer};
use tokio::try_join;
use std::io::{BufReader, Read};
use std::num::{NonZeroU32, NonZeroU64};
use std::sync::{mpsc, Arc};
use tar::Archive;
use util::SmolStr;

mod dashboard_scrape;

struct Incident<Date, Set> {
    start: Date, end: Date,
    headroom: u32, lookaround: u8,
    names: Set,
}

// start end crates affected
const DOWNLOAD_SPAM_INCIDENTS: [Incident<&'static str, &'static [&'static str]>; 7] = [
    Incident { start: "2021-12-10", end: "2022-04-10", headroom: 4000, lookaround: 7, names: &[
        "vsdb", "btm", "vsdbsled", "vsdb_derive","ruc",
    ]},
    Incident { start: "2021-12-21", end: "2022-01-22", headroom: 4000, lookaround: 7, names: &[
        "ppv-lite86", "serde_cbor","time","fast-math","ieee754","time-macros","once_cell", "clap", "serde", "memoffset",
        "rand", "rand_core", "lazy_static", "half", "nix","cfg-if", "log", "libc", "num-traits", "serde_derive", "getrandom", "rand_chacha", "parking_lot", "lock_api",
        "instant", "parking_lot_core", "smallvec", "scopeguard", "sha3", "digest", "keccak", "proc-macro2", "unicode-xid", "quote", "syn", "fs2", "crc32fast", "fxhash",
        "byteorder", "crossbeam-epoch", "crossbeam-utils", "memoffset", "autocfg", "const_fn", "rio", "bitflags",
        "rocksdb", "librocksdb-sys", "zstd", "zstd-safe", "zstd-sys", "cc", "jobserver", "crypto-common", "generic-array", "version_check", "typenum", "block-buffer",
        "unicode-width", "atty", "itoa",
    ]},
    Incident { start: "2022-06-15", end: "2022-07-17", headroom: 0, lookaround: 7, names: &[
        "audiopus_sys", "mmids-core", "proc-mounts", "serde-pickle", "speexdsp-resampler", "stdext", "azure_data_cosmos",
        "azure_core", "tonic-web", "wav", "webrtc", "ccm", "debug-helper", "interceptor", "mock_instant", "sdp", "stdext",
        "turn", "webrtc-data", "webrtc-ice", "webrtc-dtls", "webrtc-mdns", "webrtc-media", "cranelift-isle", "str_stack", "ed25519-consensus", "igd",
        "riff", "webrtc-srtp", "webrtc-sctp", "partition-identity", "substring", "iter-read", "cidr-utils", "der-oid-macro",
        "x509-parser", "yasna"
    ]},
    Incident { start: "2022-06-29", end: "2022-07-14", headroom: 0, lookaround: 7, names: &[
        "va_list", "vkrs", "xmpp-parsers", "vpncloud", "symbolic-sourcemap", "symbolic-symcache", "tag_safe", "ydcv-rs", "way-cooler", "superlu-sys",
        "symbolic-proguard", "wemo", "xhtmlchardet", "tinysearch", "tenacious", "tofu", "tojson_macros", "urdict", "twitter-api",
        "uchardet", "task_queue", "yatlv", "table", "zmq-rs", "unixbar", "unrar", "zombie", "toml-config", "vorbis",
        "symbolic-debuginfo", "telemetry", "xcolor", "syntaxext_lint", "treena", "traverse", "symbolic_polynomials",
        "symbolic-unreal", "skeletal_animation", "td_revent", "symbolic-symcache", "telegram-bot", "v8-sys", "secp256k1",
        "test-assembler",
    ]},
    Incident { start: "2022-06-29", end: "2022-07-13", headroom: 0, lookaround: 2, names: SOURCEGRAPH_SPAMMED},
    Incident { start: "2022-06-29", end: "2022-07-13", headroom: 300, lookaround: 7, names: &[]},
    Incident { start: "2022-07-06", end: "2022-07-13", headroom: 100, lookaround: 2, names: &[]},
];

const SOURCEGRAPH_SPAMMED: &[&str] = &[
    "abomonation", "alsa", "assimp-sys", "aster", "atom_syndication", "aws-smithy-protocol-test", "bio", "bootloader", "cargo", "cargo-edit",
    "cargo-outdated", "cargo-readme ", "cobs", "clippy", "compiletest_rs", "conrod", "coreaudio-sys", "cortex-a", "cpython", "cron", "crust", "datetime",
    "decimal", "devicemapper", "elastic-array ", "elastic-array", "euclid", "flame", "flexi_logger", "freetype-rs", "ftp", "gdk", "generator",
    "genmesh", "gleam", "gstreamer", "gtk", "igd", "imageproc", "immeta", "jsonrpc", "kuchiki", "liquid", "llvm-sys", "lodepng", "mp4parse",
    "mysql", "nalgebra", "notify-rust", "obj", "opencv", "pbr", "pcap", "piston2d-gfx_graphics ", "piston2d-opengl_graphics", "piston_window",
    "pistoncore-sdl2_window ", "plist", "pnet_macros", "polars-core", "postgis", "primal-sieve ", "primal-sieve", "pty", "r2d2-diesel",
    "r2d2_mysql", "racer", "regex_macros", "router", "routing", "rss", "rust-htslib", "rustfmt", "rustler", "select", "self_encryption",
    "servo-fontconfig-sys", "servo-glutin", "servo-skia", "signal", "sprs", "stb_truetype", "symbolic-debuginfo", "symbolic_demangle",
    "sysfs_gpio", "sysinfo", "systemd", "systemstat", "timely", "tobj", "tokei", "utime", "va_list", "wavefront_obj", "xcb",
    "tract-core", "vkrs", "vpncloud", "xmpp-parsers", "way-cooler", "ydcv-rs", "tag_safe", "symbolic-sourcemap", "symbolic-unreal",
    "symbolic-symcache", "skeletal_animation", "v8-sys", "td_revent", "superlu-sys", "symbolic-proguard", "telegram-bot", "rustorm",
    "rustfbp", "scaly", "slack", "scribe", "rustwlc", "rsgenetic", "select_color", "rs-graph", "sdl2_ttf", "slabmalloc", "sdl2_image",
    "rsteam", "rusted_cypher", "speedtest-rs", "sassers", "spaceapi", "rust-mpfr", "sdl2_mixer", "shuttle", "sexp", "serde_xml",
    "sel4-start", "smtp", "by_address", "sawtooth-xo", "rosc", "ssbh_lib", "ssdp", "sel4-sys", "static-server", "squash-sys",
    "tokei", "rs-es", "rusp", "snailquote", "sql_lexer", "rusoto", "cobs",
];

const NUM_CRATES: usize = 170_000;
type BoxErr = anyhow::Error;
use anyhow::Context;

enum ParsedFile {
    DateOfFileInit(DateTime<Utc>),
    CrateOwners(CrateOwners),
    Crates(CratesMap),
    Teams(Teams),
    Users(Users),
    Downloads(VersionDownloads),
    Versions(VersionsMap),
    Dependencies(CrateDepsMap),
}

#[tokio::main]
async fn main() {
    env_logger::init();

    if let Err(e) = run().await {
        eprintln!("datadump failed: {e}");
        let mut src = e.source();
        while let Some(e) = src {
            eprintln!(" • {e}");
            src = e.source();
        }
        std::process::exit(1);
    }
}

    async fn run() -> Result<(), BoxErr> {
        let path = std::env::args_os().nth(1);
        let mut date_of_file = std::env::args().nth(2)
            .map(|d| DateTime::<Utc>::from_utc(date_from_str(&d).expect("yyyy-mm-dd").and_hms_opt(0,0,0).expect("datearg"), Utc))
            .unwrap_or_else(Utc::now);
        let handle = tokio::runtime::Handle::current();
        let ksink = Arc::new(KitchenSink::new_default().await?);
        let last_fetch_timestamp_path = ksink.main_data_dir().join("datadump.timestamp");
        let wk = Utc::now().iso_week();
        let rust_usage_crates_io_stats = ksink.main_data_dir().join(format!("rust_usage_crates_io_stats-{}-{}.csv", wk.year(), wk.week()));
        if let Err(e) = dashboard_scrape::refresh_usage(&rust_usage_crates_io_stats).await {
            eprintln!("error: {e} in {rust_usage_crates_io_stats:?}");
        }

        let (parsed_file_send, parsed_file_recv) = mpsc::sync_channel(2);

        let process_res = handle.clone().spawn_blocking({
            let ksink = ksink.clone();
            move || {
            let mut crate_owners: Option<CrateOwners> = None;
            let mut crates: Option<CratesMap> = None;
            let mut teams: Option<Teams> = None;
            let mut users: Option<Users> = None;
            let mut downloads: Option<VersionDownloads> = None;
            let mut versions: Option<VersionsMap> = None;
            let mut dependencies: Option<CrateDepsMap> = None;
            let mut indexed_active_rev_dependencies = false;
            let mut indexed_despammed_downloads = false;
            let mut indexed_users = false;
            let mut date_of_file = None;
            let mut result = None;

            for file in parsed_file_recv {
                match file {
                    ParsedFile::DateOfFileInit(d) => {
                        date_of_file = Some(d);
                    },
                    ParsedFile::CrateOwners(f) => {
                        crate_owners = Some(f);
                    },
                    ParsedFile::Crates(f) => {
                        crates = Some(f);
                    },
                    ParsedFile::Teams(f) => {
                        teams = Some(f);
                    },
                    ParsedFile::Users(f) => {
                        users = Some(f);
                    },
                    ParsedFile::Downloads(f) => {
                        downloads = Some(f);
                    },
                    ParsedFile::Versions(f) => {
                        versions = Some(f);
                    },
                    ParsedFile::Dependencies(f) => {
                        dependencies = Some(f);
                    },
                };

                if !indexed_users {
                    if let (Some(crates), Some(crate_owners), Some(teams), Some(users), Some(date_of_file)) = (&crates, &crate_owners, &teams, &users, date_of_file) {
                        indexed_users = true;
                        eprintln!("Indexing owners of {} crates", crate_owners.len());
                        result = Some((process_owners(crates, crate_owners, teams, users), date_of_file.to_string()));
                    }
                    if indexed_users {
                        users = None; teams = None;
                    }
                }

                if !indexed_despammed_downloads {
                    if let (Some(crates), Some(versions), Some(dependencies), Some(date_of_file)) = (&crates, &versions, &dependencies, date_of_file) {
                        if let Some(mut downloads) = downloads.take() {
                            indexed_despammed_downloads = true;
                            filter_download_spam(crates, versions, &mut downloads);
                            eprintln!("Versions histogram");
                            versions_histogram(crates, versions, dependencies, &downloads, &ksink, date_of_file)
                                .context("versions_histogram")?;
                            eprintln!("Indexing downloads for {} crates, {} dl-versions", versions.len(), downloads.len());
                            index_downloads(crates, versions, &downloads, &ksink);
                        }
                    }
                }

                if !indexed_active_rev_dependencies {
                    if let (Some(crates), Some(versions), Some(dependencies), Some(crate_owners), Some(date_of_file)) = (&crates, &versions, &dependencies, &crate_owners, date_of_file) {
                        indexed_active_rev_dependencies = true;
                        eprintln!("Indexing dependencies for {} crates", dependencies.len());
                        index_active_rev_dependencies(crates, versions, dependencies, crate_owners, &ksink, date_of_file)
                            .context("index_active_rev_dependencies")?;
                    }
                }

                if indexed_despammed_downloads && indexed_active_rev_dependencies {
                    versions = None;
                    dependencies = None;
                }

                if indexed_users && indexed_active_rev_dependencies {
                    crate_owners = None;
                }
            }
            Ok::<_, BoxErr>(result)
        }});

        let parse_csv = handle.clone().spawn_blocking({
            let last_fetch_timestamp_path = last_fetch_timestamp_path.clone();
            move || -> Result<(), BoxErr> {
            let date_of_file_str;
            let mut tmp1;
            let mut tmp2;
            let src: &mut dyn Read = if let Some(path) = path {
                date_of_file_str = date_of_file.to_string();
                eprintln!("Loading local with date {date_of_file_str}");
                tmp1 = std::fs::File::open(path)?;
                &mut tmp1
            } else {
                let last_fetch_job = std::fs::read_to_string(&last_fetch_timestamp_path).ok();

                // I can't be bothered to make async stream adapter to make async body impl Read
                tmp2 = reqwest::blocking::get("https://static.crates.io/db-dump.tar.gz")?;
                if let Some(lastmod) = tmp2.headers().get(header::LAST_MODIFIED)
                    .and_then(|h| h.to_str().ok())
                    .and_then(|d| DateTime::parse_from_rfc2822(d).ok())
                    .map(|d| d.with_timezone(&Utc)) {
                    if lastmod > Utc::now() - chrono::Duration::try_seconds(48 * 3600).unwrap() {
                        date_of_file = lastmod;
                    } else {
                        eprintln!("ERROR: last-modified says {lastmod}; db dump became stale?");
                    }
                }
                date_of_file_str = date_of_file.to_string();
                if last_fetch_job.as_ref().is_some_and(|j| j == &date_of_file_str) {
                    eprintln!("Already fetched dump from {date_of_file_str}; skipping");
                    return Ok::<_, BoxErr>(());
                }

                eprintln!("Fetching from net with date {date_of_file_str} (previous {last_fetch_job:?})");
                &mut tmp2
            };
            parsed_file_send.send(ParsedFile::DateOfFileInit(date_of_file)).unwrap();

            let res = BufReader::with_capacity(8_000_000, src);
            let mut a = Archive::new(Decoder::new(res)?);

            for file in a.entries()? {
                let file = file?;
                if !file.header().entry_type().is_file() {
                    continue;
                }
                let path = file.path()?;
                let Some(path) = path.file_name().and_then(|f| f.to_str()) else { continue };

                eprint!("{path} ({}MB): ", file.header().size()? / 1000 / 1000);
                let parsed = match path {
                    "crate_owners.csv" => {
                        eprintln!("parse_crate_owners…");
                        ParsedFile::CrateOwners(parse_crate_owners(file).context("crate_owners.csv")?)
                    },
                    "crates.csv" => {
                        eprintln!("parse_crates…");
                        ParsedFile::Crates(parse_crates(file).context("crates.csv")?)
                    },
                    "teams.csv" => {
                        eprintln!("parse_teams…");
                        ParsedFile::Teams(parse_teams(file).context("teams.csv")?)
                    },
                    "users.csv" => {
                        eprintln!("parse_users…");
                        ParsedFile::Users(parse_users(file).context("users.csv")?)
                    },
                    "version_downloads.csv" => {
                        eprintln!("parse_version_downloads…");
                        ParsedFile::Downloads(parse_version_downloads(file).context("version_downloads.csv")?)
                    },
                    "versions.csv" => {
                        eprintln!("parse_versions…");
                        ParsedFile::Versions(parse_versions(file).context("versions.csv")?)
                    },
                    "dependencies.csv" => {
                        eprintln!("parse_dependencies");
                        ParsedFile::Dependencies(parse_dependencies(file).context("dependencies.csv")?)
                    },
                    // expected ignored
                    "reserved_crate_names.csv" | // not publishing any
                    "version_authors.csv" | // is in index
                    "badges.csv" | // got from cargo.tomls
                    "crate_downloads.csv" | // version downloads is better
                    "crates_categories.csv" | // got better data than this
                    "crates_keywords.csv" | "keywords.csv" | // got better data than this
                    "categories.csv" | // got my own categories
                    "metadata.json" | "README.md" | // not relevant
                    "import.sql" | "export.sql" | "schema.sql" | // NoSQL
                    "metadata.csv" | "default_versions.csv"
                    => {
                        eprintln!("skip");
                        continue;
                    },
                    p => {
                        eprintln!("Ignored unexpected file {p}");
                        continue;
                    },
                };
                parsed_file_send.send(parsed).unwrap();
            }
            eprintln!("Finished csv download.");
            Ok(())
        }});

    let (process_res, parse_csv) = try_join!(process_res, parse_csv)?;
    parse_csv?;

    if let Some((owners, date_of_file_str)) = process_res? {
        if let Err(e) = dashboard_scrape::refresh_usage(&rust_usage_crates_io_stats).await {
            eprintln!("error: {e} in {rust_usage_crates_io_stats:?}");
        }

        eprintln!("Upserting owners of {} crates", owners.len());
        ksink.index_crates_io_crate_all_owners(owners, date_of_file).await
            .inspect_err(|e| eprintln!("will not mark job as done {e}"))?;
        eprintln!("DONE {date_of_file_str}");
        std::fs::write(&last_fetch_timestamp_path, date_of_file_str)?;
    }
    Ok(())
}

// Cap spammed download data to ~prev week's max during incident period
#[inline(never)]
fn filter_download_spam(crates: &CratesMap, versions_by_crate: &VersionsMap, downloads: &mut VersionDownloads) {
    // the downloads in the datadump aren't complete, so incidents can be fixed only when they're recent
    let earliest_date_available = downloads.values().flatten().map(|&(d, _, _)| d).min().unwrap();
    let incidents: Vec<_> = DOWNLOAD_SPAM_INCIDENTS.iter().filter_map(|&Incident {start,end,headroom,lookaround,names}| {
        let start = date_from_str(start).unwrap();
        // otherwise it won't have enough before/after data
        if earliest_date_available > start - chrono::Duration::try_days(lookaround.into()).unwrap() {
            return None;
        }
        Some(Incident {
            start,
            end: date_from_str(end).unwrap(),
            headroom, lookaround,
            names: names.iter().copied().collect::<HashSet<_>>(),
        })
    }).collect();
    if incidents.is_empty() {
        return;
    }
    eprintln!("Despamming {} incidents", incidents.len());
    for (crate_id, name) in crates {
        let crate_vers = versions_by_crate.get(crate_id).expect(name);
        for &Incident { start, end, headroom, lookaround, .. } in incidents.iter().filter(|i| i.names.is_empty() || i.names.contains(name.as_str())) {
            let min_date = start - chrono::Duration::try_days(lookaround.into()).unwrap();
            let max_date = end + chrono::Duration::try_days(lookaround.into()).unwrap();

            for version_id in crate_vers.iter().map(|row| row.id) {
                if let Some(dl) = downloads.get_mut(&version_id) {
                    let before = dl.iter().filter(|(day, _, _)| (*day >= min_date && *day < start) )
                        .map(|&(_, dl, _)| dl).max().unwrap_or(0);
                    let after = dl.iter().filter(|(day, _, _)| (*day <= max_date && *day > end) )
                        .map(|&(_, dl, _)| dl).max().unwrap_or(before * 5 / 4);
                    let max_dl = headroom + before.max(after);
                    let expected = (before + after)/2;

                    dl.iter_mut()
                        .filter(move |(day, dl, _)| {
                            *dl > max_dl && *day >= start && *day <= end
                        })
                        .for_each(move |(_day, dl, ovr)| {
                            // eprintln!("cut {day} {dl} > {max_dl} to {expected} for {name} in {start}-{end} incident");
                            *dl = expected;
                            *ovr = true;
                        });
                }
            }
        }
    }
}

#[inline(never)]
fn index_downloads(crates: &CratesMap, versions: &VersionsMap, downloads: &VersionDownloads, ksink: &KitchenSink) {
    for (crate_id, name) in crates {
        if let Some(vers) = versions.get(crate_id) {
            let data = vers
                .iter()
                .filter_map(|version| {
                    if let Some(d) = downloads.get(&version.id) {
                        return Some((version.num.as_str(), d.as_slice()));
                    }
                    None
                })
                .collect();
            if let Err(e) = ksink.index_crate_downloads(name, &data) {
                eprintln!("Can't index downloads for {name}: {e}");
            }
        } else {
            eprintln!("Bad crate? {crate_id} {name}");
        }
    }
    ksink.post_index_crate_downloads();
}

const EXAMPLES_PER_BUCKET: usize = 5;

fn versions_histogram(crates: &CratesMap, versions: &VersionsMap, deps: &CrateDepsMap, downloads: &VersionDownloads, ksink: &KitchenSink, now: DateTime<Utc>) -> Result<(), BoxErr> {
    let mut num_releases = HashMap::default();
    let mut crate_sizes = HashMap::default();
    let mut crate_bandwidth_sizes = HashMap::default();
    let mut num_deps = HashMap::default();
    let mut age = HashMap::default();
    let mut maintenance = HashMap::default();
    let mut languish = HashMap::default();
    let today = now.date_naive();
    let monthly_downloads_cutoff_date = today - chrono::Duration::days(30);

    // hopefully the hashmap is randomizing examples
    for (crate_id, name) in crates {
        let name = name.as_str();
        let Some(v) = versions.get(crate_id) else { continue };
        let non_yanked_releases = v.iter().filter(|d| !d.yanked).count() as u32;
        if non_yanked_releases == 0 {
            continue;
        }

        if let Some(latest) = v.iter().max_by_key(|v| v.id) {
            let Some(oldest) = v.iter().min_by_key(|v| v.id) else { continue };
            let latest_date = latest.created_at.with_timezone(&Utc).date_naive();
            let oldest_date = oldest.created_at.with_timezone(&Utc).date_naive();
            insert_hist_item(&mut age, today.signed_duration_since(oldest_date).num_weeks() as u32, name);
            insert_hist_item(&mut languish, today.signed_duration_since(latest_date).num_weeks() as u32, name);
            let maintenance_weeks = if v.len() == 1 {
                0
            } else {
                latest_date.signed_duration_since(oldest_date).num_weeks().max(1) as u32
            };
            insert_hist_item(&mut maintenance, maintenance_weeks, name);

            let bandwidth = v.iter()
                .filter_map(|ver| {
                    let dl = downloads.get(&ver.id)?;
                    let sz = ver.crate_size?.get();
                    Some(dl.iter().copied()
                        .filter(|&(date, _, _)| date >= monthly_downloads_cutoff_date)
                        .map(|(_, num, _)| u64::from(num) * sz)
                        .sum::<u64>())
                })
                .sum::<u64>();

            // coarser buckets
            let bandwidth_size_mib = if bandwidth > 1_000_000 * 10_000 {
                bandwidth / (1_000_000 * 1000) * 1000
            } else if bandwidth > 1_000_000 * 100 {
                bandwidth / (1_000_000 * 10) * 10
            } else {
                bandwidth / 1_000_000
            };
            if bandwidth_size_mib > 0 {
                insert_hist_item(&mut crate_bandwidth_sizes, bandwidth_size_mib.min(u64::from(u32::MAX)) as u32, name);
            }

            if let Some(size) = latest.crate_size {
                let size_kib = if size.get() > 5_000_000 {
                    size.get() / (1000 * 500) * 500 // coarser buckets
                } else {
                    size.get() / 1000
                };
                insert_hist_item(&mut crate_sizes, size_kib.min(u64::from(u32::MAX)) as u32, name);
            }

            insert_hist_item(&mut num_deps, deps.get(&latest.id).map(|d| d.as_slice()).unwrap_or_default().len() as u32, name);
        }
        insert_hist_item(&mut num_releases, non_yanked_releases, name);
    }
    ksink.index_stats_histogram("releases", num_releases)?;
    ksink.index_stats_histogram("sizes", crate_sizes)?;
    ksink.index_stats_histogram("bandwidth_sizes", crate_bandwidth_sizes)?;
    ksink.index_stats_histogram("deps", num_deps)?;
    ksink.index_stats_histogram("age", age)?;
    ksink.index_stats_histogram("maintenance", maintenance)?;
    ksink.index_stats_histogram("languish", languish)?;


    Ok(())
}

fn insert_hist_item(histogram: &mut StatsHistogram, val: u32, name: &str) {
    let t = histogram.entry(val).or_insert((0, Vec::<String>::new()));
    t.0 += 1;
    if t.1.len() < EXAMPLES_PER_BUCKET {
        t.1.push(name.to_owned());
    }
}

struct DepUse {
    start_date: MiniDate,
    end_date: MiniDate,
    expired: bool,
    by_crate_id: CrateId,
}

#[derive(Default)]
pub struct DepChangeAggregator {
    pub added: u16,
    /// Crate has released a new version without this dependency
    pub removed: u16,
    /// Crate has this dependnecy, but is not active any more
    pub expired: u16,
    pub by_owner: HashMap<(NonZeroU32, u8), f32>, // owner id, owner kind
}

/// Direct reverse dependencies, but with release dates (when first seen or last used)
#[inline(never)]
fn index_active_rev_dependencies(crates: &CratesMap, versions: &VersionsMap, deps: &CrateDepsMap, owners: &CrateOwners, ksink: &KitchenSink, date_of_file: DateTime<Utc>) -> Result<(), BoxErr> {
    let mut deps_changes = HashMap::with_capacity_and_hasher(crates.len(), Default::default());

    for (crate_id, name) in crates {
        let has_multiple_owners = owners.get(crate_id).is_some_and(|o| o.len() > 1);
        let Some(vers) = versions.get(crate_id) else {
            eprintln!("Bad crate? {crate_id} {name}");
            continue;
        };
        let mut releases_from_oldest: Vec<_> = vers.iter().map(|v| {
            (v.id, MiniDate::new(v.created_at.date_naive()), &v.num)
        }).collect();
        if releases_from_oldest.is_empty() {
            continue; // shouldn't happen
        }
        releases_from_oldest.sort_unstable_by_key(|a| a.0);

        let mut over_time = HashMap::default();
        let mut releases = releases_from_oldest.iter().peekable();
        while let Some(&(ver_id, release_date, version_str)) = releases.next() {
            let expired;
            let end_date = if let Some(&&(_, next_release_date, _)) = releases.peek() {
                expired = false;
                // if the next releases is going to drop it, then attribute drop date
                // to some time between the releases
                release_date.half_way(next_release_date)
            } else {
                expired = true;
                // it's the final release, so relevance is now until the death of the crate.
                //
                // approximate how junky or stable the crate is
                // assuming that experimental versions get abandoned quickly
                // and 1.x are relevant for longer
                //
                // FIXME: this logic should be unified with ranking
                let junk = version_str.as_str() == "0.1.0";
                let unstable = version_str.starts_with("0.");
                // a little bit of deterministic fuzzyness
                // to make date cut-offs less sharp
                let rand = (ver_id.get() % 17) as i32;
                let days_fresh = if junk {
                    30 * 2 + rand + if has_multiple_owners { 14 } else { 0 } // assume more owners means it's likely to be a dead hobby project
                } else if unstable && releases_from_oldest.len() < 3 {
                    30 * 6 + rand * 2 + if has_multiple_owners { 30 } else { 0 }
                } else if unstable || releases_from_oldest.len() < 3 {
                    365 + rand * 3 + if has_multiple_owners { 90 } else { 0 }
                } else {
                    365 * 2 + rand * 4 + if has_multiple_owners { 365 } else { 0 }
                };
                release_date.days_later(days_fresh)
            };

            for &dep_crate_id in deps.get(&ver_id).map(Vec::as_slice).unwrap_or_default() {
                if dep_crate_id == *crate_id {
                    // libcpocalypse semver trick - not relevant
                    continue;
                }
                let e = over_time.entry(dep_crate_id).or_insert(DepUse {start_date: release_date, end_date, expired, by_crate_id: *crate_id});
                if e.end_date < end_date {
                    e.end_date = end_date;
                    e.expired = expired;
                }
            }
        }

        for (dep_crate_id, first_last) in over_time {
            deps_changes.entry(dep_crate_id).or_insert_with(Vec::new).push(first_last);
        }
    }

    let today = MiniDate::new(date_of_file.date_naive());
    deps_changes.par_iter().try_for_each(move |(dep_crate_id, uses)| {
        let mut by_day = HashMap::with_capacity_and_hasher(uses.len() * 2, Default::default());
        for &DepUse {start_date, end_date, expired, by_crate_id} in uses {
            let start_use = by_day.entry(start_date).or_insert_with(DepChangeAggregator::default);
            start_use.added += 1;
            // owners are supposed to add up to 1, so that a one crate with lots of owners doesn't create lots of users, only one "user"
            let owners = owners.get(&by_crate_id).map(Vec::as_slice).unwrap_or_default();
            if owners.is_empty() {
                continue;
            }
            let per_owner_weight = 1. / owners.len() as f32;
            for o in owners {
                *start_use.by_owner.entry((o.owner_id, o.owner_kind)).or_default() += per_owner_weight;
            }
            if end_date <= today {
                let e = by_day.entry(end_date).or_insert_with(DepChangeAggregator::default);
                for o in owners {
                    *e.by_owner.entry((o.owner_id, o.owner_kind)).or_default() -= per_owner_weight;
                }
                if expired {
                    e.expired += 1;
                } else {
                    e.removed += 1;
                }
            }
        }
        let mut by_day: Vec<_> = by_day.into_iter().collect();
        by_day.sort_unstable_by_key(|a| a.0);

        let versions = versions.get(dep_crate_id).map(|v| v.as_slice()).unwrap_or_default();
        let crates_own_owners: HashSet<_> = owners.get(dep_crate_id).map(Vec::as_slice).unwrap_or_default()
            .iter().map(|o| (o.owner_id, o.owner_kind))
            .chain([(362.try_into().unwrap(), 1), (21274.try_into().unwrap(), 0)]) // rust-bus doesn't count
            .chain(versions.iter().filter_map(|v| v.published_by).map(|by_id| (by_id, 0)))
            .collect();
        let mut users_aggregate = HashMap::<(NonZeroU32, u8), f64>::default();
        let deps_by_day = by_day.into_iter().map(|(at, DepChangeAggregator { added, removed, expired, by_owner })| {
            for (owner_id, net_change) in by_owner {
                if crates_own_owners.contains(&owner_id) {
                    continue;
                }
                *users_aggregate.entry(owner_id).or_default() += f64::from(net_change);
            }
            // one owner can't count as more than 1 user, but fraction of an owner is kept as a fraction (so many partial co-users add up to one real user)
            let users_abs = users_aggregate.values().map(|&v| v.min(1.)).sum::<f64>().ceil() as u16;
            DependerChanges { at, added, removed, expired, users_abs }
        }).collect::<Vec<_>>();

        let name = crates.get(dep_crate_id).expect("bork crate");
        let origin = Origin::from_crates_io_name(name);
        ksink.index_dependers_liveness_ranges(&origin, deps_by_day)
    })?;
    Ok(())
}

fn process_owners(crates: &CratesMap, owners: &CrateOwners, teams: &Teams, users: &Users) -> Vec<(Origin, Vec<CrateOwner>)> {
    owners.par_iter()
    .filter_map(move |(crate_id, owners)| {
        crates.get(crate_id).map(move |k| (owners.as_slice(), Origin::from_crates_io_name(k)))
    })
    .map(move |(owners, origin)| {
        let mut owners: Vec<_> = owners.iter()
            .filter_map(|o| {
                // teams can't invite users
                let invited_by_github_id = o.created_by_id.and_then(|id| users.get(&id).and_then(|u| u.github_id.try_into().ok()));
                let mut o = match o.owner_kind {
                    0 => {
                        if o.owner_id.get() == 21274 { // rust-bus
                            return None;
                        }
                        let u = users.get(&o.owner_id).expect("owner consistency");
                        if u.github_id <= 0 {
                            return None;
                        }
                        CrateOwner {
                            crates_io_login: u.login.clone(),
                            invited_at: Some(o.created_at),
                            invited_by_github_id,
                            github_id: u.github_id.try_into().ok(),
                            name: if u.name != u.login { Some(u.name.clone()) } else { None },
                            avatar: None,
                            url: None,
                            kind: OwnerKind::User,
                            last_seen_at: None,
                            contributor_only: false,
                            not_a_person: false,
                            crates_io_id: Some(o.owner_id.get()),
                        }
                    },
                    1 => {
                        if o.owner_id.get() == 362 {
                            return None;
                        }
                        let Some(u) = teams.get(&o.owner_id) else {
                            eprintln!("warning: id {} is not in teams (len={}). Bad obj: {o:?} {origin:?}", o.owner_id, teams.len());
                            return None;
                        };
                        CrateOwner {
                            crates_io_login: u.login.clone(),
                            invited_at: Some(o.created_at),
                            github_id: u.github_id,
                            invited_by_github_id,
                            name: if u.name != u.login.split(':').next().unwrap_or_default() { Some(u.name.clone()) } else { None },
                            avatar: None,
                            url: None,
                            kind: OwnerKind::Team,
                            last_seen_at: None,
                            contributor_only: false,
                            not_a_person: false,
                            crates_io_id: None, // team, not user
                        }
                    },
                    _ => unreachable!("bad owner type"),
                };
                if o.github_id == o.invited_by_github_id {
                    o.invited_by_github_id = None;
                }
                Some(o)
            })
            .collect();

        owners.sort_unstable_by(|a,b| a.invited_at.cmp(&b.invited_at));

        // crates.io has some data missing in old crates
        if let Some((first_owner, rest)) = owners.split_first_mut() {
            for other in rest {
                if other.invited_by_github_id.is_none() {
                    other.invited_by_github_id = first_owner.invited_by_github_id.or(first_owner.github_id);
                }
            }
        }
        (origin, owners)
    }).collect()
}

#[derive(Debug, Deserialize)]
struct CrateOwnerRow {
    crate_id: CrateId,
    #[serde(deserialize_with = "date_fudge")]
    created_at: DateTime<Utc>,
    created_by_id: Option<UserId>,
    owner_id: NonZeroU32,
    owner_kind: u8,
}

fn date_fudge<'de, D>(deserializer: D) -> Result<DateTime<Utc>, D::Error>
    where D: Deserializer<'de>,
{
    let date_str = <&str>::deserialize(deserializer)?;

    let date_part = date_str.split('.').next().unwrap(); // trim millis
    match Utc.datetime_from_str(date_part, "%Y-%m-%d %H:%M:%S") {
        Ok(date) => Ok(date),
        Err(e) => panic!("Bad date: {date_str}: {e}"),
    }
}

type CrateId = NonZeroU32;
type CrateOwners = HashMap<CrateId, Vec<CrateOwnerRow>>;

#[inline(never)]
fn parse_crate_owners(file: impl Read) -> Result<CrateOwners, BoxErr> {
    let mut csv = csv::ReaderBuilder::new().has_headers(true).flexible(false).from_reader(file);
    let mut out = HashMap::with_capacity_and_hasher(NUM_CRATES, Default::default());
    for r in csv.records() {
        let r = r?;
        let r = r.deserialize::<CrateOwnerRow>(None).map_err(|e| anyhow::anyhow!("wat? {r:#?} {e}"))?;
        out.entry(r.crate_id).or_insert_with(|| Vec::with_capacity(1)).push(r);
    }
    Ok(out)
}

#[derive(Deserialize)]
#[allow(unused)]
struct TeamRow {
    avatar: Box<str>,
    /// ID not of the org login, but membership within the org
    team_github_id: u32,
    /// cratesio
    id: TeamId,
    /// in the funny format
    login: SmolStr,
    /// human str
    name: SmolStr,
    /// Global account ID
    github_id: Option<u32>,
}

type TeamId = NonZeroU32;
type Teams = HashMap<TeamId, TeamRow>;

#[inline(never)]
fn parse_teams(file: impl Read) -> Result<Teams, BoxErr> {
    let mut csv = csv::ReaderBuilder::new().has_headers(true).flexible(false).from_reader(file);
    let mut out = HashMap::with_capacity_and_hasher(NUM_CRATES, Default::default());
    for r in csv.records() {
        let r = r?;
        let r = r.deserialize::<TeamRow>(None).map_err(|e| anyhow::anyhow!("{e}: {r:?}"))?;
        out.insert(r.id, r);
    }
    Ok(out)
}

#[derive(Deserialize)]
struct UserRow {
    #[allow(unused)]
    avatar: Box<str>,
    github_id: i32, // there is -1 :(
    login: SmolStr,
    id: UserId,
    name: SmolStr,
}

type UserId = NonZeroU32;
type Users = HashMap<UserId, UserRow>;

#[inline(never)]
fn parse_users(file: impl Read) -> Result<Users, BoxErr> {
    let mut csv = csv::ReaderBuilder::new().has_headers(true).flexible(false).from_reader(file);
    let mut out = HashMap::with_capacity_and_hasher(NUM_CRATES, Default::default());
    for r in csv.records() {
        let r = r?;
        let row = r.deserialize::<UserRow>(None).map_err(|e| anyhow::anyhow!("{e}: {r:?}"))?;
        out.insert(row.id, row);
    }
    Ok(out)
}

type CrateVersionId = NonZeroU32;

#[derive(Debug)]
struct CrateVersionRowMin {
    crate_size: Option<NonZeroU64>,
    created_at: DateTime<Utc>,
    id: CrateVersionId,
    num: SmolStr, // ver
    yanked: bool,
    // API audit calls are still needed to get yanks
    published_by: Option<NonZeroU32>,
}

#[derive(Deserialize, Debug)]
struct CrateVersionRow {
    crate_id: CrateId,
    crate_size: Option<NonZeroU64>,
    #[serde(deserialize_with = "date_fudge")]
    created_at: DateTime<Utc>,
    id: CrateVersionId,
    num: SmolStr, // ver
    published_by: Option<NonZeroU32>,
    yanked: char,
}

type VersionsMap = HashMap<CrateId, Vec<CrateVersionRowMin>>;

#[inline(never)]
fn parse_versions(file: impl Read) -> Result<VersionsMap, BoxErr> {
    let mut csv = csv::ReaderBuilder::new().has_headers(true).flexible(false).from_reader(file);
    let mut out = HashMap::with_capacity_and_hasher(NUM_CRATES, Default::default());
    let headers = csv.headers()?.clone();
    for r in csv.records() {
        let r = r?;
        let row = r.deserialize::<CrateVersionRow>(Some(&headers)).with_context(|| format!("{r:?}"))?;
        out.entry(row.crate_id).or_insert_with(Vec::new).push(CrateVersionRowMin {
            crate_size: row.crate_size,
            created_at: row.created_at,
            id: row.id,
            num: row.num,
            yanked: row.yanked == 't',
            published_by: row.published_by,
        });
    }
    Ok(out)
}

/// Date, downloads, true if despammed and overwrites past database data
type VersionDownloads = HashMap<CrateVersionId, Vec<(NaiveDate, u32, bool)>>;

fn date_from_str(date: &str) -> Option<NaiveDate> {
    let y = date[0..4].parse().ok()?;
    let m = date[5..7].parse().ok()?;
    let d = date[8..10].parse().ok()?;
    NaiveDate::from_ymd_opt(y, m, d)
}

#[inline(never)]
fn parse_version_downloads(file: impl Read) -> Result<VersionDownloads, BoxErr> {
    let mut csv = csv::ReaderBuilder::new().has_headers(true).flexible(false).from_reader(file);
    let mut out = HashMap::with_capacity_and_hasher(NUM_CRATES, Default::default());
    for r in csv.records() {
        let r = r?;
        let mut r = r.iter();
        let date = date_from_str(r.next().context("no date")?).context("date")?;
        let downloads = r.next().and_then(|s| s.parse().ok()).context("bad dl1")?;
        let version_id = r.next().and_then(|s| s.parse().ok()).context("bad dl2")?;
        out.entry(version_id).or_insert_with(|| Vec::with_capacity(365 * 4)).push((date, downloads, false));
    }
    Ok(out)
}

type CratesMap = HashMap<CrateId, SmolStr>;

#[inline(never)]
fn parse_crates(file: impl Read) -> Result<CratesMap, BoxErr> {
    let mut csv = csv::ReaderBuilder::new().has_headers(true).flexible(false).from_reader(file);
    let mut out = HashMap::with_capacity_and_hasher(NUM_CRATES, Default::default());
    for r in csv.records() {
        let r = r?;
        let id: CrateId = r.get(4).and_then(|s| s.parse().ok()).context("bad record1")?;
        let name = r.get(7).context("bad record2")?;
        debug_assert!(Origin::try_from_crates_io_name(name).is_some(), "bad name: {name:?}' cio_id={id} {r:?}");
        out.insert(id, name.into());
    }
    Ok(out)
}

/// version ID depends on crate ID
type CrateDepsMap = HashMap<CrateVersionId, Vec<CrateId>>;

#[inline(never)]
fn parse_dependencies(file: impl Read) -> Result<CrateDepsMap, BoxErr> {
    // crate_id,default_features,features,id,kind,optional,req,target,version_id
    let mut csv = csv::ReaderBuilder::new().has_headers(true).flexible(false).from_reader(file);
    let mut out = HashMap::with_capacity_and_hasher(NUM_CRATES, Default::default());
    for r in csv.records() {
        let r = r?;
        // 0crate_id,1default_features,2explicit_name,3features,4id,5kind,6optional,7req,8target,9version_id
        let crate_id: CrateId = r.get(0).and_then(|s| s.parse().ok()).with_context(|| format!("{r:?} bad record3"))?;
        let version_id: CrateVersionId = r.get(9).or_else(|| r.get(8)).and_then(|s| s.parse().ok()).with_context(|| format!("{r:?} bad record4"))?;
        out.entry(version_id).or_insert_with(Vec::new).push(crate_id);
    }
    Ok(out)
}
