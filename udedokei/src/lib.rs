use util::FxHashMap as HashMap;
use serde::{Deserialize, Serialize};
use std::path::Path;

pub use synscrape;

pub use tokei::LanguageType as Language;

#[derive(Debug, PartialEq, Eq, Clone, Default, Serialize, Deserialize)]
pub struct Stats {
    pub langs: HashMap<Language, Lines>,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Default, Serialize, Deserialize)]
pub struct Lines {
    pub comments: u32,
    pub code: u32,
}

#[derive(Debug)]
pub struct Collect {
    stats: Stats,
    items: Vec<synscrape::ModuleScrape>,
}

pub trait LanguageExt {
    fn is_code(&self) -> bool;
    fn color(&self) -> &'static str;
}

pub fn from_path(path: impl AsRef<Path>) -> Option<Language> {
    use std::os::unix::ffi::OsStrExt;

    let path = path.as_ref();

    let name = path.file_name()?;
    if name.as_bytes().starts_with(b".") {
        return None;
    }

    // Silence warning from tokei
    if let Some(ext) = path.extension().and_then(|p| p.to_str()) {
        if matches!(ext, "png" | "jpeg" | "bmp" |  "webp" | "gif" | "tiff" | "orig" | "lock" |
            "pem" | "docx" | "xslx" | "pyc" | "sublime-project" | "sublime-snippet" | "ttf" | "gz" | "ico" | "pdb") {
            return None;
        }
    }

    Language::from_path(path, &tokei::Config {
        no_ignore: Some(true),
        no_ignore_parent: Some(true),
        treat_doc_strings_as_comments: Some(true),
        ..Default::default()
    })
}

impl LanguageExt for Language {
    fn is_code(&self) -> bool {
        !matches!(*self,
            Self::AutoHotKey |
            Self::Autoconf |
            Self::CHeader |
            Self::CMake |
            Self::CShell |
            Self::CppHeader |
            Self::Css |
            Self::DeviceTree |
            Self::Dockerfile |
            Self::DreamMaker |
            Self::EmacsDevEnv |
            Self::Fish |
            Self::Html |
            Self::Hex |
            Self::Org |
            Self::Json |
            Self::Less |
            Self::LinkerScript |
            Self::Makefile |
            Self::Markdown |
            Self::Meson |
            Self::ModuleDef |
            Self::MsBuild |
            Self::Nix |
            Self::Protobuf |
            Self::Rakefile |
            Self::ReStructuredText |
            Self::Sass |
            Self::Svg |
            Self::Tex |
            Self::Text |
            Self::Toml |
            Self::UrWebProject |
            Self::VerilogArgsFile |
            Self::Xaml |
            Self::Xml |
            Self::Yaml)
    }

    fn color(&self) -> &'static str {
        match *self {
            Self::Abap => "#E8274B",
            Self::ActionScript => "#882B0F",
            Self::Ada => "#02f88c",
            Self::Agda => "#315665",
            Self::Asp => "#6a40fd",
            Self::AspNet => "#6a40fd",
            Self::Assembly => "#6E4C13",
            Self::AutoHotKey => "#6594b9",
            Self::Batch => "#C1F12E",
            Self::CShell => "#C1F12E",
            Self::C => "#555555",
            Self::CHeader => "#555555",
            Self::Cassius => "#ccccff",
            Self::Ceylon => "#dfa535",
            Self::Clojure => "#db5855",
            Self::ClojureC => "#db5855",
            Self::ClojureScript => "#db5855",
            Self::CoffeeScript => "#244776",
            Self::ColdFusion => "#ed2cd6",
            Self::Cpp => "#f34b7d",
            Self::Crystal => "#776791",
            Self::CSharp => "#178600",
            Self::Css => "#563d7c",
            Self::D => "#ba595e",
            Self::Dart => "#00B4AB",
            Self::Dockerfile => "#0db7ed",
            Self::Edn => "#ccce35",
            Self::Elisp => "#c065db",
            Self::Elixir => "#6e4a7e",
            Self::Elm => "#60B5CC",
            Self::Elvish => "#913960",
            Self::Erlang => "#B83998",
            Self::FEN => "#FFF4F3",
            Self::Fish => "#88ccff",
            Self::Forth => "#341708",
            Self::FortranLegacy => "#4d41b1",
            Self::FortranModern => "#4d41b1",
            Self::FSharp => "#b845fc",
            Self::Fstar => "#14253c",
            Self::GdScript => "#8fb200",
            Self::Glsl => "#a78649",
            Self::Go => "#375eab",
            Self::Groovy => "#e69f56",
            Self::Handlebars => "#f0a9f0",
            Self::Haskell => "#5e5086",
            Self::Haxe => "#df7900",
            Self::Hex => "#0e60e3",
            Self::Html => "#e34c26",
            Self::Idris => "#b30000",
            Self::IntelHex => "#a9188d",
            Self::Isabelle => "#FEFE00",
            Self::Jai => "#9EEDFF",
            Self::Java => "#b07219",
            Self::JavaScript => "#f1e05a",
            Self::Jsx => "#40d47e",
            Self::Julia => "#a270ba",
            Self::KakouneScript => "#28431f",
            Self::Kotlin => "#F18E33",
            Self::Lean => "#4C3023",
            Self::Less => "#499886",
            Self::LinkerScript => "#185619",
            Self::Lua => "#000080",
            Self::Lucius => "#cc9900",
            Self::CMake => "#427819",
            Self::Makefile => "#427819",
            Self::Autoconf => "#427819",
            Self::Markdown => "#4A76B8",
            Self::Meson => "#007800",
            Self::Mint => "#62A8D6",
            Self::ModuleDef => "#b7e1f4",
            Self::MsBuild => "#28431f",
            Self::Mustache => "#ff2b2b",
            Self::Nim => "#37775b",
            Self::Nix => "#7e7eff",
            Self::ObjectiveC => "#438eff",
            Self::ObjectiveCpp => "#6866fb",
            Self::OCaml => "#3be133",
            Self::Org => "#b0b77e",
            Self::Oz => "#fab738",
            Self::Pascal => "#E3F171",
            Self::Perl => "#0298c3",
            Self::Php => "#4F5D95",
            Self::Polly => "#dad8d8",
            Self::Processing => "#0096D8",
            Self::Prolog => "#74283c",
            Self::Protobuf => "#7fa2a7",
            Self::PSL => "#7055b5",
            Self::PureScript => "#1D222D",
            Self::Python => "#3572A5",
            Self::Qcl => "#0040cd",
            Self::R => "#198CE7",
            Self::Racket => "#22228f",
            Self::Razor => "#9d5200",
            Self::ReStructuredText => "#358a5b",
            Self::Ruby => "#701516",
            Self::Rust => "#dea584",
            Self::Sass => "#64b970",
            Self::Scala => "#c22d40",
            Self::Scheme => "#1e4aec",
            Self::Scons => "#0579aa",
            Self::Sh => "#89e051",
            Self::Bash => "#89e051",
            Self::Sml => "#596706",
            Self::SpecmanE => "#AA6746",
            Self::Spice => "#3F3F3F",
            Self::Sql => "#646464",
            Self::SRecode => "#B34936",
            Self::Svg => "#b2011d",
            Self::Swift => "#ffac45",
            Self::SystemVerilog => "#DAE1C2",
            Self::Tcl => "#e4cc98",
            Self::Tex => "#3D6117",
            Self::Text => "#00004c",
            Self::Toml => "#A0AA87",
            Self::TypeScript => "#2b7489",
            Self::UnrealScript => "#a54c4d",
            Self::UrWeb => "#cf142b",
            Self::UrWebProject => "#cf142b",
            Self::Vala => "#fbe5cd",
            Self::Verilog => "#b2b7f8",
            Self::VerilogArgsFile => "#b2b7f8",
            Self::Vhdl => "#adb2cb",
            Self::VimScript => "#199f4b",
            Self::VisualBasic => "#945db7",
            Self::Vue => "#2c3e50",
            Self::Wolfram => "#42f1f4",
            Self::Xaml => "#7582D1",
            Self::Xml => "#EB8CEB",
            Self::Yaml => "#4B6BEF",
            Self::Zig => "#99DA07",
            Self::Zsh => "#5232e7",
            _ => "#a5a3a0",
        }
    }
}

impl Collect {
    #[must_use]
    pub fn new() -> Self {
        Self { stats: Stats::default(), items: vec![] }
    }

    #[must_use] pub fn finish(self) -> (Stats, Vec<synscrape::ModuleScrape>) {
        (self.stats, self.items)
    }

    pub fn add_to_stats(&mut self, lang: Language, file_stem: &str, file_content: &str) {
        if lang == Language::Rust {
            self.rust_code_stats(file_stem, file_content);
        }
        let res = lang.parse_from_str(file_content, &tokei::Config {
            no_ignore: Some(true),
            no_ignore_parent: Some(true),
            treat_doc_strings_as_comments: Some(true),
            ..Default::default()
        });
        let stats = self.stats.langs.entry(lang).or_insert(Lines { comments: 0, code: 0 });
        stats.comments += res.comments as u32;
        stats.code += res.code as u32;
    }

    fn rust_code_stats(&mut self, file_stem: &str, file_content: &str) {
        match synscrape::ModuleScrape::from_file(file_stem, file_content) {
            Ok(m) => self.items.push(m),
            Err(e) => log::warn!("syn parse error: {file_stem} {e}"),
        };
    }
}
