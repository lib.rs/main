#![allow(dead_code)]
#![allow(unused)]

use rand::rngs::SmallRng;
use rand::SeedableRng;
use util::FxHashMap as HashMap;
use anyhow::anyhow;
use blocking::awatch;
use blocking::block_in_place;
use blocking::timeout;
use crate_git_checkout::CloneOptions;
use debcargo_list::DebcargoList;
use feat_extractor::{is_autopublished, is_deprecated, is_squatspam, wlita};
use futures::future::join_all;
use futures::Future;
use futures::FutureExt;
use futures::join;
use futures::stream::StreamExt;
use futures::try_join;
use log::error;
use tarball::comparator::CompareDb;
use itertools::Itertools;
use kitchen_sink::{
    running, Stop, stop, stopped, ArcRichCrateVersion, CrateOwners, KitchenSink, Origin, RichCrate, RichCrateVersion, SemVer, SpawnAbortOnDrop, VersionPopularity
};
use log::{debug, info, warn};
use parking_lot::Mutex;
use rand::seq::SliceRandom;
use ranking::{CrateTemporalInputs, CrateVersionInputs, OverallScoreInputs};
use render_readme::{Links, LinksContext, Renderer};
use repo_url::Repo;
use rich_crate::Dependency;
use search_index::{CrateSearchIndex, Indexer, IndexerData};
use simple_cache::TempCache;
use tarball::comparator::Report;
use tokio::task::spawn_blocking;
use std::borrow::Cow;
use std::cmp::Reverse;
use std::pin::Pin;
use std::process::ExitCode;
use std::time::{Duration, Instant};
use tokio::sync::mpsc;
use triomphe::Arc;
use udedokei::LanguageExt;
use util::{CowAscii, SmolStr};

/// `(ver, downloads_per_month, source_code_texts, score)`
type SearchIndexDataTmp = (ArcRichCrateVersion, usize, Vec<SmolStr>, f64);

fn main() -> ExitCode {
    if let Err(e) = run() {
        eprintln!("Failed: {e}");
        for c in e.chain() {
            eprintln!("  {c}");
        }
        ExitCode::FAILURE
    } else {
        ExitCode::SUCCESS
    }
}

fn run() -> anyhow::Result<()> {
    let rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .thread_name("repocompare")
        .build()?;


    let kitchen_sink = rt.block_on(KitchenSink::new_default())?;
    let _ = std::fs::create_dir_all(kitchen_sink.main_data_dir().join("rust-repo-checks"));

    let explicit_crates: Vec<_> = std::env::args().skip(1).filter_map(|n| Origin::try_from_crates_io_name(&n)).collect();
    let crate_origins = if !explicit_crates.is_empty() {
        explicit_crates
    } else {
        let mut all = kitchen_sink.index()?.all_crates()?;
        all.shuffle(&mut SmallRng::from_entropy());
        all
    };
    running()?;

    rt.block_on(SpawnAbortOnDrop(rt.spawn(main_loop(Arc::new(kitchen_sink), crate_origins))))?;

    Ok(())
}

async fn main_loop(kitchen_sink: Arc<KitchenSink>, crate_origins: Vec<Origin>) -> Result<(), anyhow::Error> {
    let index = kitchen_sink.index()?;
    let db = &Arc::new(CompareDb::new(kitchen_sink.main_data_dir().join("rust-repo-checks")));
    let kitchen_sink = &kitchen_sink;
    futures::stream::iter(crate_origins.into_iter().enumerate()).for_each_concurrent(3, move |(i, origin)| async move {
        log::debug!("{i} {origin:?}…");
        if let Err(e) = timeout(format!("comp-{origin:?}"), 200, do_crate(kitchen_sink.clone(), db.clone(), &origin)).await {
            log::error!("Failed {}: {e}", origin.package_name_icase());
            for c in e.chain() {
                log::error!("  • {c}");
            }
        }
    }).await;
    Ok(())
}

async fn do_crate(kitchen_sink: Arc<KitchenSink>, db: Arc<CompareDb>, origin: &Origin) -> Result<(), anyhow::Error> {
    let k = kitchen_sink.rich_crate_version_async(origin).await?; // TODO should prefer stable
    if k.category_slugs().iter().any(|s| s == "cryptography::cryptocurrencies") {
        return Ok(());
    }
    let Some(repo_url) = k.repository().cloned() else {
        return Ok(()); // TODO: mark as lacking repo
    };

    if let Ok(report) = db.report_for(k.short_name(), k.version()).inspect_err(|e| warn!("old report for {}: {e}", k.short_name())) {
        if report.version >= 28 || (report.version >= 26 && report.result().all_files_verified) {
            debug!("Existing report for {}: {:#?}", k.short_name(), report.result());
            for m in report.messages().0 {
                if m.important || m.issue.is_fatal() {
                    info!("{}@{}: {}", k.short_name(), k.version(), m.issue.message());
                    if m.issue.is_fatal() {
                        break;
                    }
                }
            }
            return Ok(());
        }
        log::info!("Old report {report:#?} trying again");
    }

    let (path_in_repo, sha1) = k.repository_vcs_info();
    log::info!("Comparing {} with {}@{}", repo_url.canonical_http_url(path_in_repo.unwrap_or_default(), sha1.map(hex::encode).as_deref()), k.short_name(), k.version());

    let tarball = kitchen_sink.download_crate_tarball(k.short_name(), k.version()).await?;

    let (_g, stop) = Stop::new();
    spawn_blocking(move || {
        let (path_in_repo, sha1) = k.repository_vcs_info();

        let mut ko = tarball::comparator::Comparator::new(k.short_name(), k.version(), &tarball, path_in_repo, sha1, kitchen_sink.git_checkout_path());
        let report = ko.check(&repo_url, &stop)?;
        db.save_report_for(&report, k.short_name(), k.version())?;
        if !report.result().all_files_verified {
            log::warn!("NEW REPORT: {}@{} not verified: {report:#?}", k.short_name(), k.version());
            for m in report.messages().0 {
                if m.important || m.issue.is_fatal() {
                    info!("{}@{}: {}", k.short_name(), k.version(), m.issue.message());
                    if m.issue.is_fatal() {
                        break;
                    }
                }
            }
        }
        Ok(())
    }).await?
}











