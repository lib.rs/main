use crate::{DepsErr, Origin};
use util::FxHashSet as HashSet;
use std::fs;
use std::path::Path;

pub struct LocalGitCratesIndex {
    index: HashSet<Origin>,
}

impl LocalGitCratesIndex {
    pub fn new(dir: &Path) -> Result<Self, DepsErr> {
        let path = dir.join("git_crates.txt");
        let index = if path.exists() {
            match fs::read_to_string(&path) {
                Ok(file) => file.split('\n').map(|s| s.trim()).filter(|s| !s.is_empty()).map(Origin::from_str).collect(),
                Err(e) => return Err(DepsErr::LocalGitIndexFile(path, e.to_string())),
            }
        } else {
            Default::default()
        };
        Ok(Self { index })
    }

    pub fn has(&self, origin: &Origin) -> bool {
        self.index.contains(origin)
    }

    pub fn crates(&self) -> impl Iterator<Item = &Origin> {
        self.index.iter()
    }

    pub fn len(&self) -> usize {
        self.index.len()
    }
}
