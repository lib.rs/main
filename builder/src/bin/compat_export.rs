use kitchen_sink::SemVer;
use util::FxHashMap as HashMap;
use futures::StreamExt;
use kitchen_sink::stopped;
use kitchen_sink::KitchenSink;
use kitchen_sink::Origin;
use std::io::Write;

#[tokio::main]
async fn main() {
    let filter = &std::env::args().nth(1);

    let crates = &KitchenSink::new_default().await.unwrap();
    println!("crate,version,published,oldest_ok,newest_bad,oldest_ok_certain,newest_bad_certain");
    let all = &*crates.index().unwrap().crates_io_crates().unwrap();
    let mut all = all.keys().collect::<Vec<_>>();
    all.sort_unstable();

    let mut errors_seen = 0;

    futures::stream::iter(all)
    .for_each_concurrent(4, |lowercased_name| async move {
        if stopped() {
            return;
        }
        if let Some(f) = filter {
            if !lowercased_name.contains(f) {
                return;
            }
        }
        let origin = Origin::from_crates_io_name(lowercased_name);
        let rank = crates.crate_ranking_for_builder(&origin).unwrap_or(0.3);
        if rank < 0.15 {
            return;
        }
        if let Err(e) = crate_compat(crates, &origin).await {
            for e in e.chain() {
                eprintln!("•• {}: {e}", origin.package_name_icase());
            }
            errors_seen += 1;
            if errors_seen > 100 {
                panic!("Too many errors");
            }
        }
    }).await;
}

async fn crate_compat(crates: &KitchenSink, origin: &Origin) -> kitchen_sink::CResult<()> {
    let all = crates.rich_crate_async(origin).await?;
    let compat = crates.rustc_compatibility(&all).await?;
    let date_by_ver = all.versions()
        .iter()
        .filter_map(|v| Some((SemVer::parse(&v.num).ok()?, v.created_at)))
        .collect::<HashMap<_, _>>();

    let mut out = std::io::stdout().lock();
    for (version, ranges) in compat {
        writeln!(&mut out, "{},{version},{},{},{},{},{}", all.name(),
            date_by_ver.get(&version).map(|d| d.date_naive().to_string()).unwrap_or_default(),
            ranges.oldest_ok().map(i32::from).unwrap_or(-1),
            ranges.newest_bad().map(i32::from).unwrap_or(-1),
            ranges.oldest_ok_certain().map(i32::from).unwrap_or(-1),
            ranges.newest_bad_likely().map(i32::from).unwrap_or(-1),
        )?;
    }

    Ok(())
}
