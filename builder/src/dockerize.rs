use std::path::Path;
use anyhow::Context;
use log::debug;
use std::process::ExitStatus;
use std::io;
use std::process::Command;
use std::process::Stdio;
use kitchen_sink::running;
use std::path::PathBuf;
use std::io::{BufRead, BufReader, Write};

pub(crate) struct Docker {
    concurrency: u8,
    tag_name: &'static str,
    cwd: PathBuf,
    envs: Vec<String>,
}

pub const DOCKERFILE_COMMON: &str = "FROM ubuntu:jammy
RUN apt-get update # 2
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-suggests --no-install-recommends gdk-pixbuf-2.0 libpango1.0-dev libatk1.0 libavfilter7 libcurl4 libgit2-dev gdk-3.0 gstreamer1.0 zstd ca-certificates fontconfig libcairo2-dev libelf-dev protobuf-compiler qemu-user curl git automake llvm-dev ca-certificates build-essential openssl zstd libzstd-dev libssl-dev libbz2-dev clang libfreetype6-dev pkg-config libicu-dev libjpeg-dev liblzma-dev libncurses5 libopenal-dev libopencv-dev libopengl0 libpcre3-dev libpq-dev libsdl2-dev libsqlite3-dev sqlite3 libtool libvpx-dev libwebp-dev libx11-dev libxml2-dev linux-headers-generic llvm musl-tools meson cmake nasm zlib1g-dev && rm -rf /var/lib/apt/lists/* /var/cache/apt
RUN useradd -u 4321 --create-home --user-group -s /bin/bash rustyuser
RUN chown -R 4321:4321 /home/rustyuser
RUN swapoff -a || true
USER rustyuser
WORKDIR /home/rustyuser
";

#[derive(Clone, Copy)]
pub enum Mount {
    Ro, Delegated, Rw,
}

impl Docker {
    pub fn new(tag_name: &'static str, cwd: PathBuf) -> Self {
        Self {
            concurrency: 4,
            tag_name,
            cwd,
            envs: Vec::new(),
        }
    }

    pub fn set_env(&mut self, var: &str, val: &str) {
        self.envs.push(format!("{var}={val}"));
    }

    pub fn build(&self, script: &str) -> io::Result<()> {
        let mut child = Command::new("docker")
            .current_dir(&self.cwd)
            .arg("build")
            .arg("-t").arg(self.tag_name)
            .arg("-")
            .stdin(Stdio::piped())
            .stdout(Stdio::inherit())
            .stderr(Stdio::inherit())
            .spawn()?;

        let mut stdin = child.stdin.take().unwrap();
        stdin.write_all(script.as_bytes())?;
        drop(stdin);

        let res = child.wait()?;
        if !res.success() {
            return Err(io::Error::new(io::ErrorKind::Other, "Failed Docker build"));
        }
        Ok(())
    }

    pub fn set_concurrency(&mut self, concurrency: u8) {
        self.concurrency = concurrency;
    }

    pub(crate) fn run_script(&self, script: &str, dirs: &[(&str, &str, Mount)], out_chan: Option<crossbeam_channel::Sender<Line>>) -> Result<(), anyhow::Error> {
        running()?;

        let mut cmd = Command::new("docker");
        cmd.current_dir(&self.cwd)
            .args(["run", "--rm", "-m5500m", "--log-opt=max-size=10m", "--oom-score-adj=-990", &format!("--cpus={}", self.concurrency)]);

        for &(from, to, ro_src) in dirs {
            let ro_src = match ro_src { Mount::Ro => ":ro", Mount::Delegated => ":delegated", Mount::Rw => "" };
            cmd.args(["-v", &format!("{from}:{to}{ro_src}")]);
        }
        for env in &self.envs {
            cmd.args(["-e", env]);
        }
        cmd.args([self.tag_name, "bash", "-c", script]);

        debug!("Running {cmd:?}");

        let mut child = cmd
            .stdin(Stdio::null())
            .stdout(if out_chan.is_some() { Stdio::piped() } else { Stdio::inherit() })
            .stderr(if out_chan.is_some() { Stdio::piped() } else { Stdio::inherit() })
            .spawn()?;

        if let Some(chan) = &out_chan {
            if let Some(io) = child.stdout.take() {
                streamfetch(BufReader::new(io), false, chan.clone());
            }
            if let Some(io) = child.stderr.take() {
                streamfetch(BufReader::new(io), true, chan.clone());
            }
        }

        let status = child.wait()?;
        if let Some(ch) = &out_chan {
            let _ = ch.send(Line::Eof(status));
        }

        if !status.success() {
            anyhow::bail!("exit failure {status:?}");
        }
        Ok(())
    }
}


pub(crate) enum Line {
    StdOut(String),
    StdErr(String),
    Eof(ExitStatus),
}

fn streamfetch(inp: impl BufRead + Send + 'static, is_stderr: bool, out_chan: crossbeam_channel::Sender<Line>) {
    std::thread::spawn(move || {
        for line in inp.lines() {
            let Ok(line) = line else {
                break;
            };
            if out_chan.send(if is_stderr { Line::StdErr(line) } else { Line::StdOut(line) }).is_err() {
                break;
            }
        }
    });
}


pub(crate) fn chownperm(path: &Path, externally_writeable: bool) -> Result<(), anyhow::Error> {
    use std::os::unix::fs::PermissionsExt;
    let mut perms = std::fs::metadata(path).with_context(|| format!("{path:?} perms read"))?.permissions();
    perms.set_mode(if externally_writeable { 0o777 } else { 0o774 });
    std::fs::set_permissions(path, perms).with_context(|| format!("{path:?} perms set"))?;
    std::os::unix::fs::lchown(path, Some(4321), Some(4321)).with_context(|| format!("{path:?} chown"))?;
    Ok(())
}
