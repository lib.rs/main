@use crate::Page;
@use crate::format_downloads_verbose;
@use crate::GlobalStats;
@use crate::global_stats::round2;
@use crate::global_stats::format_number;
@use crate::global_stats::Vs;
@use crate::global_stats::url_for_crate_name;
@use crate::global_stats::url_for_rev_deps;
@use crate::global_stats::versions_for_crate_name;
@use crate::templates::base_html;
@use crate::templates::histogram_html;
@use crate::templates::global_stats_rustc_html;
@use crate::templates::global_stats_rustc_use_html;
@use crate::Urler;

@(page: &Page, dl_by_week: &[(u64, u64)], stats: &GlobalStats, url: &Urler)

@:base_html(page, {
    <header id="home">
        <div class="inner-col">
            <h1><a href="/">Lib.rs</a> › Stats</h1>
            <p>Rust crates ecosystem statistics.</p>
            <form role="search" id=search method="get" action="/search">
                <input accesskey="s" placeholder="name, keywords, description" autocapitalize="off" autocorrect="off" spellcheck="false"autocomplete="off" tabindex="1" type=search id=search_q name=q><button type=submit>Search</button>
            </form>
            <nav>
                <ul><li><a href="/">Categories</a></li>
                <li><a href="/new">New and trending</a></li>
                <li class="active">Stats</li>
                <li><a href="/std">Popular</a></li>
                <li><a href="/dash">Maintainer Dashboard</a></li>
            </ul></nav>
        </div>
    </header>

    <main id="global-stats">
        <div class="inner-col">
        <section>
            <nav class="toc">
                <ul>
                    <li><a href="#downloads-growth">Growth of downloads and crate owners</a></li>
                    <li>Histograms
                        <ul>
                            <li><a href="#crates-stats-histograms">Number of direct dependencies</a></li>
                            <li><a href="#num-deps">Number of direct dependencies</a></li>
                            <li><a href="#rev-deps">Number of transitive reverse dependencies (popularity)</a></li>
                            <li><a href="#crate-time-in-dev">How long a crate has been in development</a></li>
                            <li><a href="#time-without-updates">Time without any updates</a></li>
                            <li><a href="#crate-age">How old crates are</a></li>
                            <li><a href="#releases-per-crate">Number of releases per crate</a></li>
                            <li><a href="#crates-per-user">Number of crates per user</a></li>
                            <li><a href="#crate-sizes">Crate source size</a></li>
                            <li><a href="#crate-bandwidth">Total bandwidth of top crates</a></li>
                        </ul>
                    </li>
                    <li><a href="#categories">Popular categories</a></li>
                    <li><a href="#vs">Major competing crates</a></li>
                    <li>Rust compatibility
                        <ul>
                            <li><a href="#rustc">Versions of Rust supported by latest crates</a></li>
                            <li><a href="#rustc-usage">Versions of Rust used by crates.io users</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>

            <h2 id="downloads-growth">Growth of the crates.io registry</h2>

            <svg viewBox="0 -5 1030 116" width=1040 height=116 style="width:100%; margin: 1em auto 0">
                <title>Download history of all crates since 2015</title>
                @for y in (0..stats.max_downloads_per_week).step_by(stats.dl_grid_line_every as _) {
                    <line x1=0 x2=1000 y1="@round2(100. - y as f32 / (stats.max_downloads_per_week as f32 / 100.))" y2="@round2(100. - y as f32 / (stats.max_downloads_per_week as f32 / 100.))"
                        stroke-width=1 stroke-linecap="butt" stroke="#aaaaaa" stroke-opacity=0.3 />
                    @if y > 0 {
                        <text stroke="none" fill="#999999" font-size="11" x="1003" y="@round2(103. - y as f32 / (stats.max_downloads_per_week as f32 / 100.))">@((y as f32 / 7_000_000.).round())M</text>
                    }
                }
                <path d="M0 100
    @for (i, (weekday, weekend)) in dl_by_week.iter().copied().enumerate() {
    L@round2(i as f32 / (dl_by_week.len() as f32 / 1000.)) @round2(100. - (weekday + weekend) as f32 / (stats.max_downloads_per_week as f32 / 100.))
    }
    V100" fill="#fcbc4a" stroke-width="1" stroke="black" />
                @for x in (0..dl_by_week.len() as u32).step_by(52).skip(1) {
                    <line y1=100 y2=105
                    x1="@round2((x - stats.start_week_offset) as f32 / (dl_by_week.len() as f32 / 1000.))"
                    x2="@round2((x - stats.start_week_offset) as f32 / (dl_by_week.len() as f32 / 1000.))"
                        stroke-width=1 stroke-linecap="butt" stroke="#aaaaaa" stroke-opacity=0.3 />
                }
                @for x in (0..(dl_by_week.len() as u32 - 26)).step_by(52) {
                    <text stroke="none" fill="#999999" text-anchor=middle font-size="11" y="111"
                    x="@round2((x + 26_u32.saturating_sub(stats.start_week_offset)) as f32 / (dl_by_week.len() as f32 / 1000.))">@(x / 52 + 2015)</text>
                }
            </svg>
            <p class=legend>Daily downloads since Rust 1.0, 7-day average</p>

            <p>Crate downloads are growing at a rate of
            @GlobalStats::relative_increase((stats.dl_per_day_this_year.0 + stats.dl_per_day_this_year.1, stats.dl_per_day_last_year.0 + stats.dl_per_day_last_year.1)) per year.</p>

            <p>crates.io has served @if let Some((val, unit)) = Some(format_downloads_verbose(stats.max_daily_downloads_rate)) { @val @unit } downloads in a <em>single day</em>,
                which is more than all&nbsp;downloads in the first <em>@((stats.weeks_to_reach_max_downloads as f32 / (365./12./7.)).floor()) months</em> since the release of Rust 1.0 in May 2015.</p>

            <p>Traffic during weekdays is typically @GlobalStats::relative_increase(stats.dl_per_day_this_year) higher than during weekends
                (@if stats.dl_ratio_up() {up} else {down} from @GlobalStats::relative_increase(stats.dl_per_day_last_year) a year before).</p>
            <svg viewBox="0 -5 1030 116" width=1040 height=116 style="width:100%; margin: 1em auto 0">
                <title>@stats.max_total_owners crate owners registered on crates.io</title>
                @for y in (0..stats.max_total_owners).step_by((stats.max_total_owners as usize / 6_000 * 1_000).max(1)) {
                    <line x1=0 x2=1000 y1="@round2(100. - y as f32 / (stats.max_total_owners as f32 / 100.))" y2="@round2(100. - y as f32 / (stats.max_total_owners as f32 / 100.))"
                        stroke-width=1 stroke-linecap="butt" stroke="#aaaaaa" stroke-opacity=0.3 />
                    @if y > 0 {
                        <text fill="#999999" font-size="11" x="1003" y="@round2(103. - y as f32 / (stats.max_total_owners as f32 / 100.))">@((y as f32 / 1_000.).round())K</text>
                    }
                }
                @for (i, num) in stats.total_owners_at_month.iter().copied().enumerate() {
                    <rect
                        x="@round2((0.1 + i as f32) / (stats.total_owners_at_month.len() as f32 / 1000.))"
                        width="@round2(0.8 / (stats.total_owners_at_month.len() as f32 / 1000.))"
                        y="@round2(100. - num as f32 / (stats.max_total_owners as f32 / 100.))"
                        height="@round2(num as f32 / (stats.max_total_owners as f32 / 100.))"
                        fill="#4aa9fc" stroke="none"
                    />
                }
                @for x in (0..stats.total_owners_at_month.len() as u32).step_by(12).skip(1) {
                    <line y1=100 y2=105
                    x1="@round2((x - 5) as f32 / (stats.total_owners_at_month.len() as f32 / 1000.))"
                    x2="@round2((x - 5) as f32 / (stats.total_owners_at_month.len() as f32 / 1000.))"
                        stroke-width=1 stroke-linecap="butt" stroke="#aaaaaa" stroke-opacity=0.3 />
                }
                @for x in (0..(stats.total_owners_at_month.len() as u32 - 5)).step_by(12) {
                    <text stroke="none" fill="#999999" text-anchor=middle font-size="11" y="111"
                    x="@round2((x + 6 - 5) as f32 / (stats.total_owners_at_month.len() as f32 / 1000.))">@(x / 12 + 2015)</text>
                }
            </svg>
            <p class=legend>Number of users/teams owning a crate on crates.io</p>

<p>There are @format_number(stats.max_total_owners) users or teams that have a crate on crates.io. The number of owners is growing at a rate of
@GlobalStats::relative_increase((stats.max_total_owners as _, stats.total_owners_at_month[stats.total_owners_at_month.len()-12] as _)) per year.</p>

<p>Lib.rs has indexed @stats.total_crate_num crates.</p>

    </section>
    <section id="crates-stats-histograms">
        <h2>Dependencies</h2>
        <section id="num-deps">
            <h3>Number of direct dependencies</h3>
            <p>Number of libraries explicitly used by each crate. Includes&nbsp;dev, build-time and optional dependencies.</p>
            @:histogram_html(&stats.hs_deps1, 0, url, url_for_crate_name)
            @:histogram_html(&stats.hs_deps2, 5, url, url_for_crate_name)</section>
        <section id="rev-deps">
            <h3>Number of transitive reverse dependencies (popularity)</h3>
            <p>How often each library crate is used as a dependency in other crates, directly or indirectly. Includes dev, build-time, and optional dependencies.</p>
            @:histogram_html(&stats.hs_rev_deps, 10, url, url_for_rev_deps)</section>
        <h2>Development</h2>
        <section id="crate-time-in-dev">
            <h3>How long a crate has been in development</h3>
            <p>Time between its oldest and newest release.</p>
            @:histogram_html(&stats.hs_maintenance, 5, url, versions_for_crate_name)</section>
        <section id="time-without-updates">
            <h3>Time without any updates</h3>
            <p>Time since crate's most recent release. Keep in mind that some crates can be "done" and not need regular updates.</p>
            @:histogram_html(&stats.hs_languish, 5, url, url_for_crate_name)</section>
        <section id="crate-age">
            <h3>Age</h3>
            <p>Time since crate's first release.</p>
            @:histogram_html(&stats.hs_age, 5, url, url_for_crate_name)</section>
        <section id="releases-per-crate">
            <h3>Number of releases per crate</h3>
            <p>Number of unique versions of each crate, excluding yanked versions.</p>
            @:histogram_html(&stats.hs_releases, 5, url, versions_for_crate_name)</section>
        <section id="crates-per-user">
            <h3>Number of crates per user</h3>
            <p>How many crates a single account (user or team) owns. Excluding&nbsp;all other people in the world who have 0 crates.</p>
            @:histogram_html(&stats.hs_owner_crates, 8, url, Urler::crate_owner_by_github_login)</section>
        <h2>Sizes</h2>
        <section id="crate-sizes">
            <h3>Crate size (KB)</h3>
            <p>Amount of data downloaded as a compressed tarball. Size of code&nbsp;+&nbsp;bundled data files. Individual crate pages show size of uncompressed code.</p>
            @:histogram_html(&stats.hs_sizes, 5, url, url_for_crate_name)</section>
        <section id="crate-bandwidth">
            <h3>Crate total bandwidth (GB)</h3>
            <p>Amount of data downloaded as a compressed tarball&nbsp;&times;&nbsp;number of downloads per month.</p>
            @:histogram_html(&stats.hs_bandwidth_sizes, 10, url, url_for_crate_name)</section>
    </section>

    <section id="categories">
        <h2>Categories</h2>
        <p>Number of crates in each category. The area is proportional to the number&nbsp;of distinct crate&nbsp;owners publishing&nbsp;in each category. Colors are only for decoration.</p>
        <svg viewBox="0 0 1000 600" width=1000 height=600 style="width:100%; margin: 1em auto 0">
            <style>
            rect.cat @{stroke: white;@}
            @@media (prefers-color-scheme: dark) @{
                rect.cat @{stroke: black;@}
            @}
            </style>
            @for b in &stats.categories {
                <rect class="cat" fill="@b.color" fill-opacity="0.7" x=@round2(b.bounds.x as f32) y=@round2(b.bounds.y as f32) width=@round2(b.bounds.w as f32) height=@round2(b.bounds.h as f32)>
                    <title>@b.title (@b.count crates, @b.owners authors)</title></rect>
                <a href="@url.category(&b.cat, Default::default())">
                @for (i, w) in b.label.lines().enumerate() {<text
                    font-size="@round2(b.font_size)" fill="black" stroke="none" x="@round2(b.bounds.x as f32 + 3.)" y="@round2(b.line_y(i))"
                    text-anchor=start><title>@b.title (@b.count crates, @b.owners authors)</title>@w
</text>}
                </a>
                @if b.can_fit_count() {
                    <text font-size="@round2(b.font_size.min(10.5))" fill="black" opacity="0.5" stroke="none"
                    x="@round2(b.bounds.x as f32 + 3.)" y="@round2(b.line_y(b.label.lines().count()))"
                                    text-anchor=start>@b.count <title>@b.count crates, @b.owners authors</title></text>
                }
            }
        </svg>
    </section>
    <section id="vs">
        <h2>Ecosystem</h2>
        <p>Number of crates that have each of the alternatives in their dependencies. More&nbsp;popular doesn't mean better. These&nbsp;crates and categories were hand-picked.</p>
        @for vs in &stats.vs {
            <table>
                <caption>@vs.label</caption>
                <tbody class="bar">
                <tr>@for (i, (prc, v)) in vs.values.iter().enumerate() {<td @if i > 2 && *prc < 2.0 {class="tiny"} width="@(prc)%" style="color:@Vs::fg(i);background:@Vs::bg(i)">@v</td>}</tr>
                <tfoot class="bar">
                <tr>@for (i, (l, (prc, _))) in vs.labels.iter().zip(&vs.values).enumerate() {<th @if i > 2 && *prc < 2.0 {class="tiny"} width="@(prc)%"><a href="/crates/@l">@l</a></th>}</tr></tfoot>
                </tbody>
            </table>
        }

    </section>
    <section id="rustc">
        <h2>Rustc compatibility</h2>
        <p>Percentage of crates (in their current version) that can be compiled with a given Rust version. The&nbsp;data is based on sampling of <code>cargo check</code> and <code>clippy::incompatible_msrv</code> on Linux/aarch64. Excludes&nbsp;crates that never built (e.g. because they need nightly, or Windows, or unusual C dependencies).</p>
        <p>These stats are best-case scenarios that assume use of a most-compatible <code>Cargo.lock</code> file. Fresh&nbsp;projects without a lockfile will be less compatible due to too-new transitive dependencies.</p>
        <p>Compatibility weighed by crates' popularity is not a realistic representation of ecosystem-wide compatibility.
            Actual&nbsp;compatibility is always worse, because <abbr title="minimum supported rust version">MSRV</abbr> of projects is not an average of their dependencies <abbr title="minimum supported rust version">MSRV</abbr>, but entirely dependent on their least-compatible dependency.</p>
        <svg width=1 height=1><defs>
        <linearGradient id="svggrad1" x1="0%" y1="0%" x2="100%" y2="0%">
          <stop offset="0%" style="stop-color:#8c9f8c;" />
          <stop offset="100%" style="stop-color:#bb8282;" />
        </linearGradient>
      </defs></svg>
      <div class="breakdowns">
        <section>
            <h3>All crates, including unmaintained ones</h3>
            @:global_stats_rustc_html(&stats.rustc_stats_all)
        </section>
        <section>
            <h3>@stats.rustc_stats_recent_num most recently updated crates</h3>
            @:global_stats_rustc_html(&stats.rustc_stats_recent)
        </section>
        @if let Some(usage) = &stats.rustc_usage {
            <section>
                <h3>Usage of Rust versions based on crates.io traffic</h3>
                <p>This is based on crates.io request log data from the&nbsp;last week. This&nbsp;is not&nbsp;limited to real users, and includes automated traffic like CI.
                    @if let Some((_,_,nightly)) = usage.iter().max_by_key(|(_,_,n)| (n*100.) as u32) {
                        @((100. - nightly).round())%&nbsp;of&nbsp;requests were from stable (non-nightly) Rust versions.
                    }
                    @if let Some((ver,stable,_)) = usage.last() {
                        Less&nbsp;than @(((100.-stable)*100.).round()/100.)%&nbsp;of&nbsp;requests came from Rust versions older than 1.@ver.
                    }
                </p>
                @:global_stats_rustc_use_html(usage)
            </section>
        }
      </div>
    </td></tr>
    </section>
    </main>

    <footer>
        <div class="inner-col" role="contentinfo">
        <p><a href="/">All categories</a>. <a href="/about">About the site</a>. <a href="/atom.xml">Feed</a>. By <a href="/~kornelski">kornelski</a>. Not affiliated with the Rust Project.</div></footer>
})
