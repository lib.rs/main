use crate::{base_urls_fallback, make_link_fixer, render_maybe_markdown_paragraph, templates, Page, Renderer, SmolStr, Urler};
use util::{FxHashMap as HashMap, FxHashSet as HashSet};
use cargo_toml::features::{Feature, Features, Kind, Resolver};
use cargo_toml::Manifest;
use rich_crate::Repo;
use std::borrow::Cow;

use kitchen_sink::{Cfg, CratesIndexVersion, Index, KitchenSink, KitchenSinkErr, Target};
use log::error;
use manifest::is_no_std_feature;
use render_readme::LinksContext;
use rich_crate::{DetailedFeatures, Origin, RichCrateVersion};
use semver::{Version as SemVer, VersionReq};
use std::cell::RefCell;
use std::collections::BTreeSet;

use udedokei::synscrape::FeaturesEnableItems;

pub type FeaturesPageBorrow<'a> = &'a FeaturesPage<'a>;

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone)]
pub(crate) enum Order {
    Default,
    Std,
    Alloc,
    Full,
    Manual(u16), // as in [features] section
    OtherExplicit,
    Dependency(u16), // as in [dependencies] section
    OtherDependency,
    OtherImplicitLast,
    OtherJunk, // these are implied features probably not meant to be set
}

pub struct FeaturesPageCollector<'a> {
    pub(crate) features: HashMap<&'a str, FeatExtra<'a>>,
    pub(crate) default_feature_is_empty: bool,
    pub(crate) features_enabled_by_default: HashSet<&'a str>,
    pub(crate) removed_hidden_features: bool,
    pub(crate) preface: String,
    pub(crate) deps_preface: String,
    /// Key is package name
    pub(crate) has_items_enabled_by_features: bool,
    all_deps: HashMap<&'a str, DepMetaKindExtra<'a>>,
}

pub struct FeaturesPage<'a> {
    pub(crate) collected: FeaturesPageCollector<'a>,
    pub(crate) features: Vec<FeatExtra<'a>>,
    pub(crate) only_dependencies: Vec<FeatExtra<'a>>,
    pub(crate) ver: &'a RichCrateVersion,
    renderer: &'a Renderer,
    kitchen_sink: &'a KitchenSink,
    dep_comment_once: RefCell<HashSet<(&'a str, &'a str)>>,

    pub(crate) cargo_toml_url: Option<String>,
    // >0 features count
    pub(crate) all_features_are_boring: usize,
    pub(crate) has_long_keys: bool,
}

#[derive(Debug)]
pub(crate) struct DepMetaKindExtra<'a> {
    pub origin: Origin,
    crate_name: &'a str,
    pub only_for_targets: Vec<Target<SmolStr>>,
    pub conditional_features: HashSet<&'a str>,
    pub unconditional_features: HashSet<&'a str>,
    pub normal_dep_kind: Kind,
    pub has_no_default_features: bool,
    pub optional_dep_kind: Option<Kind>,
    pub comment: &'a str,
    /// req matches old dep
    pub not_latest: Option<VersionReq>,
    /// this is info from the dependency, not from the features
    target_crate_default_features: Vec<SmolStr>,
    /// Seen action with dep:? for it? so its features are explicit and not visible in feature-deps
    is_dep_only: bool,
    hidden_features: HashMap<SmolStr, BTreeSet<SmolStr>>,
}

impl<'a> DepMetaKindExtra<'a> {
    pub(crate) fn set_comment(&mut self, key: &str, comment: &'a str) {
        if !self.comment.is_empty() {
            return;
        }
        let trimmed_comment = comment.trim();
        if trimmed_comment == key || trimmed_comment == self.crate_name {
            return;
        }
        self.comment = comment;
    }
}

#[derive(Debug)]
pub(crate) struct FeatExtra<'a> {
    order: Order,
    pub(crate) dep_kind: Kind,
    pub(crate) merged_keys: Vec<&'a str>,
    pub(crate) comment: &'a str,
    pub(crate) comment_for_dep: Option<(&'a str, &'a str)>,
    pub(crate) f: Feature<'a>,
    pub(crate) items_enabled_by_feature: Vec<&'a str>,
}

impl<'a> FeatExtra<'a> {
    fn set_comment(&mut self, comment: &'a str) {
        if !self.comment.is_empty() {
            return;
        }
        // skip trivial repetition
        let trimmed_comment = comment.trim();
        // TODO: check against target package name too
        if trimmed_comment == self.f.key || self.f.enables_deps.contains_key(trimmed_comment) || self.f.enables_features.contains(trimmed_comment) {
            return;
        }
        self.comment = comment;
    }
}

impl<'a> FeaturesPageCollector<'a> {
    pub(crate) fn new(manifest: &'a Manifest, krate: &'a RichCrateVersion, index: &'a Index) -> Result<Self, KitchenSinkErr> {
        let items_enabled_by_features = krate.features_enable_items();

        let Features { features, dependencies: all_deps, removed_hidden_features , .. } = Resolver::<util::FxBuildHasher>::new_with_hasher_and_filter(&|k| {
            items_enabled_by_features.contains_key(k)
        }).parse(manifest);
        let mut all_deps: HashMap<_, _> = all_deps.into_iter().map(|(k, d)| {

            let (normal_dep_kind, normal_dep) = d.targets.iter().next().expect("one must be set");
            let req = normal_dep.req();
            let optional_dep_kind = d.targets.iter().find(|&(_, d)| d.optional()).map(|(k, _)| k.kind);
            let origin = if let Some(repo) = normal_dep.git().and_then(|git| Repo::new(git).ok()).and_then(|r| Origin::from_repo(&r, d.crate_name)) {
                repo
            } else {
                Origin::from_crates_io_name(d.crate_name)
            };

            let has_no_default_features = normal_dep.detail().is_some_and(|d| !d.default_features);

            let (not_latest, target_crate_default_features, hidden_features) = Self::target_crate_features_for_dependency(&origin, req, index).unwrap_or_default();

            // Used later to remove redundant dep-features
            let mut unconditional_features = HashSet::default();
            let mut conditional_features = HashSet::default();
            for (tk, d) in &d.targets {
                let always_applicable = !d.optional() &&
                    (normal_dep_kind == tk || tk.kind != Kind::Dev) &&
                    (normal_dep_kind == tk || tk.target.is_none());

                let set = if always_applicable {
                    &mut unconditional_features
                } else if d.optional() {
                    &mut conditional_features
                } else {
                    continue;
                };
                set.extend(d.req_features().iter().map(|k| k.as_str()));
                if d.detail().map_or(true, |det| det.default_features) {
                    set.insert("default");
                }
            }

            if !conditional_features.is_empty() {
                for &f in &unconditional_features {
                    conditional_features.remove(f);
                }
            }

            // Don't set features that the crate has on by default
            if unconditional_features.contains("default") {
                for f in &target_crate_default_features {
                    unconditional_features.remove(f.as_str());
                    conditional_features.remove(f.as_str());
                }
            }
            if conditional_features.contains("default") {
                for f in &target_crate_default_features {
                    conditional_features.remove(f.as_str());
                }
            }

            let only_for_targets = if normal_dep_kind.target.is_some() {
                d.targets.keys().filter_map(|k| k.target)
                    .collect::<HashSet<_>>().into_iter() // dedup
                    .map(|target| {
                        Target::<SmolStr>::parse_generic(target).unwrap_or_else(|_| {
                            Target::Cfg(Cfg::Is(target.into()))
                        })
                    })
                    .collect()
            } else { Vec::new() };

            (k, DepMetaKindExtra {
                hidden_features,
                only_for_targets,
                crate_name: d.crate_name,
                conditional_features,
                unconditional_features,
                normal_dep_kind: normal_dep_kind.kind,
                optional_dep_kind,
                origin,
                comment: "", not_latest,
                target_crate_default_features,
                has_no_default_features,
                is_dep_only: false,
            })
        }).collect();

        let mut features: HashMap<_, _> = features.into_iter().map(|(k, mut f)| {
            f.enables_deps.iter().filter(|&(_, a)| a.is_dep_only).for_each(|(&dep_key, _)| {
                if let Some(d) = all_deps.get_mut(dep_key) {
                    d.is_dep_only = true;
                }
            });

            let dep_kind = f.enables_deps.iter_mut()
                .filter_map(|(&dep_key, a)| Some((a, all_deps.get(dep_key)?)))
                .map(|(a, d)| {

                    let mut features_to_remap = Vec::new();
                    a.dep_features.retain(|f| {
                        if let Some(replace) = d.hidden_features.get(&**f) {
                            features_to_remap.push(replace);
                            false
                        } else {
                            true
                        }
                    });
                    for replace in features_to_remap {
                        a.dep_features.extend(replace.iter().map(|c| Cow::Owned(c.to_string())));
                    }

                    // BTW, clean redundant features from actions
                    if !a.dep_features.is_empty() {
                        let default_feature_enabled = d.unconditional_features.contains("default") || a.dep_features.contains("default");
                        if default_feature_enabled {
                            for f in &d.target_crate_default_features {
                                a.dep_features.remove(f.as_str());
                            }
                        }

                        for &k in &d.unconditional_features {
                            a.dep_features.remove(k);
                        }
                    }
                    (a, d)
                })
                .map(|(a, d)| if a.is_conditional { d.normal_dep_kind } else { d.optional_dep_kind.unwrap_or(d.normal_dep_kind) })
                .reduce(|a, b| {
                    use Kind::*;
                    if a == Normal || b == Normal { Normal }
                    else if a == Build || b == Build { Build }
                    else { a }
                })
                .unwrap_or(Kind::Normal);

            let order = if dep_kind == Kind::Dev {
                Order::OtherJunk
            } else if f.explicit {
                if k == "default" { Order::Default } else { Order::OtherExplicit }
            } else {
                // referenced implicit features are probably just deps in the pre-"dep:" scheme
                if f.is_referenced() { Order::OtherImplicitLast } else { Order::OtherDependency }
            };

            // reduce name repetition for binary names
            if f.required_by_bins.len() == 1 && f.required_by_bins[0] == krate.short_name() {
                f.required_by_bins[0] = ""; // front-end template supports this
            }

            (k, FeatExtra {
                f, dep_kind, order,
                merged_keys: Vec::new(), comment: "",
                items_enabled_by_feature: Vec::new(),
                comment_for_dep: None,
            })
        }).collect();

        let mut preface = String::new();
        let mut deps_preface = String::new();
        if let Some(d) = krate.detailed_features() {
            Self::add_sort_order_and_comments(&mut preface, &mut deps_preface, d, &mut features, &mut all_deps);
        }
        let has_items_enabled_by_features = Self::add_items_enabled_by_feature(&mut features, items_enabled_by_features);

        let default_feature = features.get("default");
        let features_enabled_by_default = default_feature.into_iter().flat_map(|f| f.f.enables_features.iter().copied()).collect();
        let default_feature_is_empty = default_feature.map_or(true, |f| f.f.enables_deps.is_empty() && f.f.enables_features.is_empty() && !f.f.is_referenced());
        let default_feature_has_comment = default_feature.is_some_and(|f| !f.comment.is_empty());
        // just don't bother showing it if it's a dud
        if default_feature_is_empty && !default_feature_has_comment {
            features.remove("default");
        }

        Ok(FeaturesPageCollector {
            features,
            default_feature_is_empty,
            features_enabled_by_default,
            removed_hidden_features,
            preface,
            deps_preface,
            has_items_enabled_by_features,
            all_deps
        })
    }

    /// get actual features from the index (used later to remove irrelevant defaults)
    /// first arg is version if it wasn't latest
    fn target_crate_features_for_dependency(origin: &Origin, req: &str, index: &Index) -> Option<(Option<VersionReq>, Vec<SmolStr>, HashMap<SmolStr, BTreeSet<SmolStr>>)> {
        let Origin::CratesIo(dep_name_lc) = origin else {
            return None;
        };
        let crates_io_crates = index.crates_io_crates().ok()?;
        let krate = crates_io_crates.get(dep_name_lc)?;

        let mut latest_stable = None;
        let mut latest_unstable = None;
        let req = VersionReq::parse(req).ok()?;
        let (matched_ver, ver) = krate.versions().iter().rev() // from most recent
            .filter(|ver| !ver.is_yanked())
            .filter_map(|ver| {
                let v = SemVer::parse(ver.version()).ok()?;
                let latest = if v.pre.is_empty() {
                    &mut latest_stable
                } else {
                    &mut latest_unstable
                };
                if latest.as_ref().map_or(true, |l| *l < v) {
                    *latest = Some(v.clone());
                }
                if req.matches(&v) { Some((v, ver)) } else { None }
            })
            .max_by(|a,b| a.0.cmp(&b.0))?;
        let is_latest = latest_stable.is_some_and(|l| l == matched_ver) || latest_unstable.is_some_and(|l| l == matched_ver);

        let dep_parser = ManifestDepParser::new(ver);
        let ft = dep_parser.parse();

        let all_default_features = if let Some(default_feature) = ft.features.get("default") {
            let (features, _) = default_feature.enables_recursive(&ft.features);
            features.into_keys().map(SmolStr::from).collect()
        } else {
            vec![]
        };

        let hidden_features = ft.hidden_features.into_iter()
            .map(|(k, v)| (SmolStr::from(k), v.into_iter().map(SmolStr::from).collect())).collect::<HashMap<_, _>>();

        Some((if !is_latest { Some(req) } else { None }, all_default_features, hidden_features))
    }

    fn add_sort_order_and_comments(preface: &mut String, deps_preface: &mut String, details: &'a DetailedFeatures, all_features: &mut HashMap<&'a str, FeatExtra<'a>>, all_deps: &mut HashMap<&'a str, DepMetaKindExtra<'a>>) {
        // Apply manual order and comments to [features] section
        for (i, det) in details.features.iter().enumerate() {
            if !det.key.is_empty() {
                if let Some(f) = all_features.get_mut(&*det.key) {
                    // comments must be displayed in order, because they contain phrases like "features below are…"
                    f.order = Order::Manual(i as _);
                    f.set_comment(&det.comment);
                }
            } else {
                // empty key is a hack to mean preface
                preface.clone_from(&det.comment);
            }
        }

        // Empty key is a hack to mean preface
        if let Some(det) = details.normal.iter().find(|d| d.key.is_empty()) {
            deps_preface.clone_from(&det.comment);
        }

        // Apply manual order and comments to [dependencies] sections
        for (i, src) in [&details.normal, &details.build, &details.dev].into_iter().flatten().enumerate() {
            // deps_preface is already handled, and intentionally skips build/dev because UI doesn't group by build/dev
            if src.key.is_empty() { continue; }

            if !src.comment.is_empty() {
                if let Some(dep) = all_deps.get_mut(&*src.key) {
                    dep.set_comment(&src.key, &src.comment);
                }
            }
            if let Some(f) = all_features.get_mut(&*src.key) {
                if matches!(f.order, Order::OtherDependency | Order::OtherImplicitLast) {
                    // dev-only fetures are useless
                    f.order = Order::Dependency(i as _);
                }
            }
        }
    }

    // use it after adding comments
    fn add_items_enabled_by_feature(all_features: &mut HashMap<&'a str, FeatExtra<'a>>, items_enabled_by_features: &'a FeaturesEnableItems) -> bool {
        let mut added = false;
        for (f_key, items) in items_enabled_by_features {
            if let Some(feature) = all_features.get_mut(f_key.as_str()) {
                let items: Vec<_> = items.iter()
                    .map(|item| item.as_str())
                    .filter(|&item| !feature.comment.contains(item)).collect();
                if !items.is_empty() {
                    added = true;
                }
                feature.items_enabled_by_feature = items;
            }
        }
        added
    }

    pub fn needs_additional_crates(&self) -> HashSet<&Origin> {
        self.features.values()
            .filter(|f| f.comment.is_empty() && !f.f.enables_deps.is_empty())
            .flat_map(|f| f.f.enables_deps.iter().filter(|&(_, a)| {
                // good dep-features will get crate description
                (!f.f.explicit && f.f.is_user_facing()) || (!a.dep_features.is_empty() && a.dep_features.iter().any(|k| *k != "default"))
            }))
            .filter_map(|(&dep_key, _)| self.all_deps.get(dep_key))
            .filter(|d| d.comment.is_empty())
            .map(|d| &d.origin)
            .collect()
    }

    pub(crate) fn add_additional_crates(&mut self, more_crates: &HashMap<&'a Origin, &'a RichCrateVersion>) {
        let mut unique_comments = HashSet::default();
        for feature in self.features.values_mut() {
            if feature.comment.is_empty() && !feature.f.enables_deps.is_empty() {
                for (action, s) in feature.f.enables_deps.iter().filter_map(|(k, action)| Some((action, self.all_deps.get(k)?))) {
                    if !s.comment.is_empty() && s.comment != feature.f.key {
                        break;
                    }
                    let Some(krate) = more_crates.get(&s.origin) else { continue };
                    let extra_comment = krate.detailed_features()
                        .and_then(|f| {
                            f.features.iter() // comments from other crate's deps are too disconnected
                                .filter(|f| !f.comment.is_empty() && f.comment != feature.f.key && f.key != "default")
                                .filter(|&f| {
                                    // comment for a feature when setting multiple features makes it weird
                                    let default_len = if action.dep_features.contains("default") { 1 } else { 0 };
                                    (action.dep_features.len() == 1+default_len && action.dep_features.contains(f.key.as_str())) ||
                                    (action.dep_features.len() == default_len && f.key == feature.f.key)
                                })
                                .map(|f| f.comment.as_str())
                                .find(|&c| unique_comments.insert(c))
                        });
                    if let Some(extra_comment) = extra_comment {
                        feature.comment_for_dep = Some((s.crate_name, extra_comment));
                        break;
                    }
                    // dep-enabling features can get their target's (short) description
                    if !feature.f.explicit && feature.f.is_user_facing() && feature.f.enables_deps.len() == 1 {
                        if let Some(desc) = krate.description() {
                            if desc.len() < 100 {
                                feature.comment_for_dep = Some((s.crate_name, desc));
                            }
                        }
                    }
                }
            }
        }
    }
}

impl<'a> FeaturesPage<'a> {
    pub(crate) fn render_comment(&self, s: &str) -> templates::Html<String> {
        let mut tmp = None;
        let base_url = base_urls_fallback(self.ver, &mut tmp);
        let own_crate_name = Some(self.ver.short_name());
        let kitchen_sink = self.kitchen_sink;
        let check = &move |name: &str| Origin::try_from_crates_io_name(name).is_some_and(move |o| kitchen_sink.crate_exists(&o));
        let res = render_maybe_markdown_paragraph(s, self.renderer, &LinksContext {
            base_url, own_crate_name,
            nofollow: render_readme::Links::Ugc,
            link_own_crate_to_crates_io: false,
            link_fixer: Some(&make_link_fixer(own_crate_name, base_url, Some(&check))),
        }, true);
        res // borrow checker nonsense
    }

    pub(crate) fn new(mut collected: FeaturesPageCollector<'a>, renderer: &'a Renderer, krate: &'a RichCrateVersion, kitchen_sink: &'a KitchenSink, _urler: &Urler) -> Result<Self, KitchenSinkErr> {
        let is_test_related_crate = krate.category_slugs().iter().any(|s| s == "development-tools::testing");
        let is_profiling_related_crate = krate.category_slugs().iter().any(|s| s == "development-tools::profiling");

        let (mut features_resolved, mut only_dependencies): (Vec<_>, Vec<_>) = std::mem::take(&mut collected.features).into_values().partition(|f| f.f.explicit);
        Self::sort(&mut features_resolved, is_test_related_crate, is_profiling_related_crate);
        Self::sort(&mut only_dependencies, is_test_related_crate, is_profiling_related_crate);

        let has_long_keys = features_resolved.iter().any(|f| f.f.key.len() > 30 || f.merged_keys.iter().any(|f| f.len() > 25));
        if features_resolved.len() >= 5 {
            Self::merge(&mut features_resolved, has_long_keys);
        }

        let all_features_are_boring = only_dependencies.is_empty() && !collected.has_items_enabled_by_features &&
            features_resolved.iter().all(|f| f.comment.is_empty() && f.f.required_by_bins.is_empty())
            && collected.all_deps.values().all(|d| d.comment.is_empty());

        Ok(Self {
            // merged deps make counting later hard
            all_features_are_boring: if all_features_are_boring { features_resolved.len() } else { 0 },
            features: features_resolved,
            only_dependencies,
            collected,
            dep_comment_once: Default::default(),
            has_long_keys,
            kitchen_sink,
            cargo_toml_url: krate.file_in_http_repo_url("Cargo.toml"),
            ver: krate,
            renderer,
        })
    }

    fn merge(features: &mut Vec<FeatExtra<'a>>, has_long_keys: bool) {
        let mut last: Option<&mut FeatExtra<'a>> = None;
        let mut merged_some = false;
        for feature_extra in features.iter_mut() {
            let FeatExtra { comment, f, dep_kind, items_enabled_by_feature, .. } = &mut *feature_extra;
            if f.key == "default" {
                last = None;
                continue;
            }
            if let Some(last) = last.as_mut() {
                if *comment == last.comment &&
                f.explicit && last.f.explicit &&
                f.enables_deps == last.f.enables_deps &&
                f.enables_features == last.f.enables_features &&
                f.enabled_by == last.f.enabled_by &&
                *dep_kind == last.dep_kind &&
                f.required_by_bins == last.f.required_by_bins &&
                *items_enabled_by_feature == last.items_enabled_by_feature {
                    let mut key_text_len = f.key.len() as u32 + last.f.key.len() as u32 + last.merged_keys.iter().map(|k| k.len() as u32 + 3).sum::<u32>();
                    if has_long_keys {
                        key_text_len = key_text_len * 2 / 3; // has_long_keys makes the font smaller
                    }
                    let labels_len = last.f.enabled_by.iter().map(|k| k.len().min(30) as u32 + 2).sum::<u32>() * 2 / 3;
                    let features_len = last.f.enables_features.iter().map(|k| k.len().min(30) as u32).sum::<u32>() * 3 / 4;
                    // either fit all on one line, or if the line has to be long anyway, then cram it
                    if features_len < 80 && (key_text_len < 38 || key_text_len + labels_len + features_len < 50 || labels_len + features_len > 50) {
                        last.merged_keys.push(last.f.key);
                        last.f.key = f.key;
                        f.key = "";

                        merged_some = true;
                        continue;
                    }
                }
            }
            last = Some(feature_extra);
        }
        if merged_some {
            features.retain(|f| !f.f.key.is_empty());
        }
    }

    /// Sort only items between comments, because comments sometimes say "features below are xxx"
    fn sort(features: &mut [FeatExtra<'a>], is_test_related_crate: bool, is_profiling_related_crate: bool) {
        let cmp = |a: &FeatExtra<'_>, b: &FeatExtra<'_>| a.order.cmp(&b.order)
            .then_with(|| a.f.enabled_by.len().cmp(&b.f.enabled_by.len()))
            .then_with(|| b.f.enables_features.len().cmp(&a.f.enables_features.len()))
            .then_with(|| a.f.key.cmp(b.f.key));
        // sort first by best-known order
        features.sort_unstable_by(cmp);

        // find de-facto "all" feature
        let mut changed = false;
        let num_features = features.len()/2;
        // rev because last max wins
        if let Some(biggest) = features.iter_mut().rev().max_by_key(|f| f.f.enables_features.len()) {
            if biggest.order >= Order::OtherExplicit && biggest.f.enables_features.len() > 5.max(num_features/2) {
                biggest.order = Order::Full;
                changed = true;
            }
        }

        features.iter_mut().filter(|f| f.comment.is_empty()).for_each(|f| {
            if let Some(o) = Self::order_for_feature_name(f.f.key, is_test_related_crate, is_profiling_related_crate) {
                if f.order != o {
                    changed = true;
                    f.order = o;
                }
            }
        });

        if !changed {
            return;
        }

        let mut f = features;
        loop {
            let skip = f.iter().take_while(|f| !f.comment.is_empty()).count();
            if skip > 0 {
                f = &mut f[skip..];
            }
            let take = f.iter().take_while(|f| f.comment.is_empty()).count();
            if take == 0 {
                return;
            }
            let (sort, rest) = f.split_at_mut(take);
            debug_assert!(sort.iter().all(|f| f.comment.is_empty()));
            sort.sort_unstable_by(cmp);
            f = rest;
        }
    }

    pub(crate) fn if_is_dep(&self, f: &FeatExtra<'a>) -> Option<&DepMetaKindExtra<'a>> {
        if f.f.explicit { None } else { self.collected.all_deps.get(f.f.key) }
    }

    pub(crate) fn page(&self) -> Page {
        let no_useful_content = self.only_dependencies.is_empty() && self.features.is_empty();
        Page {
            title: format!("Feature flags of {} crate", self.ver.capitalized_name()),
            item_name: Some(self.ver.short_name().to_string()),
            item_description: self.ver.description().map(|d| d.to_string()),
            noindex: no_useful_content || self.ver.is_yanked() || self.ver.is_spam() || self.ver.is_hidden(),
            search_meta: false,
            critical_css_data: Some(include_str!("../../style/public/features.css")),
            critical_css_dev_url: Some("/features.css"),
            ..Default::default()
        }
    }

    fn order_for_feature_name(key: &str, is_test_related_crate: bool, is_profiling_related_crate: bool) -> Option<Order> {
        if key == "default" {
            return Some(Order::Default);
        }
        if is_no_std_feature(key) {
            return Some(if key.contains("alloc") { Order::Alloc } else { Order::Std });
        }

        if key.starts_with('_') || key.ends_with('_') {
            return Some(Order::OtherJunk);
        }
        let tmp;
        let key_name_simplified = if !key.bytes().any(|b| b == b'_') { key } else {
            tmp = key.replace('_', "-"); &tmp
        }.trim_matches('-');
        let is_internal = key_name_simplified.starts_with("internal-") || key_name_simplified.ends_with("-private");
        if is_internal {
            return Some(Order::OtherJunk);
        }
        match key_name_simplified.strip_prefix("unstable-").unwrap_or(key_name_simplified) {
            // TODO: std not handled until we can check comments and it's okay to change order?
            "doc" | "docs" | "docsrs" | "docs-rs" | "docs-only" | "doc-cfg" | "rustdoc" |
            "nightly-docs" | "external-doc" => Some(Order::OtherJunk), // common hacks

            "deprecated" => Some(Order::OtherJunk),

            // these are used to enable private features
            "test" | "test-util" | "test-utils" | "testutils" | "test-helpers" | "test-e2e" | "testing" | "nightly-testing" |
            "test-support" | "integration-tests" | "integration-test" | "ui-tests" | "slow-tests" |
            "test-suite" | "testsuite" | "all-tests" | "test-vectors" | "testmock" | "testing-helpers" |
            "fuzz" | "fuzz-testing" | "fuzztarget" | "fuzzing" | "with-fuzzer" | "fuzz-helpers" | "libfuzzer" |
            "mock" | "arbitrary" | "criterion" | "proptest" if !is_test_related_crate => Some(Order::OtherJunk),

            "bench" | "benchmark" | "benching" | "benchmarks" | "runtime-benchmarks" | "with-bench" | "nightly-bench" |
            "benchmarking" | "bench-it" | "bencher" | "criterion-bench" if !is_profiling_related_crate => Some(Order::OtherJunk),

            "full" | "all" => Some(Order::Full),
            _ => None,
        }
    }
}

#[derive(Debug)]
pub(crate) struct ACDep<'a> {
    pub key: &'a str,
    pub is_conditional: bool,
    pub dep: &'a DepMetaKindExtra<'a>,
    pub comment: &'a str,
    pub comment_from_crate: bool,
}

#[derive(Debug)]
pub(crate) struct ActionsCombined<'a> {
    /// key, conditional, dep, comment
    pub deps: Vec<ACDep<'a>>,
    pub dep_features: &'a BTreeSet<Cow<'a, str>>,
    pub default_features: bool,
    pub default_dep_features_sample: Option<&'a [SmolStr]>,
    pub hide_deps: bool,
    pub show_comment: bool,
    any_conditional: bool,
}

impl<'a> ActionsCombined<'a> {
    pub fn dep_comment(&self) -> Option<(&str, Option<&str>)> {
        if self.show_comment {
            self.deps.iter()
                .filter(|&ac| !ac.comment.is_empty())
                .min_by_key(|&ac| ac.comment_from_crate)
                .map(|ac| (ac.comment, if ac.comment_from_crate || self.deps.len() > 1 {Some(ac.dep.crate_name)} else {None}))
        } else {
            None
        }
    }

    pub fn an_example_feature(&self) -> Option<&'a str> {
        if self.default_features { Some("default") }
        else {
            self.dep_features.first().map(|s| &**s)
        }
    }

    // features, second feature if related, + package
    pub fn all_features(&self) -> impl Iterator<Item = ((&str, Option<&[SmolStr]>), &Origin)> {
        let feature_names = self.dep_features.iter().map(|s| &**s)
            .filter(|&f| f != "default")
            .map(|f| (f, None))
            .chain(if self.default_features { Some(("default", self.default_dep_features_sample)) } else { None });
        let mut n = 0;
        let deps = self.deps.as_slice();
        let example_deps = std::iter::from_fn(move || {
            let some_dep = deps.get(n % deps.len()).map(|ac| &ac.dep.origin);
            n += 1;
            some_dep
        });
        feature_names.zip(example_deps)
    }
}

impl<'a> FeaturesPage<'a> {
    /// Merge adjacent actions to reduce repetition
    pub(crate) fn enables_deps_merged(&self, fe: &'a FeatExtra<'_>) -> Option<(&'static str, Vec<ActionsCombined<'_>>)> {
        let feature = &fe.f;
        if feature.enables_deps.is_empty() {
            return None;
        }

        let mut unique_comments = HashSet::default();
        let mut out = Vec::<ActionsCombined<'_>>::with_capacity(feature.enables_deps.len());
        for (&dep_key, action) in &feature.enables_deps {
            let Some(s) = self.collected.all_deps.get(dep_key) else {
                error!("all deps should have been collected for {dep_key} of {}", self.ver.short_name());
                continue;
            };

            // use comment from the other crate as last resort
            let (comment_from_crate, the_comment) = if s.comment.is_empty() && fe.comment.is_empty() {
                (true, fe.comment_for_dep.filter(|&(k, _)| k == s.crate_name).map(|(_, c)| c).unwrap_or_default())
            } else {
                (false, s.comment)
            };

            let show_comment = !the_comment.is_empty() && the_comment != fe.comment && the_comment != dep_key && the_comment != feature.key &&
                unique_comments.insert(the_comment) &&
                self.dep_comment_once.borrow_mut().insert((s.crate_name, the_comment));

            let can_enable_default = s.has_no_default_features &&
                ((action.dep_features.contains("default") && !s.unconditional_features.contains("default")) ||
                 (!action.is_conditional && s.conditional_features.contains("default")));
            let default_features = can_enable_default && !s.target_crate_default_features.is_empty();

            let acdep = ACDep {
                key: dep_key,
                is_conditional: action.is_conditional,
                dep: s,
                comment: if show_comment { the_comment } else { "" },
                comment_from_crate: comment_from_crate && show_comment,
            };

            if let Some(last) = out.last_mut() {
                if last.dep_features == &action.dep_features &&
                    // don't make crate-specific comment group-wide, causes wrong attribution
                    (!last.show_comment || !show_comment || (show_comment && comment_from_crate)) &&
                    last.default_features == default_features
                {
                    if show_comment { last.show_comment = true; }
                    if action.is_conditional { last.any_conditional = true; }
                    last.deps.push(acdep);
                    continue;
                }
            }

            out.push(ActionsCombined {
                hide_deps: false,
                any_conditional: action.is_conditional,
                deps: vec![acdep],
                dep_features: &action.dep_features,
                default_features,
                show_comment,
                default_dep_features_sample: None,
            });
        }

        out.iter_mut().for_each(|dc| {
            if dc.default_features {
                dc.default_dep_features_sample = dc.deps.iter()
                    .map(|ac| ac.dep.target_crate_default_features.as_slice())
                    .reduce(|a, b| {
                        if a == b { a } else { &[] } // all defaults must equal
                    })
                    .filter(|d| !d.is_empty() && d.len() < 3); // don't want long list
            }
        });

        let mut intro_text = "Enables";

        if let [only] = out.as_mut_slice() {
            let all_deps_boring = !only.show_comment && only.deps.iter().all(move |ac| {
                ac.dep.not_latest.is_none() &&
                (!feature.explicit || !ac.dep.is_dep_only) && // otherwise there's no link to the crate from the simplified feature
                !ac.is_conditional &&
                ac.key == feature.key && ac.dep.origin.package_name_icase().eq_ignore_ascii_case(feature.key)
            });
            // hiding the extra deps line also hides the link to the dep being enabled if it's an explicit feature (with dep: syntax) :/
            if all_deps_boring {
                let has_any_features_set = only.all_features().next().is_some();
                if !has_any_features_set && !only.show_comment && fe.items_enabled_by_feature.is_empty() {
                    return None; // default values, no deps no comments, nothing to show
                }

                intro_text = if has_any_features_set { "With" } else { "" };
                only.hide_deps = true;
            }
        } else {
            out.iter_mut().for_each(|dc| {
                // stable sort. already sorted by key. put optional and build last
                dc.deps.sort_by(|a,b| {
                    // comment at the end
                    a.is_conditional.cmp(&b.is_conditional)
                    .then_with(move || (a.dep.normal_dep_kind != Kind::Normal).cmp(&(b.dep.normal_dep_kind != Kind::Normal)))
                });
            });
            out.sort_by(|a,b| {
                // with comments last to avoid breaking the flow
                a.show_comment.cmp(&b.show_comment)
                    // if features are first, it's unclear how they apply to no-features crates
                    .then_with(move || (!a.dep_features.is_empty()).cmp(&(!b.dep_features.is_empty())))
                    // conditional last
                    .then_with(move || a.any_conditional.cmp(&b.any_conditional))
                    // longer last, because they're unreadable
                    .then_with(move || a.deps.len().cmp(&b.deps.len()))
            });
        }

        Some((intro_text, out))
    }
}

pub(crate) struct ManifestDepParser<'manifest> {
    version_meta: &'manifest CratesIndexVersion,
    deps_for_features: Vec<cargo_toml::Dependency>,
}

impl<'manifest> ManifestDepParser<'manifest> {
    pub fn new(version_meta: &'manifest CratesIndexVersion) -> Self {
        Self {
            version_meta,
            // Must store in a Vec for lifetime of &dep
            deps_for_features: version_meta.dependencies().iter().take(2000).map(|d| {
                if !d.is_optional() && d.has_default_features() && d.package().is_none() {
                    cargo_toml::Dependency::Simple(d.requirement().into())
                } else {
                    let mut dep = cargo_toml::DependencyDetail::default();
                    dep.version = Some(d.requirement().into());
                    dep.features = d.features().into();
                    dep.default_features = d.has_default_features();
                    dep.optional = d.is_optional();
                    dep.package = d.package().map(String::from);
                    cargo_toml::Dependency::Detailed(Box::new(dep))
                }
            }).collect()
        }
    }

    pub fn parse(&self) -> Features<'manifest, '_> {
        let deps_to_parse = self.version_meta.dependencies().iter().zip(&self.deps_for_features).map(|(d, dep)| {
            cargo_toml::features::ParseDependency {
                key: d.name(),
                target: d.target(),
                kind: match d.kind() {
                    kitchen_sink::DependencyKind::Normal => Kind::Normal,
                    kitchen_sink::DependencyKind::Dev => Kind::Dev,
                    kitchen_sink::DependencyKind::Build => Kind::Build,
                },
                dep,
            }
        });
        Resolver::new().parse_custom(self.version_meta.features(), deps_to_parse)
    }
}
