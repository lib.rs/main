use categories::Category;
use heck::ToKebabCase;
use kitchen_sink::{CrateAuthor, SortOrder, UserType};
use rich_crate::{Origin, Repo, RichCrateVersion, RichDep};
use std::borrow::Cow;
use std::hash::Hash;
use urlencoding::Encoded;
use util::PushString;

/// One thing responsible for link URL scheme on the site.
/// Should be used for every internal `<a href>`.
pub struct Urler {
    own_crate: Option<Origin>,
}

impl Urler {
    #[must_use] pub fn new(own_crate: Option<Origin>) -> Self {
        Self { own_crate }
    }

    /// Add scheme and domain if it hasn't have one yet. Not a safety mechanism.
    pub fn to_absolute<'a>(&self, url: impl Into<Cow<'a, str>>) -> Cow<'a, str> {
        fn to_abs(url: Cow<'_, str>) -> Cow<'_, str> {
            let prefix = if url.starts_with("//") {
                "https:"
            } else if url.starts_with('/') {
                "https://lib.rs"
            } else {
                return url;
            };
            Cow::Owned(match url {
                Cow::Owned(mut s) => {
                    s.insert_str(0, prefix);
                    s
                },
                Cow::Borrowed(b) => {
                    let mut out = String::with_capacity(prefix.len() + b.len());
                    out.push_str_in_cap(prefix);
                    out.push_str_in_cap(b);
                    out
                },
            })
        }
        to_abs(url.into())
    }

    /// Link to a dependency of a crate
    #[must_use]
    pub fn dependency(&self, dep: &RichDep) -> String {
        if let Some(git) = dep.dep.git() {
            if let Ok(repo) = Repo::new(git) {
                return match repo.host() {
                    Repo::GitHub(repo) => format!("/gh/{}/{}/{}", Encoded(&*repo.owner), Encoded(&*repo.repo), Encoded(&*dep.package)),
                    Repo::GitLab(repo) => format!("/lab/{}/{}/{}", Encoded(&*repo.owner), Encoded(&*repo.repo), Encoded(&*dep.package)),
                    _ => repo.canonical_http_url("", dep.dep.git_rev()),
                };
            }
        } else if dep.dep.detail().is_some_and(|d| d.path.is_some()) {
            if let Some(Origin::GitHub(r)) = &self.own_crate {
                return format!("/gh/{}/{}/{}", Encoded(&*r.repo.owner), Encoded(&*r.repo.repo), Encoded(&*dep.package));
            }
            if let Some(Origin::GitLab(r)) = &self.own_crate {
                return format!("/lab/{}/{}/{}", Encoded(&*r.repo.owner), Encoded(&*r.repo.repo), Encoded(&*dep.package));
            }
        }
        format!("/crates/{}", Encoded(&*dep.package))
    }

    /// Summary of all crate's features
    #[must_use]
    pub fn features(&self, origin: &Origin, feature_name: Option<&str>) -> String {
        let mut url = match origin {
            Origin::CratesIo(lowercase_name) => {
                format!("/crates/{}/features", Encoded::str(lowercase_name))
            },
            Origin::GitHub(r) => format!("https://deps.rs/repo/github/{}/{}", Encoded(&*r.repo.owner), Encoded(&*r.repo.repo)),
            Origin::GitLab(r) => format!("https://deps.rs/repo/gitlab/{}/{}", Encoded(&*r.repo.owner), Encoded(&*r.repo.repo)),
        };
        if let Some(f) = feature_name {
            use std::fmt::Write;
            let _ = write!(&mut url, "#feature-{}", Encoded::str(f));
        }
        url
    }

    #[must_use]
    pub fn install(&self, origin: &Origin) -> String {
        match origin {
            Origin::CratesIo(lowercase_name) => {
                format!("/install/{}", Encoded::str(lowercase_name))
            },
            Origin::GitHub(r) | Origin::GitLab(r) => {
                let host = if let Origin::GitHub { .. } = origin { "gh" } else { "lab" };
                format!("/install/{host}/{}/{}/{}", Encoded(&*r.repo.owner), Encoded(&*r.repo.repo), Encoded::str(&r.package))
            },
        }
    }

    #[must_use]
    pub fn all_versions(&self, origin: &Origin) -> Option<String> {
        match origin {
            Origin::CratesIo(lowercase_name) => {
                Some(format!("/crates/{}/versions", Encoded::str(lowercase_name)))
            }
            Origin::GitHub(_) | Origin::GitLab(_) => {
                // let host = if let Origin::GitHub { .. } = origin { "gh" } else { "lab" };
                // format!("/{}/{}/{}/{}/versions", host, Encoded(&*r.repo.owner), Encoded(&*repo.repo), Encoded::str(&package))
                None
            },
        }
    }

    #[must_use]
    pub fn reviews(&self, origin: &Origin) -> Option<String> {
        match origin {
            Origin::CratesIo(lowercase_name) => {
                Some(format!("/crates/{}/audit", Encoded::str(lowercase_name)))
            },
            _ => None,
        }
    }

    #[must_use]
    pub fn reverse_deps(&self, origin: &Origin) -> Option<String> {
        match origin {
            Origin::CratesIo(lowercase_name) => Some(format!("/crates/{}/rev", Encoded::str(lowercase_name))),
            Origin::GitHub { .. } | Origin::GitLab { .. } => None,
        }
    }

    #[must_use]
    pub fn crates_io_crate(&self, origin: &Origin) -> Option<String> {
        match origin {
            Origin::CratesIo(lowercase_name) => Some(self.crates_io_crate_by_lowercase_name(lowercase_name)),
            _ => None,
        }
    }

    #[must_use]
    pub fn crates_io_crate_at_version(&self, origin: &Origin, version: &str) -> Option<String> {
        match origin {
            Origin::CratesIo(lowercase_name) => Some(format!("https://crates.io/crates/{}/{}", Encoded::str(lowercase_name), Encoded(version))),
            _ => None,
        }
    }

    fn crates_io_crate_by_lowercase_name(&self, crate_name: &str) -> String {
        format!("https://crates.io/crates/{}", Encoded(crate_name))
    }

    #[must_use] pub fn docs_rs_source(&self, crate_name: &str, version: &str) -> String {
        format!("https://docs.rs/crate/{crate_name}/{version}/source/")
    }

    #[must_use]
    pub fn git_source(&self, origin: &Origin, version: &str) -> String {
        let mut url = self.crate_abs_path_by_origin(origin);

        // Bots crawl these links and I don't like it.
        // I'll make these temporary eventually.
        use std::hash::Hasher;
        let mut h = util::FxHasher::default();
        origin.hash(&mut h);
        version.hash(&mut h);
        let decoy = h.finish();

        use std::fmt::Write;
        let _ = write!(&mut url, "/source?at={}&nofollow={decoy:x}", Encoded(version));
        url
    }

    /// Link to crate individual page
    #[must_use] pub fn krate(&self, krate: &RichCrateVersion) -> String {
        self.crate_by_origin(krate.origin())
    }

    #[must_use]
    pub fn crate_by_origin(&self, o: &Origin) -> String {
        if self.own_crate.as_ref() != Some(o) {
            self.crate_abs_path_by_origin(o)
        } else {
            self.crates_io_crate_by_lowercase_name(o.package_name_icase())
        }
    }

    #[must_use]
    pub fn crate_abs_path_by_origin(&self, o: &Origin) -> String {
        match o {
            Origin::CratesIo(lowercase_name) => {
                format!("/crates/{}", Encoded::str(lowercase_name))
            },
            Origin::GitHub(r) => {
                format!("/gh/{}/{}/{}", Encoded(&*r.repo.owner), Encoded(&*r.repo.repo), Encoded::str(&r.package))
            },
            Origin::GitLab(r) => {
                format!("/lab/{}/{}/{}", Encoded(&*r.repo.owner), Encoded(&*r.repo.repo), Encoded::str(&r.package))
            },
        }
    }

    /// FIXME: it doesn't normalize keywords as well as the db inserter
    #[must_use] pub fn keyword(&self, name: &str) -> String {
        format!("/keywords/{}", Encoded(&name.to_kebab_case()))
    }

    /// First page of category listing
    #[must_use]
    pub fn category(&self, cat: &Category, sort_order: SortOrder) -> String {
        let mut out = String::with_capacity(1 + cat.slug.len() + if sort_order != SortOrder::Quality { 15 } else { 0 });
        for s in cat.slug.split("::") {
            out.push_ascii_in_cap(b'/');
            out.push_str_in_cap(s);
        }
        out.push_str_in_cap(match sort_order {
            SortOrder::Quality => "",
            SortOrder::Popularity => "?sort=popular",
            SortOrder::Recency => "?sort=new",
        });
        out
    }

    /// Crate author's URL
    ///
    /// This will probably change to a listing page rather than arbitrary personal URL
    #[must_use]
    pub fn author<'a>(&self, author: &'a CrateAuthor) -> Option<Cow<'a, str>> {
        if let Some(gh) = &author.github {
            Some(match (gh.user_type, author.owner) {
                (UserType::User, true) => self.crate_owner_by_github_login(&gh.login).into(),
                (UserType::User | UserType::Org | UserType::Bot, _) => format!("https://github.com/{}", Encoded(&gh.login)).into(),
            })
        } else if let Some(info) = &author.info {
            if let Some(em) = &info.email {
                Some(format!("mailto:{em}").into()) // add name? encode?
            } else if let Some(url) = &info.url {
                if url.starts_with("http://") || url.starts_with("https://") || url.starts_with("mailto:") {
                    Some(url.into())
                } else {
                    println!("bad info url: {author:?}");
                    None
                }
            } else {
                None
            }
        } else {
            None
        }
    }

    #[must_use] pub fn crate_owner_by_github_login(&self, login: &str) -> String {
        format!("/~{}", Encoded(login))
    }

    #[must_use] pub fn crate_maintainer_dashboard_by_github_login(&self, login: &str) -> String {
        format!("/~{}/dash", Encoded(login))
    }

    #[must_use] pub fn crate_maintainer_dashboard_github_login_atom_feed(&self, login: &str) -> String {
        format!("/~{}/dash.xml", Encoded(login))
    }

    #[must_use] pub fn search_crates_io(&self, query: &str) -> String {
        format!("https://crates.io/search?q={}", Encoded(query))
    }

    #[must_use] pub fn search_lib_rs(&self, query: &str) -> String {
        format!("/search?q={}", Encoded(query))
    }

    #[must_use] pub fn search_ddg(&self, query: &str) -> String {
        format!("https://duckduckgo.com/?q=site%3Alib.rs+{}", Encoded(query))
    }
}
