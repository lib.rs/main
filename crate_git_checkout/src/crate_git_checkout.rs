use util::error::IntoIoError;
pub use crate::iter::{HistoryIter, HistoryItem};
use cargo_toml::{Inheritable, Manifest, Package};
use git2::{AutotagOption, Blob, Commit, FetchOptions, Reference, Remote, RemoteCallbacks, RemoteUpdateFlags, RepositoryInitOptions};
pub use git2::{Oid, Repository, FileMode, ObjectType, Object, Tree, TreeEntry};
use lazy_static::lazy_static;
use log::{debug, error, info, warn};
use render_readme::Markup;
use repo_url::Repo;
use std::cmp::Reverse;
use std::collections::hash_map::Entry::Vacant;
use util::{FxHashMap as HashMap, FxHashSet as HashSet};
use std::collections::HashSet as StdHashSet;
use std::path::{Path, PathBuf};
use std::sync::atomic::AtomicU32;
use std::sync::atomic::Ordering::Acquire;
use std::sync::{Arc, Mutex};
use std::time::{Duration, Instant};
use std::{fs, io};
use util::{PushString, SmolStr};
pub use git2::Error as GitError;
pub use git2;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("Git {:?}/{:?}", _0.class(), _0.code())]
    Git(#[from] #[source] git2::Error),
    #[error("Cargo.toml")]
    Toml(#[from] #[source] cargo_toml::Error),
    #[error("Aborted")]
    Aborted,
    #[error("Forbidden URL")]
    ForbiddenURL,
    #[error("Broken repo: {0}")]
    BrokenRepo(&'static str),
    #[error("No disk space")]
    NoDiskSpace,
}

const HECK_NO: &[&str] = &[
    "http://www.test.com/",
    "https://git.tait.tech/tait/lunanode/",
    "http://www.coturnix.fr/darcs/ykcryptsetup", // crash
    "https://dev.spacekookie.de/kookie/nomicon/src/development/tools/cargo-workspace2", // crash
    "https://g.tylerm.dev/tylermurphy534/leak_memory", // crash
    "https://git.radial.gg/lily/lid", // crash
    "https://gitea.nebulanet.cc/Acrimon/actl", // crash
    "https://gitea.pep.foundation/pEp.foundation/pEpEngineSequoiaBackend", // crash
    "https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs/tree/master/gst-plugin", // crash
    "https://www.jeanzh.com/", // crash
    "https://github.com/aptos-labs/aptos-core.git",
    "https://github.com/groncoin/gringron.git",
    "https://gihtub.com/HumanEntity/wave_function_collapse.git",
    "https://github.com/jkristell/feather-f405.git",
    "https://github.com/paritytech/polkadot-sdk.git",
    "https://github.com/algesten/hreq.git",
    "https://github.com/arcnmx/ddc-rs.git",
    "https://github.com/azure/azure-sdk-for-rust.git",
    "https://github.com/facebook/hhvm.git",
    "https://github.com/fadedbee/turnstile.git",
    "https://github.com/gilnaa/ptsd.git",
    "https://github.com/github.com/clbexchange/certified-token-list.git",
    "https://github.com/github.com/supercolony-net/openbrush-contracts.git",
    "https://github.com/gwihlidal/meshopt-rs.git",
    "https://github.com/hyperledger/indy-sdk.git",
    "https://github.com/janonard/lv2rs-reservation.git",
    "https://github.com/khonsulabs/kludgine.git",
    "https://github.com/marmistrz/tokio-ctrlc-error.git",
    "https://github.com/mwatelescope/mwa_hyperdrive.git",
    "https://github.com/nical/lyon.git",
    "https://github.com/noostech/dose.git",
    "https://github.com/paomianzuis/rust-numerical-library.git",
    "https://github.com/parkuman/roggle.git",
    "https://github.com/sile/hls_m3u8.git",
    "https://github.com/trussed-dev/interchange.git",
    "https://github.com/wasmerio/ate.git",
    "https://github.com/yangkx-1024/mmkv.git",
];
const ALWAYS_SHALLOW: &[&str] = &[
    "https://github.com/null/decancer.git",
    "https://github.com/acheul/louvre.git",
    "https://github.com/aevyrie/bevy_mod_bounding.git",
    "https://github.com/alsa-project/hitaki-rs.git",
    "https://github.com/anarchy1n/c-arrow.git",
    "https://github.com/andrewdavidmackenzie/flow.git",
    "https://github.com/aneeshdurg/hostfile.git",
    "https://github.com/ascendingcreations/axumsessionsauth.git",
    "https://github.com/atsam-rs/atsam-pac.git",
    "https://github.com/atsamd-rs/atsamd.git",
    "https://github.com/attilio-oliva/converter-buddy.git",
    "https://github.com/awslabs/avp-local-agent.git",
    "https://github.com/awslabs/aws-sdk-rust.git",
    "https://github.com/azyobuzin/rust-oauthcli.git",
    "https://github.com/cyberbotics/webots.git",
    "https://github.com/bk-rs/golang-rs.git",
    "https://github.com/bpetit/rs-docker-sync.git",
    "https://github.com/bytecodealliance/wasmtime.git",
    "https://github.com/calyxir/calyx.git",
    "https://github.com/changecaps/ike.git",
    "https://github.com/cloudpeers/xbuild.git",
    "https://github.com/cognoscan/dynamic-lru-cache.git",
    "https://github.com/divad1196/github_submodule_hook.git",
    "https://github.com/donaldwhyte/double.git",
    "https://github.com/doumanash/os-sync.git",
    "https://github.com/efm32-rs/efm32gg-pacs.git",
    "https://github.com/embali/enimda-rs.git",
    "https://github.com/embarkstudios/physx-rs.git",
    "https://github.com/embarkstudios/rust-gpu.git",
    "https://github.com/entropyxyz/entropy-tss-placeholder.git",
    "https://github.com/erikbrinkman/gambit-parser-rs.git",
    "https://github.com/erikh/cpuburn.git",
    "https://github.com/facebook/hhvm.git",
    "https://github.com/get-eventually/eventually-rs.git",
    "https://github.com/get-eventually/eventually-rs.git",
    "https://github.com/gkkachi/firestore-grpc.git",
    "https://github.com/grantshandy/nominatim-rs.git",
    "https://github.com/hkalbasi/rust.git",
    "https://github.com/hyperchain/hyperchain.git",
    "https://github.com/inanna-malick/tracing-honeycomb.git",
    "https://github.com/iron-fish/ironfish.git",
    "https://github.com/justjanne/fcmlib.git",
    "https://github.com/kanidm/webauthn-rs.git",
    "https://github.com/katyo/msdfgen-rs.git",
    "https://github.com/khonsulabs/bonsaidb.git",
    "https://github.com/kuchiki-rs/kuchiki.git",
    "https://github.com/kuchiki-rs/kuchiki.git",
    "https://github.com/kuleuven-cosic/scale-mamba.git",
    "https://github.com/kylemayes/ktm5e-dice.git",
    "https://github.com/larsmbergvall/ray-debug.git",
    "https://github.com/letterlock/brr.git",
    "https://github.com/lumen/lumen.git",
    "https://github.com/materializeinc/materialize.git",
    "https://github.com/mattrob33/morph_gnt_rust.git",
    "https://github.com/maxall41/rustsasa.git",
    "https://github.com/mdsteele/rust-msi.git",
    "https://github.com/mechiru/google-api-proto.git",
    "https://github.com/mgree/ffs.git",
    "https://github.com/microsoft/windows-rs.git",
    "https://github.com/mmatvein/libliquidfun-sys.git",
    "https://github.com/narigo/keepass-diff.git",
    "https://github.com/nathan7/libfringe.git",
    "https://github.com/nervjs/taro.git",
    "https://github.com/nirklav/task_queue.git",
    "https://github.com/nokia/not-perf.git",
    "https://github.com/nuqlear/rmb.git",
    "https://github.com/oliver-giersch/closure.git",
    "https://github.com/open-flash/swf-tree.git",
    "https://github.com/openanolis/dragonball-sandbox.git",
    "https://github.com/paomianzuis/rust-numerical-library.git",
    "https://github.com/parallelchain-io/pchain-client-rust.git",
    "https://github.com/pepelzorro/ad9361-rs.git",
    "https://github.com/pylon-protocol/pylon-core-contracts.git",
    "https://github.com/race-game/race.git",
    "https://github.com/radialhuman/rust.git",
    "https://github.com/ramirezmike/not_snake_game.git",
    "https://github.com/ramirezmike/not_snake_game.git",
    "https://github.com/rconan/m2-ctrl.git",
    "https://github.com/reem/rust-co.git",
    "https://github.com/rerun-io/re_arrow2.git",
    "https://github.com/ruffle-rs/ruffle.git",
    "https://github.com/rusoto/rusoto.git",
    "https://github.com/rust-lang/rust.git",
    "https://github.com/sabinchitrakar/sd-rs.git",
    "https://github.com/sebadob/cryptr.git",
    "https://github.com/sebcrozet/nphysics.git",
    "https://github.com/second-state/sewup.git",
    "https://github.com/servo/servo.git",
    "https://github.com/shurizzle/tokio-switching-sleep.git",
    "https://github.com/siku2/rust-monaco.git",
    "https://github.com/singularity-data/risingwave.git",
    "https://github.com/singularity-data/risingwave.git",
    "https://github.com/smups/rustronomy-fits.git",
    "https://github.com/solana-labs/sealevel.git",
    "https://github.com/swc-project/swc.git",
    "https://github.com/taborkelly/csv2qr.git",
    "https://github.com/tailhook/vagga.git",
    "https://github.com/tetcoin/tetcore.git",
    "https://github.com/theawiteb/data2sound.git",
    "https://github.com/timleslie/sparse_graph.git",
    "https://github.com/tnahs/readstor.git",
    "https://github.com/tock/tock.git",
    "https://github.com/tree-sitter/tree-sitter-haskell.git",
    "https://github.com/truelayer/reqwest-middleware.git",
    "https://github.com/utherii/cargo-doc-l10n.git",
    "https://github.com/vandenheuvel/relp.git",
    "https://github.com/vaticle/typedb-protocol.git",
    "https://github.com/vaticle/typedb-protocol.git",
    "https://github.com/vosen/toml_document.git",
    "https://github.com/wasmedge/elasticsearch-rs-wasi.git",
    "https://github.com/ximea-gmbh/xiapi-sys.git",
    "https://github.com/yangkx-1024/mmkv.git",
    "https://github.com/yds12/tarsila.git",
];

mod iter;

lazy_static! {
    static ref GLOBAL_LOCK: Mutex<HashMap<String, Arc<Mutex<()>>>> = Mutex::new(HashMap::default());
}

#[derive(Debug, Clone)]
pub struct ParseError(pub String);

fn commit_history_iter<'a>(repo: &Repository, commit: &Reference<'a>) -> Result<HistoryIter<'a>, git2::Error> {
    if repo.is_shallow() {
        let mut origin = repo.find_remote("origin")?;
        let url = origin.url().unwrap_or_default();
        if !ALWAYS_SHALLOW.contains(&url) && !HECK_NO.contains(&url) {
            let _ = origin.fetch(&["HEAD"], None, None).inspect_err(|e| error!("unshallow master: {e}"));
        }
    }
    Ok(HistoryIter::new(commit.peel_to_commit()?))
}

pub struct ConnectedRemote<'r> {
    connection: Remote<'r>,
}

impl ConnectedRemote<'static> {
    pub fn new_detached(repo_url: &Repo) -> Result<Self, Error> {
        let url = checked_repo_url(repo_url)?;
        let mut connection = Remote::create_detached(url)?;
        // Connect to the remote and download the list of references
        connection.connect(git2::Direction::Fetch)?;

        Ok(Self {
            connection
        })
    }
}

impl<'r> ConnectedRemote<'r> {
    pub fn new(repository: &'r Repository, origin_name: &str) -> Result<Self, Error> {
        let mut connection = repository.find_remote(origin_name)?;
        connection.connect(git2::Direction::Fetch)?;
        Ok(Self {
            connection,
        })
    }

    pub fn ls(&self) -> Result<Vec<(SmolStr, [u8; 20])>, Error> {
        let references = self.connection.list()?;

        Ok(references.iter().filter_map(|r| {
            let name = r.name();
            if let Some(("pulls" | "pull" | "pipelines" | "reviewable" | "merge-requests", _)) = name.strip_prefix("refs/").and_then(|rest| rest.split_once('/')) {
                return None;
            }
            let hash = r.oid().as_bytes().try_into().ok()?;
            Some((name.into(), hash))
        }).take(2500).collect())
    }

    pub fn default_branch(&self) -> Result<SmolStr, Error> {
        let default_branch = self.connection.default_branch()?;
        let default_branch = default_branch.as_str().ok_or(Error::BrokenRepo("default branch is not a string"))?;
        Ok(default_branch.into())
    }

    pub fn url(&self) -> Result<&str, Error> {
        self.connection.url().ok_or(Error::BrokenRepo("no URL on git connection"))
    }
}


/// True if cloned, false if reused
pub fn clone_repository(repo_url: &Repo, base_path: &Path, opts: &CloneOptions, stop: &AtomicU32) -> Result<Repository, Error> {
    let (git_repo, is_empty) = open_repository(repo_url, base_path)?;

    if stop.load(Acquire) > 0 {
        return Err(Error::Aborted);
    }

    if is_empty || opts.update || git_repo.head().and_then(|h| h.peel_to_tree()).inspect_err(|e| warn!("{repo_url:?}: needs cloning again {e}")).is_err() {
        let repo_path = git_repo.path();
        let mut remote = ConnectedRemote::new(&git_repo, "origin")?;
        fetch_repository(&mut remote, repo_path, opts, stop)
            .inspect_err(|e| if let Error::BrokenRepo(cause) = e {
                error!("Deleting repo {cause}");
                let _ = fs::remove_dir_all(repo_path);
            } else {
                info!("Not deleting the repo, will try to recover: {e}");
            })?;

        if is_empty && git_repo.head().is_err() {
            warn!("Had to reset to branch {}", remote.default_branch().as_deref().unwrap_or("???"));
            git_repo.set_head(&format!("refs/remotes/origin/{}", remote.default_branch()?))?;
        }
    }
    Ok(git_repo)
}

pub fn fetch_repository(remote: &mut ConnectedRemote<'_>, repo_path: &Path, opts: &CloneOptions, stop: &AtomicU32) -> Result<(), Error> {
    let url = remote.url()?;

    if HECK_NO.contains(&url) {
        return Err(Error::ForbiddenURL);
    }

    let lock = GLOBAL_LOCK.lock().unwrap().entry(url.into()).or_default().clone();
    let _lock = lock.lock().unwrap();

    if stop.load(Acquire) > 0 {
        return Err(Error::Aborted);
    }

    let mut shallow = opts.shallow_depth;
    if shallow.is_none() && opts.avoid_heavy_repos && ALWAYS_SHALLOW.contains(&url) {
        shallow = Some(64);
    }

    let mut cbs = RemoteCallbacks::default();
    let deadline = opts.deadline;
    cbs.sideband_progress(move |_| {
        let timed_out = stop.load(Acquire) != 0 || Instant::now() > deadline;
        if timed_out {
            warn!("Repo fetch timed out, aborting git2");
        }
        !timed_out
    });
    cbs.transfer_progress(move |_| {
        let timed_out = stop.load(Acquire) != 0 || Instant::now() > deadline;
        if timed_out {
            warn!("Repo fetch timed out, aborting git2");
        }
        !timed_out
    });

    let mut f = FetchOptions::default();
    f.remote_callbacks(cbs);

    if let Some(d) = shallow {
        f.prune(git2::FetchPrune::Off);
        f.depth(d as _);
    }

    f.download_tags(if shallow.is_none() && opts.fetch_shas.is_empty() {
        AutotagOption::Auto
    } else {
        AutotagOption::None
    });

    let mut refspecs = vec![];

    for tag in &opts.fetch_tags {
        refspecs.push(format!("+refs/tags/{tag}:refs/tags/{tag}"));
    }
    for sha in &opts.fetch_shas {
        refspecs.push(format!("+{}:refs/remotes/origin/hash/{}", hex::encode(sha), hex::encode(&sha[..5])));
    }
    if refspecs.is_empty() {
        let default_refs = format!("+{def}:{def}", def = remote.default_branch()?);
        debug!("Fetching fallback ref {default_refs}");
        refspecs.push(default_refs);
    }

    if stop.load(Acquire) > 0 {
        return Err(Error::Aborted);
    }

    debug!("Fetching {refspecs:?} from {url}");
    let res = remote.connection.fetch(&refspecs, Some(&mut f), None).or_else(|e| {
        warn!("Fetch of {refspecs:?} is failing {e}");
        let mut retry = false;
        if e.class() == git2::ErrorClass::Net && (e.message() == "unexpected packet type" || e.message().starts_with("error parsing")) {
            refspecs.retain(|s| !s.contains("/hash/"));
            if refspecs.is_empty() {
                refspecs.push("HEAD".into());
            }
            retry = true;
        } else if e.class() == git2::ErrorClass::Odb {
            if let Some((oid, _)) = e.message().strip_prefix("object not found - no match for id (").and_then(|s| s.split_once(')')) {
                refspecs.clear();
                refspecs.push(format!("+{oid}:refs/remotes/origin/hash/{oid}"));
                refspecs.push("HEAD".into());
                retry = true;
            }
        }
        if retry {
            let mut git = std::process::Command::new("git");
            git.current_dir(repo_path)
                .args(["fetch", "--unshallow", "--no-recurse-submodules",
                    "--force", "--filter=blob:limit=2m", "--keep", "--no-auto-gc", "--update-head-ok", "origin"])
                .args(&refspecs);
            warn!("Retrying due to {e}: {refspecs:#?} {git:?}");
            let s = git.status();
            warn!("Git fetch fallback: {} {s:?}", s.as_ref().is_ok_and(|s| s.success()));
            match remote.connection.fetch(&refspecs, Some(&mut f), None).map_err(Error::from)
                .and_then(|_| remote.default_branch()) {
                Err(e) => {
                    error!("Unfixable: {e} {e:#?}");
                    let _ = fs::remove_dir_all(repo_path);
                    Err(Error::BrokenRepo("protocol error, missing OIDs or HEAD"))
                },
                Ok(branch) => {
                    debug!("Fetched repo default={branch} okay");
                    Ok(())
                },
            }
        } else {
            Err(e.into())
        }
    });
    if let Err(e) = res {
        error!("Git error: {e} {e:?};");
        // this is due to wrong sha
        return Err(e);
    }
    let _ = remote.connection.update_tips(None, RemoteUpdateFlags::UPDATE_FETCHHEAD, AutotagOption::None, None)
        .inspect_err(|e| warn!("update tips err: {e}"));

    Ok(())
}

/// at: tree, commit
#[inline]
pub fn iter_blobs<E, F>(repo: &Repository, try_start_path: Option<&str>, cb: F) -> Result<(), E>
    where F: FnMut(&str, &Tree<'_>, &str, Blob<'_>) -> Result<(), E>, E: From<Error>
{
    let head = repo_head(repo).map_err(Error::from)?;
    let tree = head.peel_to_tree().map_err(Error::from)?;
    iter_blobs_in_tree(repo, &tree, try_start_path, cb)
}

#[inline]
fn iter_blobs_in_tree<E, F>(repo: &Repository, root_tree: &Tree<'_>, try_start_path: Option<&str>, mut cb: F) -> Result<(), E>
    where F: FnMut(&str, &Tree<'_>, &str, Blob<'_>) -> Result<(), E>, E: From<Error>
{
    let new_tree;
    let mut tree = root_tree;
    let mut path = String::with_capacity(500);
    if let Some(p) = try_start_path {
        new_tree = tree.get_path(p.as_ref()).and_then(|e| e.to_object(repo)).and_then(|o| o.peel_to_tree());
        if let Ok(new_tree) = &new_tree {
            tree = new_tree;
            path.push_str(p);
        }
    }
    iter_blobs_recurse(repo, tree, &mut path, &mut cb)?;
    Ok(())
}

fn iter_blobs_recurse<E, F>(repo: &Repository, tree: &Tree<'_>, path: &mut String, cb: &mut F) -> Result<(), E>
    where F: FnMut(&str, &Tree<'_>, &str, Blob<'_>) -> Result<(), E>, E: From<Error>
{
    for i in tree {
        let Some(name) = i.name() else { continue };
        match i.kind() {
            Some(ObjectType::Tree) => {
                let sub = i.to_object(repo).and_then(|o| o.peel_to_tree()).map_err(Error::from)?;
                let pre_len = path.len();
                path.reserve(name.len()+1);
                if !path.is_empty() {
                    path.push_ascii_in_cap(b'/');
                }
                path.push_str_in_cap(name);
                iter_blobs_recurse(repo, &sub, path, cb)?;
                path.truncate(pre_len);
            },
            Some(ObjectType::Blob) => {
                // symlink?
                if (i.filemode() & 0o0120000) == 0o0120000 {
                    continue;
                }
                cb(path, tree, name, i.to_object(repo).and_then(|o| o.peel_to_blob()).map_err(Error::from)?)?;
            },
            _ => {},
        }
    }
    Ok(())
}

pub struct CloneOptions {
    pub shallow_depth: Option<u32>,
    pub avoid_heavy_repos: bool,
    pub deadline: Instant,
    pub fetch_tags: Vec<String>,
    pub fetch_shas: Vec<[u8; 20]>,
    pub update: bool,
}

impl CloneOptions {
    #[must_use] pub fn shallow(shallow: bool) -> Self {
        Self {
            shallow_depth: shallow.then_some(64),
            avoid_heavy_repos: true,
            deadline: Instant::now() + Duration::from_secs(60),
            fetch_tags: vec![],
            fetch_shas: vec![],
            update: false,
        }
    }
}

impl Default for CloneOptions {
    fn default() -> Self {
        Self::shallow(false)
    }
}

fn checked_repo_url(repo_url: &Repo) -> Result<String, Error> {
    let url = repo_url.canonical_git_url();
    if !url.starts_with("http://") && !url.starts_with("https://") && !url.starts_with("git@github.com:") {
        warn!("Rejecting non-HTTP git URL: {url}");
        return Err(Error::ForbiddenURL);
    }
    Ok(url)
}

fn repo_access_lock(repo: &Repo) -> Result<Arc<Mutex<()>>, Error> {
    let url = checked_repo_url(repo)?;
    Ok(GLOBAL_LOCK.lock().unwrap().entry(url).or_default().clone())
}

/// True if newly created with nothing in it
pub fn open_repository(repo_url: &Repo, base_path: &Path) -> Result<(Repository, bool), Error> {
    if let Ok(size) = fs4::available_space(base_path) {
        let free_mib = size / (1024 * 1024);
        if free_mib < 3000 {
            error!("Refusing to git clone due to lack of free disk space {free_mib}MB on {}", base_path.display());
            return Err(Error::NoDiskSpace)
        }
    }

    // ensure one clone per dir at a time
    let lock = repo_access_lock(repo_url)?;
    let _lock = if let Ok(l) = lock.try_lock() { l } else {
        info!("Alredy busy with checkout of {repo_url:?}");
        lock.lock().unwrap()
    };

    let url = &*checked_repo_url(repo_url)?;

    if HECK_NO.contains(&url) {
        return Err(Error::ForbiddenURL);
    }

    let repo_path = base_path.join(&*urlencoding::encode(url));

    if let Ok(repo) = Repository::open(&repo_path) {
        let repo_looks_okay = check_origin_url(&repo, url) || {
            if repo.remote("origin", url).is_err() {
                repo.remote_set_url("origin", url)?;
            }
            check_origin_url(&repo, url) && repo_head_commit(&repo).is_ok_and(|h| h.tree().is_ok())
        };
        if repo_looks_okay {
            return Ok((repo, false))
        } else {
            warn!("Repo at {} is not okay", repo_path.display());
        }
    }

    let mut init_opts = RepositoryInitOptions::new();
    init_opts.bare(true)
        .no_reinit(true)
        .external_template(false)
        .origin_url(url);

    let _ = fs::remove_dir_all(&repo_path);
    let repo = Repository::init_opts(repo_path, &init_opts)?;
    Ok((repo, true))
}

fn check_origin_url(repo: &Repository, url: &str) -> bool {
    repo.find_remote("origin").ok().as_ref().and_then(|r| r.url()).is_some_and(|origin_url| origin_url == url)
}

/// Returns (path, Tree Oid, Cargo.toml)
pub fn find_manifests(repo: &Repository, stop: &AtomicU32) -> Result<(Vec<FoundManifest>, Vec<ParseError>), Error> {
    let commit = repo_head_commit(repo)?;
    find_manifests_in_tree(repo, &commit, None, stop)
}

pub struct GitFS<'a, 'repo> {
    pub current_dir: &'a Path,
    pub repo: &'a Repository,
    pub root: &'a Tree<'repo>,
}

pub fn join_path(base: impl Into<PathBuf>, rel_path: impl AsRef<Path>) -> Option<PathBuf> {
    let mut path = base.into();
    let mut rest = rel_path.as_ref().components();
    while let Some(c) = rest.next() {
        match c {
            std::path::Component::Prefix(_) |
            std::path::Component::RootDir => return None, // absolute paths for workspace dir are not portable
            std::path::Component::CurDir => continue,
            std::path::Component::ParentDir => { path.pop(); }, // Rust std has no normalize()
            std::path::Component::Normal(sub) => {
                path.try_reserve(rest.as_path().as_os_str().len()).ok()?;
                path.push(sub);
            },
        }
    }
    Some(path)
}

impl GitFS<'_, '_> {
    fn abs_path(&self, rel_path: &str) -> Option<PathBuf> {
        join_path(self.current_dir, rel_path)
    }

    fn root_get_path(&self, path: &Path) -> io::Result<Object<'_>> {
        let git_repo_path = if path.has_root() {
            let mut iter = path.components();
            iter.next(); // skip '/'
            iter.as_path()
        } else {
            path
        };
        if git_repo_path != Path::new("") { // get_path doesn't allow ""
            self.root.get_path(git_repo_path)
                .map_err(|e| io::ErrorKind::NotFound.into_io_error_with(e))?
                .to_object(self.repo).map_err(|e| io::ErrorKind::InvalidData.into_io_error_with(e))
        } else {
            self.repo.find_object(self.root.id(), Some(ObjectType::Tree))
                .map_err(|e| io::ErrorKind::InvalidData.into_io_error_with(e))
        }
    }
}

impl cargo_toml::AbstractFilesystem for GitFS<'_, '_> {
    fn file_names_in(&self, dir_path: &str) -> Result<StdHashSet<Box<str>>, io::Error> {
        let path = self.abs_path(dir_path).ok_or(io::ErrorKind::InvalidInput)?;
        let tree = self.root_get_path(&path)?.peel_to_tree()
            .map_err(|e| io::ErrorKind::InvalidData.into_io_error_with(e))?;

        let res = tree.into_iter().filter_map(|entry| {
            Some(entry.name()?.into())
        }).collect::<StdHashSet<_>>();
        Ok(res)
    }

    fn parse_root_workspace(&self, rel_path_hint: Option<&str>) -> Result<(Manifest<cargo_toml::Value>, PathBuf), cargo_toml::Error> {
        if let Some(rel_path) = rel_path_hint {
            let mut path = self.abs_path(rel_path).ok_or(cargo_toml::Error::Other("bad path"))?;
            path.push("Cargo.toml");
            let blob = self.root_get_path(&path)?.peel_to_blob()
                .map_err(|e| io::ErrorKind::InvalidData.into_io_error_with(e))?;
            return Ok((Manifest::from_slice(blob.content())?, path));
        }
        // 8 because it's quadratic, and I can't be bothered to cache the parent dirs
        self.current_dir.ancestors().take(8).find_map(|dir| {
            let path = dir.join("Cargo.toml");
            let blob = self.root_get_path(&path).ok()?.peel_to_blob().ok()?;
            let m = Manifest::from_slice(blob.content()).ok()?;
            m.workspace.is_some().then_some((m, path))
        }).ok_or_else(|| {
            io::Error::new(io::ErrorKind::NotFound, format!("Can't find workspace in {}/..", self.current_dir.display())).into()
        })
    }
}

pub struct FoundManifest {
    pub inner_path: String,
    pub tree: Oid,
    pub commit: Oid,
    pub manifest: Manifest,
}

/// Path, tree Oid, parsed TOML
pub fn find_manifests_in_tree(repo: &Repository, commit: &Commit<'_>, try_start_path: Option<&str>, stop: &AtomicU32) -> Result<(Vec<FoundManifest>, Vec<ParseError>), Error> {
    let start_tree = commit.tree()?;
    let commit_id = commit.id();
    let mut tomls = Vec::with_capacity(8);
    let mut warnings = Vec::new();
    iter_blobs_in_tree::<Error, _>(repo, &start_tree, try_start_path, |inner_dir_path, inner_tree, name, blob| {
        if stop.load(std::sync::atomic::Ordering::Relaxed) > 0 {
            return Err(Error::Aborted);
        }

        if name == "Cargo.toml" {
            let blob_content = blob.content();
            match Manifest::from_slice(blob_content) {
                Ok(mut toml) => {
                    if toml.package.is_some() {
                        let ws = toml.complete_from_abstract_filesystem::<(), _>(GitFS {
                            current_dir: Path::new(inner_dir_path),
                            repo,
                            root: &start_tree
                        }, None);
                        if let Err(err) = ws {
                            warnings.push(ParseError(format!("Broken workspace: {err} in {}", repo.path().display())));
                        }
                        tomls.push(FoundManifest {
                            inner_path: inner_dir_path.to_owned(),
                            tree: inner_tree.id(),
                            commit: commit_id,
                            manifest: toml,
                        });
                    }
                },
                Err(err) => {
                    warnings.push(ParseError(format!("Can't parse {}/{inner_dir_path}/{name}: {err}", repo.path().display())));
                },
            }
        }
        Ok(())
    })?;
    Ok((tomls, warnings))
}

pub fn path_in_repo(repo: &Repository, crate_name: &str, stop: &AtomicU32) -> Result<Option<FoundManifest>, Error> {
    let commit = repo_head_commit(repo)?;
    path_in_repo_in_tree(repo, &commit, crate_name, stop)
}

fn path_in_repo_in_tree(repo: &Repository, commit: &Commit<'_>, crate_name: &str, stop: &AtomicU32) -> Result<Option<FoundManifest>, Error> {
    Ok(find_manifests_in_tree(repo, commit, None, stop)?.0
        .into_iter()
        .find(|f| f.manifest.package.as_ref().is_some_and(|p| p.name == crate_name)))
}

#[derive(Debug, Copy, Clone, Default)]
struct State {
    since: Option<usize>,
}

#[derive(Debug, Clone)]
pub struct PackageVersionTimestamp {
    pub timestamp: i64,
    pub commit: [u8; 20],
    pub inner_path: String,
}

pub type PackageVersionTimestamps = HashMap<String, HashMap<String, PackageVersionTimestamp>>;

pub fn local_tags<'r>(repo: &'r Repository, package_name: &str) -> Result<Vec<(SmolStr, Result<Commit<'r>, Error>)>, Error> {
    let tags = repo.tag_names(None)?;
    let mut tags: Vec<_> = tags.iter().flatten().collect();
    // prefer v1.2.3 tags, avoid plain names
    tags.sort_by_key(|&tag_name| {
        let (has_prefix, name) = if let Some(n) = tag_name.strip_prefix(package_name) {
            (true, n.trim_start_matches(|c: char| c.is_ascii_punctuation()))
        } else {
            (false, tag_name)
        };
        Reverse((has_prefix, name.starts_with('v'), name.contains(|c: char| c.is_ascii_digit()), name))
    });

    Ok(tags.into_iter().map(|tag| {
        let res = repo.find_reference(&format!("refs/tags/{tag}")).and_then(|r| r.peel_to_commit()).map_err(From::from);
        (SmolStr::from(tag), res)
    }).collect())
}

pub fn find_versions(repo: &Repository, package: &str, stop: &AtomicU32) -> Result<PackageVersionTimestamps, Error> {
    let mut package_versions: PackageVersionTimestamps = HashMap::with_capacity_and_hasher(16, Default::default());
    for (_tag, commit) in local_tags(repo, package)?.into_iter().filter(|(t, _)| t.chars().any(|c| c.is_ascii_digit())).take(100) {
        let Ok(commit) = commit else { continue };

        for f in find_manifests_in_tree(repo, &commit, None, stop)?.0 {
            if let Some(pkg) = f.manifest.package {
                if pkg.name == package {
                    add_package(&mut package_versions, pkg, f.inner_path, &commit);
                }
            }
        }
    }

    info!("no tags, falling back to slow versions");
    if package_versions.is_empty() {
        return find_dependency_changes(repo, |_, _, _| {}, stop);
    }

    Ok(package_versions)
}

fn is_alnum(q: &str) -> bool {
    q.as_bytes().iter().copied().all(|c| c.is_ascii_alphanumeric() || c == b'_' || c == b'-')
}

fn add_package(package_versions: &mut PackageVersionTimestamps, pkg: Package, inner_path: String, commit: &Commit<'_>) {
    if pkg.name.is_empty() || !is_alnum(&pkg.name) {
        info!("bad crate name {}", pkg.name);
        return;
    }
    let Inheritable::Set(version) = pkg.version else {
        return;
    };

    // Find oldest occurence of each version, assuming it's a release date
    let timestamp = commit.time().seconds();
    let commit = commit.id().as_bytes().try_into().unwrap();
    #[allow(deprecated)]
    let ver_time = package_versions.entry(pkg.name).or_default()
        .entry(version).or_insert(PackageVersionTimestamp {
            timestamp,
            commit,
            inner_path: inner_path.clone(),
        });
    if ver_time.timestamp > timestamp {
        ver_time.timestamp = timestamp;
        ver_time.commit = commit;
        ver_time.inner_path = inner_path;
    }
}

fn repo_head_commit(repo: &Repository) -> Result<Commit<'_>, Error> {
    Ok(repo_head(repo)?.peel_to_commit()?)
}

fn repo_head(repo: &Repository) -> Result<Reference<'_>, Error> {
    match repo.head() {
        Ok(head) => return Ok(head),
        Err(e) => {
            warn!("repo without head? {e} in {}", repo.path().display());
            for r in ["refs/remotes/origin/HEAD", "HEAD", "FETCH_HEAD", "refs/remotes/origin/main", "refs/heads/main", "refs/heads/master"] {
                if let Ok(found) = repo.find_reference(r) {
                    info!("Had to fall back to {r} in {}", repo.path().display());
                    return Ok(found);
                }
            }
            Err(e.into())
        },
    }
}

/// Callback gets added, removed, number of commits ago.
pub fn find_dependency_changes(repo: &Repository, mut cb: impl FnMut(HashSet<String>, HashSet<String>, usize), stop: &AtomicU32) -> Result<PackageVersionTimestamps, Error> {
    let head = repo_head(repo)?;

    let mut newer_deps: HashMap<String, State> = HashMap::with_capacity_and_hasher(100, Default::default());
    let mut package_versions: PackageVersionTimestamps = HashMap::with_capacity_and_hasher(4, Default::default());

    // iterates from the latest!
    // The generation number here is not quite accurate (due to diamond-shaped histories),
    // but I need the fiction of it being linerar for this implementation.
    // A recursive implementation could do it better, maybe.
    let commits = commit_history_iter(repo, &head)?.filter(|c| !c.is_merge).map(|c| c.commit);
    for (age, commit) in commits.enumerate().take(1000) {
        // All deps in a repo, because we traverse history once per repo, not once per crate,
        // and because moving of deps between internal crates doesn't count.
        let mut older_deps = HashSet::with_capacity_and_hasher(100, Default::default());
        for f in find_manifests_in_tree(repo, &commit, None, stop)?.0 {
            // Find oldest occurence of each version, assuming it's a release date
            if let Some(pkg) = f.manifest.package {
                add_package(&mut package_versions, pkg, f.inner_path, &commit);
            }

            older_deps.extend(f.manifest.dependencies.into_keys());
            older_deps.extend(f.manifest.dev_dependencies.into_keys());
            older_deps.extend(f.manifest.build_dependencies.into_keys());
        }

        let mut added = HashSet::with_capacity_and_hasher(10, Default::default());
        let mut removed = HashSet::with_capacity_and_hasher(10, Default::default());

        for (dep, state) in &mut newer_deps {
            // if it's Some(), it's going to be added in the future! so it's not there now
            // (as a side effect if dependency is added, removed, then re-added, it tracks only the most recent add/remove)
            if state.since.is_none() && !older_deps.contains(dep) {
                added.insert(dep.clone());
                state.since = Some(age);
            }
        }

        for dep in older_deps {
            if let Vacant(e) = newer_deps.entry(dep) {
                if age > 0 {
                    removed.insert(e.key().clone());
                    e.insert(State { since: None }); // until: Some(age)
                } else {
                    e.insert(State::default());
                }
            }
        }

        cb(added, removed, age);
    }
    Ok(package_versions)
}

// FIXME: buggy, barely works
pub fn find_readme(repo: &Repository, package: &Package, stop: &AtomicU32) -> Result<Option<(PathBuf, Markup, bool, Option<[u8; 20]>)>, Error> {
    let commit = repo_head_commit(repo)?;
    let commit_id = commit.id();
    let mut readme = None;
    let mut found_best = false; // it'll find many readmes, including fallbacks

    let f = path_in_repo_in_tree(repo, &commit, &package.name, stop)?;
    let prefix = Path::new(f.as_ref().map_or("", |f| f.inner_path.as_str()));
    debug!("repo prefix of {} is {} [{commit_id}]", package.name, prefix.display());

    iter_blobs_in_tree::<Error, _>(repo, &commit.tree()?, None, |base, _inner_tree, name, blob| {
        if stop.load(std::sync::atomic::Ordering::Relaxed) > 0 {
            return Err(Error::Aborted);
        }
        if found_best {
            return Ok(()); // done
        }

        let base = Path::new(base);
        let stripped = base.strip_prefix(prefix).ok();
        let is_correct_dir = stripped.is_some();
        let rel_path = if let Some(stripped) = stripped {
            stripped
        } else if readme.is_none() {
            base
        } else {
            return Ok(()); // don't search bad dirs if there's some readme already
        };
        let rel_path_name = rel_path.join(name);
        if is_readme_filename(&rel_path_name, Some(package), is_correct_dir) {
            let text = String::from_utf8_lossy(blob.content()).into_owned();
            if text.trim_start().is_empty() {
                debug!("found readme, but it's an empty file? {}", rel_path_name.display());
                return Ok(()); // skip
            }
            let ext = rel_path_name.extension().unwrap_or("".as_ref());
            let markup = if ext == "rst" {
                Markup::Rst(text)
            } else if ext == "adoc" || ext == "asciidoc" {
                Markup::AsciiDoc(text)
            } else {
                Markup::Markdown(text)
            };
            debug!("found a readme in repo at {} (best={is_correct_dir})", rel_path_name.display());
            readme = Some((base.to_owned(), markup, is_correct_dir, commit_id.as_bytes().try_into().ok()));
            found_best = is_correct_dir;
        }
        Ok(())
    })?;
    Ok(readme)
}

/// Check if given filename is a README. If `package` is missing, guess.
fn is_readme_filename(path: &Path, package: Option<&Package>, is_correct_dir: bool) -> bool {
    let Some(s) = path.to_str() else { return false };
    let package_specified_name = package.map(|p| p.readme()).and_then(|p| p.as_path()).and_then(|p| p.to_str());

    if let Some(r) = package_specified_name {
        let r = r.trim_start_matches('.').trim_start_matches('/'); // hacky hack for ../readme
        if r.eq_ignore_ascii_case(s) { // crates published on Mac have this
            return true;
        }
        // if the package specified a file name, and it isn't there, then don't fall back to something else
        if is_correct_dir {
            return false;
        }
    }
    render_readme::is_readme_filename(path)
}

#[test]
fn join_path_test() {
    assert_eq!(join_path("foo/bar", "../quz").unwrap().to_str().unwrap(), "foo/quz");
}

#[test]
fn git_fs() {
    let _ = env_logger::try_init();

    let repo = Repository::open("../.git").expect("own git repo");
    let (m, w) = find_manifests(&repo, &AtomicU32::new(0)).expect("has manifests");
    assert_eq!(0, w.len());
    assert_eq!(33, m.len());
}
