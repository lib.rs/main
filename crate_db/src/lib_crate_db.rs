use util::FxHashMap as HashMap;
use chrono::prelude::*;
use feat_extractor::stopwords::{COND_STOPWORDS, KEYWORD_STOPWORDS as STOPWORDS};
use log::{debug, error, info, warn};
use once_cell::sync::OnceCell;
use rich_crate::{CrateOwner, CrateVersionSourceData, Manifest, ManifestExt, Markup, Origin, Readme, Repo, RichCrate};
use rusqlite::*;
use std::borrow::Cow;
use std::fmt::Write;
use std::path::Path;
use std::time::Duration;
use parking_lot::{Mutex, MutexGuard};
use util::{CowAscii, SmolStr};

type FResult<T, E = Error> = std::result::Result<T, E>;

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("DB sqlite error?")]
    LezyDbErrorWithoutContext(#[source] #[from] rusqlite::Error),

    #[error("DB sqlite error in {1}")]
    Db(#[source] rusqlite::Error, &'static str),

    #[error("DB I/O error")]
    Io(#[source] #[from] std::io::Error),

    #[error("{0}")]
    Other(String),
}

pub mod builddb;

mod schema;


pub struct CrateDb {
    url: String,
    conn: Mutex<Option<Connection>>,
    all_explicit_keywords_cache: OnceCell<HashMap<SmolStr, u32>>,
}

pub struct CrateVersionData<'a> {
    pub origin: &'a Origin,
    pub source_data: &'a CrateVersionSourceData,
    pub manifest: &'a Manifest,
    pub deps_stats: &'a [(&'a str, f32)],
    pub is_build: bool,
    pub is_dev: bool,
    pub is_spam_or_hidden: bool,
    pub authors: &'a [rich_crate::Author],
    pub category_slugs: &'a [Cow<'a, str>],
    pub repository: Option<&'a Repo>,
    pub visible_auto_keywords: &'a [(f32, SmolStr)],
    pub invisible_auto_keywords: &'a [(f32, SmolStr)],
    pub cache_key: u64,
}

/// Metadata guessed
pub struct DbDerived {
    pub categories: Vec<SmolStr>,
}

pub struct CrateOwnerStat {
    pub github_id: u64,
    pub created_at: (u16, u8, u8),
    pub num_crates: u32,
}

impl CrateDb {
    pub fn new(path: &Path) -> FResult<Self> {
        Ok(Self {
            url: format!("file:{}?cache=shared", path.display()),
            conn: Mutex::new(None),
            all_explicit_keywords_cache: OnceCell::new(),
        })
    }

    #[inline(never)]
    fn get_conn(&self, context: &str) -> FResult<MutexGuard<'_, Option<Connection>>> {
        let mut conn = self.conn.try_lock_for(Duration::from_secs(15))
            .ok_or_else(|| Error::Other(format!("sqlite too busy for {context}")))?;
        if conn.is_none() {
            *conn = Some(blocking::block_in_place(format_args!("conn {context}"), || {
                Self::db(&self.url)
            })?);
        }
        Ok(conn)
    }

    #[inline]
    fn with_read<F, T>(&self, context: &'static str, cb: F) -> FResult<T> where F: FnOnce(&mut Connection) -> FResult<T> {
        let mut conn = self.get_conn(context)?;
        let conn = conn.as_mut().ok_or_else(|| Error::Other(String::new()))?; // can't happen
        blocking::block_in_place(format_args!("read {context}"), move || {
            let now = std::time::Instant::now();
            let res = cb(conn);
            let elapsed = now.elapsed();
            if elapsed > Duration::from_secs(1) {
                warn!("{context} read callback took {}ms", elapsed.as_millis() as u32);
            }
            res
        })
    }

    #[inline]
    fn with_write<F, T>(&self, context: &'static str, cb: F) -> FResult<T> where F: FnOnce(&Connection) -> FResult<T> {
        let mut conn = self.get_conn(context)?;
        let conn = conn.as_mut().ok_or_else(|| Error::Other(String::new()))?; // can't happen
        blocking::block_in_place(format_args!("write {context}"), move || {
            let tx = conn.transaction_with_behavior(TransactionBehavior::Immediate)
                .map_err(|e| Error::Db(e, "w-tx"))?;
            let now = std::time::Instant::now();
            let res = cb(&tx)?;
            tx.commit().map_err(|e| Error::Db(e, context))?;
            let elapsed = now.elapsed();
            if elapsed > Duration::from_secs(2) {
                warn!("{context} write callback took {}ms", elapsed.as_millis() as u32);
            }
            Ok(res)
        })
    }

    #[inline]
    pub fn latest_crate_update_timestamp(&self) -> FResult<Option<u32>> {
        self.with_read("latest_crate_update_timestamp", |conn| {
            Ok(none_rows(conn.query_row("SELECT max(created) FROM crate_versions", [], |row| row.get(0)))?)
        })
    }

    pub fn crate_versions(&self, origin: &Origin) -> FResult<Vec<(SmolStr, u32)>> {
        let origin = origin.to_str();
        self.with_read("crate_versions", move |conn| {
            let mut q = conn.prepare_cached("SELECT v.version, v.created FROM crates c JOIN crate_versions v ON v.crate_id = c.id WHERE c.origin = ?1")
                .map_err(|e| Error::Db(e, "cv prep"))?;
            let res = q.query_map([&origin], |row| {
                Ok((row.get_ref(0)?.as_str()?.into(), row.get(1)?))
            })?;
            Ok(res.collect::<Result<Vec<(SmolStr, u32)>>>()?)
        })
    }

    pub fn before_index_latest(&self, origin: &Origin) -> FResult<()> {
        self.with_write("before_index_latest", |tx| {
            let next_timestamp = (Utc::now().timestamp() + 3600 * 24) as u32;
            let mut mark_updated = tx.prepare_cached("UPDATE crates SET next_update = ?2 WHERE origin = ?1")?;
            mark_updated.execute((origin.to_str(), next_timestamp)).map_err(|e| Error::Db(e, "upd-l"))?;
            Ok(())
        })
    }

    /// Add data of the latest version of a crate to the index
    /// Score is a ranking of a crate (0 = bad, 1 = great)
    pub fn index_latest(&self, c: CrateVersionData<'_>) -> FResult<DbDerived> {
        let origin = c.origin.to_str();
        let mut insert_keyword = self.gather_crate_keywords(&c)?;

        let mut out = String::with_capacity(200);
        let _ = write!(&mut out, "https://lib.rs/crates/{} ", if c.origin.is_crates_io() { c.origin.package_name_icase() } else { &origin });

        c.category_slugs.iter().for_each(|k| debug_assert!(categories::CATEGORIES.from_slug(k).1, "'{k}' must exist"));

        let categories = self.with_write("insert_crate", |tx| {
            let mut insert_crate = tx.prepare_cached("INSERT OR IGNORE INTO crates (origin, recent_downloads, ranking) VALUES (?1, ?2, ?3)")
                .map_err(|e| Error::Db(e, "ins prep"))?;
            let mut mark_updated = tx.prepare_cached("UPDATE crates SET next_update = ?2 WHERE id = ?1")?;
            let mut insert_repo = tx.prepare_cached("INSERT OR REPLACE INTO crate_repos (crate_id, repo) VALUES (?1, ?2)")?;
            let mut delete_repo = tx.prepare_cached("DELETE FROM crate_repos WHERE crate_id = ?1")?;
            let mut prev_categories = tx.prepare_cached("SELECT slug FROM categories WHERE crate_id = ?1")?;
            let mut clear_categories = tx.prepare_cached("DELETE FROM categories WHERE crate_id = ?1")?;
            let mut insert_category = tx.prepare_cached("INSERT OR IGNORE INTO categories (crate_id, slug, rank_weight, relevance_weight) VALUES (?1, ?2, ?3, ?4)")?;
            let mut get_crate_id = tx.prepare_cached("SELECT id, recent_downloads, ranking FROM crates WHERE origin = ?1")?;

            insert_crate.execute((&origin, 0i32, 0i32))?;
            let (crate_id, downloads, ranking): (u32, u32, Option<f32>) = get_crate_id.query_row([&origin], |row| Ok((row.get_unwrap(0), row.get_unwrap(1), row.get_unwrap(2))))
                .map_err(|e| Error::Db(e, "crate id"))?;
            let ranking = ranking.unwrap_or(0.);
            let mut is_important_ish = (ranking > 0.4 || downloads > 15000) && !c.is_spam_or_hidden;

            if let Some(repo) = c.repository {
                let url = repo.canonical_git_url();
                insert_repo.execute((crate_id, &url)).map_err(|e| Error::Db(e, "insert repo"))?;
            } else {
                delete_repo.execute([crate_id])?;
            }

            let prev_c = prev_categories.query_map([&crate_id], |row| Ok(SmolStr::from(row.get_ref(0)?.as_str()?)))?.collect::<Result<Vec<SmolStr>,_>>()?;
            clear_categories.execute([crate_id]).map_err(|e| Error::Db(e, "clear cat"))?;
            insert_keyword.before_save(tx, crate_id)?;

            // guessing categories if needed
            let categories = {
                self.extract_crate_categories(tx, &c, &insert_keyword.keywords, is_important_ish || downloads > 5000)?
            };

            let had_explicit_categories = categories.iter().any(|c| c.explicit);
            if !had_explicit_categories {
                if categories.is_empty() {
                    let _ = write!(&mut out, "[no categories] {prev_c:?}");
                } else {
                    let _ = write!(&mut out, "[guessed]: ");
                }
            }

            for c in &categories {
                let _ = write!(&mut out, ">{}{}, ", if !prev_c.contains(&c.slug) { "NEW " } else { "" }, c.slug);
            }

            if categories.iter().any(|c| c.slug == "cryptography::cryptocurrencies") {
                is_important_ish = false;
            }

            for slug in &prev_c {
                if !categories.iter().any(|old| old.slug == *slug) {
                    let _ = write!(&mut out, ">LOST {slug}");
                }
            }

            if !had_explicit_categories {
                let mut tmp = insert_keyword.keywords.iter().collect::<Vec<_>>();
                tmp.sort_unstable_by(|a, b| b.1.partial_cmp(a.1).unwrap_or(std::cmp::Ordering::Equal));
                let _ = write!(&mut out, " #{}", tmp.into_iter().take(10).map(|(k, _)| k.as_str()).collect::<Vec<_>>().join(" #"));
            }

            for &CategoryCandidate {rank_weight, category_relevance, ref slug, explicit} in &categories {
                insert_category.execute((crate_id, &**slug, rank_weight, category_relevance))
                    .map_err(|e| Error::Db(e, "insert cat"))?;
                if explicit {
                    insert_keyword.add_raw((&**slug).into(), category_relevance/3., false);
                }
            }

            // yanked crates may contain garbage, or needlessly come up in similar crates
            // so knock all keywords' importance if it's yanked
            insert_keyword.save(tx, crate_id, if c.source_data.is_yanked || c.is_spam_or_hidden {0.1} else {1.})?;

            let extra_days = if ranking > 0. { ((0.6 - ranking).max(0.) * 90.) as i64 } else { 0 };
            let days = extra_days + if c.is_spam_or_hidden || c.source_data.is_yanked || (ranking > 0. && ranking < 0.2) { 360 } else if is_important_ish { 16 } else { 32 };
            let next_timestamp = (Utc::now().timestamp() + 3600 * 24 * days) as u32;

            mark_updated.execute((crate_id, next_timestamp))?;
            println!("{out}");
            Ok(categories.into_iter().map(|cc| cc.slug).collect::<Vec<_>>())
        })?;

        Ok(DbDerived {
            categories,
        })
    }

    fn gather_crate_keywords(&self, c: &CrateVersionData<'_>) -> Result<KeywordInsert, Error> {
        let manifest = c.manifest;
        let package = manifest.package.as_ref().expect("package");

        let mut insert_keyword = KeywordInsert::new()?;

        if let Some(lib) = c.source_data.lib_file.as_ref() {
            insert_keyword.add_raw(hex_hash(lib).into(), 1., false);
        }
        if let Some(bin) = c.source_data.bin_file.as_ref() {
            insert_keyword.add_raw(hex_hash(bin).into(), 1., false);
        }
        if let Some(Readme { markup: Markup::Markdown(txt) | Markup::Html(txt) | Markup::Rst(txt) , ..}) = c.source_data.readme.as_ref() {
            insert_keyword.add_raw(hex_hash(txt).into(), 1., false);
        }

        for (w, k) in c.visible_auto_keywords {
            insert_keyword.add(k, f64::from(*w), true);
        }
        for (w, k) in c.invisible_auto_keywords {
            insert_keyword.add(k, f64::from(*w), false);
        }

        if let Some(url) = package.homepage() {
            if url.len() > 5 {
                insert_keyword.add_raw(format!("url:{url}").into(), 1., false); // crates sharing homepage are likely same project
            }
        }
        for feat in manifest.features.keys() {
            if feat != "default" && feat != "std" && feat != "nightly" && feat != "all" {
                insert_keyword.add_raw(format!("feature:{feat}").into(), 0.55, false);
            }
        }
        if manifest.is_sys(c.source_data.has_buildrs || package.build.is_some()) {
            insert_keyword.add_raw("has:is_sys".into(), 0.01, false);
        }
        if manifest.is_proc_macro() {
            insert_keyword.add_raw("has:proc_macro".into(), 0.3, false);
        }
        if manifest.has_bin() {
            insert_keyword.add_raw("has:bin".into(), 0.01, false);
            if manifest.has_cargo_bin() {
                insert_keyword.add_raw("has:cargo-bin".into(), 0.2, false);
            }
        }
        if c.is_build {
            insert_keyword.add_raw("has:is_build".into(), 0.01, false);
        }
        if c.is_dev {
            insert_keyword.add_raw("has:is_dev".into(), 0.01, false);
        }
        for &(dep, weight) in c.deps_stats {
            insert_keyword.add_raw(format!("dep:{dep}").into(), weight.into(), false);
        }
        for (i, k) in c.authors.iter().filter_map(|a| a.email.as_ref().or(a.name.as_ref())).enumerate() {
            let w: f64 = 50. / (100 + i) as f64;
            insert_keyword.add_raw(k.into(), w, false);
        }
        if let Some(repo) = c.repository {
            let url = repo.canonical_git_url();
            insert_keyword.add_raw(format!("repo:{url}").into(), 1., false); // crates in monorepo probably belong together
            if let Some(owner) = repo.host().url_owner_name() {
                insert_keyword.add_raw(format!("by:{owner}").into(), 1., false);
            }
        }
        Ok(insert_keyword)
    }

    /// (rank-relevance, relevance, slug)
    ///
    /// Rank relevance is normalized and biased towards one top category
    fn extract_crate_categories(&self, conn: &Connection, c: &CrateVersionData<'_>, keywords: &HashMap<SmolStr, (f64, bool)>, is_important_ish: bool) -> FResult<Vec<CategoryCandidate>> {
        let had_explicit_categories = !c.category_slugs.is_empty();
        let candidates = if had_explicit_categories {
            let cat_w = 10.0 / (8.0 + c.category_slugs.len() as f64);
            c.category_slugs
                .iter()
                .enumerate()
                .map(|(i, slug)| {
                    let w = 100. / (3 + i) as f64 * cat_w;
                    ((&**slug).into(), w)
                })
                .collect()
        } else {
            let cat_w = 0.2f64.mul_add(c.manifest.package().keywords().len() as f64, 0.2);
            let mut candidates = Self::candidate_crate_categories_tx(conn, c.origin)?;
            for w in candidates.values_mut() {
                *w = (*w * cat_w).min(0.99);
            }
            candidates
        };
        let threshold = if had_explicit_categories {0.001} else if is_important_ish {0.1} else {0.25};

        let categories = categories::adjusted_relevance(candidates, keywords, threshold, had_explicit_categories);

        debug!("{:?} picked categories {categories:?} (thr={threshold} had={had_explicit_categories})", c.origin);

        let max_weight = categories.iter().map(|&(w, _)| w)
            .max_by(f64::total_cmp)
            .unwrap_or(0.1)
            .max(0.1); // prevents div/0, ensures odd choices stay low

        let categories = categories.into_iter()
            .map(|(category_relevance, slug)| {
                let rank_weight = category_relevance / max_weight
                    * if category_relevance >= max_weight * 0.98 { 1. } else { 0.4 } // a crate is only in 1 category
                    * if category_relevance > 0.2 { 1. } else { 0.8 }; // keep bad category guesses out of sight
                CategoryCandidate {rank_weight, category_relevance, slug, explicit: had_explicit_categories}
            })
            .collect();

        Ok(categories)
    }

    /// Update crate <> repo association
    ///
    /// Along with `index_latest` it establishes 2-way association.
    /// It solves two problems:
    ///
    /// 1. A published crate can define what repository it is from, but *any* crate
    ///    can point to *any* repo, so that alone is not enough to prove it actually
    ///    is the crate's real repository.
    ///    Checking what crates are in the repository confirms or disproves the association.
    /// 2. A repository can contain more than one crate (monorepo). Search of the repo
    ///    finds location of the crate within the repo, adding extra precision to the
    ///    crate's repo URL (needed for e.g. GitHub README relative links), and adds
    ///    interesting relationship information for crates.
    pub fn index_repo_crates(&self, repo_url: &Repo, paths_and_names: impl Iterator<Item = (impl AsRef<str>, impl AsRef<str>, impl AsRef<str>)>) -> FResult<()> {
        let repo = repo_url.canonical_git_url();
        self.with_write("index_repo_crates", |tx| {
            let mut insert_repo = tx.prepare_cached("INSERT OR IGNORE INTO repo_crates (repo, path, crate_name, revision) VALUES (?1, ?2, ?3, ?4)")
                .map_err(|e| Error::Db(e, "repo prep"))?;
            let repo = repo.as_str();
            for (path, name, revision) in paths_and_names {
                let name = name.as_ref();
                let path = path.as_ref();
                let revision = revision.as_ref();
                insert_repo.execute((repo, path, name, revision))
                    .map_err(|e| Error::Db(e, "repo rev insert"))?;
            }
            Ok(())
        })
    }

    pub fn crates_in_repo(&self, repo: &Repo) -> FResult<Vec<SmolStr>> {
        let repo = repo.canonical_git_url();
        self.with_read("crates_in_repo", move |conn| {
            let mut q = conn.prepare_cached("
                SELECT crate_name
                FROM repo_crates
                WHERE repo = ?1
                ORDER BY path, crate_name LIMIT 100
            ").map_err(|e| Error::Db(e, "repo c"))?;
            let q = q.query_map([&repo], |r| {
                let s = r.get_ref_unwrap(0).as_str()?;
                Ok(if Origin::is_valid_crate_name(s) { Some(s.into()) } else { None })
            })?.filter_map(|r| r.map_err(|e| warn!("crates-in-repo: {e}")).ok().flatten());
            Ok(q.collect())
        })
    }

    pub fn github_repos_with_crate(&self, login: &str, crate_name: &str) -> FResult<Vec<Repo>> {
        self.with_read("gh_crates_in_repo", move |conn| {
            let mut q = conn.prepare_cached("
                SELECT repo
                FROM repo_crates
                WHERE repo LIKE ?1 AND crate_name = ?2
                ORDER BY path, repo LIMIT 50
            ").map_err(|e| Error::Db(e, "repo2"))?;
            let repo_url_prefix = format!("https://github.com/{}/%", &*login.as_ascii_lowercase());
            let q = q.query_map([repo_url_prefix.as_str(), crate_name], |r| r.get(0))?
                .filter_map(|r: Result<String, _>| Repo::new(&r.ok()?).ok());
            Ok(q.collect())
        })
    }

    pub fn parent_crate(&self, repo_url: &Repo, child_name: &str) -> FResult<Option<Origin>> {
        let repo_url_str = repo_url.canonical_git_url();
        let paths = self.with_read("parent_crate", move |conn| {
            let mut q = conn.prepare_cached("SELECT rc.path, rc.crate_name, MAX(0.5 + c.ranking * c.recent_downloads)
                FROM repo_crates rc
                JOIN crates c ON c.origin = 'crates.io:' || LOWER(rc.crate_name)
                WHERE rc.repo = ?1
                GROUP BY rc.crate_name -- there may be duplicate names
                LIMIT 200")
                .map_err(|e| Error::Db(e, "parent"))?;
            let tmp = q.query_map([&repo_url_str], |r| {
                Ok((r.get_ref(0)?.as_str()?.into(), (r.get_ref(1)?.as_str()?.into(), r.get(2)?)))
            })?;
            Ok(tmp.collect::<Result<HashMap<SmolStr, (SmolStr, f32)>, _>>()?)
        })?;

        Ok(find_parent_in_repo(paths, repo_url, child_name))
    }

    /// additions and removals
    pub fn index_repo_changes(&self, repo_url: &Repo, changes: &[RepoChange]) -> FResult<()> {
        let repo_url_str = repo_url.canonical_git_url();
        self.with_write("index_repo_changes", |tx| {
            let mut insert_change = tx.prepare_cached("INSERT OR IGNORE INTO repo_changes (repo, crate_name, replacement, weight) VALUES (?1, ?2, ?3, ?4)")?;
            for change in changes {
                match *change {
                    RepoChange::Replaced { ref crate_name, ref replacement, weight } => {
                        assert!(Origin::is_valid_crate_name(crate_name));
                        assert!(Origin::is_valid_crate_name(replacement));
                        insert_change.execute((&repo_url_str, crate_name.as_str(), Some(replacement.as_str()), weight))
                    },
                    RepoChange::Removed { ref crate_name, weight } => {
                        assert!(Origin::is_valid_crate_name(crate_name));
                        insert_change.execute((&repo_url_str, crate_name.as_str(), None::<&str>, weight))
                    },
                }?;
            }
            Ok(())
        })
    }

    pub fn path_in_repo(&self, repo_url: &Repo, crate_name: &str) -> FResult<Option<String>> {
        self.with_read("path_in_repo", |conn| Self::path_in_repo_tx(conn, repo_url, crate_name))
    }

    pub fn path_in_repo_tx(conn: &Connection, repo_url: &Repo, crate_name: &str) -> FResult<Option<String>> {
        let repo = repo_url.canonical_git_url();
        let mut get_path = conn.prepare_cached("SELECT path FROM repo_crates WHERE repo = ?1 AND crate_name = ?2")
            .map_err(|e| Error::Db(e, "pir"))?;
        none_rows(get_path.query_row((repo, crate_name), |row| row.get(0))).map_err(|e| Error::Db(e, "path_in_repo"))
    }

    /// Update download counts of the crate
    pub fn index_versions(&self, all: &RichCrate, score: f64, downloads_per_month: Option<usize>) -> FResult<()> {
        self.with_write("index_versions", |tx| {
            let mut get_crate_id = tx.prepare_cached("SELECT id, ranking FROM crates WHERE origin = ?1")
                .map_err(|e| Error::Db(e, "gcid"))?;
            let mut insert_version = tx.prepare_cached("INSERT OR IGNORE INTO crate_versions (crate_id, version, created) VALUES (?1, ?2, ?3)")?;

            let origin = all.origin().to_str();
            let (crate_id, prev_ranking): (u32, f64) = get_crate_id.query_row([&origin], |row| Ok((row.get(0)?, row.get(1)?)))
                .map_err(|e| Error::Db(e, "the crate hasn't been indexed yet"))?;

            let recent_90_days = downloads_per_month.unwrap_or(0) as u32 * 3;
            let mut update_recent = tx.prepare_cached("UPDATE crates SET recent_downloads = ?1, ranking = ?2 WHERE id = ?3")?;
            update_recent.execute((recent_90_days, score, crate_id))?;

            if (prev_ranking - score).abs() > 0.01 {
                info!("ranking changed by {:0.4}; {:?} = {:0.5} => {:0.5}", (prev_ranking - score), all.origin(), prev_ranking, score);
            }

            for ver in all.versions() {
                let timestamp = ver.created_at.timestamp();
                insert_version.execute((crate_id, ver.num.as_str(), timestamp))?;
            }
            Ok(())
        })
    }
/*
2020-05-06 13:50:44
*/
    /// `github_id`, `created_at`, number of crates
    pub fn crate_all_current_owners(&self) -> FResult<Vec<CrateOwnerStat>> {
        self.with_read("all_owners", |tx| {
            let mut query = tx.prepare_cached("SELECT github_id, min(invited_at), count(*) FROM author_crates2 WHERE deleted_at IS NULL GROUP BY github_id")
                .map_err(|e| Error::Db(e, "own"))?;
            let q = query.query_map([], |row| {
                let s = row.get_ref(1)?.as_str()?;
                let y = s[0..4].parse().map_err(|e| error!("{} = {}", s, e)).ok();
                let m = s[5..7].parse().map_err(|e| error!("{} = {}", s, e)).ok();
                let d = s[8..10].parse().map_err(|e| error!("{} = {}", s, e)).ok();
                Ok((row.get(0)?, (y,m,d), row.get(2)?))
            })?;
            let res: Vec<_> = q.filter_map(|row| row.map_err(|e| error!("owner: {}", e)).ok()).filter_map(|row| {
                Some(CrateOwnerStat {
                    github_id: row.0,
                    created_at: ((row.1).0?, (row.1).1?, (row.1).2?),
                    num_crates: row.2,
                })
            }).collect();
            assert!(res.len() > 1000);
            Ok(res)
        })
    }

    /// Replaces entire `author_crates` table
    pub fn index_crate_all_owners(&self, all_owners: &[(Origin, Vec<CrateOwner>)], now: DateTime<Utc>) -> FResult<bool> {
        self.with_write("index_crate_owners", |tx| {
            let mut get_crate_id = tx.prepare_cached("SELECT id FROM crates WHERE origin = ?1 LIMIT 1")?;
            let mut get_current = tx.prepare_cached("SELECT github_id, invited_by_github_id, invited_at
                FROM author_crates2
                WHERE crate_id = ?1
                AND deleted_at IS NULL")?;

            let mut to_upsert = Vec::new();
            let mut to_delete = Vec::new();

            debug!("Diffing owners of {} crates", all_owners.len());
            for (i, (origin, owners)) in all_owners.iter().enumerate() {
                let owners = owners.as_slice();
                let crate_id: u32 = match get_crate_id.query_row([&origin.to_str()], |row| row.get(0)) {
                    Ok(id) => id,
                    Err(rusqlite::Error::QueryReturnedNoRows) => {
                        warn!("got {} owners for unknown crate {origin:?} [{}/{}]", owners.len(), i, all_owners.len()); continue
                    },
                    Err(e) => return Err(e.into()),
                };
                let mut current_owners = get_current.query_map([&crate_id], |row| {
                    let github_id: u32 = row.get(0)?;
                    let invited_by_github_id: Option<u32> = row.get(1)?;
                    let invited_at = row.get_ref(2)?.as_str().ok().and_then(parse_invited_at);
                    Ok((github_id, (invited_by_github_id, invited_at)))
                })?.collect::<Result<util::FxHashMap<u32, _>>>()?;

                for o in owners {
                    let Some(github_id) = o.github_id() else {
                        debug!("discarded owner {o:?} of {origin:?}");
                        continue
                    };
                    let invited_by_github_id = o.invited_by_github_id.filter(|&id| id != github_id);
                    match current_owners.remove(&github_id) {
                        Some(old) if old == (invited_by_github_id, o.invited_at) => {}, // no change
                        current => {
                            // invited user can't ever become the original owner (maybe this shouldn't even allow updates at all?)
                            let invited_by_github_id = invited_by_github_id
                                .or(current.and_then(|(by, _)| by)).filter(|&id| id != github_id);

                            let is_new_invite = current.is_none() && invited_by_github_id.is_some();
                            let invited_at = o.invited_at
                                .or(current.and_then(|(_, at)| at)) // don't allow clearing invite date, user can't become the first owner after the fact
                                .or(if is_new_invite { Some(now) } else { None }) // explicit invite without a date!?
                                .map(|d| d.to_rfc3339());
                            to_upsert.push((github_id, crate_id, invited_by_github_id, invited_at));
                        },
                    }
                }
                for old_github_id in current_owners.into_keys() {
                    to_delete.push((old_github_id, crate_id, origin.package_name_icase()));
                }
            }
            if !to_delete.is_empty() {
                info!("{} owner deletes", to_delete.len());
                let mut mark_deleted = tx.prepare_cached("UPDATE author_crates2 SET deleted_at = ?1 WHERE github_id = ?2 AND crate_id = ?3")?;
                let deleted_at = now.to_rfc3339();
                for (old_github_id, crate_id, name) in to_delete {
                    info!("Noticed that ghid={old_github_id} was removed from {name}");
                    mark_deleted.execute((deleted_at.as_str(), old_github_id, crate_id))?;
                }
            }
            let mut upsert = tx.prepare_cached("INSERT OR REPLACE INTO author_crates2(github_id, crate_id, invited_by_github_id, invited_at, deleted_at) VALUES(?1, ?2, ?3, ?4, NULL)")?;
            info!("{} owner changes/additions", to_upsert.len());
            for u in to_upsert {
                upsert.execute(u)?;
            }
            Ok(true)
        })
    }

    pub fn deleted_owners_of_crate(&self, origin: &Origin) -> FResult<Vec<u32>> {
        let origin_str = origin.to_str();
        self.with_read("owners_of_crate", move |conn| {
            let mut query = conn.prepare_cached(r#"SELECT ac.github_id
                FROM author_crates2 ac
                JOIN crates c ON c.id = ac.crate_id
                WHERE c.origin = ?1 AND ac.deleted_at IS NOT NULL
                LIMIT 2000
            "#).map_err(|e| Error::Db(e, "aoc"))?;
            let q = query.query_map([&origin_str], |row| {
                let github_id: u32 = row.get(0)?;
                Ok(github_id)
            })?;
            Ok(q.filter_map(|x| x.map_err(|e| error!("by o={origin_str}: {e}")).ok()).collect())
        })
    }

    pub fn all_crates_of_github_id(&self, github_id: u32) -> FResult<Vec<CrateOwnerRow>> {
        self.with_read("crates_of_author", move |conn| {
            let mut query = conn.prepare_cached(r#"SELECT c.origin, ac.invited_by_github_id, ac.invited_at, max(cv.created), min(cv.created), deleted_at, c.ranking
                FROM author_crates2 ac
                JOIN crate_versions cv USING(crate_id)
                JOIN crates c ON c.id = ac.crate_id
                WHERE ac.github_id = ?1
                GROUP BY ac.crate_id
                LIMIT 2000
            "#).map_err(|e| Error::Db(e, "coa"))?;
            let q = query.query_map([&github_id], |row| {
                let origin = Origin::from_str(row.get_ref(0)?.as_str()?);
                let invited_by_github_id: Option<u32> = row.get(1)?;
                let invited_at = row.get_ref(2)?.as_str().ok().and_then(parse_invited_at);
                let latest_timestamp: u32 = row.get(3)?;
                let oldest_timestamp: u32 = row.get(4)?;
                let deleted_at = row.get_ref(5)?.as_str().ok().and_then(parse_invited_at);
                let crate_ranking: Option<f64> = row.get(6)?;
                Ok(CrateOwnerRow {
                    crate_ranking: crate_ranking.unwrap_or_else(|| { warn!("{origin:?} has no ranking (yet?)"); 0.3 }) as f32,
                    origin,
                    invited_by_github_id,
                    invited_at,
                    deleted_at,
                    latest_release: Utc.timestamp_opt(i64::from(latest_timestamp), 0).unwrap(),
                    oldest_release: Utc.timestamp_opt(i64::from(oldest_timestamp), 0).unwrap(),
                })
            })?;
            Ok(q.filter_map(|x| x.map_err(|e| error!("by owner of ghid={github_id}: {e}")).ok()).collect())
        })
    }

    fn candidate_crate_categories_tx(conn: &Connection, origin: &Origin) -> FResult<HashMap<SmolStr, f64>> {
        let mut query = conn.prepare_cached(r#"
        select cc.slug, sum(cc.relevance_weight * ck.weight * relk.relevance)/(8+count(*)) as w
        from (
        ----------------------------------
            select avg(ck.weight) * srck.weight / (8000+sum(ck.weight)) as relevance, ck.keyword_id
            -- find the crate to categorize
            from crates
            -- find its keywords
            join crate_keywords srck on crates.id = srck.crate_id
            -- find other crates using these keywords
            -- ck.weight * srck.weight gives strenght of the connection
            -- and divided by count(*) for tf-idf for relevance
            join crate_keywords ck on ck.keyword_id = srck.keyword_id
            where crates.origin = ?1
            group by ck.keyword_id
            order by 1 desc
        ----------------------------------
        ) as relk
        join crate_keywords ck on ck.keyword_id = relk.keyword_id
        join categories cc on cc.crate_id = ck.crate_id
        group by slug
        order by 2 desc
        limit 10"#).map_err(|e| Error::Db(e, "findcat"))?;
        let candidates = query.query_map([&origin.to_str()], |row| Ok((SmolStr::from(row.get_ref(0)?.as_str()?), row.get(1)?)))?;
        let candidates = candidates.collect::<std::result::Result<HashMap<_,_>, _>>()?;
        candidates.keys().for_each(|k| debug_assert!(categories::CATEGORIES.from_slug(k).1, "'{k}' must exist"));

        Ok(candidates)
    }

    /// Find most relevant keyword for the crate
    ///
    /// Returns (top n-th for the keyword, the keyword)
    pub fn top_keyword(&self, origin: &Origin) -> FResult<Option<(u32, String)>> {
        let origin = origin.to_str();
        self.with_read("top_keyword", move |conn| {
            let mut query = conn.prepare_cached(r#"
            select top, keyword from (
                select count(*) as total, sum(case when oc.ranking >= c.ranking then 1 else 0 end) as top, k.keyword from crates c
                        join crate_keywords ck on ck.crate_id = c.id
                        join crate_keywords ock on ock.keyword_id = ck.keyword_id
                        join crates oc on ock.crate_id = oc.id
                        join keywords k on k.id = ck.keyword_id
                        where c.origin = ?1
                        and ck.explicit
                        and ock.explicit
                        and k.visible
                        group by ck.keyword_id
                        having count(*) >= 5
                ) as tmp
            order by top + (top+30.0)/total
            "#).map_err(|e| Error::Db(e, "tkw"))?;
            Ok(none_rows(query.query_row([&origin], |row| Ok((row.get_unwrap(0), row.get_unwrap(1)))))?)
        })
    }

    /// Number of crates with a given keyword
    ///
    /// NB: must be lowercase
    pub fn crates_with_keyword(&self, keyword: &str) -> FResult<u32> {
        let keyword = keyword.as_ascii_lowercase();
        let keyword = &*keyword;
        self.with_read("crates_with_keyword", move |conn| {
            let mut query = conn.prepare_cached("SELECT count(*) FROM crate_keywords
                WHERE explicit AND keyword_id = (SELECT id FROM keywords WHERE keyword = ?1)")?;
            Ok(none_rows(query.query_row([&keyword], |row| row.get(0)))?.unwrap_or(0))
        })
    }

    /// Sorted by most popular first
    pub fn all_explicit_keywords(&self) -> FResult<&HashMap<SmolStr, u32>> {
        if let Some(res) = self.all_explicit_keywords_cache.get() {
            return Ok(res)
        }

        self.with_read("allkw", |conn| {
            self.all_explicit_keywords_cache.get_or_try_init(move || {
                let mut query = conn.prepare_cached("SELECT
                        k.keyword,
                        count(*) as cnt,
                        k.precision * sum(ck.weight) as w
                    FROM keywords k
                    JOIN crate_keywords ck ON (ck.keyword_id = k.id)
                    WHERE k.visible
                    GROUP BY k.id
                    HAVING count(*) > 4
                    ORDER BY 3 DESC
                    LIMIT 25000
                ").map_err(|e| Error::Db(e, "akw"))?;
                let mut max_cnt = 0;
                let mut max_w = 0.;
                let res = query.query_map([], |row| {
                    let k = SmolStr::from(row.get_ref(0)?.as_str()?);
                    let cnt = row.get_ref(1)?.as_i64()? as u32;
                    let w = row.get_ref(2)?.as_f64()?;

                    if w > max_w { max_w = w; }
                    if cnt > max_cnt { max_cnt = cnt; }

                    // use weight, but scale it to count for simplicity
                    let cnt = (w * f64::from(max_cnt) / max_w) as u32;

                    Ok((k, cnt))
                })?;
                Ok(res.collect::<Result<_,_>>()?)
            })
        })
    }

    /// Categories similar to the given category
    pub fn related_categories(&self, slug: &str) -> FResult<Vec<String>> {
        self.with_read("related_categories", |conn| {
            let mut query = conn.prepare_cached(r#"
                select sum(c2.relevance_weight * c1.relevance_weight) as w, c2.slug
                from categories c1
                join categories c2 on c1.crate_id = c2.crate_id
                where c1.slug = ?1
                and c2.slug != c1.slug
                group by c2.slug
                having w > 250
                order by 1 desc
                limit 6
            "#)?;
            let res = query.query_map([&slug], |row| row.get(1))?;
            Ok(res.collect::<std::result::Result<_,_>>()?)
        })
    }

    pub fn replacement_crates(&self, crate_name: &str) -> FResult<Vec<Origin>> {
        self.with_read("replacement_crates", |conn| {
            let mut query = conn.prepare_cached(r#"
                SELECT sum(weight) as w, replacement
                FROM repo_changes
                WHERE crate_name = ?1
                AND replacement IS NOT NULL
                GROUP BY replacement
                HAVING w > 20
                ORDER by 1 desc
                LIMIT 4
            "#)?;
            let res = query.query_map([&crate_name], |row| {
                let s = row.get_ref_unwrap(1).as_str()?;
                crates_io_name(s)
            })?;
            Ok(res.collect::<std::result::Result<_,_>>()?)
        })
    }

    /// f32 is ranking
    pub fn related_crates(&self, origin: &Origin, min_recent_downloads: u32) -> FResult<Vec<(Origin, f32)>> {
        let origin = origin.to_str();
        self.with_read("related_crates", move |conn| {
            let mut query = conn.prepare_cached(r#"
                SELECT sum(k2.weight * k1.weight * kk.precision) as w, c2.origin, c2.ranking
                FROM crates c1
                JOIN crate_keywords k1 on k1.crate_id = c1.id
                JOIN crate_keywords k2 on k1.keyword_id = k2.keyword_id
                JOIN keywords kk on k1.keyword_id = kk.id
                JOIN crates c2 on k2.crate_id = c2.id
                WHERE c1.origin = ?1
                AND k2.crate_id != c1.id
                AND c2.recent_downloads > ?2
                AND c2.ranking > 0.01
                GROUP by k2.crate_id
                HAVING w > 1
                ORDER by 1 desc
                LIMIT 15
            "#).map_err(|e| Error::Db(e, "rel"))?;
            let res = query.query_map((origin, min_recent_downloads), |row| {
                Ok((Origin::from_str(row.get_ref_unwrap(1).as_str()?), row.get_unwrap(2)))
            })?;
            Ok(res.collect::<std::result::Result<_,_>>()?)
        })
    }

    /// Find most relevant/popular keywords in the category
    pub fn top_keywords_in_category(&self, slug: &str) -> FResult<Vec<SmolStr>> {
        self.with_read("top_keywords_in_category", |conn| {
            let mut query = conn.prepare_cached(r#"
                select sum(k.weight * c.relevance_weight), kk.keyword from categories c
                    join crate_keywords k using(crate_id)
                    join keywords kk on kk.id = k.keyword_id
                    where explicit and c.slug = ?1
                    group by k.keyword_id
                    having sum(k.weight) > 7 and count(*) >= 4
                    order by 1 desc
                    limit 20
            "#).map_err(|e| Error::Db(e, "tkic"))?;
            let q = query.query_map([&slug], |row| Ok(row.get_ref(1)?.as_str()?.into()))?;
            let q = q.filter_map(|r| r.map_err(|e| error!("kw: {}", e)).ok());
            Ok(q.collect())
        })
    }

    /// Most popular crates in the category
    /// Returns weight/importance as well
    pub fn top_crates_in_category_partially_ranked(&self, slug: &str, limit: u32, sort_order: SortOrder) -> FResult<Vec<(Origin, f64)>> {
        self.with_read("top_crates_in_category_partially_ranked", |conn| {
            let (sort_expr, filter_expr) = match sort_order {
                SortOrder::Quality => ("c.rank_weight * k.ranking", ""),
                SortOrder::Popularity => ("k.recent_downloads * (c.rank_weight+1) * CASE WHEN k.ranking < 0.4 THEN k.ranking ELSE 0.4 END", "AND k.recent_downloads > 2000"),
                SortOrder::Recency => ("c.rank_weight * k.ranking * (3600*24*7) + (select MAX(created) as cr from crate_versions v where v.crate_id = k.id)", ""),
            };
            // sort by relevance to the category, downrank for being crappy (later also downranked for being removed from crates)
            // low number of downloads is mostly by rank, rather than downloads
            let mut query = conn.prepare_cached(
            &format!("SELECT k.origin, ({sort_expr}) as w
                FROM categories c
                JOIN crates k on c.crate_id = k.id
                WHERE c.slug = ?1
                AND k.ranking > 0.12 {filter_expr}
                ORDER by w DESC
                LIMIT ?2"),
            )?;
            let q = query.query_map((slug, limit), |row| {
                Ok((Origin::from_str(row.get_ref_unwrap(0).as_str()?), row.get_unwrap(1)))
            })?;
            let q = q.filter_map(|r| r.map_err(|e| error!("top: {}", e)).ok());
            Ok(q.collect())
        })
    }

    pub fn top_crates_uncategorized(&self, limit: u32) -> FResult<Vec<(Origin, f64)>> {
        self.with_read("top_crates_uncategorized", move |conn| {
            // sort by relevance to the category, downrank for being crappy (later also downranked for being removed from crates)
            // low number of downloads is mostly by rank, rather than downloads
            let mut query = conn.prepare_cached(
            "SELECT k.origin, k.ranking as w
                FROM crates k
                LEFT JOIN categories c on c.crate_id = k.id
                WHERE c.slug IS NULL AND w > 0.2
                ORDER by w desc
                LIMIT ?1",
            )?;
            let q = query.query_map([limit], |row| {
                Ok((Origin::from_str(row.get_ref_unwrap(0).as_str()?), row.get_unwrap(1)))
            })?;
            let q = q.filter_map(|r| r.map_err(|e| error!("top: {}", e)).ok());
            Ok(q.collect())
        })
    }

    /// Newly added or updated crates in the category, filtered to include only not-terrible crates
    ///
    /// Returns `origin` strings
    pub fn recently_updated_crates_in_category(&self, slug: &str) -> FResult<Vec<(Origin, f64)>> {
        let slug = slug.to_owned();
        self.with_read("recently_updated_crates_in_category", move |conn| {
            let mut query = conn.prepare_cached(r#"
                select max(created) + 3600*24*7 * c.rank_weight * k.ranking, -- week*rank ~= best this week
                        k.origin, c.rank_weight * k.ranking
                    from categories c
                    join crate_versions v using (crate_id)
                    join crates k on v.crate_id = k.id
                    where c.slug = ?1
                        and k.ranking > 0.33 -- skip spam
                    group by v.crate_id
                    having count(*) > 1 -- so these are updates, not new releases
                    order by 1 desc
                    limit 20
            "#)?;
            let q = query.query_map([&slug], |row| {
                Ok((Origin::from_str(row.get_ref_unwrap(1).as_str()?), row.get_unwrap(2)))
            })?;
            let q = q.filter_map(|r| r.map_err(|e| error!("upd: {}", e)).ok());
            Ok(q.collect())
        })
    }

    /// Newly added or updated crates in any category
    ///
    /// Returns `origin` strings
    pub fn recently_updated_crates(&self, limit: u32) -> FResult<Vec<(Origin, f64)>> {
        self.with_read("recently_updated_crates", move |conn| {
            let mut query = conn.prepare_cached(r#"
                select max(created) + 3600*24*7 * k.ranking, -- week*rank ~= best this week
                    k.ranking,
                    k.origin
                    from crate_versions v
                    join crates k on v.crate_id = k.id
                    group by v.crate_id
                    having count(*) > 1 -- so these are updates, not new releases
                    order by 1 desc
                    limit ?1
            "#)?;
            let q = query.query_map([&limit], |row| {
                let origin = Origin::from_str(row.get_ref_unwrap(2).as_str()?);
                Ok((origin, row.get(1)?))
            })?;
            let q = q.filter_map(|r| r.map_err(|e| error!("ruc: {}", e)).ok());
            Ok(q.collect())
        })
    }

    /// Newly added or updated crates in any category
    ///
    /// Returns `origin` strings
    pub fn most_downloaded_crates(&self, limit: u32) -> FResult<Vec<(Origin, u32)>> {
        self.with_read("recently_updated_crates", move |conn| {
            let mut query = conn.prepare_cached(r#"
                select recent_downloads, origin from crates order by recent_downloads * min(0.5, ranking) desc limit ?1
            "#)?;
            let q = query.query_map([&limit], |row| {
                let origin = Origin::from_str(row.get_ref_unwrap(1).as_str()?);
                Ok((origin, row.get(0)?))
            })?;
            let q = q.filter_map(|r| r.map_err(|e| error!("mdc: {}", e)).ok());
            Ok(q.collect())
        })
    }

    #[inline]
    pub fn crate_rank(&self, origin: &Origin) -> FResult<f64> {
        let origin = origin.to_str();
        self.with_read("crate_rank", move |conn| {
            let mut query = conn.prepare_cached("SELECT ranking FROM crates WHERE origin = ?1")
                .map_err(|e| Error::Db(e, "r"))?;
            Ok(none_rows(query.query_row([&origin], |row| row.get(0)))?.unwrap_or(0.))
        })
    }

    /// List of all notable crates
    /// Returns origin, rank, last updated unix timestamp
    pub fn sitemap_crates(&self) -> FResult<Vec<(Origin, f64, i64)>> {
        self.with_read("sitemap_crates", |conn| {
            let mut q = conn.prepare(r#"
                SELECT origin, ranking, max(created) as last_update
                FROM crates c
                JOIN crate_versions v ON c.id = v.crate_id
                WHERE ranking > 0.33
                GROUP BY c.id
            "#)?;
            let q = q.query_map([], |row| -> Result<(Origin, f64, i64)> {
                Ok((Origin::from_str(row.get_ref_unwrap(0).as_str()?), row.get_unwrap(1), row.get_unwrap(2)))
            })?.filter_map(|r| r.map_err(|e| error!("sitemap: {}", e)).ok());
            Ok(q.collect())
        })
    }

    /// Number of crates and unique authors in every category
    pub fn category_crate_counts(&self) -> FResult<HashMap<String, (u32, u32)>> {
        self.with_read("category_crate_counts", |conn| {
            let mut q = conn.prepare(r#"
                SELECT c.slug, COUNT(DISTINCT a.crate_id) as crates, COUNT(DISTINCT a.github_id) as authors
                    FROM author_crates2 a
                    JOIN categories c USING (crate_id)
                    GROUP by c.slug;
            "#).map_err(|e| Error::Db(e, "ccc"))?;
            let q = q.query_map([], |row| -> Result<(String, (u32, u32))> {
                Ok((row.get_unwrap(0), (row.get_unwrap(1), row.get_unwrap(2))))
            })?.filter_map(|r| r.map_err(|e| error!("counts: {}", e)).ok());
            Ok(q.collect())
        })
    }

    /// When crates were first created, by category
    pub fn category_first_published_timestamps(&self) -> FResult<HashMap<SmolStr, Vec<u64>>> {
        self.with_read("category_pubs", |conn| {
            let mut q = conn.prepare(r#"
                SELECT max(relevance_weight) as group_sort_only_hack, c.slug, min(created) FROM categories c JOIN crate_versions v USING(crate_id) GROUP BY c.crate_id
            "#).map_err(|e| Error::Db(e, "ccp"))?;
            let mut out = HashMap::default();
            let mut q = q.query([])?;
            while let Some(row) = q.next()? {
                let slug = row.get_ref_unwrap(1).as_str().unwrap();
                let v = match out.get_mut(slug) {
                    Some(v) => v,
                    None => out.entry(SmolStr::from(slug)).or_insert_with(|| Vec::with_capacity(512)),
                };
                v.push(row.get_unwrap(2));
            }
            Ok(out)
        })
    }

    /// Crates overdue for an update
    pub fn crates_to_reindex(&self, limit: u32) -> FResult<Vec<Origin>> {
        self.with_read("crates_to_reindex", |conn| {
            let mut q = conn.prepare("SELECT origin FROM crates WHERE next_update < ?1 LIMIT ?2")?;
            let timestamp = Utc::now().timestamp() as u32;
            let q = q.query_map([&timestamp, &limit], |r| {
                let s = r.get_ref_unwrap(0).as_str()?;
                Ok(Origin::from_str(s))
            })?.filter_map(|r| r.map_err(|e| error!("reindx: {}", e)).ok());
            Ok(q.collect())
        })
    }

    pub fn update_keyword_precision(&self) -> FResult<()> {
        self.with_write("precupd", |conn| {
            conn.execute("
                UPDATE keywords
                SET precision = f.precision
                FROM (
                    SELECT (1.0+max(w))/(avg(w)*count(*)/50) as precision, id from (
                        select sum(1.0 + ck.weight * c.relevance_weight) as w, k.id, c.slug
                            from crate_keywords ck
                            join categories c on c.crate_id = ck.crate_id
                            join keywords k on k.id = ck.keyword_id
                            and c.relevance_weight > 0.1
                            group by ck.keyword_id, c.slug
                             having count(*) > 2
                        ) as inner
                        group by inner.id
                        having count(*) > 1
                    ) as f
                where keywords.id = f.id", [])?;
            Ok(())
        })
    }

    pub fn delete_crate(&self, origin: &Origin) -> FResult<()> {
        self.with_write("delete", |conn| {
            let origin_str = origin.to_str();
            let mut q = conn.prepare("DELETE from categories WHERE crate_id in (SELECT id FROM crates WHERE origin = ?1 LIMIT 1)")
                .map_err(|e| Error::Db(e, "del1"))?;
            q.execute([&origin_str])
                .map_err(|e| Error::Db(e, "del2"))?;
            let mut q = conn.prepare("DELETE from crate_keywords WHERE crate_id in (SELECT id FROM crates WHERE origin = ?1 LIMIT 1)")?;
            q.execute([&origin_str])?;
            let mut q = conn.prepare("DELETE from crate_repos WHERE crate_id in (SELECT id FROM crates WHERE origin = ?1 LIMIT 1)")?;
            q.execute([&origin_str])?;
            let mut q = conn.prepare("DELETE from crate_versions WHERE crate_id in (SELECT id FROM crates WHERE origin = ?1 LIMIT 1)")?;
            q.execute([&origin_str])?;
            let mut q = conn.prepare("DELETE from crates WHERE origin = ?1")?;
            q.execute([&origin_str])?;
            Ok(())
        })
    }

    pub fn raw_keyword_dump(&self, min_ranking: f32) -> FResult<(HashMap<usize, CrateId>, HashMap<u32, KeywordId>)> {
        self.with_read("kwdump", |conn| {
            let mut cid = conn.prepare("SELECT id, origin, ranking FROM crates WHERE ranking > ?1")?;
            let mut kid = conn.prepare("SELECT id, keyword, visible FROM keywords")?;
            let mut ck = conn.prepare("SELECT crate_id, keyword_id, weight, explicit FROM crate_keywords")?;

            let mut crates = cid.query_map([&min_ranking], |row| Ok((row.get_unwrap(0), CrateId {
                origin: Origin::from_str(row.get_ref(1)?.as_str()?),
                ranking: row.get(2).unwrap_or(0.), // ranking is NULL for broken or not-indexed-yet crates
                keywords: Vec::new(),
            })))?.collect::<Result<HashMap<usize, _>, _>>()?;

            let keywords = kid.query_map([], |row| Ok((row.get_unwrap(0), KeywordId {
                str: row.get_ref(1)?.as_str()?.into(),
                visible: row.get_unwrap(2),
            })))?.collect::<Result<HashMap<u32, _>, _>>()?;

            ck.query_map([], |row| {
                if let Some(c) = crates.get_mut(&row.get_unwrap(0)) {
                    c.keywords.push(CrateKeyword {
                        keyword_id: row.get_unwrap(1),
                        weight: row.get_unwrap(2),
                        explicit: row.get_unwrap(3),
                    });
                }
                Ok(())
            })?.collect::<Result<()>>()?;

            Ok((crates, keywords))
        })
    }
}

pub struct CrateKeyword {
    pub keyword_id: u32,
    pub weight: f32,
    pub explicit: bool,
}

pub struct CrateId {
    pub origin: Origin,
    pub ranking: f32,
    pub keywords: Vec<CrateKeyword>,
}

pub struct KeywordId {
    pub str: SmolStr,
    pub visible: bool,
}

fn unprefixed(mut s: &str) -> &str {
    if let Some((rest, "derive" | "impl" | "core" | "internal" | "sys" | "common")) = s.rsplit_once(['-','_']) {
        if rest.is_empty() {
            return s;
        }
        s = rest;
    }
    if let Some(("rust" | "cargo", rest)) = s.split_once(['-','_']) {
        if rest.is_empty() {
            return s;
        }
        s = rest;
    }
    if let Some((rest, "rs" | "cli" | "rust")) = s.rsplit_once(['-','_']) {
        if rest.is_empty() {
            return s;
        }
        return rest;
    }
    s.trim_start_matches("rust").trim_start_matches("lib")
}

fn find_parent_in_repo(mut paths: HashMap<SmolStr, (SmolStr, f32)>, repo_url: &Repo, child_name: &str) -> Option<Origin> {
    if paths.len() <= 1 {
        return None;
    }

    let (path_in_repo, &(_, child_rank)) = paths.iter().find(|&(_, (n, _))| n == child_name)?;
    let mut path_in_repo = &*path_in_repo.clone();
    let child_segments = path_in_repo.split('/').count();

    // keep child in there for later
    paths.retain(|path, (_, rank)| {
        // we only want parents or all crates are siblings
        path.split('/').count() <= child_segments &&
        // Remove potentially unused/junk crates
        *rank >= child_rank * 0.2
    });

    if !path_in_repo.is_empty() {
        loop {
            path_in_repo = path_in_repo.rsplit_once('/').unwrap_or_default().0;
            if let Some((parent_name, _)) = paths.get(path_in_repo) {
                return Origin::try_from_crates_io_name(parent_name);
            }
            if path_in_repo.is_empty() {
                // in these paths "" is the root
                break;
            }
        }
    }

    let repo_name = repo_url.repo_name().unwrap_or_default();
    let repo_base_name = unprefixed(repo_name);
    let url_owner_name = repo_url.url_owner_name().unwrap_or_default();

    // if there's no dir hierarchy, make one by names and ranking
    let mut by_hier_score: Vec<_> = paths.iter().map(|(path, (name, rank))| {
        let rank = (*rank * 10.) as u32;
        let name = &**name;
        let segments = path.split('/').count() as u16;
        let base_name = unprefixed(name);
        let matches =
            u8::from(child_name.contains(name)) +
            u8::from(child_name.contains(base_name)) +
            u8::from(name == repo_name) +
            u8::from(name == url_owner_name) +
            u8::from(base_name == name) + // no prefix
            u8::from(base_name == repo_name) +
            u8::from(base_name == repo_base_name) +
            u8::from(base_name == url_owner_name);

        // higher is better
        (matches, !segments, base_name, rank, name)
    }).collect();
    by_hier_score.sort_unstable();

    // find next after child
    let idx = by_hier_score.iter().position(|&(.., name)| name == child_name)?;
    by_hier_score.get((idx+1)..).unwrap_or_default()
        .iter()
        // -derive crate is never a parent
        .find(|(.., name)| !name.rsplit_once(['-','_']).is_some_and(|(_, s)| s == "derive"))
        .and_then(|(.., name)| Origin::try_from_crates_io_name(name))
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Default, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub enum SortOrder {
    #[default]
    Quality,
    Popularity,
    Recency,
}

pub enum RepoChange {
    Removed { crate_name: SmolStr, weight: f64 },
    Replaced { crate_name: SmolStr, replacement: SmolStr, weight: f64 },
}

pub struct KeywordInsert {
    /// k => (weight, explicit)
    keywords: HashMap<SmolStr, (f64, bool)>,
    ready: bool,
}

impl KeywordInsert {
    pub fn new() -> FResult<Self> {
        Ok(Self {
            keywords: HashMap::default(),
            ready: false,
        })
    }

    pub fn add(&mut self, word: &str, mut weight: f64, mut visible: bool) {
        if word.is_empty() || weight <= 0.000001 {
            return;
        }
        if word == "rust" || word == "rs" {
            return;
        }
        debug_assert_eq!(word, word.as_ascii_lowercase());
        if STOPWORDS.contains(word) {
            weight *= 0.1;
        }
        if word.len() > 50 { // too long
            visible = false;
        }
        self.add_raw(word.into(), weight, visible);
    }

    pub fn add_raw(&mut self, word: SmolStr, weight: f64, visible: bool) {
        if word.is_empty() {
            return;
        }
        if weight > 1. {
            warn!("Keyword {word} {visible} weight = {weight} > 1");
        }
        let weight = weight.min(1.);
        let k = self.keywords.entry(word).or_insert((weight, visible));
        if k.0 < weight {k.0 = weight}
        if visible {k.1 = visible}
    }

    /// Clears old keywords from the db
    pub fn before_save(&mut self, conn: &Connection, crate_id: u32) -> FResult<()> {
        let mut clear_keywords = conn.prepare_cached("DELETE FROM crate_keywords WHERE crate_id = ?1")?;
        clear_keywords.execute([&crate_id])?;
        self.ready = true;
        Ok(())
    }

    /// Call `before_save` first
    pub fn save(mut self, conn: &Connection, crate_id: u32, overall_weight: f64) -> FResult<()> {
        assert!(self.ready);

        let mut select_id = conn.prepare_cached("SELECT id, visible FROM keywords WHERE keyword = ?1")
            .map_err(|e| Error::Db(e, "prepare kw"))?;
        let mut insert_name = conn.prepare_cached("INSERT OR IGNORE INTO keywords (keyword, visible) VALUES (?1, ?2)")?;
        let mut insert_value = conn.prepare_cached("INSERT OR IGNORE INTO crate_keywords(keyword_id, crate_id, weight, explicit)
            VALUES (?1, ?2, ?3, ?4)")?;
        let mut make_visible = conn.prepare_cached("UPDATE keywords SET visible = 2 WHERE id = ?1")?;

        for (cond, stopwords) in COND_STOPWORDS.iter() {
            if self.keywords.contains_key(*cond) {
                match stopwords {
                    Some(stopwords) => for stop in *stopwords {
                        if let Some(k) = self.keywords.get_mut(*stop) {
                            k.0 /= 3.0;
                        }
                    },
                    None => for (_, &mut (ref mut w, _)) in self.keywords.iter_mut().filter(|(k, _)| k != cond) {
                        *w /= 2.0;
                    }
                }
            }
        }

        for (word, &(weight, visible)) in &self.keywords {
            let word = word.as_str();
            let keyword_id = if let Ok((keyword_id, old_vis)) = select_id.query_row([word], |r| Ok((r.get_unwrap::<_, u32>(0), r.get_unwrap::<_, u32>(1)))) {
                if visible && old_vis == 0 {
                    make_visible.execute([&keyword_id])?;
                }
                keyword_id
            } else {
                insert_name.execute((word, if visible { 1i32 } else { 0i32 }))?;
                select_id.query_row([word], |r| Ok(r.get_unwrap(0)))?
            };
            let weight = weight * overall_weight;
            insert_value.execute((keyword_id, crate_id, weight, if visible { 1i32 } else { 0i32 }))?;
        }
        Ok(())
    }
}

#[allow(deprecated)]
fn parse_invited_at(date_str: &str) -> Option<DateTime<Utc>> {
    DateTime::parse_from_rfc3339(date_str).map(|d| d.with_timezone(&Utc))
        .or_else(move |_| Utc.datetime_from_str(date_str, "%Y-%m-%d %H:%M:%S"))
        .map_err(move |e| error!("Can't parse {date_str}, because {e}")).ok()
}

fn crates_io_name(name: &str) -> std::result::Result<Origin, rusqlite::Error> {
    Origin::try_from_crates_io_name(name)
                    .ok_or_else(|| rusqlite::Error::ToSqlConversionFailure(format!("bad name {name}").into()))
}

fn hex_hash(s: &str) -> String {
    format!("*{}", blake3::hash(s.as_bytes()).to_hex())
}

struct CategoryCandidate {
    rank_weight: f64,
    category_relevance: f64,
    slug: SmolStr,
    /// false if guessed
    explicit: bool,
}

#[derive(Debug)]
pub struct CrateOwnerRow {
    pub origin: Origin,
    pub crate_ranking: f32,
    pub invited_by_github_id: Option<u32>,
    pub invited_at: Option<DateTime<Utc>>,
    pub deleted_at: Option<DateTime<Utc>>,
    pub latest_release: DateTime<Utc>,
    pub oldest_release: DateTime<Utc>,
}

#[inline]
fn none_rows<T>(res: std::result::Result<T, rusqlite::Error>) -> std::result::Result<Option<T>, rusqlite::Error> {
    match res {
        Ok(dat) => Ok(Some(dat)),
        Err(rusqlite::Error::QueryReturnedNoRows) => Ok(None),
        Err(err) => Err(err),
    }
}

#[test]
fn try_indexing() {
    let rt = tokio::runtime::Runtime::new().unwrap();
    let f = rt.spawn(async move {
    let t = tempfile::NamedTempFile::new().unwrap();

    let db = CrateDb::new(t.as_ref()).unwrap();
    let origin = Origin::from_crates_io_name("cratedbtest");
    let source_data = CrateVersionSourceData {
        capitalized_name: "captname".into(),
        ..Default::default()
    };
    let manifest = Manifest::from_str(r#"[package]
name="crates-indexing-unit-test-hi"
version="1.2.3"
keywords = ["test-CRATE", "t2"]
categories = ["1", "two", "GAMES", "science", "::science::math::"]
"#).unwrap();
    let new_derived = db.index_latest(CrateVersionData {
        source_data: &source_data,
        manifest: &manifest,
        origin: &origin,
        deps_stats: &[],
        is_build: false,
        is_dev: false,
        is_spam_or_hidden: false,
        authors: &[],
        category_slugs: &["science".into()],
        repository: None,
        cache_key: 1,
        visible_auto_keywords: &[(1., "auto-keyword".into())],
        invisible_auto_keywords: &[(1., "hidden-keyword".into())],
    }).unwrap();
    assert_eq!(1, db.crates_with_keyword("auto-keyword").unwrap());
    assert_eq!(0, db.crates_with_keyword("hidden-keyword").unwrap());
    assert_eq!(new_derived.categories.len(), 1); // uses slugs, not manifest
    assert_eq!(&*new_derived.categories[0], "science");
    });
    rt.block_on(f).unwrap();
}

