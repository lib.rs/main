// use std::sync::atomic::AtomicU64;
use std::sync::atomic::AtomicBool;
use std::time::Instant;

/// Context for page renders
#[derive(Debug)]
pub struct RendCtx {
    pub deadline: Instant,
    incomplete: AtomicBool,
    // last_modified_timestamp: AtomicU64,
}

impl RendCtx {
    #[must_use]
    pub fn new(deadline: Instant) -> Self {
        Self { deadline, incomplete: false.into() }
    }

    pub fn set_incomplete(&self) {
        self.incomplete.store(true, std::sync::atomic::Ordering::Relaxed);
    }

    pub fn is_incomplete(&self) -> bool {
        self.incomplete.load(std::sync::atomic::Ordering::Relaxed)
    }
}

