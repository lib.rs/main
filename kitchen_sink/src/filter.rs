use log::debug;
use fetcher::Fetcher;
use futures::executor::block_on;
use log::warn;
use render_readme::{FilteredImage, ImageFilter};
use repo_url::github_blob_url_to_raw_url;
use serde::{Deserialize, Serialize};
use simple_cache::{Error, TempCacheJson};
use std::borrow::Cow;
use std::path::Path;
use std::sync::Arc;
use std::time::{Duration, Instant};
use tokio::time::timeout_at;

#[derive(Clone, Copy, Deserialize, Serialize, Debug)]
struct ImageOptimImageMeta {
    width: u32,
    height: u32,
}

/// Filter through <https://imageoptim.com/api>
pub struct ImageOptimAPIFilter {
    /// Get one from <https://imageoptim.com/api/register>
    img_prefix: String,
    meta_prefix: String,
    cache: Arc<TempCacheJson<ImageOptimImageMeta>>,
    handle: tokio::runtime::Handle,
    throttle: tokio::sync::Semaphore,
}

impl ImageOptimAPIFilter {
    pub async fn new(api_id: &str, cache_path: impl AsRef<Path>, handle: tokio::runtime::Handle) -> Result<Self, Error> {
        Ok(Self {
            img_prefix: format!("https://img.gs/{api_id}/"),
            meta_prefix: format!("https://img.gs/{api_id}/meta,timeout=3/"),
            cache: Arc::new(TempCacheJson::new(cache_path, Arc::new(Fetcher::new(8)), Duration::from_secs(3600 * 24 * 7))?),
            handle,
            throttle: tokio::sync::Semaphore::new(10),
        })
    }
}

impl ImageFilter for ImageOptimAPIFilter {
    fn filter_url<'a>(&self, url: &'a str, wanted_width: Option<u32>, wanted_height: Option<u32>, container_max_width: u32, deadline: Instant) -> FilteredImage<'a> {
        // Readmes with /blob/ URLs work on GitHub by magic, but these URLs aren't real image URLs.
        let url = github_blob_url_to_raw_url(url).map(Cow::Owned).unwrap_or(Cow::Borrowed(url));

        // let some badges through, because they're SVG (don't need 2x scaling),
        // and show uncacheable info that needs to be up to date.
        // Can't let them all through, because of CSP.
        let no_proxying = url.starts_with("https://img.shields.io/") || url.starts_with("https://img.gs/");
        // SVG badges don't have reliable dimensions and get blown up to full page size
        let size_as_is = !no_proxying && (url.contains(".svg") || url.contains("svg=true") || url.contains("t=svg") || url.contains("buildstats.info"));

        let container_max_width = container_max_width.min(wanted_width.unwrap_or(container_max_width));

        let Some((width, height)) = self.image_size(&url, deadline) else {
            return FilteredImage {
                src: if no_proxying { url }
                    else if size_as_is { format!("{}full/{url}", self.img_prefix).into() }
                    else { format!("{}{container_max_width},2x/{url}", self.img_prefix).into() },
                srcset: None,
                width: None,
                height: None,
            };
        };

        let container_max_height = container_max_width * 4 / 3;
        let ratio = (f64::from(wanted_width.unwrap_or(container_max_width).min(container_max_width)) / f64::from(width))
            .min(f64::from(wanted_height.unwrap_or(container_max_height).min(container_max_height)) / f64::from(height))
            .min(1.);
        let width = ((f64::from(width) * ratio).round() as u32).max(1);
        let height = ((f64::from(height) * ratio).round() as u32).max(1);
        if ratio > 0.75 || no_proxying {
            FilteredImage {
                src: if no_proxying { url } else { format!("{}full/{url}", self.img_prefix).into() },
                srcset: None,
                width: Some(width),
                height: Some(height),
            }
        } else {
            FilteredImage {
                src: format!("{}{width}/{url}", self.img_prefix).into(),
                srcset: Some(format!("{}{width},2x/{url} 2x", self.img_prefix).into()),
                width: Some(width),
                height: Some(height),
            }
        }
    }
}

impl ImageOptimAPIFilter {
    fn image_size(&self, image_url: &str, deadline: Instant) -> Option<(u32, u32)> {
        let api_url = format!("{}{image_url}", self.meta_prefix);
        let c = blocking::block_in_place(image_url, || self.cache.get(image_url));
        if let Ok(Some(ImageOptimImageMeta { width, height })) = c {
            return Some((width, height));
        }
        debug!("is uncached, needs fetching: {image_url}");
        blocking::watch(format_args!("img miss {image_url}"), move || {
            let rt = self.handle.enter();
            let _g = block_on(blocking::awatch("throtl", timeout_at(deadline.into(), self.throttle.acquire()))).ok()?;
            let image_url_owned = image_url.to_owned();
            let cache = Arc::clone(&self.cache);
            let cache_future = blocking::awatch("spw1n", self.handle.spawn(timeout_at(deadline.into(), async move {
                blocking::awatch(format!("inner img {image_url_owned}"), cache.get_json(&image_url_owned, api_url, |f| f)).await
            })));
            let ImageOptimImageMeta { width, height } = blocking::watch("blockon im", || block_on(cache_future))
                .map_err(|e| warn!("spawn failed for {image_url}: {e}")).ok()?
                .map_err(|e| warn!("image req to meta of {image_url} timed out: {e}")).ok()?
                .map_err(|e| warn!("image req to meta of {image_url} failed: {e}"))
                .ok()??;
            drop(rt);
            Some((width, height))
        })
    }
}
