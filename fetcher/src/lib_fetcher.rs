pub use reqwest::header::{self, HeaderMap};

use std::panic::{RefUnwindSafe, UnwindSafe};
use std::time::Duration;
use tokio::sync::SemaphorePermit;
use tokio::time::timeout;

#[derive(Debug)]
pub struct Fetcher {
    client: reqwest::Client,
    sem: tokio::sync::Semaphore,
    sem_timeout: u16,
}

impl UnwindSafe for Fetcher {}
impl RefUnwindSafe for Fetcher {}

use quick_error::quick_error;

quick_error! {
    #[derive(Debug)]
    pub enum Error {
        Req(err: reqwest::Error) {
            display("Fetch error ({})", err)
            source(err)
            from()
        }
        Res(url: Box<str>, err: String) {
            display("{}: {}", url, err)
        }
        Timeout(url: String) {
            display("Timed out {}", url)
            from(s: String) -> (s)
        }
    }
}

impl Fetcher {
    #[must_use]
    pub fn new(max_concurrent: u16) -> Self {
        Self::new_with_headers(max_concurrent, HeaderMap::new())
    }

    #[must_use]
    pub fn new_with_headers(max_concurrent: u16, default_headers: HeaderMap) -> Self {
        let client = reqwest::Client::builder()
            .default_headers(default_headers)
            .https_only(true)
            .no_deflate()
            .brotli(true)
            .user_agent("lib.rs/1.3")
            .connect_timeout(Duration::from_secs(5))
            .build()
            .unwrap();

        Self {
            client,
            sem_timeout: (max_concurrent + 4).max(5),
            sem: tokio::sync::Semaphore::new(max_concurrent.into()),
        }
    }

    pub async fn post<U: serde::de::DeserializeOwned, T: serde::Serialize>(&self, url: &str, body: &T) -> Result<U, Error> {
        let _s = self.lock(url).await?;

        let res = timeout(Duration::from_secs(38), self.client.post(url).json(body).send()).await
            .map_err(|_| format!("POST to {url}"))??;
        if res.status().is_success() {
            Ok(timeout(Duration::from_secs(61), res.json()).await
                    .map_err(|_| format!("downloading JSON {url}"))??)
        } else {
            let why = timeout(Duration::from_secs(3), res.text()).await
                .map_err(|_| format!("downloading error page {url}"))??;
            Err(Error::Res(url.into(), why))
        }
    }

    pub async fn fetch(&self, url: &str) -> Result<Vec<u8>, Error> {
        let _s = self.lock(url).await?;

        let res = timeout(Duration::from_secs(21), self.client.get(url)
            .send()).await
            .map_err(|_| format!("request to {url}"))??
            .error_for_status()?;
        Ok(timeout(Duration::from_secs(61), res.bytes()).await
            .map_err(|_| format!("download of {url}"))??
            .to_vec())
    }

    async fn lock(&self, url: &str) -> Result<SemaphorePermit<'_>, Error> {
        if let Ok(s) = self.sem.try_acquire() {
            log::info!("REQ (start) {url}");
            Ok(s)
        } else {
            log::info!("REQ (waiting up to {}s) {url}", self.sem_timeout);
            let now = std::time::Instant::now();
            let s = timeout(Duration::from_secs(self.sem_timeout.into()), self.sem.acquire()).await
                .map_err(|_| format!("waiting to start {url}"))?
                .map_err(|e| Error::Timeout(e.to_string()))?;
            log::debug!("REQ (starts after {}ms) {url}", now.elapsed().as_millis() as u32);
            Ok(s)
        }
    }
}
