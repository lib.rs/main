use util::FxHashMap as HashMap;
use heck::AsKebabCase;
use itertools::Itertools;
use log::error;
use std::borrow::Cow;
use std::collections::hash_map::Entry;
use std::path::Path;
use std::{fs, io};
use util::{smol_fmt, SmolStr};

pub struct Synonyms {
    mapping: HashMap<SmolStr, (SmolStr, u8)>,
}

impl Synonyms {
    pub fn new(data_dir: &Path) -> io::Result<Self> {
        let lines = fs::read_to_string(data_dir.join("tag-synonyms.csv"))?;
        let mut mapping = HashMap::<SmolStr, (SmolStr, u8)>::with_capacity_and_hasher(2500, Default::default());
        let mut needs_fixing = false;
        for l in lines.lines().filter(|l| !l.starts_with('#') && !l.is_empty()) {
            let fail = move || io::Error::new(io::ErrorKind::InvalidData, format!("synonyms error at: {l}"));
            let mut cols = l.splitn(3, ',');
            let find = SmolStr::from(cols.next().ok_or_else(fail)?);
            let replace = SmolStr::from(cols.next().ok_or_else(fail)?);
            let score: u8 = cols.next().and_then(|p| p.parse().ok()).ok_or_else(fail)?;
            match mapping.entry(find) {
                Entry::Occupied(mut e) => if e.get().1 < score {
                    error!("duplicate synonym {l} and {}", e.get().0);
                    needs_fixing = true;
                    e.insert((replace, score));
                },
                Entry::Vacant(e) => { e.insert((replace, score)); }
            }
        }
        if cfg!(debug_assertions) {
            let mut dump = Vec::new();
            for k in mapping.keys() {
                let mut s = &**k;
                if std::iter::from_fn(|| { s = &*mapping.get(s)?.0; Some(s) }).take(21).count() >= 20 {
                    error!("Loop at {k},{s}");
                    needs_fixing = true;
                    dump.push(k.clone());
                }
            }
            for k in dump {
                mapping.remove(&k);
            }
            if needs_fixing {
                let mut fixed = mapping.iter().map(|(k, (v, n))| {
                   format!("{k},{v},{n}")
                }).collect::<Vec<String>>();
                fixed.sort();
                fs::write(data_dir.join("tag-synonyms.csv"), fixed.join("\n")).unwrap();
            }
        }
        Ok(Self { mapping })
    }

    #[inline]
    #[must_use]
    pub fn get(&self, keyword: &str) -> Option<(&str, f32)> {
        let (tag, votes) = self.mapping.get(keyword)?;
        let relevance = (f32::from(*votes) / 5. + 0.1).min(1.);
        Some((tag, relevance))
    }

    fn get_matching(&self, keyword: &str, min_votes: u8) -> Option<&str> {
        let (tag, votes) = self.mapping.get(keyword)?;
        if *votes >= min_votes {
            return Some(tag);
        }
        None
    }

    #[must_use]
    pub fn max_normalize<'a>(&'a self, keyword: &'a str) -> Cow<'a, str> {
        self.max_normalize_inner(&keyword, 2)
    }

    fn max_normalize_inner<'a>(&'a self, keyword: &'a str, mut depth: u8) -> Cow<'a, str> {
        let mut keyword = Cow::Borrowed(self.mapping.get(keyword).map(|(k,_)| {
            self.mapping.get(k.as_str()).map(|(k2,_)| k2.as_str()).unwrap_or(k)
        }).unwrap_or(keyword));
        if depth == 0 {
            return keyword;
        }
        depth -= 1;
        let mut has_multiple_hyphens = false;
        if let Some((start, end)) = keyword.split_once('-') {
            if end.contains('-') {
                has_multiple_hyphens = true;
            }
            let start2 = self.max_normalize_inner(start, depth);
            let end2 = self.max_normalize_inner(end, depth);
            if start2 != start || end2 != end {
                keyword = if start2 != end2 {
                    format!("{start2}-{end2}").into()
                } else {
                    start2.to_string().into()
                };
            }
        }
        if has_multiple_hyphens {
            if let Some((start, end)) = keyword.rsplit_once('-') {
                let start2 = self.max_normalize_inner(start, depth);
                let end2 = self.max_normalize_inner(end, depth);
                if start2 != start || end2 != end {
                    keyword = format!("{start2}-{end2}").into();
                }
            }
        }
        keyword
    }

    #[must_use]
    pub fn normalize<'a>(&'a self, keyword: &'a str, min_votes: u8) -> &'a str {
        if let Some(alt) = self.get_matching(keyword, 2.max(min_votes).min(5)) {
            debug_assert_ne!(alt, keyword);
            if let Some(alt2) = self.get_matching(alt, 4.max(min_votes + 1).min(5)) {
                debug_assert_ne!(alt2, keyword);
                return alt2;
            }
            return alt;
        }
        keyword
    }

    #[inline]
    pub fn map(&self, word: SmolStr) -> SmolStr {
        if let Some(w) = self.get_matching(&word, 2) {
            w.into()
        } else {
            word
        }
    }
}

#[must_use]
#[cfg_attr(debug_assertions, track_caller)]
pub fn normalize_keyword(k_input: &str) -> SmolStr {
    debug_assert!(!k_input.trim_start().is_empty(), "empty kw: '{k_input}' {}", k_input.len());
    if k_input.as_bytes().iter().all(|&b| (b.is_ascii_lowercase() && b.is_ascii_alphanumeric()) || b == b'-') {
        return k_input.trim_matches('-').into();
    }

    let k = k_input.split(|c: char| c.is_ascii_whitespace() || c == '_' || c == '-').map(|k| {
        let mut k = match k.trim_end_matches("'s").trim_end_matches("’s") {
            "I/O" => "io",
            "i/o" => "io",
            "C++" => "cpp",
            "c++" => "cpp",
            "IoT" => "iot",
            "NaN" => "nan",
            "TeX" => "tex",
            "LaTeX" => "latex",
            other => other,
        };

        // SSDs, APIs, etc.
        let mut chars = k.chars();
        if let Some((last, before_last)) = chars.next_back().zip(chars.next_back()) {
            if k.len() > 2 && last == 's' && before_last.is_ascii_uppercase() {
                k = k.trim_end_matches('s');
            }
        }

        let mut k = Cow::Borrowed(k);

        if k.contains('%') {
            k = k.replace('%', "percent").into();
        }

        if k.contains('&') {
            k = k.replace('&', " and ").into();
        }

        // MiB, GiB
        if k.ends_with("iB") {
            k.to_mut().make_ascii_lowercase();
        }

        // iOS etc.
        let mut chars = k.chars();
        if let Some((first, second)) = chars.next().zip(chars.next()) {
            if first.is_ascii_lowercase() && second.is_ascii_uppercase() {
                k.to_mut().make_ascii_lowercase();
            }
        }

        // i-os and java-script look bad
        if k.contains("Script") {
            k.to_mut().make_ascii_lowercase();
        }

        k
    })
    .filter(|k| !k.is_empty())
    .take(30)
    .join(" ");

    // heck messes up CJK
    if !k.is_ascii() {
        return k.chars().flat_map(|c| if c.is_ascii_punctuation() || c.is_ascii_whitespace() { '-' } else { c }.to_lowercase()).collect();
    }

    let mut res = smol_fmt!("{}", AsKebabCase(k.as_str()));
    if cfg!(debug_assertions) && res.trim_start().is_empty() {
        log::error!("'{k_input}' created empty keyword");
    }
    if res.len() > 65 && res.get(..55).is_some() {
        res.truncate(55);
        res.push('…');
    }
    res
}

#[test]
fn loads_data_file() {
    Synonyms::new("../data/".as_ref()).unwrap();
}
