use log::{debug, error};
use rusqlite::Connection;
use serde::de::DeserializeOwned;
use serde::Serialize;
use std::marker::PhantomData;
use std::path::Path;
use std::sync::{Arc, Mutex, MutexGuard, Weak};
use std::time::Duration;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("event db error")]
    Db(#[from] #[source] rusqlite::Error),
    #[error("disconnected")]
    Gone,
    #[error("event db connection error")]
    Connection,
    #[error("serialize error")]
    Ser(#[from] #[source] rmp_serde::encode::Error),
    #[error("deserialize error")]
    De(#[from] #[source] rmp_serde::decode::Error),
}

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Clone)]
pub struct EventLog<T> {
    db: Arc<Db>,
    _event_t: PhantomData<fn(T)>,
}

#[derive(Debug)]
struct Db {
    db: Mutex<Connection>,
}

impl Db {
    fn db(&self) -> Result<MutexGuard<'_, Connection>> {
        Ok(self.db.lock().unwrap())
    }
}

#[derive(Debug)]
pub struct Subscription<T> {
    name: String,
    db: Weak<Db>,
    _event_t: PhantomData<fn(T)>,
}

// clone is a bad derive bound
impl<T: DeserializeOwned + Serialize + Clone> EventLog<T> {
    /// Store events at this location
    pub fn new(path: impl AsRef<Path>) -> Result<Self> {
        let path = path.as_ref();
        let db = Connection::open(path)?;
        db.busy_timeout(Duration::from_secs(60))?;
        db.execute_batch("
            CREATE TABLE IF NOT EXISTS events(id INTEGER PRIMARY KEY AUTOINCREMENT, data BLOB NOT NULL);
            CREATE TABLE IF NOT EXISTS subscribers(name TEXT PRIMARY KEY, last_event_id INTEGER NOT NULL);
        ")?;
        Ok(Self {
            db: Arc::new(Db {
                db: Mutex::new(db),
            }),
            _event_t: PhantomData,
        })
    }

    /// Create or continue event observation
    pub fn subscribe(&self, name: impl Into<String>) -> Result<Subscription<T>> {
        let name = name.into();
        let db = self.db.db()?;
        let mut q = db.prepare_cached("INSERT OR IGNORE INTO subscribers(name, last_event_id) VALUES(?1, ?2)")?;
        q.execute((&name, 0i32))?;
        Ok(Subscription {
            name,
            db: Arc::downgrade(&self.db),
            _event_t: PhantomData,
        })
    }

    /// Fire an event
    pub fn post(&self, event: &T) -> Result<()> {
        let event_bytes = rmp_serde::to_vec_named(event)?;
        let db = self.db.db()?;
        let mut q = db.prepare_cached("INSERT INTO events(data) VALUES(?1)")?;
        q.execute([&event_bytes])?;
        Ok(())
    }
}

#[derive(Debug)]
pub struct EventBatch<'sub, T: DeserializeOwned + Serialize + Clone> {
    db: Arc<Db>,
    allow_ack_on_drop: bool,
    ack: Option<u64>,
    sub: &'sub Subscription<T>,
    events: Vec<(u64, Vec<u8>)>,
    _event_t: PhantomData<fn(T)>,
}

impl<T: DeserializeOwned + Serialize + Clone> EventBatch<'_, T> {
    #[must_use] pub fn is_empty(&self) -> bool {
        self.events.is_empty()
    }

    pub fn allow_auto_ack(&mut self, allow_ack_on_drop: bool) {
        self.allow_ack_on_drop = allow_ack_on_drop;
    }

    pub fn ack(&mut self) -> Result<(), Error> {
        if let Some(ack) = self.ack {
            blocking::block_in_place("ack", || {
                let conn = self.db.db()?;
                self.sub.mark_ack(ack, &conn)
            }).map_err(|e| { error!("drop-ack: {}", e); e })
        } else {
            Ok(())
        }
    }
}

impl<T: DeserializeOwned + Serialize + Clone> Iterator for EventBatch<'_, T> {
    type Item = Result<T>;

    fn next(&mut self) -> Option<Self::Item> {
        let (k, v) = self.events.pop()?;
        self.ack = Some(k);
        Some(rmp_serde::from_slice(&v).map_err(From::from))
    }
}

impl<T: DeserializeOwned + Serialize + Clone> Drop for EventBatch<'_, T> {
    fn drop(&mut self) {
        if std::thread::panicking() || !self.allow_ack_on_drop {
            return;
        }
        let _ = self.ack();
    }
}

impl<T: DeserializeOwned + Serialize + Clone> Subscription<T> {
    fn mark_ack(&self, id: u64, db: &Connection) -> Result<()> {
        let mut q = db.prepare_cached("INSERT OR REPLACE INTO subscribers(name, last_event_id) VALUES(?1, ?2)")?;
        q.execute((&self.name, id))?;
        debug!("ACKd events of {} up to {}", self.name, id);
        Ok(())
    }

    fn fetch_batch(&self, batch_size: usize) -> Result<EventBatch<'_, T>> {
        // TODO: some kind of lock against concurrent access, so that last_event_id isn't messed up
        let db_arc = self.db.upgrade().ok_or(Error::Gone)?;
        let events = {
            let db = db_arc.db()?;
            let mut q = db.prepare_cached(
                "SELECT e.id, e.data
                FROM events e WHERE e.id > (SELECT last_event_id FROM subscribers WHERE name = ?1)
                ORDER BY e.id LIMIT ?",
            )?;
            let mut events = q.query_map((&self.name, batch_size), |row| Ok((row.get(0)?, row.get(1)?)))?.collect::<Result<Vec<_>, _>>()?;
            events.reverse(); // batch iterator pops them!
            events
        };
        Ok(EventBatch {
            sub: self,
            allow_ack_on_drop: true,
            ack: None,
            events,
            db: db_arc,
            _event_t: PhantomData,
        })
    }

    pub async fn next_batch(&mut self, batch_size: usize) -> Result<EventBatch<'_, T>> {
        let mut wait = 2;
        loop {
            let batch = blocking::block_in_place(format_args!("batch w={wait}"), || {
                self.fetch_batch(batch_size)
            })?;
            if !batch.is_empty() {
                debug!("found event batch for {} with events {}-{}", self.name, batch.events[0].0, batch.events.last().unwrap().0);
                return Ok(batch);
            }
            tokio::time::sleep(Duration::from_secs(wait)).await;
            if wait < 10 { wait += 1; }
        }
    }
}
