#![allow(clippy::manual_range_contains)]

use log::debug;
use cargo_toml::Edition;
use util::CowAscii;
mod scorer;
pub use self::scorer::*;
use cargo_toml::MaintenanceStatus;
use chrono::prelude::*;
use itertools::Itertools;
use render_readme::{Handle, NodeData};
use rich_crate::{Author, CrateOwner, CrateVersion, OwnerKind, TractionStats};
use semver::Version as SemVer;
#[cfg(test)]
use std::time::{Duration, Instant};

/// Only changes when a new version is released
pub struct CrateVersionInputs<'a> {
    pub versions: &'a [CrateVersion],
    pub description: &'a str,
    pub readme: Option<&'a Handle>,
    pub owners: &'a [CrateOwner],
    pub authors: &'a [Author],

    pub repo_is_forked: bool,
    pub contributors: Option<u32>, // based on source history
    // contributors can be easily faked by pushing git history, so cross-check with other activity
    pub stargazers_count: Option<u32>,
    pub open_issues_count: Option<u32>,
    pub crates_share_of_the_repo: f32,

    /// How old contributor's account was when this version has been released
    pub release_contributors: Vec<ExperienceAtReleaseDate>,

    pub edition: Edition,
    pub is_app: bool,
    pub has_build_rs: bool,
    pub has_code_of_conduct: bool,
    pub has_links: bool,
    pub has_documentation_link: bool,
    pub has_homepage_link: bool,
    pub has_repository_link: bool,
    pub has_verified_repository_link: bool,
    pub has_keywords: bool,
    pub has_own_categories: bool,
    pub has_features: bool,
    // pub has_lockfile: bool,
    pub has_changelog: bool,
    pub license: &'a str,
    pub has_badges: bool,
    pub maintenance: MaintenanceStatus,
    pub is_nightly: bool,

    pub total_code_lines: u32,
    pub rust_comment_lines: u32,
    pub rust_code_lines: u32,

    /// 0..1, judged clippy warnings
    pub clippy_fail_score: f32,
    // (relative) weight of dependencies?

    // rust loc
    // #[test] cases
    // assert! calls
    // comments ratio (normalized to project size)

    // look for deprecated in the description
}

/// Changes over time, but doesn't depend on crate's own ranking
pub struct CrateTemporalInputs<'a> {
    pub owners: &'a [CrateOwner],
    pub owner_trust: &'a [f64],

    pub versions: &'a [CrateVersion],
    // 1.0 fresh, 0.0 totally outdated and deprecated
    pub dependency_freshness: Vec<f32>,
    pub downloads_per_month: u32,
    /// Looking at downloads of direct dependencies.
    /// This way internal derive/impl/core crates that have one big user get 0 here.
    pub downloads_per_month_minus_most_downloaded_user: u32,
    pub is_app: bool,
    pub is_proc_macro: bool,
    pub has_docs_rs: bool,
    pub is_nightly: bool,

    // low priority, because it's unranked! it'll be re-evaluated later
    pub number_of_direct_reverse_deps: u32,
    /// use max(runtime, dev, build), because the crate is going to be one of these kinds
    pub number_of_indirect_reverse_deps: u32,
    /// Includes non-optional (i.e. it's the upper bound, not just the optional ones)
    pub number_of_indirect_reverse_optional_deps: u32,

    /// Whether Debian has packaged this crate
    pub is_in_debian: bool,
    // most recent commit
    // avg time issues are left unanswered?
    // pub crate_score_context_free: f64,
    // pub owner_pageranks: Vec<f32>,
    // pub reverse_deps_rankings: Vec<f32>,
    pub traction_stats: Option<TractionStats>,

    pub has_vet_reviews: bool,
    /// trusting authors
    pub has_vet_trust: bool,
    pub has_crev_reviews: bool,
}

/// Used to calculate experience
#[derive(Debug, Copy, Clone)]
pub struct ExperienceAtReleaseDate {
    /// days since first crate
    pub rust_age: u32,
    /// days since gh account creation
    pub gh_age: u32,
    /// contributon to the current crate, biased towards owners
    pub contrib_weight: f64,
    /// owner trust score from page-rank-like collaboration (crate_owners_trust())
    /// - it's decreased for old releases, because the value is calculated on latest data
    /// and not state at the time of the release
    pub trust: f64,
}

pub struct Env {
    pub max_recent_downloads: u32,
    pub max_crates: u32,
}

fn cargo_toml_score(cr: &CrateVersionInputs<'_>) -> Score {
    let mut s = Score::new();

    s.frac("description len", 30, (cr.description.len() as f64 / 300.).min(1.));

    // build.rs slows compilation down, so better not use it unless necessary (links means a sys create, so somewhat justified)
    s.n("build.rs", 10, if !cr.has_build_rs && !cr.has_links {10} else if cr.has_links {5} else {0});

    // probably less buggy than if winging it
    s.has("has_code_of_conduct", 10, cr.has_code_of_conduct); // FIXME: should check git
    // probably optimized

    // docs are very important (TODO: this may be redundant with docs.rs)
    s.has("has_documentation_link", 20, cr.has_documentation_link);
    s.has("has_homepage_link", 15, cr.has_homepage_link);
    s.has("has_changelog", 5, cr.has_changelog);

    // we care about being able to analyze
    s.has("has_repository_link", 10, cr.has_repository_link);
    s.has("has_verified_repository_link", 15, cr.has_verified_repository_link);

    // helps lib.rs show crate in the right place
    s.has("has_keywords", 7, cr.has_keywords);
    s.has("has_categories", 5, cr.has_own_categories);

    // probably non-trivial crate
    s.has("has_features", 4, cr.has_features);

    // it's the best practice, may help building old versions of the project
    // s.has("has_lockfile", 5, cr.has_lockfile);
    // assume it's CI, which helps improve quality
    // TODO: detect travis, etc. without badges, since they're deprecated now
    s.has("has_badges", 1, cr.has_badges);

    s.n("maintenance status", 30, match cr.maintenance {
        MaintenanceStatus::ActivelyDeveloped => 30,
        MaintenanceStatus::Experimental => 19,
        MaintenanceStatus::None => 20,
        MaintenanceStatus::PassivelyMaintained => 10,
        MaintenanceStatus::AsIs => 5,
        MaintenanceStatus::LookingForMaintainer => 4,
        MaintenanceStatus::Deprecated => 0,
    });

    if cr.is_nightly {
        s.has("works on stable", 20, false);
    }
    // fresh
    s.n("edition", 5, match cr.edition { Edition::E2015 => 0, Edition::E2018 => 4, _ => 5 });

    let common_permissive = cr.license.contains("MIT") || cr.license.contains("BSD") ||
        cr.license.contains("Apache") || cr.license.contains("CC0") ||
        cr.license.contains("IJG") || cr.license.contains("Zlib");
    let common_relaxed_copyleft = cr.license.contains("MPL");
    let common_copyleft = cr.license.contains("GPL") || cr.license.contains("CC-BY-SA");
    // license proliferation is bad
    s.n("useful license", 10, if cr.is_app {
        // for end-user apps assume user freedom > developer freedom
        if common_copyleft || common_relaxed_copyleft {10} else if common_permissive {8} else {0}
    } else {
        // for libs assume developer freedom > user freedom
        if common_permissive {10} else if common_relaxed_copyleft {9} else if common_copyleft {1} else {0}
    });

    s
}

#[derive(Default)]
struct MarkupProps {
    text_len: usize,
    code_len: usize,
    list_or_table_rows: u16,
    images: u16,
    pre_blocks: u16,
    sections: u16,
}

fn fill_props(node: &Handle, props: &mut MarkupProps, mut in_code: bool) {
    match node.data {
        NodeData::Text { ref contents } => {
            let len = contents.borrow().trim().len();
            if len > 0 {
                if in_code {
                    props.code_len += len + 1; // +1 to account for separators that were trimmed
                } else {
                    props.text_len += len + 1;
                }
            }
            return; // has no children
        },
        NodeData::Element { ref name, ref attrs, .. } => {
            match name.local.get(..).unwrap() {
                "img" => {
                    if let Some(src) = attrs.borrow().iter().find(|a| a.name.local.get(..).unwrap() == "src") {
                        if render_readme::is_badge_url(&src.value) {
                            return; // don't count badges
                        }
                    }
                    props.images += 1;
                    return;
                },
                "li" | "tr" => props.list_or_table_rows += 1,
                "a" => {
                    if let Some(href) = attrs.borrow().iter().find(|a| a.name.local.get(..).unwrap() == "href") {
                        if render_readme::is_badge_url(&href.value) {
                            return; // don't count badge image children
                        }
                    }
                },
                "pre" => {
                    in_code = true;
                    props.pre_blocks += 1;
                },
                "code" => in_code = true,
                "h1" | "h2" | "h3" | "h4" | "h5" => props.sections += 1,
                _ => {},
            }
        },
        _ => {},
    }
    for child in node.children.borrow().iter() {
        fill_props(child, props, in_code);
    }
}

fn readme_score(readme: Option<&Handle>, little_code_expected: bool) -> Score {
    let mut s = Score::new();
    let mut props = Default::default();
    if let Some(readme) = readme {
        fill_props(readme, &mut props, false);
    }
    s.frac("text length", 75, (props.text_len as f64 / 3000.).min(1.0));
    // code examples are not expected for apps
    s.frac("code length", if little_code_expected { 25 } else { 100 }, (props.code_len as f64 / 2000.).min(1.0));
    s.n("code blocks", if little_code_expected { 15 } else { 25 }, props.pre_blocks * 5);
    s.has("has code", if little_code_expected { 10 } else { 30 }, props.code_len > 150 && props.pre_blocks > 0); // people really like seeing a code example
    s.n("images", 35, props.images * 25); // I like pages with logos
    s.n("sections", 30, props.sections * 4);
    s.n("list or table rows", 25, props.list_or_table_rows * 2);
    s
}

fn versions_score(ver: &[CrateVersion]) -> Score {
    let mut s = Score::new();
    let semver = ver.iter()
        .filter(|s| !s.yanked)
        .filter_map(|s| SemVer::parse(&s.num).ok())
        .collect::<Vec<_>>();

    let non_ancient_releases = ver.iter()
        .filter(|s| !s.yanked && s.created_at.year() >= 2017)
        .take(2)
        .count();
    s.has("more than one release", 20, non_ancient_releases > 1);

    if semver.is_empty() { // all yanked
        return s;
    }

    let oldest = ver.iter().map(|v| &v.created_at).min().copied();
    let newest = ver.iter().map(|v| &v.created_at).max().copied();
    if let (Some(oldest), Some(newest)) = (oldest, newest) {
        s.n("development history", 40, (newest - oldest).num_days() / 11);

        s.has("not ancient", 10, newest.year() >= 2019); // such old Rust crates are all suspicious
    }

    // don't count 0.0.x
    s.n("number of non-experimental releases", 15, semver.iter().filter(|v| (v.major > 0 || v.minor > 0) && v.pre.is_empty()).count() as u32);

    // patch releases are correlated with stable, polished code
    s.n("patch releases", 15, 3 * semver.iter().filter(|v| v.major > 0 && v.patch > 0).count() as u32);
    s.n("a high patch release", 10, semver.iter()
        .filter(|v| v.major < 1000) // ignore cal-ver and other non-semver
        .map(|v| v.patch as u32 / if v.major < 1 { 2 } else { 1 }).max().unwrap_or(0));
    // for 0.x crates it's hard to know what is a patch release
    s.has("an unstable patch/feature release", 3, semver.iter().any(|v| v.major == 0 && v.patch > 1));
    // careful release process is a sign of maturity
    s.has("a prerelease", 5, semver.iter().any(|v| !v.pre.is_empty()));
    s.has("a stable release", 10, semver.iter().any(|v| v.major > 0 && v.major < 20));
    s.has("yanked", 2, ver.iter().any(|v| v.yanked)); // author cares to remove bad versions
    s
}

fn authors_score(cr: &CrateVersionInputs<'_>) -> Score {
    // trust/reputation score is not used here, because it's indexed separately, and varies over time
    // see temporal inputs for trust.
    let mut s = Score::new();
    let owners = cr.owners.len() as u32;
    // fudged fallback authors field is now optional
    let authors = (cr.authors.len() as u32).max(owners + if owners > 1 { 1 } else { 0 });
    s.has("more than one owner", 4, owners > 1);
    s.n("authors", 4, authors);

    // estimate of Rust experience - first release of the first crate is assumed to be worse
    // than releases after programming for >1 year
    // old GH accounts get small boost under assumption of some transferable experience from other langs
    let mut score_sum = 0.;
    let mut weight_sum = 0.;
    for exp in &cr.release_contributors {
        const RUST_EXP_AGE_MAX: u32 = 6*365;
        const GH_EXP_AGE_MAX: u32 = 12*365;

        let rust_age = exp.rust_age.min(RUST_EXP_AGE_MAX);
        let gh_age = exp.gh_age.min(GH_EXP_AGE_MAX);

        let rust_exp = rust_age.saturating_sub(365) * rust_age / RUST_EXP_AGE_MAX;
        const RUST_EXP_MAX: u32 = RUST_EXP_AGE_MAX - 365;

        let gh_exp = gh_age.saturating_sub(3*365) / 10;
        const GH_EXP_MAX: u32 = GH_EXP_AGE_MAX.saturating_sub(3*365) / 10;

        // trust level is computed from things that imply skills and experience
        let exp_from_trust = (exp.trust / 200.).min(0.25);

        let score = f64::from(rust_exp + gh_exp) / f64::from(RUST_EXP_MAX + GH_EXP_MAX) + exp_from_trust;

        debug!("cio={rust_age}d + gh={gh_age}d = {score} * w={}", exp.contrib_weight);

        score_sum += score.min(1.) * exp.contrib_weight;
        weight_sum += exp.contrib_weight;
    }

    let overall_contributor_experience = score_sum / (weight_sum + 0.0001);
    s.frac("experience", 6, overall_contributor_experience);

    // fallback assumes true, as otherwise it'd be github-centric
    let has_open_issues = cr.open_issues_count.unwrap_or(1) > 0;
    // authors can be easily faked, so cap relative to number of owners, which are harder to fake
    let fallback_contributors = owners + authors.min(owners + 2);
    let num_contributors = cr.contributors.unwrap_or(fallback_contributors).max(fallback_contributors)
        // if you have contributors but not stars, it's suspicious
        .min(cr.stargazers_count.unwrap_or(400)/5)
        // I don't believe when you have lots of contributors and inbox 0
        .min(if has_open_issues { 100 } else { 30 });

    let contributors = f64::from(num_contributors)
        // monorepos shouldn't lift all crates in there
        * f64::from(cr.crates_share_of_the_repo)
        // contributors likely came with the forked history, so check for other activity
        * if cr.repo_is_forked && cr.stargazers_count.unwrap_or(0) < (200+num_contributors*5) {
            if has_open_issues { 0.8 } else { 0.4 }
        } else { 1. };
    s.frac("contributors", 6, (contributors / 10.).min(1.));
    s.frac("lots contributors", 4, (contributors / 100.).min(1.));
    s
}

fn code_score(cr: &CrateVersionInputs<'_>) -> Score {
    let mut s = Score::new();
    if cr.total_code_lines == 0 {
        log::warn!("no code found?");
    }
    if cr.total_code_lines != 0 && cr.total_code_lines < 100 { // if 0 maybe a bug?
        s.has("HelloWorld", 10, false);
        return s;
    }

    if cr.clippy_fail_score > 0. {
        let non_trivial = cr.total_code_lines > 10000; // it's easy to have no issues on a toy project
        // not having any clippy warnings can't be a positive signal, since many crates simply lack this data
        // so this is only a negative if present.
        let max_score = ((cr.clippy_fail_score.min(1.) * 8.).round() as u32) // the score is already computed and scaled to 0..=1
            .saturating_sub(if non_trivial { 1 } else { 0 });
        if max_score > 0 {
            s.has("clippy warnings", max_score, false);
        }
    }

    // TODO: this should be size per age?
    s.has("Non-trivial", 2, cr.rust_code_lines > 700); // usually trivial/toy programs
    s.has("Non-giant", 1, cr.total_code_lines < 80_000); // these should be split into crates
    s.frac("Rust LoC", 3, (f64::from(cr.rust_code_lines) / 10000.).min(1.)); // prefer substantial projects (and ignore vendored non-Rust code)
    s.frac("Comments", 3, (10. * f64::from(cr.rust_comment_lines) / (3000. + f64::from(cr.rust_code_lines))).min(1.)); // it's easier to keep small project commented
    s
}

#[must_use]
pub fn crate_score_version(cr: &CrateVersionInputs<'_>) -> Score {
    let mut score = Score::new();

    let ct_score = cargo_toml_score(cr);
    debug!("cargo_toml score: {ct_score:?}");
    score.group("Cargo.toml", 2, ct_score);
    let tiny_lib = cr.rust_code_lines > 50 && cr.rust_code_lines < 1000;
    score.group("README", 5, readme_score(cr.readme, cr.is_app || tiny_lib));
    score.group("Code", 4, code_score(cr));
    score.group("Versions", 5, versions_score(cr.versions));
    let a_score = authors_score(cr);
    debug!("authors score: {a_score:?}");
    score.group("Authors/Owners", 4, a_score);

    score
}

#[must_use]
pub fn crate_score_temporal(cr: &CrateTemporalInputs<'_>) -> Score {
    let mut score = Score::new();
    // if it's bin+lib, treat it as a lib.
    let is_app_only = cr.is_app && cr.number_of_direct_reverse_deps == 0;

    let growth = cr.traction_stats.map_or(1., |t| t.growth);
    if !is_app_only {
        score.frac("Growth", 2, (growth - 1.).clamp(0., 1.));
    }

    let active_users = if let Some(t) = cr.traction_stats {
        t.growth.min(1.2) * f64::from(t.active_users) // amplify by user growth
    } else { 1. };

    let now = Utc::now();
    let oldest = cr.versions.iter().map(|v| v.created_at).min().expect("at least 1 ver?");
    let newest = cr.versions.iter().max_by_key(|v| &v.created_at).expect("at least 1 ver?");
    // Assume higher versions, and especially patch versions, mean the crate is more mature
    // and needs fewer updates
    let version_stability_interval = match SemVer::parse(&newest.num) {
        Ok(ref ver) if ver.patch > 3 && ver.major > 0 => 700,
        Ok(ref ver) if ver.patch > 3 => 450,
        Ok(ref ver) if ver.patch > 0 => 300,
        Ok(ref ver) if ver.major > 0 => 200,
        Ok(ref ver) if ver.minor > 3 => 140,
        _ => 80,
    };
    let expected_update_interval = version_stability_interval.min(cr.versions.len() as u32 * 50)
        * if cr.downloads_per_month > 50_000 { 2 } else { 1 } // there are some old but still used crates, like fxhash
        * if active_users > 10. { 2 } else { 1 }
        / if cr.is_nightly { 4 } else { 1 };
    let latest_release_age = (now - newest.created_at).num_days().max(0) as u32;
    let crate_age = (now - oldest).num_days();
    let days_past_expiration_date = latest_release_age.saturating_sub(expected_update_interval);

    // no recent releases and no recent users smells like abandoned crate
    if days_past_expiration_date > 0 && active_users < 0.1 {
        let age_years = now.years_since(newest.created_at).unwrap_or(0);
        let peanalty = (age_years*5).saturating_sub(5);

        // if we didn't see usage, then active_users==0 is not that indicative
        let actually_lost_users = cr.traction_stats.is_some_and(|c| c.active_users < c.peak_active_users);
        let peanalty = if actually_lost_users { peanalty } else { peanalty / 2 };

        if peanalty > 0 {
            score.has("Is dead", peanalty, false);
        }
    }

    // score decays for a ~year after the crate should have been updated
    let decay_days = expected_update_interval/2 + if cr.is_nightly { 30 } else if is_app_only {300} else {200};
    // multiply by growth - new traction saves old crates, loss of traction quickens demise
    let freshness_score = (growth * f64::from(decay_days.saturating_sub(days_past_expiration_date)) / f64::from(decay_days)).min(1.);
    score.frac("Freshness of latest release", 16, freshness_score);

    // one bad dep shouldn't totally kill the score, unless it is the only dep
    let dep_score_base = match cr.dependency_freshness.len() {
        1 => 0.,
        2 => 0.2,
        _ => 0.3,
        // if you have more deps it's your problem to keep them up to date
    };
    score.frac("Freshness of deps", 12, cr.dependency_freshness.iter()
        .map(|d| dep_score_base + d * (1.-dep_score_base))
        .product::<f32>());

    let external_usage = cr.traction_stats.map_or(0.9, |t| t.external_usage);

    // Low numbers are just bots/noise.
    let downloads = (f64::from(cr.downloads_per_month) - 150.).max(0.) + 150.;
    let downloads_cleaned = external_usage.mul_add((f64::from(cr.downloads_per_month_minus_most_downloaded_user) / if is_app_only { 1. } else { 2. } - 50.).max(0.), 50.);
    // distribution of downloads follows power law.
    // apps have much harder to get high download numbers.
    let pop = (downloads.log2() - 6.0).max(0.) / (if is_app_only { 5. } else { 6. });
    let pop_cleaned = (downloads_cleaned.log2() - 5.0).max(0.);
    debug_assert!(pop > 0.);
    debug_assert!(pop_cleaned > 0.);
    score.score_f("Downloads", 5., pop);

    // there are internal-use-only libraries that get downloads, but no other users
    if !is_app_only && downloads > 300_000. && active_users < 1. {
        score.has("Not internal detail", 3, false);
    }

    // downloads are a smaller part of the score for brand new crates, since they're expected to grow slowly
    let max_download_score = 7.0f64.mul_add((crate_age as f64 / 365.).min(1.), 10.);
    score.score_f("Downloads (cleaned)", max_download_score, pop_cleaned);

    if crate_age < 30 {
        // give very very new crates a chance to show up
        score.has("Brand new", 5, true);
    } else if latest_release_age > 356 * 5 {
        score.score_f("Old crates must have users", 20., ((active_users / 10. + f64::from(cr.downloads_per_month) / 200_000.) - 1.).max(0.));
    }

    let has_any_traction = active_users > 1. || (f64::from(cr.downloads_per_month) * freshness_score) > 1200.;
    // freshness_score, because if it's aging, it'd better have more users
    score.has("Any traction", 2, has_any_traction);

    // proc macro used in exactly 1 crate is most likley a private implementation detail
    if cr.is_proc_macro {
        score.has("Other users", 2, cr.number_of_direct_reverse_deps != 1);
    }

    // Score added in an unusual way, because not being in Debian is not neccessarily a bad thing
    // (e.g. a crate may be for Windows or Mac only)
    if cr.is_in_debian {
        score.has("Debian endorsement", 3, true);
    }

    debug_assert_eq!(cr.owners.len(), cr.owner_trust.len());
    // Newly-published crates can ride on their author's reputation (or be treated with suspicion if comes out of nowhere)
    let reputation_importance = (2. + 5. * 365. / (365. + crate_age as f64)) as u32;
    // your best owner is your endorsement
    let trust_from_owners = cr.owner_trust.iter().zip(cr.owners).filter_map(|(&t, o)| {
        if o.github_id.is_none() || o.contributor_only { None } else { Some(t) }
    });
    let best_owner_reputation = trust_from_owners.max_by(f64::total_cmp).unwrap_or(0.);
    score.frac("Best owner reputation", reputation_importance, best_owner_reputation.min(200.) / 200.);
    // your worst owner is your weakest link.
    let trust_except_orgs = cr.owner_trust.iter().zip(cr.owners).filter_map(|(&t, o)| {
        // unfortunately org membership is mostly opaque and we don't have good reputation data for orgs, so ranking shouldn't punish for that.
        // t is exactly 0 only if we're missing data
        if o.github_id.is_none() || o.kind == OwnerKind::Team || o.not_a_person || o.contributor_only || t == 0. { None } else { Some(t) }
    });
    score.frac("Worst owner reputation", reputation_importance/2, trust_except_orgs.min_by(f64::total_cmp).unwrap_or(best_owner_reputation).min(200.) / 200.);

    // Don't expect apps to have rev deps (omitting these entirely proprtionally increases importance of other factors)
    let rev_deps_sqrt = (external_usage * f64::from(cr.number_of_direct_reverse_deps)).sqrt();
    let active_users_sqrt = active_users.sqrt();
    // if has lots of users, override other scores
    if active_users_sqrt > 8. {
        let bonus = (active_users_sqrt - 8.).min(10.) + (active_users_sqrt.log2() - 3.);
        score.score_f("Active users (bonus)", bonus, bonus);
    }
    if !is_app_only {
        score.score_f("Active users", 10., active_users_sqrt);
        score.score_f("Direct rev deps", 9., rev_deps_sqrt);
        let indirect = 1. + (external_usage * f64::from(cr.number_of_indirect_reverse_optional_deps)) / 4.;
        score.score_f("Indirect rev deps", 6., indirect.log2());
    }

    // If it's super popular, nothing else matters. Millions of crustaceans can't be wrong.
    let bonus = ((downloads_cleaned - 1_000_000.) / (5_000_000. / 10.)).min(10.);
    if bonus > 0. {
        // this way less popular crates aren't penalized, it only marginalizes other scores
        score.score_f("Downloads bonus", bonus, bonus);
    }
    let bonus = ((rev_deps_sqrt - 20.) / 2.).min(20.);
    if bonus > 0. {
        score.score_f("Extra popularity bonus", bonus, bonus);
    }

    if !is_app_only || cr.has_docs_rs {
        score.has("docs.rs", 1, cr.has_docs_rs);
    }

    // important enough and manageable enough to be reviewed
    score.n("Vetted", 4, if cr.has_vet_reviews {2} else {0}
        + if cr.has_crev_reviews {1} else {0}
        + if cr.has_vet_trust {1} else {0});

    score
}

#[derive(Debug)]
pub struct OverallScoreInputs {
    // ratio of peak active users to current active users (1.0 = everyone active, 0.0 = all dead)
    pub former_glory: f64,
    pub is_proc_macro: bool,
    pub is_sys: bool,
    pub is_sub_component: bool,
    pub is_internal: bool,
    pub is_autopublished: bool,
    pub is_deprecated: bool,
    /// From external sources
    pub is_unmaintained: bool,
    pub is_crates_io_published: bool,
    pub is_yanked: bool,
    pub is_squatspam: bool,
    pub is_vaporware_or_ponzi_scheme: bool,
    pub ablocklist_deadend: bool,
    pub ablocklist_redirect: bool,
    pub traction_stats: Option<TractionStats>,
}

#[must_use] pub fn combined_score(base_score: &Score, temp_score: &Score, f: &OverallScoreInputs) -> f64 {
    let base_score = base_score.total(); debug_assert!(base_score >= 0. && base_score <= 1.);
    let mut temp_score = temp_score.total(); debug_assert!(temp_score >= 0. && temp_score <= 1.);

    if f.is_autopublished {
        temp_score *= 0.7;
    }

    let excels = base_score.max(temp_score);
    let mut score = base_score.mul_add(0.4, temp_score * 0.5) + if excels > 0.3 { excels * 0.1 } else { 0. };

    // The scores are averages of many individual properties, which gives them a normal distribution.
    // Due to sturgeon's law, this normal is centered close to 0.
    // Then subsequent multiplications shift it further towards exponential distribution.
    // However, it's much nicer to work with ranking as a linear 0..1 property,
    // so here's a linearizing fudge factor
    score = 1. - (1. - score).powf(1.3);

    score *= f.former_glory;

    // there's usually a non-macro sibling
    if f.is_proc_macro {
        // if the crate has no users except its owner, assume it's internal-only and not interesting
        let active_users = f.traction_stats.map_or(2, |t| t.active_users);
        score *= if active_users <= 1 { 0.4 } else { 0.8 };
    }
    // there's usually a higher-level wrapper
    if f.is_sys {
        // like proc-macro, but not as harsh, since sys crates may still be useful standalone
        let active_users = f.traction_stats.map_or(2, |t| t.active_users);
        score *= if active_users <= 1 { 0.8 } else { 0.95 };
    }

    if f.is_sub_component {
        score *= 0.8;
    }

    if f.is_internal {
        score *= 0.6;
    }

    if f.is_deprecated {
        score *= 0.2;
    } else if f.is_unmaintained {
        score *= 0.4;
    }

    // Cryptocurrencies use technology to obscure that they are modern versions of pump and dump scams,
    // ponzi schemes, investment fraud, unregulated gambling, and large-scale money laundering.
    // The promises are empty. The real effects are proliferation of ransomware and other criminal activity
    if f.is_vaporware_or_ponzi_scheme {
        score *= 0.4;
    }

    if !f.is_crates_io_published {
        // installation and usage of other crate sources is more limited
        score *= 0.75;
    }

    // k bye
    if f.is_yanked || f.is_squatspam {
        score *= 0.001;
    }

    // hidden crates show a "sorry the princes is in another castle" pages, which are
    // not useful for lib.rs users. This is a balance between hiding useless pages,
    // and not surprising users with crates entirely missing.
    if f.ablocklist_deadend {
        score *= 0.7;
    } else if f.ablocklist_redirect {
        score *= 0.9;
    }

    score
}

pub struct OwnerRankingInputs<'a> {
    pub oldest_crate: Option<DateTime<Utc>>,
    pub github_id: u32,
    pub lowercase_github_org_names: Vec<String>,
    pub login: &'a str,
}

/// 0-100
fn trust_for_org(org: &str) -> Option<u8> {
    Some(match org {
        // if you use rust, you implicitly trust the rust org
        "rust-lang" => 100,
        "rust-lang-nursery" => 50,
        "rust-lang-deprecated" => 10,

        // bonus for early rust nostalgia
        "mozilla" | "servo" => 25,

        // they're supposed to know how to program… OTOH these are big orgs
        "google" | "microsoft" | "cloudflare" | "epicgames" | "aws" => 20,

        // Some professional orgs that hopefully set inclusion bar higher than just "hey, can I join?".
        // This intentionally doesn't include small crate-specific and community orgs.
        "bytecodealliance" | "w3c" | "unicode-org" | "mozilla-standards" | "awslabs" |
        "whatwg" | "webassembly" | "llvm" | "rustsec" | "integer32llc" | "atlassian" => 10,

        // these may not be selective or that relevant to rust, but at least they may help
        // differentiate between somewhat known people and drive-by anon accounts.
        "rust-for-linux" | "rayon-rs" | "rust-community" | "sentry" | "actix" |
        "tikv" | "denoland" | "hyperium" | "tokio-rs" | "async-rs" | "gfx-rs" | "georust" | "diesel-rs" |
        "rustaudio" | "rust-secure-code" | "http-rs" | "rust-embedded" | "bevyengine" | "rust-fuzz" |
        "rust-ml" | "pistondevelopers" | "image-rs" | "materializeinc" | "gtk-rs" | "open-i18n" |
        "ruffle-rs" | "rust-docs" | "crossbeam-rs" | "serde-rs" | "rustls" | "rust-analyzer" => 5,
        "maintainers" | "apache" | "tc39" | "rails" | "vercel" | "azure" | "getsentry" | "nixos" | "janestreet" |
        "django" | "googlers" | "amethyst" | "rustasync" | "firefoxgraphics" | "protocolbuffers" | "nodejs" => 3,
        _ => return None, // it's trivial to make your own orgs, so unknown ones have no value
    })
}

pub fn owner_ranking(f: &OwnerRankingInputs<'_>) -> f64 {
    debug_assert!(f.github_id > 0);

    // unfortunately, the membership must be public. neither github nor crates-io disclose private orgs.
    // in github, orgs and usernames of orgs are the same thing, so behave as if org-named user belongs to its org
    let score_from_orgs = f.lowercase_github_org_names.iter().map(|s| s.as_str()).chain(Some(&*f.login.as_ascii_lowercase())).filter_map(|org| {
        debug_assert!(org.as_ascii_lowercase() == org);
        trust_for_org(org)
    })
    // orgs are not pokemon, so count only the best three
    .map(std::cmp::Reverse)
    .k_smallest(3)
    // and use half of the second one, since the orgs aren't entirely uncorrelated
    .enumerate()
    .map(|(i, n)| f64::from(n.0) / (1 + i) as f64)
    .sum::<f64>();

    // you can't fake time, and staying on crates.io without getting banned is a good sign
    let now = Utc::now();
    let age_on_crates_io = f.oldest_crate.map_or(0, |s| now.signed_duration_since(s).num_days().max(0));
    let age_score = ((age_on_crates_io as f64 / 365.) - 1.).clamp(0., 10.).powf(1.5);

    // old github account. you can't get these IDs anymore!
    let old_guard_score = 10. * 300_000. / (300_000. + f64::from(f.github_id));

    score_from_orgs + age_score + old_guard_score
}

#[test]
fn test_readme_score() {
    let ren = render_readme::Renderer::new(None);
    let deadline = Instant::now() + Duration::from_secs(4);
    let dom = ren.page_node(&render_readme::Markup::Markdown("# hello world [link](http://hrefval)
![img](imgsrc)
![badg](http://travis-ci.org/badge.svg)

```
code
```

* list
* items
".into()), &render_readme::LinksContext {
        base_url: Some(("https://example.com", "https://example.com")),
        nofollow: render_readme::Links::Ugc,
        own_crate_name: None,
        link_own_crate_to_crates_io: false,
        link_fixer: None,
    }, false, deadline);
    let mut p = Default::default();
    fill_props(&dom, &mut p, false);
    assert_eq!(p.images, 1);
    assert_eq!(p.sections, 1);
    assert_eq!(p.list_or_table_rows, 2);
    assert_eq!(p.pre_blocks, 1);
    assert_eq!(p.code_len, 5);
    assert_eq!(p.text_len, 28);
}
