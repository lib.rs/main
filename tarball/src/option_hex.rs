    use hex::{FromHex, ToHex};
    use serde::de::{Error, Visitor};
    use serde::{Deserializer, Serializer};
    use std::fmt;
    use std::marker::PhantomData;

    pub fn serialize<S: Serializer, T>(data: &Option<T>, serializer: S) -> Result<S::Ok, S::Error>
        where for<'a> &'a T: ToHex,
    {
        match data {
            Some(data) => hex::serde::serialize(data, serializer),
            None => serializer.serialize_none(),
        }
    }

    pub fn deserialize<'de, D, T: FromHex>(deserializer: D) -> Result<Option<T>, D::Error>
    where
        D: Deserializer<'de>,
        <T as FromHex>::Error: fmt::Display,
    {
        struct HexStrVisitor<T>(PhantomData<T>);

        impl<'de, T: FromHex> Visitor<'de> for HexStrVisitor<T>
        where <T as FromHex>::Error: fmt::Display,
        {
            type Value = Option<T>;

            fn expecting(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                write!(f, "an optional hex encoded string")
            }

            fn visit_str<E: Error>(self, data: &str) -> Result<Self::Value, E> {
                self.visit_borrowed_bytes(data.as_bytes())
            }

            fn visit_borrowed_str<E: Error>(self, data: &'de str) -> Result<Self::Value, E> {
                self.visit_borrowed_bytes(data.as_bytes())
            }

            fn visit_bytes<E: Error>(self, data: &[u8]) -> Result<Self::Value, E> {
                self.visit_borrowed_bytes(data)
            }

            fn visit_borrowed_bytes<E: Error>(self, data: &'de [u8]) -> Result<Self::Value, E> {
                if data.is_empty() {
                    return Ok(None);
                }
                FromHex::from_hex(data).map(Some).map_err(Error::custom)
            }

            fn visit_some<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
                where D: Deserializer<'de> {
                hex::serde::deserialize(deserializer).map(Some)
            }

            fn visit_none<E>(self) -> Result<Self::Value, E> {
                Ok(None)
            }
        }

        deserializer.deserialize_str(HexStrVisitor(PhantomData))
    }
