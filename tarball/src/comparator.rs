use util::error::IntoIoError;
use rustc_hash::{FxHashMap as HashMap, FxHashSet as HashSet};
use cargo_toml::{AbstractFilesystem, Manifest, Product};
use chrono::{DateTime, Utc};
use similar::ChangeTag;
use crate::{option_hex, UnarchiverError};
use crate::{read_archive_files, MAX_FILE_SIZE};
use crate_git_checkout::git2;
use crate_git_checkout::{GitError, CloneOptions, ObjectType, Oid, Repository};
use crate_git_checkout::{ConnectedRemote, GitFS, Tree};
use log::{debug, error, info, warn};
use repo_url::Repo;
use serde::{Deserialize, Serialize};
use std::borrow::Cow;
use std::collections::hash_map::Entry;
use std::collections::{BTreeMap, HashSet as StdHashSet};
use std::error::Error;
use std::ffi::OsStr;
use std::io::{self, Read};
use std::path::{Component, Path, PathBuf};
use std::sync::atomic::AtomicU32;
use std::sync::atomic::Ordering::Acquire;
use std::time::{Duration, Instant};
use util::{pick_top_n_unstable_by, SmolStr};

/// Some have super long `authors = []` list
const MAX_CARGO_TOML_SIZE: u64 = 65_000;
const MAX_CARGO_LOCK_SIZE: u64 = 160_000;
const MAX_DIFF_LEN: usize = 5_000_000;


#[derive(Debug, thiserror::Error)]
pub enum ComprError {
    #[error("Git checkout failure")]
    Checkout(#[from] #[source] crate_git_checkout::Error),
    #[error("Git repository error {:?}/{:?}", _0.class(), _0.code())]
    Git(#[from] #[source] GitError),
    #[error("Git repository error: {0}")]
    BrokenRepo(&'static str),
    #[error("Check timed out")]
    Timeout,
    #[error("Check aborted")]
    Aborted,
    #[error("Requested path is not in git")]
    PathNotFound,
    #[error("Can't complete check due to unrecoverable error")]
    FatalError(String),
    #[error("Issue: {}", _0.message())]
    RecoverableIssue(Issue),
    #[error("Internal error: {0}")]
    InternalError(&'static str),
}
impl ComprError {
    fn issue(issue: Issue) -> Self {
        Self::RecoverableIssue(issue)
    }
}

impl From<UnarchiverError> for ComprError {
    fn from(un: UnarchiverError) -> Self {
        match un {
            UnarchiverError::Checkout(crate_git_checkout::Error::BrokenRepo(y)) => Self::BrokenRepo(y),
            UnarchiverError::Checkout(crate_git_checkout::Error::Aborted) => Self::Aborted,
            UnarchiverError::Git(e) => Self::Git(e),
            UnarchiverError::Checkout(e) => Self::Checkout(e),
            UnarchiverError::OtherGit(msg) => Self::FatalError(msg),
            UnarchiverError::CompareError(e) => e,
            UnarchiverError::Timeout => Self::Timeout,
            UnarchiverError::InternalError(e) => Self::InternalError(e),
            other => Self::FatalError(other.to_string()),
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub enum Issue {
    NoSha1InPackage,
    NoPathInPackage,
    PathInRepoNotDiscovered,
    NoMatchingTag,
    NoMatchingTagForVer(String),
    NoMatchingTagForSha {
        #[serde(with = "hex")]
        sha1: [u8; 20],
    },
    /// The search is done on a partial fetch, so it could have been found later
    PackageNotFoundInRepo,
    /// vcs info was bad?
    PackageNotFoundAtKnownPath,
    PackageVersionNotFoundInRepo,
    /// It means tags, not whole repo
    SearchedRepoForReleases,
    SearchedRepoForCommit,
    UsedHeadCommitAsFallback,
    /// Cargo.toml lacks [package]
    ManifestWithoutPackage,
    MismatchedFeatures,
    MismatchedProduct(String),
    MismatchedPackage(String),
    MismatchedDependency(String),
    MismatchedAbsentBuildRs,
    // not used
    ManifestNotCompared,

    ManifestOrigMissing,
    ManifestCargoMissing,
    ManifestParsingError(String),
    CargoLockParsingError(String),
    CargoLockNonUtf8,
    FileTooLarge(PathBuf),
    NonUtf8Path(String),
    SkippedGitModulePath(String),
    TarSizeInvalid(PathBuf),
    /// Tarball only
    InvalidPath(PathBuf),
    /// Git
    InvalidGitPath(String, PathBuf),
    RepoIncompatibility(String),
    BrokenSubmodulePath(PathBuf),
    NotFoundInRepo(PathBuf),
    CargoMovedFile(PathBuf),
    FoundInRepoButNotAtPath(PathBuf),
    ManifestFuzzyDiffers,
    FileDiffersFromRepo(PathBuf),
    /// Tag is misleading, doesn't match expected sha1
    TagWrongSha(String),
    ShaNotInHeadBranch(u32),
    ShaNotFoundInRepo(#[serde(with = "hex")] [u8; 20]),
    FetchError(String),
    RepositoryGone(String),
    FatalError(String),
    TooManyErrors,
}

impl Issue {
    #[must_use] pub fn is_fatal(&self) -> bool {
        match self {
            Self::NoSha1InPackage => false,
            Self::NoPathInPackage => false,
            Self::PathInRepoNotDiscovered => false,
            Self::NoMatchingTag => false,
            Self::NoMatchingTagForVer(_) => false,
            Self::NoMatchingTagForSha { .. } => false,
            // the search is done on a partial fetch, so it could have been found later
            Self::PackageNotFoundInRepo => false,
            Self::PackageNotFoundAtKnownPath => false,
            Self::PackageVersionNotFoundInRepo => false,
            Self::SearchedRepoForReleases => false,
            Self::SearchedRepoForCommit => false,
            Self::UsedHeadCommitAsFallback => false,
            Self::CargoMovedFile(_) => false,
            Self::TagWrongSha(_) => false,
            Self::ShaNotInHeadBranch(_) => false,
            Self::ShaNotFoundInRepo(_) => false,
            Self::SkippedGitModulePath(_) => false,
            Self::ManifestFuzzyDiffers => false,
            Self::RepoIncompatibility(_) => false,

            Self::ManifestWithoutPackage => true,
            Self::MismatchedFeatures => true,
            Self::MismatchedProduct(_) => true,
            Self::MismatchedPackage(_) => true,
            Self::MismatchedDependency(_) => true,
            Self::MismatchedAbsentBuildRs => false,
            Self::ManifestNotCompared => true, // Unused
            Self::ManifestOrigMissing => true,
            Self::ManifestCargoMissing => true,
            Self::CargoLockNonUtf8 => true,
            Self::CargoLockParsingError(_) => true,
            Self::ManifestParsingError(_) => true,
            Self::FileTooLarge(_) => true,
            Self::NonUtf8Path(_) => true,
            Self::TarSizeInvalid(_) => true,
            Self::InvalidPath(_) => true,
            Self::InvalidGitPath { .. } => true,
            Self::BrokenSubmodulePath(_) => true,
            Self::FileDiffersFromRepo(p) if p.as_os_str() == "Cargo.lock" => false,
            Self::FileDiffersFromRepo(_) => true,
            Self::FatalError(_) => true,
            Self::RepositoryGone(_) => true,
            Self::TooManyErrors => true,

            Self::NotFoundInRepo(p) if p.as_os_str() == "Cargo.lock" => false,
            Self::NotFoundInRepo(_) => true,

            Self::FoundInRepoButNotAtPath(p) if ["README.md", "LICENSE", "LICENSE-MIT", "LICENSE-APACHE"].into_iter().any(|f| p.as_os_str().eq_ignore_ascii_case(f)) || p.as_os_str() == "Cargo.lock" => false,
            Self::FoundInRepoButNotAtPath(_) => true,

            Self::FetchError(_) => false,
        }
    }

    #[must_use] pub fn message(&self) -> Cow<'_, str> {
        match self {
            Self::FatalError(msg) => msg.into(),
            Self::FetchError(msg) => format!("Repository fetch error: {msg}.").into(),
            Self::RepositoryGone(msg) => format!("Repository doesn't exist: {msg}.").into(),
            Self::MismatchedProduct(msg) |
            Self::MismatchedPackage(msg) => format!("Manifest properties don't match: {msg}.").into(),
            Self::MismatchedDependency(msg) => format!("Dependencies don't match: {msg}.").into(),
            Self::MismatchedAbsentBuildRs => "build.rs is used in git, but disabled in the crates.io package".into(),
            Self::ManifestWithoutPackage => "Cargo.toml could not be parsed, missing [package]".into(),
            Self::MismatchedFeatures => "Cargo.toml and Cargo.toml.orig had different [features]".into(),
            Self::ManifestNotCompared => "<unused>".into(),
            Self::ManifestOrigMissing => "Cargo.toml.orig from the crate tarball could not be verified.".into(),
            Self::ManifestCargoMissing => "Cargo.toml not found in the crate tarball.".into(),
            Self::ManifestParsingError(msg) => format!("Cargo.toml parse error: {msg}.").into(),
            Self::CargoLockParsingError(msg) => format!("Cargo.lock parse error: {msg}.").into(),
            Self::ManifestFuzzyDiffers => "Cargo.toml.orig from crates.io is not an exact match with the repository.".into(),
            Self::FileTooLarge(p) if p.as_os_str() == "Cargo.lock" => "Cargo.lock was suspiciously large.".into(),
            Self::FileTooLarge(p) if p.as_os_str() == "Cargo.toml" => "Cargo.toml was suspiciously large.".into(),
            Self::FileTooLarge(p) if p.as_os_str() == ".cargo_vcs_info.json" => ".cargo_vcs_info.json was suspiciously large.".into(),
            Self::FileTooLarge(p) => format!("File '{}' was too large", p.display()).into(),
            Self::NonUtf8Path(p) => format!("Crate tarball contained a non-UTF-8 path: '{p}'.").into(),
            Self::TarSizeInvalid(p) => format!("Crate tarball contained path '{}' with incorrect file size metadata", p.display()).into(),
            Self::InvalidPath(p) => format!("Crate tarball contained a malformed or forbidden path: '{}'", p.display()).into(),
            Self::InvalidGitPath(reason, path) => format!("Crate repo contained a malformed or forbidden path ({reason}): '{}'", path.display()).into(),
            Self::BrokenSubmodulePath(p) => format!("Repository had submodule with an invalid or unsupported path: '{}'", p.display()).into(),
            Self::RepoIncompatibility(msg) => format!("Git repo: {msg}.").into(),
            Self::CargoLockNonUtf8 => "Cargo.lock is not a text file.".into(),
            Self::NotFoundInRepo(p) => format!("File '{}' has not been found in the repository", p.display()).into(),
            Self::CargoMovedFile(p) => format!("File '{}' has been found, but Cargo packaged it in a different location", p.display()).into(),
            Self::FoundInRepoButNotAtPath(p) => format!("File '{}' has been found in the repository at the expected location", p.display()).into(),
            Self::FileDiffersFromRepo(p) => format!("File '{}' in the published crate is different than the same file in the repository", p.display()).into(),
            Self::TagWrongSha(tag) => format!("Crate tarball has been published from a different commit than the commit tagged by git tag '{tag}'.").into(),
            Self::ShaNotInHeadBranch(n) => format!("Crate tarball has been published from a commit that is not in the main branch (searched {n} commits deep).").into(),
            Self::ShaNotFoundInRepo(sha1) => format!("Crate tarball has been published from the commit {}, which is not in the repository.", hex::encode(sha1)).into(),
            Self::SkippedGitModulePath(git) => format!("Skipped .git link for submodule '{git}.").into(),
            Self::TooManyErrors => "Too many errors.".into(),

            Self::NoSha1InPackage => "The crates.io package has no commit hash associated with it.".into(),
            Self::NoPathInPackage => "The crates.io package has no information about its path inside its repository.".into(),
            Self::PathInRepoNotDiscovered => "Could not discover crate's location in its repository (via tags).".into(),
            Self::NoMatchingTag => "There is no tag matching package's commit hash.".into(),
            Self::NoMatchingTagForVer(ver) => format!("There is no matching git tag containing version '{ver}'.").into(),
            Self::NoMatchingTagForSha { sha1 } => format!("The package has been published from commit {}, but there is not tag for this commit", hex::encode(sha1)).into(),
            Self::PackageNotFoundInRepo => "Could not find Cargo.toml for this package in its repo (via tags).".into(),
            Self::PackageNotFoundAtKnownPath => "Could not find Cargo.toml for this package in its repo (via vcs info).".into(),
            Self::PackageVersionNotFoundInRepo => "Found a Cargo.toml for this package in its repo (via tags), but the version was different.".into(),
            Self::SearchedRepoForReleases => "Had to search content of all git tags due to lack of commit/path information in the package.".into(),
            Self::SearchedRepoForCommit => "Had to perform fuzzy search of the head branch due to lack of commit information in the package.".into(),
            Self::UsedHeadCommitAsFallback => "Could not find any matching commit in the repository. Used latest commit as a fallback. The results may be incorrect.".into(),
        }
    }
}

pub struct Comparator<'a> {
    repository_is_gone: bool,

    crate_name: &'a str, ver: &'a str,
    tarball: &'a [u8],
    sha1: Option<[u8; 20]>,
    path_in_repo: Option<Cow<'a, str>>,

    repos: NestedRepos<'a>,

    manifest_published: Option<Manifest>,
    manifest_orig: Option<Manifest>,

    report: Report,
}

struct NestedRepos<'dir> {
    checkout_base_path: &'dir Path,
    repository: Option<Repository>,
    submodules: HashMap<PathBuf, SubmoduleDef>,
}

struct SubmoduleDef {
    url: String,
    sha1: [u8; 20],
    cloned_repo: Option<Repository>,
    failed: u8,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct UsedRepo {
    /// Actual commit used
    #[serde(default, with = "hex")]
    pub sha1: [u8; 20],

    /// The sha1 is in the HEAD/main branch starting at this commit.
    /// If this commit is still in the HEAD/main, then sha1 is too.
    /// Not always checked.
    #[serde(default, with = "option_hex", skip_serializing_if = "Option::is_none")]
    pub reachable_from: Option<[u8; 20]>,

    /// Repo URL
    pub url: String,

    /// Location in the parent repo
    #[serde(skip_serializing_if = "Option::is_none")]
    pub submodule_path: Option<String>,

    /// Location in the repo itself
    #[serde(skip_serializing_if = "Option::is_none")]
    pub crate_path: Option<String>,

    /// Tag, if any matched
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub tag: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Report {
    /// Problems found
    pub issues: Vec<Issue>,

    /// Number of files in tarball
    pub tried_files: usize,

    /// OK if `verified_files` + `skipped_files` equal `tried_files`
    pub verified_files: usize,

    /// Not included in `verified_files` due to being Cargo-generated
    #[serde(default)]
    pub skipped_files: usize,

    /// FIY to reproduce results
    #[serde(default)]
    pub repos_used: Vec<UsedRepo>,

    /// Check if needs refresh
    #[serde(default)]
    pub version: u8,

    #[serde(default)]
    pub created_at: Option<DateTime<Utc>>,

    #[serde(default)]
    pub crate_version: Option<String>,

    #[serde(default)]
    pub diffs: HashMap<String, String>,
}

#[derive(Debug, Clone)]
pub struct ReportResult {
    pub all_files_verified: bool,
    pub unclear_vcs_info: bool,
    pub has_a_good_tag: bool,
    pub cargo_lock_skipped: bool,
}

pub struct ReportMessage<'a> {
    pub important: bool,
    pub issue: &'a Issue,
    pub fix_markdown: Option<Cow<'static, str>>,
}

impl Report {
    pub fn push(&mut self, issue: Issue) {
        debug!("{}: {issue:?}", self.repos_used.get(0).map(|r| r.url.as_str()).unwrap_or("?"));
        self.issues.push(issue);
    }

    pub fn messages(&self) -> (Vec<ReportMessage<'_>>, HashMap<&Path, Vec<(ReportMessage<'_>, &OsStr)>>) {
        let mut needs_vcs_info = false;
        let mut path_may_be_bad = false;
        let mut repo_may_be_bad = false;
        let mut sha1_may_be_bad = false;
        let mut needs_valid_repo_tag = false;
        let mut fuzzy_fallbacks = false;
        let mut wrong_manifest = false;
        let mut bad_tarball = false;
        let mut check_technical_error = false;
        let mut fetch_errors = false;

        for issue in &self.issues {
            match issue {
                Issue::NoSha1InPackage => { needs_vcs_info = true; },

                Issue::NoPathInPackage => { needs_vcs_info = true; },
                Issue::PathInRepoNotDiscovered => { path_may_be_bad = true; needs_vcs_info = true; },
                Issue::PackageNotFoundInRepo => { repo_may_be_bad = true; },
                Issue::PackageNotFoundAtKnownPath => { path_may_be_bad = true; },
                Issue::PackageVersionNotFoundInRepo => { sha1_may_be_bad = true; },

                Issue::SearchedRepoForReleases => { sha1_may_be_bad = true; },

                Issue::NoMatchingTag => { needs_valid_repo_tag = true; },
                Issue::NoMatchingTagForVer(_) => { needs_valid_repo_tag = true; },
                Issue::NoMatchingTagForSha { .. } => { sha1_may_be_bad = true; needs_valid_repo_tag = true; },
                Issue::TagWrongSha(_) => { sha1_may_be_bad = true; needs_valid_repo_tag = true; },
                Issue::ShaNotFoundInRepo(_) => { sha1_may_be_bad = true; },

                Issue::ShaNotInHeadBranch(_) => { },

                Issue::SearchedRepoForCommit => { needs_valid_repo_tag = true; sha1_may_be_bad = true; fuzzy_fallbacks = true; },
                Issue::UsedHeadCommitAsFallback => { sha1_may_be_bad = true; fuzzy_fallbacks = true; },

                Issue::ManifestWithoutPackage => { wrong_manifest = true; }
                Issue::MismatchedFeatures => { wrong_manifest = true; }
                Issue::MismatchedProduct(_) => { wrong_manifest = true; }
                Issue::MismatchedPackage(_) => { wrong_manifest = true; }
                Issue::MismatchedDependency(_) => { wrong_manifest = true; }
                Issue::MismatchedAbsentBuildRs => { wrong_manifest = true; },
                Issue::ManifestOrigMissing => { bad_tarball = true; }
                Issue::ManifestCargoMissing => { bad_tarball = true; }
                Issue::ManifestParsingError(_) => { wrong_manifest = true; }
                Issue::ManifestFuzzyDiffers => {}

                Issue::CargoLockParsingError(_) => {},
                Issue::CargoLockNonUtf8 => {},

                Issue::FileTooLarge(_) => { check_technical_error = true; },
                Issue::TarSizeInvalid(_) => { bad_tarball = true; },
                Issue::NonUtf8Path(_) => { bad_tarball = true; },

                Issue::SkippedGitModulePath(_) => { },
                Issue::BrokenSubmodulePath(_) => { check_technical_error = true; repo_may_be_bad = true; },

                Issue::NotFoundInRepo(_) => {},
                Issue::FoundInRepoButNotAtPath(_) => { path_may_be_bad = true; sha1_may_be_bad = true; },
                Issue::FileDiffersFromRepo(_) => { sha1_may_be_bad = true; },

                Issue::InvalidPath(_) => { bad_tarball = true; },
                Issue::InvalidGitPath { .. } => { repo_may_be_bad = true; check_technical_error = true; },

                Issue::RepositoryGone(_) => { repo_may_be_bad = true; },
                Issue::RepoIncompatibility(_) => { repo_may_be_bad = true; },

                Issue::FetchError(_) => { fetch_errors = true;  }
                Issue::FatalError(_) => { check_technical_error = true;  }
                Issue::TooManyErrors => {},

                Issue::CargoMovedFile(_) => {},
                Issue::ManifestNotCompared => {}, // unused
            };
        }

        let has_fatal_errors = self.issues.iter().any(|i| i.is_fatal());

        #[derive(PartialEq, Eq, Hash)]
        enum Section<'a> {
            Skip,
            TarOddball,
            VCS,
            RepoPath,
            Tagging,
            Search,
            Manifest,
            CargoLock,
            RepoBugs,
            VerificationFailure(&'a Path),
            OtherError,
        }
        use Section::*;

        let mut by_section = HashMap::default();

        for issue in &self.issues {
            let (important, section): (bool, Section<'_>) = match issue {
                Issue::NoSha1InPackage => (sha1_may_be_bad || fuzzy_fallbacks || wrong_manifest, VCS),

                Issue::NoPathInPackage => (path_may_be_bad || fuzzy_fallbacks || wrong_manifest, VCS),
                Issue::PathInRepoNotDiscovered => (!needs_vcs_info && !repo_may_be_bad, if !has_fatal_errors && !wrong_manifest { Skip } else { RepoPath }),
                Issue::PackageNotFoundInRepo => (!needs_vcs_info && !repo_may_be_bad, RepoPath),
                Issue::PackageNotFoundAtKnownPath => (!needs_vcs_info && !repo_may_be_bad, RepoPath),
                Issue::PackageVersionNotFoundInRepo => (!fetch_errors && !needs_vcs_info && !repo_may_be_bad, RepoPath),

                Issue::SearchedRepoForReleases => (false, if needs_vcs_info || sha1_may_be_bad { Skip } else { Search }),

                Issue::NoMatchingTag => (!needs_vcs_info, if needs_vcs_info || sha1_may_be_bad { Tagging } else { Skip }),
                Issue::NoMatchingTagForVer(_) => (!fetch_errors && !needs_vcs_info, if needs_vcs_info && !repo_may_be_bad { Tagging } else { Skip }),
                Issue::NoMatchingTagForSha { .. } => (!needs_vcs_info && !fuzzy_fallbacks, if sha1_may_be_bad || repo_may_be_bad { Skip } else { Tagging }),
                Issue::TagWrongSha(_) => (!needs_vcs_info, if needs_vcs_info || repo_may_be_bad { Skip } else { Tagging }),
                Issue::ShaNotFoundInRepo(_) => (!check_technical_error && !fetch_errors && !needs_vcs_info && !repo_may_be_bad, if repo_may_be_bad { Skip } else { VCS }),

                Issue::ShaNotInHeadBranch(_) => (!has_fatal_errors, if needs_vcs_info || sha1_may_be_bad || needs_valid_repo_tag { Skip } else { Tagging }),

                Issue::SearchedRepoForCommit => (!check_technical_error && wrong_manifest, if repo_may_be_bad && !wrong_manifest { Skip } else { Search }),
                Issue::UsedHeadCommitAsFallback => (has_fatal_errors, if !needs_vcs_info || has_fatal_errors || wrong_manifest { Tagging } else { Skip }),

                Issue::ManifestWithoutPackage => (!fuzzy_fallbacks && !sha1_may_be_bad && !repo_may_be_bad, Manifest),
                Issue::MismatchedFeatures => (!fuzzy_fallbacks && !sha1_may_be_bad && !repo_may_be_bad, Manifest),
                Issue::MismatchedProduct(_) => (!fuzzy_fallbacks && !sha1_may_be_bad && !repo_may_be_bad, Manifest),
                Issue::MismatchedPackage(_) => (!fuzzy_fallbacks && !sha1_may_be_bad && !repo_may_be_bad, Manifest),
                Issue::MismatchedDependency(_) => (!fuzzy_fallbacks && !sha1_may_be_bad && !repo_may_be_bad, Manifest),
                Issue::MismatchedAbsentBuildRs => (false, Manifest),
                Issue::ManifestOrigMissing => (!needs_vcs_info, Manifest),
                Issue::ManifestCargoMissing => (!needs_vcs_info, Manifest),
                Issue::ManifestParsingError(_) => (!fuzzy_fallbacks && !sha1_may_be_bad && !repo_may_be_bad, Manifest),
                Issue::ManifestFuzzyDiffers => (false, if needs_vcs_info { Skip } else { Manifest }),

                Issue::CargoLockParsingError(_) => (false, CargoLock),
                Issue::CargoLockNonUtf8 => (!bad_tarball, CargoLock),

                Issue::FileTooLarge(_) => (true, TarOddball),
                Issue::TarSizeInvalid(_) => (true, TarOddball),
                Issue::NonUtf8Path(_) => (true, TarOddball),

                Issue::SkippedGitModulePath(_) => (!fuzzy_fallbacks && !sha1_may_be_bad, RepoBugs),
                Issue::BrokenSubmodulePath(_) => (!sha1_may_be_bad, RepoBugs),

                Issue::NotFoundInRepo(p) => (false, VerificationFailure(p)),
                Issue::FoundInRepoButNotAtPath(p) => (!needs_vcs_info && !wrong_manifest, VerificationFailure(p)),
                Issue::FileDiffersFromRepo(p) => (!needs_vcs_info && !wrong_manifest, VerificationFailure(p)),

                Issue::InvalidPath(_) => (true, TarOddball),

                Issue::RepositoryGone(_) => (true, RepoBugs),
                Issue::InvalidGitPath { .. } => (!bad_tarball && !fetch_errors && !sha1_may_be_bad && !fuzzy_fallbacks, RepoBugs),
                Issue::RepoIncompatibility(_) => (!check_technical_error && !sha1_may_be_bad && !fetch_errors, RepoBugs),

                Issue::FetchError(_) => (sha1_may_be_bad || wrong_manifest, OtherError),
                Issue::FatalError(_) => (wrong_manifest || check_technical_error, OtherError),
                Issue::TooManyErrors => (false, Skip),

                Issue::CargoMovedFile(_) => (false, Skip),
                Issue::ManifestNotCompared => (false, Skip), // unused
            };

            fn safe_markdown_code(c: &str) -> &str {
                c.split(['`','\\','\r','\n','&']).next().unwrap_or(c)
            }
            let fix_markdown: Option<Cow<'static, str>> = match issue {
                Issue::NoSha1InPackage => Some("Publish a new version, without using `--allow-dirty` flag. Make sure all changes are committed before publishing. Use `include`/`exclude` in `Cargo.toml` to omit unnecessary files.".into()),
                Issue::NoPathInPackage => (!sha1_may_be_bad && !repo_may_be_bad).then_some("Publish a new version, without using `--allow-dirty` flag. The path should be included automatically for git repositories. Check that `repository` URL is up-to-date in `Cargo.toml`".into()),
                Issue::PackageNotFoundInRepo |
                Issue::PackageNotFoundAtKnownPath |
                Issue::TooManyErrors |
                Issue::PathInRepoNotDiscovered => (wrong_manifest || repo_may_be_bad).then_some("Make sure the `repository` URL in `Cargo.toml` is correct".into()),
                Issue::NoMatchingTag => self.repos_used.get(0).filter(|_| needs_valid_repo_tag && !wrong_manifest && !fuzzy_fallbacks && !repo_may_be_bad).zip(self.crate_version.as_deref()).map(|(r, ver)| format!("`git tag v{} {}; git push origin --tags`", safe_markdown_code(ver), hex::encode(r.sha1)).into()),
                Issue::NoMatchingTagForVer(verstr) => self.repos_used.get(0).filter(|_| needs_valid_repo_tag && !fuzzy_fallbacks && !wrong_manifest && !repo_may_be_bad).map(|r| format!("`git tag v{} {}; git push origin --tags`", safe_markdown_code(verstr), hex::encode(r.sha1)).into()),
                Issue::NoMatchingTagForSha { sha1 } => self.crate_version.as_deref().filter(|_| needs_valid_repo_tag && !wrong_manifest && !repo_may_be_bad).map(|ver| format!("`git tag v{} {}; git push origin --tags`", safe_markdown_code(ver), hex::encode(sha1)).into()),
                Issue::PackageVersionNotFoundInRepo => (!check_technical_error).then_some("Make sure to commit and tag after bumping the version number.".into()),
                Issue::SearchedRepoForReleases => None,
                Issue::SearchedRepoForCommit => None,
                Issue::UsedHeadCommitAsFallback => None,
                Issue::ManifestWithoutPackage => None,
                Issue::MismatchedFeatures => None,
                Issue::MismatchedProduct(_) => None,
                Issue::MismatchedPackage(_) => (!repo_may_be_bad || needs_valid_repo_tag).then(|| format!("If you have a monorepo, you can use git tags in format `cratename/v{}`", safe_markdown_code(self.crate_version.as_deref().unwrap_or("1.2.3"))).into()),
                Issue::MismatchedDependency(_) => (!sha1_may_be_bad || needs_valid_repo_tag).then(|| "Make sure to commit and tag after updating `Cargo.toml`".into()),
                Issue::MismatchedAbsentBuildRs => None,
                Issue::ManifestNotCompared => None,
                Issue::ManifestOrigMissing => None,
                Issue::ManifestCargoMissing => None,
                Issue::ManifestParsingError(msg) => (!bad_tarball && !repo_may_be_bad && !path_may_be_bad && !fuzzy_fallbacks && !check_technical_error).then_some(if msg.contains("workspace") {"Cargo Workspaces combined with submodules may be impossible to parse. File bugs at <https://gitlab.com/lib.rs/cargo_toml>"} else {"If the file is valid, file a bug at <https://gitlab.com/lib.rs/cargo_toml>"}.into()),
                Issue::CargoLockParsingError(_) |
                Issue::CargoLockNonUtf8 => (!fuzzy_fallbacks).then_some("File a bug in Cargo or <https://github.com/rustsec/rustsec/tree/main/cargo-lock>".into()),
                Issue::FileTooLarge(_) => Some("If you don't need this file, use `include` or `exclude` in `Cargo.toml`, otherwise file a bug at <https://gitlab.com/lib.rs/main>".into()),
                Issue::NonUtf8Path(_) => Some("Use a UTF-8 file system, or keep file names in ASCII".into()),
                Issue::SkippedGitModulePath(_) => Some("Use `include` patterns in `Cargo.toml` to include only relevant files".into()),
                Issue::TarSizeInvalid(_) |
                Issue::InvalidPath(_) => Some("Looks like a Cargo bug?".into()),
                Issue::InvalidGitPath(_, _) |
                Issue::RepoIncompatibility(_) |
                Issue::BrokenSubmodulePath(_) => Some("File a bug <https://gitlab.com/lib.rs/main> if you think it should work".into()),
                Issue::NotFoundInRepo(_) => None,
                Issue::CargoMovedFile(_) => Some("Paths using `../` are difficult to support. Consider putting (a copy of) the file in the crate's directory".into()),
                Issue::FoundInRepoButNotAtPath(_) => (!fuzzy_fallbacks && !path_may_be_bad).then_some(if needs_valid_repo_tag {"Use git tags for releases to help find the correct version"} else {"File a bug <https://gitlab.com/lib.rs/main> if this file should be there"}.into()),
                Issue::ManifestFuzzyDiffers => (!fuzzy_fallbacks && !sha1_may_be_bad).then_some("Make sure to commit any changes before publishing the crate. To exclude dev deps, require them via `path` without `version`.".into()),
                Issue::FileDiffersFromRepo(_) => (!fuzzy_fallbacks && !repo_may_be_bad).then_some("Make sure to commit any changes before publishing the crate.".into()),
                Issue::ShaNotFoundInRepo(_) |
                Issue::TagWrongSha(_) => (!repo_may_be_bad && !check_technical_error).then_some("Create git tags after comitting any changes, and commit changes after bumping versions and running `cargo update`.".into()),
                Issue::ShaNotInHeadBranch(_) => None,
                Issue::RepositoryGone(_) => (!check_technical_error).then_some("Make sure the `repository` URL in `Cargo.toml` is correct and works with `git clone`. The repository must be public.".into()),
                Issue::FetchError(_) |
                Issue::FatalError(_) => (!fuzzy_fallbacks && !wrong_manifest && !repo_may_be_bad).then_some("Ooops? If this shouldn't happen, file a bug <https://gitlab.com/lib.rs/main>".into()),
            };

            if section != Skip {
                by_section.entry(section).or_insert_with(Vec::new).push(ReportMessage {
                    important, issue, fix_markdown,
                });
            }
        }

        let mut by_path = HashMap::default();

        let mut by_section: Vec<_> = by_section.into_iter().filter_map(|(section, mut issues)| {
            // stable sort keeps original order
            issues.sort_by_key(|m| {
                (!m.important, !m.issue.is_fatal())
            });
            if let VerificationFailure(path) = section {
                if !issues.is_empty() {
                    let issue = issues.swap_remove(0);
                    by_path.entry(path.parent().unwrap_or(Path::new("")))
                        .or_insert_with(Vec::new)
                        .push((issue, path.file_name().unwrap_or(path.as_os_str())));
                }
                None
            } else {
                issues.dedup_by_key(|a| a.issue.message());
                Some((section, issues))
            }
        }).collect();
        by_section.sort_by_key(|(s, _)| match s {
            Skip => 0,
            TarOddball => 1,
            VCS => 2,
            RepoPath => 3,
            Tagging => 4,
            Search => 5,
            Manifest => 6,
            CargoLock => 7,
            RepoBugs => 8,
            VerificationFailure(_) => 9,
            OtherError => 10,
        });

        let messages = by_section.into_iter().flat_map(|(_, issues)| issues.into_iter()).collect::<Vec<_>>();
        (messages, by_path)
    }

    #[must_use] pub fn result(&self) -> ReportResult {
        let fatal_errors = self.issues.iter().any(|i| i.is_fatal());
        let cargo_lock_skipped = self.issues.iter().any(|i| matches!(i, Issue::NotFoundInRepo(p) if p.as_os_str() == "Cargo.lock"));

        let unclear_vcs_info = self.issues.iter().any(|i| matches!(i, Issue::UsedHeadCommitAsFallback | Issue::SearchedRepoForCommit | Issue::SearchedRepoForReleases | Issue::PathInRepoNotDiscovered | Issue::PackageNotFoundInRepo| Issue::PackageNotFoundAtKnownPath | Issue::NoPathInPackage));
        let has_a_good_tag = self.repos_used.get(0).is_some_and(|r| r.tag.is_some()) && !unclear_vcs_info && !self.issues.iter().any(|i| matches!(i, Issue::NoMatchingTagForVer(_) | Issue::NoMatchingTagForSha{..} | Issue::TooManyErrors));

        let all_files_verified = !fatal_errors && self.verified_files > 0 && if self.version < 15 {
            self.skipped_files <= 3 && (self.tried_files == self.verified_files || (cargo_lock_skipped && self.tried_files == self.verified_files + 1))
        } else {
            self.tried_files == self.verified_files + self.skipped_files
        };

        ReportResult { all_files_verified, unclear_vcs_info, has_a_good_tag, cargo_lock_skipped }
    }
}

impl<'a> Comparator<'a> {
    pub fn new(crate_name: &'a str, ver: &'a str, tarball: &'a [u8], path_in_repo: Option<&'a str>, sha1: Option<&'a [u8; 20]>, checkout_base_path: &'a Path) -> Self {
        Self {
            repository_is_gone: false,
            crate_name, ver,
            tarball,

            path_in_repo: path_in_repo.map(Cow::Borrowed),
            sha1: sha1.and_then(|s| s[..].try_into().ok()),
            repos: NestedRepos {
                repository: None,
                submodules: HashMap::default(),
                checkout_base_path,
            },
            manifest_published: None,
            manifest_orig: None,
            report: Report {
                version: 28,
                crate_version: Some(ver.into()),
                repos_used: Vec::new(),
                issues: vec![],
                verified_files: 0,
                tried_files: 0,
                skipped_files: 0,
                created_at: Some(Utc::now()),
                diffs: HashMap::default(),
            }
        }
    }

    // TODO: recheck commit shas and path_in_repo by getting and comparing Cargo.toml

    fn get_repo(&mut self, repo_url: &Repo, stop: &AtomicU32) -> Result<(), ComprError> {
        let mut fetch_shas = vec![];

        if self.sha1.is_none() {
            self.report.push(Issue::NoSha1InPackage);
        }

        if self.path_in_repo.is_none() {
            self.report.push(Issue::NoPathInPackage);
        }

        let (git_repo, _) = crate_git_checkout::open_repository(repo_url, self.repos.checkout_base_path)?;
        let repo_path = git_repo.path();
        let mut remote = ConnectedRemote::new(&git_repo, "origin")?;

        if stop.load(Acquire) > 0 {
            return Err(ComprError::Aborted);
        }

        // Search tags in the repo
        let refs = remote.ls()?;
        let ver_no_suffix = self.ver.split_once('+').map(|(v,_)| v).unwrap_or(self.ver);
        let ver_no_zero = ver_no_suffix.strip_suffix(".0");
        let mut tags_match_sha1 = false;
        let mut tags_match_sha1_without_peel = false;
        let mut version_tag_found = false;
        let mut num_tags = 0;
        let mut refs: Vec<_> = refs.into_iter().filter_map(|(full_ref_name, target_oid)| {
            debug!("{} refs {full_ref_name} = {}", self.crate_name, hex::encode(target_oid));
            let orig_tag_name = full_ref_name.strip_prefix("refs/tags/")?;
            num_tags += 1;

            // fuzzy logic to find most similar/relevant tag
            let mut fitness = 0u8;
            let mut peeled = false; // fat git tags
            let orig_tag_name = orig_tag_name.strip_suffix("^{}").inspect(|_| { peeled = true; fitness += 1; }).unwrap_or(orig_tag_name);

            let sha_matches = self.sha1.is_some_and(|sha1| if target_oid == sha1 { fitness += 20; true } else { false });
            tags_match_sha1 |= sha_matches;

            let name = orig_tag_name.strip_prefix(self.crate_name).map(|r| {
                fitness += 4;
                r.trim_start_matches(|c:char| c.is_ascii_punctuation())
            }).unwrap_or_else(|| {
                if orig_tag_name.contains(self.crate_name) {
                    fitness += 2;
                }
                orig_tag_name
            });
            let name = name.strip_prefix('v').map(|n| {
                // there's a crate using 'refs/tags/v/1.2.3'
                fitness += 2; n.trim_start_matches(|c:char| c.is_ascii_punctuation())
            }).unwrap_or(name);
            let tag_matches = if self.ver == name || ver_no_suffix == name { fitness += 1; true } else { name.ends_with(self.ver) || name.starts_with(ver_no_suffix) };
            version_tag_found |= tag_matches;
            if !peeled && sha_matches {
                tags_match_sha1_without_peel = true;
            }

            // somehow git fetch doesn't automatically follow fat tags!?
            if tag_matches && peeled {
                fetch_shas.push(target_oid);
            }

            (tag_matches || sha_matches || name.contains(ver_no_suffix) || ver_no_zero.is_some_and(|z| z == name))
                .then_some((fitness, SmolStr::from(orig_tag_name), target_oid))
        }).take(400).collect();

        // if there are many tags, pick just few most fitting
        pick_top_n_unstable_by(&mut refs, 7, |a,b| b.0.cmp(&a.0));

        if version_tag_found {
            if let Some(sha1) = &self.sha1 {
                // If the package has a sha1, and tags for versions, there should be a match
                let mut found_version_tag_matching_sha = false;
                let tag_with_ver: Vec<_> = refs.iter()
                    .map(|(_, name, oid)| (name.as_str(), oid))
                    .filter(|&(tag_name, _)| tag_name.contains(ver_no_suffix))
                    .take_while(|&(_, oid)| {
                        found_version_tag_matching_sha |= oid == sha1;
                        !found_version_tag_matching_sha
                    })
                    .collect();
                if !found_version_tag_matching_sha && !tag_with_ver.is_empty() {
                    self.report.push(Issue::TagWrongSha(tag_with_ver[0].0.into()));
                }
            }
        } else {
            self.report.push(Issue::NoMatchingTagForVer(ver_no_suffix.into()));
            debug!("No tag for {}@{ver_no_suffix} ({num_tags} total)", self.crate_name);
        }

        if !tags_match_sha1 {
            // Report on sha1 embedded by Cargo
            if let Some(sha1) = self.sha1 {
                fetch_shas.push(sha1);
                self.report.push(Issue::NoMatchingTagForSha { sha1 });
            }
        }

        // Get commits of the relevant tags and sha1. It may fail due to a bad sha1.
        let mut fetch_tags: Vec<_> = refs.iter().map(|(_, name, _)| String::from(&**name)).collect();
        let fetch_res = blocking::block_in_place(format!("fetch {fetch_tags:?}"), || crate_git_checkout::fetch_repository(&mut remote, repo_path, &CloneOptions {
            shallow_depth: (!fetch_shas.is_empty() || !fetch_tags.is_empty()).then_some(5),
            avoid_heavy_repos: false,
            deadline: Instant::now() + Duration::from_secs(60),
            fetch_tags: fetch_tags.clone(),
            fetch_shas: fetch_shas.clone(),
            update: true,
        }, stop)).inspect_err(|e| self.report.push(Issue::FetchError(e.source().map(|s| s.to_string()).unwrap_or_else(|| e.to_string()))));

        if let Err(crate_git_checkout::Error::Git(e)) = fetch_res {
            if is_fatal_git_err(&e) {
                return Err(e.into());
            }
            if is_repo_corrupted(&e) {
                error!("Deleting {}", repo_path.display());
                drop(remote); // it won't recover from rm
                let _ = std::fs::remove_dir_all(repo_path);
                return Err(e.into());
            }

            // fetching of fat tags doesn't seem to follow the tag's commit and tree!?
            if !fetch_shas.is_empty() && tags_match_sha1_without_peel && self.sha1.as_ref().is_some_and(|sha| refs.iter().any(|(.., oid)| oid == sha)) {
                // just fetch tags if they match
                fetch_shas.clear();
            } else {
                // fetch could have failed due to a bad sha1, but if the sha1 wasn't the cause
                // then just try fetching everything
                if tags_match_sha1 || self.sha1.is_none() {
                    fetch_tags.clear();
                }
            }

            warn!("Fetching {} tags again due to previous error: {e}", fetch_tags.len());
            if let Err(e) = blocking::block_in_place("fetch2", || crate_git_checkout::fetch_repository(&mut remote, repo_path, &CloneOptions {
                shallow_depth: None,
                avoid_heavy_repos: false,
                deadline: Instant::now() + Duration::from_secs(60),
                fetch_tags,
                fetch_shas: vec![],
                update: true,
            }, stop)) {
                // if git2 can't, try again with CLI git
                if !repo_path.exists() {
                    return Err(ComprError::BrokenRepo("bad checkout"));
                }
                let status = std::process::Command::new("git")
                    .current_dir(repo_path)
                    .args(["fetch", "origin", "--filter=blob:limit=1m", "--update-shallow"])
                    .status();
                warn!("using git cli fallback {status:?} on {repo_path:?}");
                if !status.as_ref().is_ok_and(|s| s.success()) {
                    self.report.push(Issue::FetchError(e.source().map(|s| s.to_string()).unwrap_or_else(|| e.to_string())));
                    if let Err(e) = status {
                        self.report.push(Issue::FatalError(format!("git CLI fallback: {e}")));
                    }
                }
            }
        }

        // now the repo should have the sha1 fetched, so it can be checked
        if let Some(sha1) = &self.sha1 {
            let maybe_commit = Oid::from_bytes(sha1).and_then(|oid| git_repo.find_commit(oid))
                .inspect_err(|e| debug!("sha1's commit == {e}"));
            let keep_the_sha = if let Ok(commit) = maybe_commit {
                if let Some(path) = self.path_in_repo.as_deref() {
                    let tree = commit.tree().inspect_err(|e| debug!("bad tree of commit {sha1:?} == {e}"))?;
                    if let Ok(item) = tree.get_path(&Path::new(path).join("Cargo.toml")) {
                        debug!("There is a {path}/Cargo.toml");
                        match item.to_object(&git_repo).and_then(|o| o.peel_to_blob()) {
                            Ok(blob) => match Manifest::from_slice(blob.content()) {
                                Ok(manif) => if manif.package.as_ref().map(|p| p.name()).is_some_and(|n| n == self.crate_name) {
                                    true
                                } else {
                                    self.report.push(Issue::PackageNotFoundAtKnownPath);
                                    self.path_in_repo = None;
                                    true
                                },
                                Err(err) => {
                                    self.report.push(Issue::ManifestParsingError(err.to_string()));
                                    false
                                },
                            },
                            Err(err) => {
                                debug!("Can't read {path}/Cargo.toml: {err}");
                                false
                            },
                        }
                    } else {
                        self.report.push(Issue::PackageNotFoundAtKnownPath);
                        false
                    }
                } else {
                    debug!("got sha1, but no path in repo");
                    true // keep the sha, search for the path later
                }
            } else {
                if tags_match_sha1 {
                    // if there's a correct tag, it's our fault we didn't get it
                    let _ = std::fs::remove_dir_all(repo_path);
                    return Err(ComprError::BrokenRepo("tag fetch failed"));
                }
                self.report.push(Issue::ShaNotFoundInRepo(*sha1));
                false
            };
            if !keep_the_sha {
                refs.retain(|(_, tag_name, commit_id)| {
                    if commit_id == sha1 { // that shouldn't happen? but just in case refs[0] is used later
                        error!("bad tag? {tag_name}");
                    }
                    commit_id != sha1
                });
                self.sha1 = None;
            }
        }

        // last chance to quickly complete the data
        let mut fallback_path_in_repo = None;
        if self.sha1.is_none() || self.path_in_repo.is_none() {
            let mut any_packages = false;
            let mut matched_package = false;
            let mut matched_package_version = false;
            let candidate_refs = self.sha1.as_ref().into_iter().map(|sha| ("sha1", sha))
                .chain(refs.iter().map(|(_, name, oid)| (name.as_str(), oid)));
            for (debug_name, commit_id) in candidate_refs {
                let Ok(commit) = Oid::from_bytes(commit_id).and_then(|oid| git_repo.find_commit(oid)) else {
                    debug!("Bad commit oid: {}", hex::encode(commit_id));
                    continue;
                };
                let (packages_found, _) = crate_git_checkout::find_manifests_in_tree(&git_repo, &commit, self.path_in_repo.as_deref(), stop)?;
                let found_ver = packages_found.iter().filter(|&f| f.manifest.package.as_ref().is_some_and(|p| {
                    any_packages = true;
                    if p.name == self.crate_name {
                        matched_package = true;
                        if p.version.get().is_ok_and(|v| v == self.ver) {
                            fallback_path_in_repo = None; // will use exact
                            matched_package_version = true;
                            return true;
                        }
                        if fallback_path_in_repo.is_none() {
                            fallback_path_in_repo = Some(f.inner_path.clone());
                        }
                    }
                    false
                }))
                .min_by_key(|f| (
                    !self.path_in_repo.as_deref().is_some_and(|p| p == f.inner_path), // prefer match
                    f.inner_path.len() as u16 // prefer rootmost package to avoid tests
                ));

                if let Some(found_ver) = found_ver {
                    fallback_path_in_repo = None;
                    if self.sha1.is_none() {
                        debug!("Using sha1 from tag {debug_name} of {}", self.crate_name);
                        self.sha1 = found_ver.commit.as_bytes().try_into().ok();
                    }
                    debug!("got path in repo after search? {:?} vs new {:?}", self.path_in_repo, found_ver.inner_path);
                    self.path_in_repo = Some(found_ver.inner_path.clone().into());
                    self.report.push(Issue::SearchedRepoForReleases);
                    break;
                }
            }
            if matched_package && !matched_package_version {
                self.report.push(Issue::PackageVersionNotFoundInRepo);
            } else if !matched_package && any_packages {
                self.report.push(Issue::PackageNotFoundInRepo);
            } else {
                debug!("scaning of {} refs finished: any?{any_packages} pkg?{matched_package} ver?{matched_package_version}", refs.len());
            }
        }

        let mut reachable_from = None;

        // still no sha after checking tags, find commit by content similarity
        if !tags_match_sha1 && self.sha1.is_none() {
            std::thread::scope(|s| {
                info!("Using slow similarity search of the repo of {}@{}", self.crate_name, self.ver);
                let (tx, rx) = crossbeam_channel::bounded(4);
                let res = s.spawn(blocking::closure("git-hash-tarball", || {
                    let mut num_hashed = 1u8;
                    read_archive_files(self.tarball, self.crate_name, self.ver, |_, mut file, is_safe| {
                        if !is_safe || num_hashed >= 128 {
                            return Ok(());
                        }
                        // code files and cargo.lock are better indicators than large blobs anyway
                        if file.size() < 1_000_000 {
                            let path = &*file.path()?;
                            // autogenerated won't be in the repo
                            if path.as_os_str() != ".cargo_vcs_info.json" && path.as_os_str() != "Cargo.toml" {
                                let is_cargo_toml = path.as_os_str() == "Cargo.toml.orig";
                                let mut buf = Vec::new();
                                file.read_to_end(&mut buf)?;
                                // extra point for cargo.toml
                                let mask = (1u128 << num_hashed) | if is_cargo_toml { 1 } else { 0 };
                                tx.send((buf, mask)).map_err(|_| crate_git_checkout::Error::Aborted)?;
                                num_hashed += 1;
                            }
                        }
                        Ok(())
                    }, stop)?;
                    drop(tx);
                    Ok::<_, ComprError>(())
                }));

                let (mut start_oid, commit_history) = history_iterator_from_head(&git_repo, &mut remote, stop)?;

                let hashes: HashMap<_, u128> = rx.into_iter().map(|(b, mask)| (blob_hash(&b), mask)).collect();
                debug!("{} tarball has {} unique hashable files", self.crate_name, hashes.len());

                res.join().map_err(|_| ComprError::InternalError("panic"))??;
                if hashes.is_empty() {
                    return Err(ComprError::FatalError("no files in the tarball!?".into()));
                }

                let mut best_commit = None::<[u8; 20]>;
                let mut best_score = 0;
                let mut best_commit_fallback = None::<[u8; 20]>;
                let mut best_score_fallback = 0;
                let mut path_in_repo = None;

                let mut any_packages = false;
                let mut matched_package = false;
                let mut total_failures = 0;
                let bits_in_hashes = hashes.values().copied().fold(0, |a, b| a|b);
                let mut dead_ends = HashSet::default();
                let deadline = Instant::now() + Duration::from_secs(15);
                let commits_from_history = commit_history.into_iter().filter(|c| !c.is_merge).map(|c| c.commit)
                    .take(1000).take_while(|_| Instant::now() < deadline);
                let commits = commits_from_history.chain(
                        // if commits in main fail, try all known tags
                        refs.iter().filter_map(|(_, _, commit_id)| {
                            git_repo.find_object(Oid::from_bytes(commit_id).ok()?, None).ok()?.peel_to_commit().ok()
                        })
                        .inspect(|_| {
                            start_oid = Oid::zero(); // tags are no longer guaranteed reachable from main
                        })
                    );
                for hist_commit in commits {
                    if stop.load(Acquire) > 0 {
                        return Err(ComprError::Aborted);
                    }
                    let mut bits_remaining = bits_in_hashes;
                    let new_score = self.check_commit_fitness(&git_repo, stop, &hist_commit, &mut dead_ends, &hashes, &mut bits_remaining)
                        .inspect_err(|_| debug!("Can't compute fitness"))?;
                    if new_score > best_score || bits_remaining == 0 {
                        if new_score >= best_score_fallback {
                            best_score_fallback = new_score;
                            best_commit_fallback = hist_commit.id().as_bytes().try_into().ok();
                        }
                        debug!("{}, Potentially better commit {new_score}/{best_score_fallback} remains={} {:08x} dead={}", self.crate_name, bits_remaining.count_ones(), bits_remaining, dead_ends.len());
                        let (packages_found, _) = match crate_git_checkout::find_manifests_in_tree(&git_repo, &hist_commit, path_in_repo.as_deref(), stop) {
                            Ok(res) => res,
                            Err(err) => {
                                debug!("{} -  slow search can't get manifests from commit: {err}", self.crate_name);
                                total_failures += 1;
                                if total_failures > 10 {
                                    warn!("Parsing packages is borked for {}, giving up", self.crate_name);
                                    break;
                                }
                                continue;
                            }
                        };
                        let found_ver = packages_found.into_iter().filter(|f| f.manifest.package.as_ref().is_some_and(|p| {
                            any_packages = true;
                            if p.name == self.crate_name {
                                matched_package = true;
                                if p.version.get().is_ok_and(|v| v == self.ver) {
                                    fallback_path_in_repo = None; // will use exact
                                    return true;
                                }
                                if fallback_path_in_repo.is_none() {
                                    fallback_path_in_repo = Some(f.inner_path.clone());
                                }
                            }
                            false
                        }))
                        .min_by_key(|f| (
                            !self.path_in_repo.as_deref().is_some_and(|p| p == f.inner_path), // prefer match
                            f.inner_path.len() as u16 // prefer rootmost package to avoid tests
                        ));

                        if let Some(found_ver) = found_ver {
                            path_in_repo = Some(found_ver.inner_path);
                            best_commit_fallback = None;
                            fallback_path_in_repo = None;
                            best_score = new_score;
                            best_commit = hist_commit.id().as_bytes().try_into().ok();
                            if bits_remaining == 0 {
                                info!("Found a perfect match for the commit in {}", self.crate_name);
                                break;
                            }
                            debug!("Found approx match ({best_score}) for the commit in {}", self.crate_name);
                        }
                    }
                }
                let matched_exact_version = path_in_repo.is_some();
                self.report.push(Issue::SearchedRepoForCommit);
                info!("Closest {} commit was {best_score}-or-{best_score_fallback}/{}", self.crate_name, hashes.len());
                if let Some(sha1) = best_commit.or(best_commit_fallback) {
                    reachable_from = start_oid.as_bytes().try_into().ok().filter(|_| !start_oid.is_zero());
                    self.sha1 = Some(sha1);
                    if let Some(path_in_repo) = path_in_repo {
                        self.path_in_repo = Some(Cow::Owned(path_in_repo));
                    }
                }
                if !matched_exact_version {
                    if matched_package {
                        self.report.push(Issue::PackageVersionNotFoundInRepo);
                    } else if any_packages {
                        self.report.push(Issue::PackageNotFoundInRepo);
                    } else {
                        warn!("Search was useless, no packages at all");
                    }
                }
                Ok(())
            })?;
        }

        let has_valid_sha1 = self.sha1.as_ref().and_then(|sha| Oid::from_bytes(sha).ok())
            .and_then(|oid| git_repo.find_commit(oid).inspect_err(|e| warn!("got sha for an invalid commit {oid}: {e}"))
                .ok()?.tree().inspect_err(|e| warn!("commit {oid} has bad tree: {e}")).ok())
            .is_some();
        if !has_valid_sha1 {
            if let Some(sha1) = self.sha1 {
                self.report.push(Issue::ShaNotFoundInRepo(sha1));
            }

            let commit = git_repo.head().and_then(|h| h.peel_to_commit())
                .or_else(|_| git_repo.find_reference("refs/heads/main")?.peel_to_commit())
                .or_else(|_| git_repo.find_reference("refs/heads/master")?.peel_to_commit())
                .or_else(|_| git_repo.find_reference("FETCH_HEAD")?.peel_to_commit())
                .inspect_err(|e| error!("repo has no main? {e} {repo_url:?}"))?;
            self.sha1 = commit.id().as_bytes().try_into().ok();
            reachable_from = self.sha1;
            self.report.push(Issue::UsedHeadCommitAsFallback);
        }

        let Some(sha1) = self.sha1 else {
            return Err(ComprError::BrokenRepo("no usable commit"));
        };

        let tag = refs.into_iter().find(|(.., commit_id)| commit_id == &sha1).map(|(_, tag_name, _)| tag_name.into());
        // if there's a nice tag, check if it's in the main branch
        if tag.is_some() && reachable_from.is_none() {
            info!("got a nice tag {tag:?}, so will check if it's in the main branch");
            match history_iterator_from_head(&git_repo, &mut remote, stop) {
                Ok((start_oid, h)) => {
                    let mut checked = 0;
                    if h.into_iter().any(|c| { checked += 1; c.commit.id().as_bytes() == sha1 }) {
                        reachable_from = start_oid.as_bytes().try_into().ok();
                        debug!("The commit is accesible from main branch, {checked} commits back");
                    } else {
                        self.report.push(Issue::ShaNotInHeadBranch(checked));
                    }
                },
                Err(e) => {
                    warn!("will not check main branch for {tag:?} of {}: {e} {e:?}", self.crate_name);
                },
            }
        }
        drop(remote); // disconnect

        if self.path_in_repo.is_none() {
            self.report.push(Issue::PathInRepoNotDiscovered);

            if let Some(p) = fallback_path_in_repo {
                self.path_in_repo = Some(p.into());
            }
        }

        self.report.repos_used.push(UsedRepo {
            sha1,
            url: repo_url.canonical_git_url(),
            submodule_path: None,
            crate_path: self.path_in_repo.as_ref().map(|c| (**c).to_owned()),
            tag,
            reachable_from,
        });

        self.repos.repository = Some(git_repo);

        Ok(())
    }

    fn check_commit_fitness(&self, git_repo: &Repository, stop: &AtomicU32, commit: &git2::Commit<'_>, dead_ends: &mut HashSet<[u8; 20]>, hashes: &HashMap<[u8; 20], u128>, bits_remaining: &mut u128) -> Result<u32, crate_git_checkout::Error> {
        let mut score = 0;
        let start_bits = *bits_remaining;
        let mut tree = commit.tree()?;
        if let Some(path) = self.path_in_repo.as_deref() {
            if let Ok(subtree) = tree.get_path(Path::new(path)).and_then(|e| e.to_object(git_repo)).and_then(|o| o.peel_to_tree()) {
                score += 2;
                tree = subtree;
            }
        }
        score += self.check_commit_tree_fitness(git_repo, stop, tree, dead_ends, hashes, bits_remaining)?;
        score += (start_bits ^ *bits_remaining).count_ones();
        Ok(score)
    }

    fn check_commit_tree_fitness(&self, git_repo: &Repository, stop: &AtomicU32, tree: Tree<'_>, dead_ends: &mut HashSet<[u8; 20]>, hashes: &HashMap<[u8; 20], u128>, bits_remaining: &mut u128) -> Result<u32, crate_git_checkout::Error> {
        let mut score = 0;
        if dead_ends.contains(tree.id().as_bytes()) {
            return Ok(0);
        }
        for e in &tree {
            if stop.load(Acquire) > 0 {
                return Err(crate_git_checkout::Error::Aborted);
            }
            if (e.filemode() & 0o0120000) == 0o0120000 {
                continue; // symlink ;(
            }
            match e.kind() {
                Some(ObjectType::Tree) => {
                    if let Ok(tree) = e.to_object(git_repo).and_then(|o| o.peel_to_tree()) {
                        score += self.check_commit_tree_fitness(git_repo, stop, tree, dead_ends, hashes, bits_remaining)?;
                    }
                },
                Some(ObjectType::Blob) => {
                    if let Some(&mask) = hashes.get(e.id().as_bytes()) {
                        if (*bits_remaining & mask) != 0 {
                            score += 1; // first time seen
                        } else if score == 0 {
                            score += 1; // always return non-0 score if there's any match for dead_ends
                        }
                        *bits_remaining &= !mask;
                    }
                },
                _ => continue,
            }
            if *bits_remaining == 0 {
                break;
            }
        }
        if score == 0 {
            if let Ok(sha) = tree.id().as_bytes().try_into() {
                if dead_ends.len() > 1000 {
                    let mut n = true;
                    dead_ends.retain(|_| { n = !n; n });
                }
                dead_ends.insert(sha);
            }
        }
        Ok(score)
    }

    pub fn check(mut self, repo_url: &Repo, stop: &AtomicU32) -> Result<Report, ComprError> {
        blocking::block_in_place(format_args!("repo-ck-{repo_url:?}"), || {
            let res = self.get_repo(repo_url, stop);
            self.result_to_report(res)?;
            if self.repository_is_gone {
                return Ok(());
            }

            if stop.load(Acquire) > 0 {
                return Err(ComprError::Aborted);
            }

            debug_assert!(self.sha1.is_some());
            let res = self.parse_gitmodules();
            self.result_to_report(res)
        })?;

        blocking::block_in_place(format!("repo-ck-{}", self.crate_name), move || {
            if !self.repository_is_gone {
                let res = self.check_inner(stop);
                self.result_to_report(res)?;
            }
            info!("{}@{} {:#?}", self.crate_name, self.ver, self.report.result());
            Ok(self.report)
        })
    }

    fn result_to_report(&mut self, res: Result<(), ComprError>) -> Result<(), ComprError> {
        use crate_git_checkout::Error as E2;
        match res {
            ok @ Ok(_) => ok,
            Err(ComprError::Checkout(E2::Git(e))) if e.class() == git2::ErrorClass::Http && e.code() == git2::ErrorCode::Auth  => {
                // remember this and don't retry
                self.repository_is_gone = true;
                self.report.push(Issue::RepositoryGone(format!("Can't checkout the repository. It's either gone or private: {e}")));
                Ok(())
            },
            Err(err @ (ComprError::Timeout | ComprError::Aborted | ComprError::BrokenRepo(_) | ComprError::InternalError(_) | ComprError::Checkout(E2::Aborted | E2::Git(_) | E2::NoDiskSpace | E2::BrokenRepo(_)))) => {
                warn!("Fatal error ({}): {err} {:?}", self.crate_name, err.source());
                Err(err)
            },
            Err(err) if self.sha1.is_none() => {
                error!("Fatal error [no sha1] ({}): {err} {:?}", self.crate_name, err.source());
                Err(err)
            },
            Err(err) => {
                debug!("Soft fail ({}): {err} {:?}", self.crate_name, err.source());
                for e in anyhow::Error::from(err).chain() {
                    self.report.push(Issue::FatalError(e.to_string()));
                }
                Ok(())
            },
        }
    }

    fn check_inner(&mut self, stop: &AtomicU32) -> Result<(), ComprError> {
        let (sender, files_in_tar) = crossbeam_channel::bounded(1);
        let mut tar_file_paths = Vec::new();
        std::thread::scope(move |s| {
            let name = self.crate_name;
            let ver = self.ver;
            let archive = self.tarball;
            let t = s.spawn(blocking::closure("git-untarball", move || {
                // gunzips on a thread
                read_archive_files(archive, name, ver, move |prefix, file, _is_safe| {
                    let path = file.path()?;
                    let Ok(rel_path) = path.strip_prefix(prefix).map(PathBuf::from) else {
                        return Err(ComprError::issue(Issue::InvalidPath(path.into_owned())).into());
                    };

                    let size = file.size();
                    let file_data = if size < MAX_FILE_SIZE {
                        let mut buf = Vec::with_capacity(size as _);
                        file.take(size+1).read_to_end(&mut buf)?;
                        Some(buf)
                    } else {
                        None
                    };
                    sender.send((file_data, size, rel_path)).map_err(|_| crate_git_checkout::Error::Aborted.into())
                }, stop)
            }));

            let (ref repository, sha1) = self.repos.repository.take().zip(self.sha1.as_ref()).ok_or(ComprError::InternalError("no repo?"))?;
            let commit_tree = repository.find_commit(Oid::from_bytes(sha1)?)?.tree()?;

            let mut aborted_early = false;
            for (file_data, size, tar_rel_path) in files_in_tar {
                self.report.tried_files += 1;

                if stop.load(Acquire) > 0 {
                    return Err(ComprError::Aborted);
                }

                let Some(file_name) = tar_rel_path.file_name() else {
                    self.report.push(Issue::InvalidPath(tar_rel_path));
                    continue;
                };

                if file_name == ".cargo_vcs_info.json" {
                    if size > 1_000 {
                        self.report.push(Issue::FileTooLarge(tar_rel_path.clone()));
                    }
                    self.report.skipped_files += 1;
                    continue; // TODO: parse?
                }

                if let Some(p) = tar_rel_path.to_str() {
                    tar_file_paths.push(p.into());
                } else {
                    self.report.push(Issue::NonUtf8Path(tar_rel_path.as_os_str().to_string_lossy().into_owned()));
                }

                if file_name == "Cargo.toml" {
                    if size > MAX_CARGO_TOML_SIZE {
                        self.report.push(Issue::FileTooLarge(tar_rel_path.clone()));
                    }
                    if self.manifest_published.is_some() {
                        self.report.push(Issue::ManifestParsingError("Duplicate Cargo.toml?".into()));
                    }
                    self.manifest_published = file_data.as_deref()
                        .ok_or_else(|| "Could not get file data".to_string())
                        .map(|f| Manifest::from_slice(f).map_err(|e| e.to_string()))
                        .and_then(|res| res)
                        .map_err(|err| {
                            self.report.push(Issue::ManifestParsingError(err));
                        }).ok();
                    self.report.skipped_files += 1;
                    continue; // it's not in repo
                }

                let Some(file_data) = file_data else {
                    self.report.push(Issue::FileTooLarge(tar_rel_path));
                    continue;
                };
                if file_data.len() != size as usize {
                    self.report.push(Issue::TarSizeInvalid(tar_rel_path));
                    continue;
                }

                if file_name == ".git" && size < 400 && tar_rel_path.parent().is_some_and(|s_path| self.repos.submodules.contains_key(s_path)) {
                    if let Ok(s) = std::str::from_utf8(&file_data) {
                        if s.starts_with("gitdir: ") {
                            self.report.push(Issue::SkippedGitModulePath(s.into()));
                            self.report.skipped_files += 1;
                            continue;
                        }
                    }
                }

                let mut cargo_lock_parsed_ok = false;
                if tar_rel_path.as_os_str() == "Cargo.lock" {
                    if size > MAX_CARGO_LOCK_SIZE {
                        self.report.push(Issue::FileTooLarge(file_name.into()));
                        continue;
                    }
                    if let Ok(lock_str) = std::str::from_utf8(&file_data) {
                        match lock_str.parse::<cargo_lock::Lockfile>() {
                            Ok(_) => cargo_lock_parsed_ok = true, // TODO: verify that checksums match crates.io
                            Err(err) => self.report.push(Issue::CargoLockParsingError(err.to_string())),
                        }
                    } else {
                        self.report.push(Issue::CargoLockNonUtf8);
                    }
                    debug!("Cargo.lock check: {cargo_lock_parsed_ok}");
                }

                let repo_rel_path = if file_name == "Cargo.toml.orig" {
                    if self.manifest_orig.is_some() {
                        self.report.push(Issue::ManifestParsingError("Duplicate Cargo.toml.orig?".into()));
                    }
                    if size > MAX_CARGO_TOML_SIZE {
                        self.report.push(Issue::FileTooLarge(tar_rel_path.clone()));
                    }

                    // orig manifest may require a workspace, so allow parsing to fail
                    self.manifest_orig = Manifest::from_slice(&file_data).map_err(|e| {
                        error!("Manifest orig parse error: {e}");
                        for e in anyhow::Error::from(e).chain() {
                            self.report.push(Issue::ManifestParsingError(format!("Cargo.toml.orig parse error: {e}")));
                        }
                    }).ok().map(|mut manif| {
                        let crate_dir = Path::new("/").join(self.path_in_repo.as_deref().unwrap_or_default());
                        debug!("Git manifest is at {} (has path={})", crate_dir.display(), self.path_in_repo.is_some());
                        if let Err(e) = manif.complete_from_abstract_filesystem::<cargo_toml::Value, _>(GitFS {
                            root: &commit_tree,
                            // needs to be absolute to distinguish workspace readme paths
                            current_dir: &crate_dir,
                            repo: repository,
                        }, None) {
                            for e in anyhow::Error::from(e).chain() {
                                error!("Manifest workspace parse failure {}: {e}", self.crate_name);
                                self.report.push(Issue::ManifestParsingError(format!("Cargo.toml.orig workspace error: {e}")));
                            }
                        }
                        manif
                    });
                    if self.manifest_orig.is_none() {
                        warn!("Manifest orig parse failure {}", self.crate_name);
                    }

                    PathBuf::from("Cargo.toml") // will be rebased on path_in_repo
                } else {
                    tar_rel_path.clone()
                };

                let expected_repo_path = if let Some(path_in_repo) = self.path_in_repo.as_deref().filter(|&p| !p.is_empty()) {
                    let mut expected_repo_path = Path::new(path_in_repo).join(&repo_rel_path);

                    // check if it's a file in the root of the crate
                    if let Some(expected_file_name) = repo_rel_path.file_name().filter(|&f| f == repo_rel_path.as_os_str() && f != "Cargo.toml" && f != "Cargo.lock" && f != "Cargo.toml.orig") {
                        // fallback to manifest_published works only for a few old crates
                        let manifest = self.manifest_orig.as_ref()
                            .or_else(|| { debug!("no manifest yet for {repo_rel_path:?} check"); self.manifest_published.as_ref() });
                        if let Some(package) = manifest.and_then(|m| m.package.as_ref()) {
                            // `readme.workspace = true` is translated to a file path without `../` ;(
                            let fixup_paths = [
                                package.readme.get().ok().and_then(|r| r.as_path()),
                                package.license_file.as_ref().and_then(|l| l.get().ok().map(|p| p.as_path())),
                                // I guess build.rs could move too, but it seems too risky to allow that
                            ];
                            for fixup_path in fixup_paths.into_iter().flatten() {
                                if fixup_path == repo_rel_path {
                                    debug!("skipping fixup of {repo_rel_path:?} <= {package:?}");
                                    break;
                                }
                                // Cargo moves ../../README to /README, and case-insensitive fs can cause case change to value in Cargo.toml
                                // https://github.com/rust-lang/cargo/issues/13722
                                if fixup_path.file_name().is_some_and(|f| f.eq_ignore_ascii_case(expected_file_name)) {
                                    // if it works anyway, then Cargo would have used it
                                    if fixup_path.components().any(|c| matches!(c, Component::ParentDir)) {
                                        // https://github.com/rust-lang/cargo/issues/12127
                                        let duplicate_path_still_works = self.repos.get_path_with_symlinks(&mut self.report, &commit_tree, repository, "".into(), &expected_repo_path, 0, stop).is_ok();
                                        if duplicate_path_still_works {
                                            self.report.push(Issue::CargoMovedFile(expected_repo_path.clone()));
                                            break;
                                        }
                                    }
                                    debug!("There is a changed path {fixup_path:?} for {repo_rel_path:?}");
                                    expected_repo_path = if fixup_path.is_absolute() { // workspace
                                        fixup_path.components().skip(1).collect()
                                    } else {
                                        crate_git_checkout::join_path(Path::new(path_in_repo), fixup_path)
                                            .ok_or_else(|| ComprError::issue(Issue::InvalidGitPath("Cargo rewrite".into(), fixup_path.into())))?
                                    };
                                    self.report.push(Issue::CargoMovedFile(expected_repo_path.clone()));
                                    break;
                                }
                            }
                        }
                    }
                    expected_repo_path
                } else {
                    repo_rel_path
                };

                let (entry_submodule_path, matching_oid) = match self.repos.get_path_with_symlinks(&mut self.report, &commit_tree, repository, "".into(), &expected_repo_path, 0, stop) {
                    Ok((repo, obj)) => (repo, Some(obj)),
                    Err(ComprError::PathNotFound) => ("".into(), None),
                    Err(ComprError::RecoverableIssue(i)) => {
                        debug!("not found in repo at {expected_repo_path:?}: {}", i.message());
                        if i.is_fatal() { // otherwise let NotFound issue to be logged later
                            self.report.push(i);
                        }
                        ("".into(), None)
                    },
                    Err(e) => {
                        error!("Check of {expected_repo_path:?} failed hard, aborting: {e} {:?}", e.source());
                        return Err(e);
                    },
                };
                let had_no_matching_entry = matching_oid.is_none();
                let entry_repository = if entry_submodule_path.as_os_str() == "" {
                    repository
                } else {
                    self.repos.submodules.get(entry_submodule_path.as_path())
                        .and_then(|s| s.cloned_repo.as_ref())
                        .ok_or_else(|| ComprError::InternalError("bad submodule path"))?
                };

                if let Some(oid) = matching_oid {
                    let blob = entry_repository.find_blob(oid).map_err(|_| ComprError::BrokenRepo("bad oid found"))?;
                    match Self::check_path(blob.content(), &file_data, &tar_rel_path)? {
                        None => {
                            self.report.verified_files += 1;
                            debug!("{}+{}={} vs {};  {} verified ok", self.report.skipped_files, self.report.verified_files, self.report.skipped_files + self.report.verified_files, self.report.tried_files, tar_rel_path.display());
                            continue;
                        },
                        Some((issue, patchfile)) => {
                            self.report.push(issue);
                            if let Some(patchfile) = patchfile {
                                self.report.diffs.insert(tar_rel_path.to_string_lossy().into_owned(), patchfile);
                            }
                            if tar_rel_path != expected_repo_path {
                                info!("Problem with a remapped path tar {tar_rel_path:?} => repo {expected_repo_path:?}");
                            }
                            if tar_rel_path.as_os_str() == "Cargo.toml.orig" && Some(true) == Self::compare_manifests_without_dev_deps(blob.content(), &file_data) {
                                debug!("Forgiven different Cargo.toml.orig");
                                self.report.push(Issue::ManifestFuzzyDiffers);
                                self.report.skipped_files += 1;
                            }
                        },
                    }
                }

                if cargo_lock_parsed_ok {
                    debug_assert_eq!(tar_rel_path.as_os_str(), "Cargo.lock");
                    debug!("Forgiven different Cargo.lock");
                    self.report.skipped_files += 1;
                }

                debug!("{}+{}={} {} FAILED the check", self.report.skipped_files, self.report.verified_files, self.report.tried_files, tar_rel_path.display());

                let oid_for_data = Oid::from_bytes(&blob_hash(&file_data[..]))?;
                if let Err(err) = entry_repository.find_blob(oid_for_data) {
                    if had_no_matching_entry { // already reported difference
                        debug!("{tar_rel_path:?} not in repo at all (oid check {err})");
                        if file_data.len() < 1000 {
                            debug!("{tar_rel_path:?} == {:?}", String::from_utf8_lossy(&file_data));
                        }
                        self.report.push(Issue::NotFoundInRepo(if tar_rel_path == Path::new("Cargo.toml.orig") { "Cargo.toml".into() } else { tar_rel_path }));
                    }
                } else {
                    self.report.push(Issue::FoundInRepoButNotAtPath(expected_repo_path));
                }
                if self.report.issues.len() > 100 {
                    aborted_early = true;
                    self.report.push(Issue::TooManyErrors);
                    break;
                }
            }

            if !aborted_early || self.report.verified_files == self.report.tried_files + self.report.skipped_files {
                self.complete_and_compare_manifests(&tar_file_paths);
            }
            Ok(t.join().unwrap()?)
        })
    }

    fn complete_and_compare_manifests(&mut self, tar_file_paths: &[String]) {
        if let Some(manif) = &mut self.manifest_published {
            let fs = PathsFs { tar_file_paths };
            // Required by autobins, etc.
            if let Err(e) = manif.complete_from_abstract_filesystem::<cargo_toml::Value, _>(&fs, None) {
                 self.report.push(Issue::ManifestParsingError(e.to_string()));
            }
        }

        if let Err(issue) = self.compare_manifests() {
            if self.report.skipped_files > 0 { // Cargo.toml would be marked as skipped, making it pass verification
                self.report.skipped_files -= 1;
            }
            debug!("manifests compare fail; fles in repo {tar_file_paths:#?}");
            if let Some((a, b)) = self.manifest_published.as_ref().zip(self.manifest_orig.as_ref()) {
                let a = toml::to_string_pretty(a).unwrap_or_default();
                let b = toml::to_string_pretty(b).unwrap_or_default();
                if let Some(diff) = Self::diff(a.as_bytes(), b.as_bytes()) {
                    debug!("Manifests diff:\n{diff}");
                }
            }
            self.report.push(issue);
        } else {
            debug!("manifests compared ok");
        }
    }


    fn compare_manifests(&mut self) -> Result<(), Issue> {
        let mpub = self.manifest_published.as_ref().ok_or(Issue::ManifestCargoMissing)?;
        let morig = self.manifest_orig.as_ref().ok_or(Issue::ManifestOrigMissing)?;

        let ppub = mpub.package.as_ref().ok_or(Issue::ManifestWithoutPackage)?;
        if ppub.name() != self.crate_name {
            self.report.push(Issue::MismatchedPackage(format!("Found wrong Cargo.toml for '{}'", ppub.name())));
        }

        if ppub.version() != self.ver {
            self.report.push(Issue::MismatchedPackage(format!("Got wrong version in Cargo.toml '{}'", ppub.version())));
        }

        if mpub.features != morig.features {
            self.report.push(Issue::MismatchedFeatures);
        }

        if mpub.features != morig.features {
            self.report.push(Issue::MismatchedFeatures);
        }

        if let Err(issue) = Self::compare_product("lib", mpub.lib.as_ref(), morig.lib.as_ref()) {
            self.report.push(issue);
        }

        let pub_bins = mpub.bin.len();
        let orig_bins = morig.bin.len();
        if pub_bins != orig_bins {
            debug!("bin mismatch: {:#?} vs orig {:#?}", mpub.bin, morig.bin);
            return Err(Issue::MismatchedProduct(format!("Number of [[bin]] inconsistent; published={pub_bins}; orig={orig_bins}; ed={:?}", ppub.edition)));
        }

        let mut pub_bin: Vec<_> = mpub.bin.iter().collect();
        pub_bin.sort_by(|a, b| a.path.cmp(&b.path).then(a.name.cmp(&b.name)));
        let mut orig_bin: Vec<_> = morig.bin.iter().collect();
        orig_bin.sort_by(|a, b| a.path.cmp(&b.path).then(a.name.cmp(&b.name)));

        for (pub_bin, orig_bin) in pub_bin.into_iter().zip(orig_bin) {
            if let Err(issue) = Self::compare_product("bin", Some(pub_bin), Some(orig_bin)) {
                self.report.push(issue);
            }
        }

        let missing = BTreeMap::new();
        let all_the_deps = [
            // crates-io crates don't care about dev deps
            (&mpub.dependencies, &morig.dependencies),
            (&mpub.build_dependencies, &morig.build_dependencies)
        ].into_iter()
            .chain(mpub.target.iter().flat_map(|(t, deps)| {
                let orig_deps = morig.target.get(t);
                [
                    (&deps.dependencies, orig_deps.map(|o| &o.dependencies).unwrap_or(&missing)),
                    (&deps.build_dependencies, orig_deps.map(|o| &o.build_dependencies).unwrap_or(&missing)),
                ]
            }));

        for (pub_deps, orig_deps) in all_the_deps {
            for (key, pub_dep) in pub_deps {
                let Ok(pub_req) = pub_dep.try_req() else {
                    self.report.push(Issue::MismatchedDependency(format!("'{key}' uses workspace inheritance even on crates.io")));
                    continue;
                };
                // this is assymetric, it's OK to have fewer deps in the published crate (Cargo will drop path/git deps)
                let Some(orig_dep) = orig_deps.get(key) else {
                    self.report.push(Issue::MismatchedDependency(format!("'{key}' is missing in the original Cargo.toml")));
                    continue;
                };
                let Ok(orig_req) = orig_dep.try_req() else {
                    self.report.push(Issue::MismatchedDependency(format!("'{key}' uses workspace inheritance, but the root workspace is not available")));
                    continue;
                };
                let pub_pkg = pub_dep.package().unwrap_or(key);
                let orig_pkg = orig_dep.package().unwrap_or(key);
                if pub_pkg != orig_pkg {
                    self.report.push(Issue::MismatchedDependency(format!("'{key}' uses different packages: '{pub_pkg}' vs '{orig_pkg}'")));
                    continue;
                }
                if pub_req != orig_req && orig_req != "*" {
                    self.report.push(Issue::MismatchedDependency(format!("'{key}' uses different versions: '{pub_req}' vs '{orig_req}'")));
                    continue;
                }
            }
        }

        if ppub.workspace.is_some() {
            return Err(Issue::ManifestParsingError("crates.io package has a workspace".into()));
        }

        let porig = morig.package.as_ref().ok_or(Issue::ManifestWithoutPackage)?;
        if ppub.name != porig.name {
            return Err(Issue::MismatchedPackage("package.name".into()));
        }

        if ppub.version != porig.version {
            return Err(Issue::MismatchedPackage("package.version".into()));
        }

        if ppub.repository != porig.repository {
            return Err(Issue::MismatchedPackage("package.repository".into()));
        }

        if ppub.links != porig.links {
            return Err(Issue::MismatchedPackage("package.links".into()));
        }

        let build_ppub = ppub.build.as_ref().map(|b| b.is_some()).unwrap_or(false);
        let build_porig = porig.build.as_ref().map(|b| b.is_some()).unwrap_or(false);

        if build_ppub != build_porig {
            warn!("Build mismatch in {}: {:?} vs {:?}", self.crate_name, ppub.build, porig.build);
            if !build_ppub {
                // A missing build.rs can't cause more damage than a present one
                self.report.push(Issue::MismatchedAbsentBuildRs);
            } else {
                return Err(Issue::MismatchedPackage(format!("package.build: crates.io={}, orig={}",
                    ppub.build.as_ref().map(|r| r.display()).unwrap_or("<unset>"),
                    porig.build.as_ref().map(|r| r.display()).unwrap_or("<unset>"),
                )));
            }
        }

        Ok(())
    }

    fn compare_product(name: &str, pub_prod: Option<&Product>, orig_prod: Option<&Product>) -> Result<(), Issue> {
        let pub_exists = pub_prod.is_some();
        let orig_exists = orig_prod.is_some();
        if pub_exists != orig_exists {
            return Err(Issue::MismatchedProduct(format!("Existence of [{name}] inconsistent: published={pub_exists}; orig={orig_exists}")));
        }

        if let Some((p, o)) = pub_prod.zip(orig_prod) {
            if p.name != o.name {
                error!("Product name diff: {:?} {:?} orig {orig_prod:?}", p.name, o.name);
            }
        }
        Ok(())
    }

    fn check_path(repo_bytes: &[u8], file_data: &[u8], tarball_path: &Path) -> Result<Option<(Issue, Option<String>)>, ComprError> {
        if repo_bytes == file_data {
            Ok(None)
        } else if Self::iter_without_cr(repo_bytes).eq(Self::iter_without_cr(file_data)) {
            debug!("Had to fix line endings in {}", tarball_path.display());
            Ok(None)
        } else {
            let patchfile = Self::diff(repo_bytes, file_data);
            if let Some(patchfile) = &patchfile {
                debug!("PATCHFILE for {}:\n{patchfile}", tarball_path.display());
            }
            Ok(Some((Issue::FileDiffersFromRepo(tarball_path.into()), patchfile)))
        }
    }

    fn iter_without_cr(a: &[u8]) -> impl Iterator<Item = u8> + '_ {
        let mut until_nl = false;
        a.iter().copied().filter(move |&b| match b {
            b'\r' if !until_nl => { until_nl = true; false },
            b'\n' => { until_nl = false; true },
            _ => true,
        })
    }

    fn compare_manifests_without_dev_deps(orig: &[u8], published: &[u8]) -> Option<bool> {
        // cargo-hack used to strip dev deps?
        let mut orig: toml::Value = toml::from_str(std::str::from_utf8(orig).ok()?).ok()?;
        let mut published: toml::Value = toml::from_str(std::str::from_utf8(published).ok()?).ok()?;

        if orig == published {
            return Some(true);
        }

        orig.as_table_mut()?.remove("dev-dependencies");
        published.as_table_mut()?.remove("dev-dependencies");
        Some(orig == published)
    }

    fn diff(left: &[u8], right: &[u8]) -> Option<String> {
        use std::fmt::Write;

        if left.len() > MAX_DIFF_LEN || right.len() > MAX_DIFF_LEN || is_binary(left) || is_binary(right) {
            return None;
        }

        fn norm(mut data: &[u8]) -> Cow<'_, str> {
            // newline at end of file
            if let Some((b'\n', trimmed)) = data.split_last() {
                data = trimmed;
            }
            let data = String::from_utf8_lossy(data);
            if data.contains("\r\n") {
                data.replace("\r\n", "\n").into()
            } else {
                data
            }
        }

        let left = &*norm(left);
        let right = &*norm(right);

        let mut patchfile = String::new();

        let diff = similar::TextDiff::configure()
            .timeout(Duration::from_secs(6))
            .algorithm(similar::Algorithm::Patience)
            .newline_terminated(true)
            .diff_lines(left, right);
        let mut prev_equal = None;
        let mut is_in_equal = false;
        let mut has_skipped = false;
        for l in diff.iter_all_changes() {
            if l.tag() == ChangeTag::Equal {
                if is_in_equal {
                    has_skipped = prev_equal.is_some();
                    prev_equal = Some(l);
                    continue;
                }
                prev_equal = None;
                has_skipped = false;
                is_in_equal = true;
            } else {
                is_in_equal = false;
            }
            if let Some(l) = prev_equal.take() {
                if has_skipped {
                   patchfile.push_str("@\n");
                }
                writeln!(&mut patchfile, "{}{}", l.tag(), l.as_str().unwrap_or_default().trim_end()).ok()?;
            }
            writeln!(&mut patchfile, "{}{}", l.tag(), l.as_str().unwrap_or_default().trim_end()).ok()?;
        }
        Some(patchfile)
    }

    fn parse_gitmodules(&mut self) -> Result<(), ComprError> {
        let (repository, sha1) = self.repos.repository.as_ref().zip(self.sha1.as_ref()).ok_or(ComprError::InternalError("no repo"))?;

        let commit_tree = repository.find_commit(Oid::from_bytes(sha1)?)?.tree()?;

        let Some(gitmodules) = commit_tree.get_name(".gitmodules") else {
            return Ok(());
        };

        let gitmodules = gitmodules.to_object(repository)?.peel_to_blob()?;
        let gitmodules = gitmodules.content();
        if gitmodules.len() > 20_000 {
            self.report.push(Issue::RepoIncompatibility(format!(".gitmodules too large ({}B)", gitmodules.len())));
            return Ok(());
        }
        debug!("Found submodules: {}", String::from_utf8_lossy(gitmodules));

        let config = gix_config::File::default();
        let s = gix_submodule::File::from_bytes(gitmodules, None, &config)
            .map_err(|e| ComprError::issue(Issue::RepoIncompatibility(e.to_string())))?;
        for name in s.names().take(200) {
            let mut url = s.url(name).map_err(|e| ComprError::issue(Issue::RepoIncompatibility(e.to_string())))?.to_string();
            let s_path = s.path(name).map_err(|e| ComprError::issue(Issue::RepoIncompatibility(e.to_string())))?.to_string();
            match self.repos.submodules.entry(s_path.into()) {
                Entry::Occupied(old) => {
                    if old.get().url != url {
                        return Err(ComprError::issue(Issue::RepoIncompatibility(format!("Submodule {name} has two URLs: {url} and {}", old.get().url))));
                    }
                },
                Entry::Vacant(new) => {
                    let path = new.key();
                    match commit_tree.get_path(path) {
                        Ok(entry) if entry.kind().is_some_and(|k| k == ObjectType::Commit) => {
                            let commit_hash = entry.id().as_bytes().try_into().unwrap();
                            debug!("Found submodule {name} at {}/ == {} @ {}", new.key().display(), url, hex::encode(commit_hash));
                            if url.starts_with(['.','/']) { // submodule url can be a relative path! relative to parent repo URL!
                                if let Some(root_repo) = self.report.repos_used.get(0) {
                                    url = if let Some(url) = crate_git_checkout::join_path(root_repo.url.as_str().trim_end_matches(".git"), Path::new(&url)) { url.to_string_lossy().into_owned() } else {
                                        self.report.push(Issue::InvalidGitPath("bad submodule path".into(), url.into()));
                                        continue;
                                    };
                                }
                                info!("Used relative submodule URL for {name} -> {url}");
                            }
                            let url = match Repo::new(&url) {
                                Ok(r) => r.canonical_git_url(),
                                Err(e) => {
                                    self.report.push(Issue::RepoIncompatibility(e.to_string()));
                                    continue;
                                },
                            };
                            new.insert(SubmoduleDef {
                                url,
                                sha1: commit_hash,
                                cloned_repo: None,
                                failed: 0,
                            });
                        },
                        _ => {
                            self.report.push(Issue::BrokenSubmodulePath(path.to_owned()));
                        }
                    }
                },
            }
        }
        Ok(())
    }
}

impl NestedRepos<'_> {
    fn get_path_with_symlinks<'repo>(&mut self, report: &mut Report, default_tree: &Tree<'repo>, default_repo: &'repo Repository, root: PathBuf, path: &Path, mut indirection_depth: usize, stop: &AtomicU32) -> Result<(PathBuf, Oid), ComprError> {
        let (root_tree, repo) = if root.as_os_str() == "" {
            (default_tree.to_owned(), default_repo)
        } else if let Some(submodule) = self.submodules.get_mut(root.as_path()) {
            let s_repo = match &mut submodule.cloned_repo {
                Some(r) => r,
                s_repo @ None => {
                    info!("Cloning submodule {} @ {} + {}", submodule.url, root.display(), path.display());
                    if report.repos_used.len() > 15 {
                        return Err(ComprError::issue(Issue::RepoIncompatibility("Too many submodules".into())));
                    }
                    if submodule.failed > 4 {
                        return Err(ComprError::BrokenRepo("too many submodule failures"));
                    }

                    let s_repo_url = Repo::new(&submodule.url).map_err(|e| ComprError::issue(Issue::RepoIncompatibility(format!("Bad submodule URL {}: {e}", submodule.url))))?;
                    let fetch_deep = submodule.failed > 2;
                    let new_repo = crate_git_checkout::clone_repository(&s_repo_url, self.checkout_base_path, &CloneOptions {
                        shallow_depth: (!fetch_deep).then_some(2),
                        avoid_heavy_repos: false,
                        deadline: Instant::now() + Duration::from_secs(80),
                        fetch_tags: vec![],
                        fetch_shas: if fetch_deep { vec![] } else { vec![submodule.sha1] },
                        update: true,
                    }, stop)
                    .inspect_err(|e| {
                        submodule.failed += 1;
                        error!("Submodule won't work: {root:?} = {e:?}");
                    })?;

                    report.repos_used.push(UsedRepo {
                        sha1: submodule.sha1,
                        tag: None,
                        url: s_repo_url.canonical_git_url(),
                        submodule_path: Some(root.as_os_str().to_string_lossy().into_owned()),
                        crate_path: None,
                        reachable_from: None,
                    });
                    s_repo.get_or_insert(new_repo)
                },
            };
            let s_tree = s_repo.find_commit(Oid::from_bytes(&submodule.sha1)?).and_then(|c| c.tree())
                .inspect_err(|e| {
                    submodule.failed += 1;
                    error!("bad submodule commit: {e:?}");
                })?;
            debug!("found submodule at {root:?}");
            (s_tree, &*s_repo)
        } else {
            return Err(ComprError::InternalError("expected submodule"));
        };

        let mut path_so_far = PathBuf::with_capacity(path.as_os_str().len()); // ancestors().rev() is not a thing ;(
        let mut rest = path.components();
        let mut current_tree = root_tree.clone();
        while let Some(comp) = rest.next() {
            let Component::Normal(name) = comp else {
                return Err(ComprError::issue(Issue::InvalidGitPath("suspicious path".into(), path.into())));
            };
            let Some(name) = name.to_str() else {
                return Err(ComprError::issue(Issue::InvalidGitPath("non-UTF-8".into(), path.into())));
            };
            path_so_far.push(name);
            let Some(entry) = current_tree.get_name(name) else {
                warn!("entry {name} of {path_so_far:?} not found in tree");
                return Err(ComprError::PathNotFound); // expected
            };
            match entry.kind() {
                Some(ObjectType::Tree) => {
                    let obj = entry.to_object(repo)?; drop(entry);
                    current_tree = obj.into_tree().map_err(|_| ComprError::BrokenRepo("bad tree"))?;
                },
                // symlink?
                Some(ObjectType::Blob) if (entry.filemode() & 0o0120000) == 0o0120000 => {
                    let new_path = {
                        let obj = entry.to_object(repo)?;
                        let Some(link) = obj.as_blob() else {
                            return Err(ComprError::issue(Issue::InvalidGitPath("symlink blob".into(), path_so_far)));
                        };
                        let Ok(link_path) = std::str::from_utf8(link.content()).map(Path::new) else {
                            return Err(ComprError::issue(Issue::InvalidGitPath("symlink utf8".into(), path_so_far)));
                        };
                        if link_path.is_absolute() || link_path.has_root() {
                            return Err(ComprError::issue(Issue::InvalidGitPath("absolute symlink".into(), link_path.into())));
                        }
                        indirection_depth += 1;
                        if indirection_depth > 5 {
                            return Err(ComprError::issue(Issue::InvalidGitPath("too many symlinks".into(), path.into())));
                        }
                        path_so_far.pop(); // go back to dir
                        info!("Following a symlink at {path_so_far:?} + {link_path:?} + {} (in {path:?})", rest.as_path().display());
                        crate_git_checkout::join_path(path_so_far, link_path)
                            .ok_or_else(|| ComprError::issue(Issue::InvalidGitPath("symlink".into(), link_path.into())))?
                            .join(rest.as_path())
                    };
                    drop(entry); drop(root_tree); drop(current_tree);
                    return self.get_path_with_symlinks(report, default_tree, default_repo, root, &new_path, indirection_depth, stop).map_err(|e| match e {
                        ComprError::PathNotFound => ComprError::RecoverableIssue(Issue::InvalidGitPath("dangling symlink".into(), new_path)),
                        e => e,
                    });
                },
                Some(ObjectType::Blob) => return if rest.next().is_none() {
                    debug_assert!(entry.to_object(repo).ok().as_ref().and_then(|o| o.as_blob()).is_some());
                    Ok((root, entry.id()))
                } else {
                    Err(ComprError::issue(Issue::InvalidGitPath("unexpected blob".into(), path_so_far)))
                },
                Some(ObjectType::Commit) => {
                    indirection_depth += 1;
                    if indirection_depth > 5 {
                        return Err(ComprError::issue(Issue::InvalidGitPath("too many submodules".into(), path.into())));
                    }
                    drop(entry); drop(root_tree); drop(current_tree); drop(root);
                    return self.get_path_with_symlinks(report, default_tree, default_repo, path_so_far, rest.as_path(), indirection_depth, stop);
                },
                None | Some(ObjectType::Any | ObjectType::Tag) => return Err(ComprError::issue(Issue::InvalidGitPath("bad entry kind".into(), path_so_far))),
            }
        }
        Err(ComprError::PathNotFound)
    }
}

fn history_iterator_from_head<'r>(git_repo: &'r Repository, remote: &mut ConnectedRemote<'r>, stop: &AtomicU32) -> Result<(Oid, crate_git_checkout::HistoryIter<'r>), ComprError> {
    blocking::block_in_place("unshallow-hist", move || {
    let default_branch = remote.default_branch()?;
    debug!("def branch is {default_branch}");

    let commit_ref_maybe = git_repo.find_reference(&default_branch).and_then(|h| h.peel_to_commit());
    if git_repo.is_shallow() || commit_ref_maybe.is_err() {
        // unshallow to have history to search
        // TODO: --filter=blob:none would be great
        crate_git_checkout::fetch_repository(remote, git_repo.path(), &CloneOptions {
            shallow_depth: None,
            avoid_heavy_repos: true,
            deadline: Instant::now() + Duration::from_secs(90),
            fetch_tags: vec![],
            fetch_shas: vec![],
            update: true
        }, stop).inspect_err(|e| warn!("can't unshallow? {e}"))?;
    }

    let commit_ref = commit_ref_maybe
        .or_else(|_| git_repo.find_reference(&default_branch)?.peel_to_commit())
        .or_else(|_| git_repo.head()?.peel_to_commit())
        .or_else(|_| git_repo.find_reference("FETCH_HEAD")?.peel_to_commit())
        .inspect_err(|e| error!("expected repo to be unshallow {e}"))?;

    Ok((commit_ref.id(), crate_git_checkout::HistoryIter::new(commit_ref)))
    })
}

fn is_repo_corrupted(e: &GitError) -> bool {
    match e.class() {
        git2::ErrorClass::Reference => true,
        git2::ErrorClass::Odb if e.code() == git2::ErrorCode::NotFound => true,
        git2::ErrorClass::None if e.message().contains("internal error") => true,
        _ => {
            debug_assert!(!e.to_string().contains("corrupt"), "{e} {e:?}");
            false
        },
    }
}

fn is_fatal_git_err(e: &GitError) -> bool {
    match e.class() {
        git2::ErrorClass::Ssl => true,
        git2::ErrorClass::Http if e.code() == git2::ErrorCode::Auth => true, // that's github's 404
        // code for 404 is dodgy?
        git2::ErrorClass::Http if e.code() == git2::ErrorCode::NotFound || e.code() == git2::ErrorCode::GenericError && e.message().contains("404") => true,
        _ => {
            debug!("{e:?} {e} may be recoverable?");
            false
        },

    }
}

pub struct CompareDb {
    root_path: PathBuf,
}

impl CompareDb {
    #[must_use] pub fn new(root_path: PathBuf) -> Self {
        Self { root_path }
    }

    fn path(&self, crate_name: &str, crate_version: &str) -> io::Result<PathBuf> {
        if !mundne_path(crate_name) || !mundne_path(crate_version) {
            return Err(io::ErrorKind::PermissionDenied.into());
        }
        Ok(self.root_path.join(format!("_{crate_name}@{crate_version}.json")))
    }

    pub fn report_for(&self, crate_name: &str, crate_version: &str) -> io::Result<Report> {
        let data = std::fs::read(self.path(crate_name, crate_version)?)?;
        let mut report: Report = serde_json::from_slice(&data).map_err(|e| io::ErrorKind::InvalidData.into_io_error_with(e))?;
        if report.crate_version.is_none() {
            report.crate_version = Some(crate_version.into());
        }
        Ok(report)
    }

    pub fn save_report_for(&self, report: &Report, crate_name: &str, crate_version: &str) -> io::Result<()> {
        let data = serde_json::to_vec(report).map_err(|e| io::ErrorKind::InvalidInput.into_io_error_with(e))?;
        std::fs::write(self.path(crate_name, crate_version)?, data)
    }
}

fn is_binary(data: &[u8]) -> bool {
    let mut cnt = [0u16; 16];
    let data = &data[0..data.len().min(1<<16)];
    data.iter().for_each(|&b| if (b as usize) < cnt.len() { cnt[b as usize] += 1; });
    let min_expected_lines = data.len()/200;

    min_expected_lines > cnt[b'\n' as usize] as usize ||
    [0, 1, 2, 3, 4, 5, 6, 14, 15].into_iter().any(|n| cnt[n] > 0)
}

fn mundne_path(p: &str) -> bool {
    p.as_bytes().iter().all(|&b| {
        b >= b' ' && b != b'/' && b != b'\\' && b != b':' && b != b'"' && b < 127
    })
}

struct PathsFs<'f> {
    tar_file_paths: &'f [String],
}

impl AbstractFilesystem for PathsFs<'_> {
    fn file_names_in(&self, str_rel_path: &str) -> Result<StdHashSet<Box<str>>, io::Error> {
        let mut rel_path = Path::new(str_rel_path);
        let mut comp = rel_path.components();
        while comp.next() == Some(Component::CurDir) {
            rel_path = comp.as_path();
        }
        let res = self.tar_file_paths.iter()
            .filter_map(move |p| Path::new(p).strip_prefix(rel_path).ok())
            .filter_map(|name| match name.components().next() {
                Some(Component::Normal(p)) => p.to_str(),
                _ => None,
            })
            .map(From::from)
            .collect();
        Ok(res)
    }
}

fn blob_hash(data: &[u8]) -> [u8; 20] {
    use sha1::{Sha1, Digest};
    let mut hasher = Sha1::new();

    hasher.update(format!("blob {}\0", data.len()).as_bytes());
    hasher.update(data);
    hasher.finalize().into()
}

#[test]
fn dot_paths() {
    let fs = PathsFs { tar_file_paths: &["relative.rs".into(), "src/dir.rs".into()] };
    assert_eq!(fs.file_names_in(".").unwrap().len(), 2);
    assert_eq!(fs.file_names_in("././src").unwrap().len(), 1);
    assert_eq!(fs.file_names_in("../src").unwrap().len(), 0);
}
