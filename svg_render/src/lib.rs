// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use resvg::{tiny_skia, usvg};

pub struct SvgRenderer {
    options: usvg::Options<'static>,
}

impl SvgRenderer {
    #[must_use]
    pub fn new() -> Self {
        let mut options = usvg::Options::default();
        options.fontdb_mut().load_system_fonts();
        Self { options }
    }

    pub fn render_to_png(&self, svg_string: &str) -> Result<Vec<u8>, anyhow::Error> {
        let xml_tree = {
            let xml_opt = usvg::roxmltree::ParsingOptions {
                allow_dtd: true,
                ..Default::default()
            };
            usvg::roxmltree::Document::parse_with_options(svg_string, xml_opt)?
        };

        let tree = usvg::Tree::from_xmltree(&xml_tree, &self.options)?;
        let size = tree.size().to_int_size();
        let mut pixmap = tiny_skia::Pixmap::new(size.width(), size.height()).ok_or_else(|| anyhow::anyhow!("Pixmap size {size:?}"))?;
        resvg::render(&tree, tiny_skia::Transform::default(), &mut pixmap.as_mut());
        let png = pixmap.encode_png()?;
        Ok(png)
    }
}

