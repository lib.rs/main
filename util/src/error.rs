use std::error::Error as StdError;
use std::{fmt, io};

struct ErrSource<T>(T);

impl<T: StdError + 'static> StdError for ErrSource<T> {
    #[inline]
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        Some(&self.0)
    }
}

impl<T: fmt::Debug> fmt::Debug for ErrSource<T> {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl<T: fmt::Display> fmt::Display for ErrSource<T> {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

pub trait IntoIoError {
    fn into_io_error_with<E: StdError + Send + Sync + 'static>(self, err: E) -> io::Error;
}

impl IntoIoError for io::ErrorKind {
    #[cold]
    fn into_io_error_with<E: StdError + Send + Sync + 'static>(self, err: E) -> io::Error {
        io::Error::new(self, ErrSource(err))
    }
}
