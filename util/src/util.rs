pub use smartstring::alias::String as SmolStr;

use std::hash::BuildHasherDefault;
pub type FxBuildHasher = BuildHasherDefault<FxHasher>;
pub use rustc_hash::FxHasher;
pub type FxHashMap<K, V> = std::collections::HashMap<K, V, FxBuildHasher>;
pub type FxHashSet<V> = std::collections::HashSet<V, FxBuildHasher>;

pub mod error;

mod push_in_cap;
pub use push_in_cap::*;

pub use std::borrow::Cow;
use std::fmt::Arguments;

pub trait CowAscii {
    fn as_ascii_lowercase(&self) -> Cow<'_, str>;
    fn as_ascii_uppercase(&self) -> Cow<'_, str>;
    fn as_ascii_normalized<F>(&self, normalize: F) -> Cow<'_, str> where F: Fn(u8) -> u8;
}

impl<'a> CowAscii for Cow<'a, str> {
    #[inline]
    fn as_ascii_lowercase(&self) -> Self {
        match (**self).as_ascii_lowercase() {
            Cow::Borrowed(_) => self.clone(),
            Cow::Owned(o) => Cow::Owned(o),
        }
    }

    #[inline]
    fn as_ascii_uppercase(&self) -> Self {
        match (**self).as_ascii_uppercase() {
            Cow::Borrowed(_) => self.clone(),
            Cow::Owned(o) => Cow::Owned(o),
        }
    }

    #[inline]
    fn as_ascii_normalized<F>(&self, normalize: F) -> Self where F: Fn(u8) -> u8 {
        match (**self).as_ascii_normalized(normalize) {
            Cow::Borrowed(_) => self.clone(),
            Cow::Owned(o) => Cow::Owned(o),
        }
    }
}

impl CowAscii for str {
    fn as_ascii_lowercase(&self) -> Cow<'_, str> {
        if self.bytes().any(|b| b.to_ascii_lowercase() != b) {
            self.to_ascii_lowercase().into()
        } else {
            self.into()
        }
    }

    fn as_ascii_uppercase(&self) -> Cow<'_, str> {
        if self.bytes().any(|b| b.to_ascii_uppercase() != b) {
            self.to_ascii_lowercase().into()
        } else {
            self.into()
        }
    }
    fn as_ascii_normalized<F>(&self, normalize: F) -> Cow<'_, str> where F: Fn(u8) -> u8 {
        as_ascii_normalized(self, normalize)
    }
}

/// Truncates
#[inline(always)]
pub fn pick_top_n_unstable_hash<T>(hash: impl IntoIterator<Item = (T, f32)>, top_n_count: usize) -> Vec<(f32, T)> {
    let mut min = std::f32::INFINITY;
    let mut out = Vec::with_capacity(top_n_count * 2);
    for (k, v) in hash {
        if out.len() < top_n_count || v < min {
            if v < min {
                min = v;
            }
            out.push((v, k));
        }
    }
    pick_top_n_unstable_by(&mut out, top_n_count, |a, b| b.0.total_cmp(&a.0));
    out
}

/// Truncates
#[inline(always)]
pub fn pick_top_n_unstable_by<T>(vec: &mut Vec<T>, top_n_count: usize, cmp: impl Fn(&T, &T) -> std::cmp::Ordering) {
    sort_top_n_unstable_by(vec, top_n_count, &cmp);
    vec.truncate(top_n_count);
}

pub fn sort_top_n_unstable_by<T>(mut slice: &mut [T], top_n_count: usize, cmp: impl Copy + Fn(&T, &T) -> std::cmp::Ordering) {
    if top_n_count > 0 {
        if slice.len() / 2 >= top_n_count {
            let (unfinished_sort_before, ..) = slice.select_nth_unstable_by(top_n_count, cmp);
            slice = unfinished_sort_before;
        }
        slice.sort_unstable_by(cmp);
    }
}

#[inline]
#[cfg_attr(debug_assertions, track_caller)]
fn as_ascii_normalized<F>(s: &str, normalize: F) -> Cow<'_, str> where F: Fn(u8) -> u8 {
    let bytes = s.as_bytes();
    let len_ok = bytes.iter().take_while(|&&b| b > 127 || normalize(b) == b).count();
    if len_ok >= bytes.len() {
        return s.into();
    }
    let (okay, rest) = bytes.split_at(len_ok);

    let mut out = Vec::with_capacity(bytes.len());
    out.extend_in_cap(okay);

    rest.iter().copied().zip(out.spare_capacity_mut()).for_each(move |(b, out)| {
        out.write(if b > 127 { b } else { normalize(b) & 0x7F });
    });
    debug_assert_eq!(out.len() + rest.len(), bytes.len());
    unsafe { out.set_len(bytes.len()); };
    debug_assert!(std::str::from_utf8(&out).is_ok());
    unsafe { String::from_utf8_unchecked(out) }.into()
}

/// Compare letters case-insensitively and ignore separator char differences
#[must_use]
#[allow(clippy::trivially_copy_pass_by_ref)]
pub fn crate_name_fuzzy_eq(a: &str, b: &str) -> bool {
    if a.is_empty() || b.is_empty() {
        return false;
    }
    fn not_sep(&x: &u8) -> bool {
        !(x == b'-' || x == b'_' || x == b' ')
    }
    itertools::Itertools::zip_longest(
        a.as_bytes().iter().copied().filter(not_sep),
        b.as_bytes().iter().copied().filter(not_sep),
    ).all(|x| x.both().is_some_and(|(a, b)| a.to_ascii_lowercase() == b.to_ascii_lowercase()))
}

#[inline(always)]
#[must_use]
pub fn smol_fmt_write(args: Arguments<'_>) -> SmolStr {
    use std::fmt::Write;
    let mut s = SmolStr::new();
    let _ = s.write_fmt(args);
    s
}

#[macro_export]
macro_rules! smol_fmt {
    ($($arg:tt)*) => {
        $crate::smol_fmt_write(::std::format_args!($($arg)*))
    };
}
