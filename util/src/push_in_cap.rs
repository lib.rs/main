pub trait PushInCapacity<T> {
    fn push_in_cap(&mut self, val: T);
    fn extend_in_cap(&mut self, val: &[T]) where T: Clone;
}

pub trait PushString {
    fn push_in_cap(&mut self, val: char);
    /// This will reallocate if needed, but ignores >1 byte chars
    fn push_ascii(&mut self, val: u8);
    fn push_ascii_in_cap(&mut self, val: u8);
    fn push_str_in_cap(&mut self, val: &str);
}

impl<T> PushInCapacity<T> for Vec<T> {
    #[inline(always)]
    #[cfg_attr(debug_assertions, track_caller)]
    fn push_in_cap(&mut self, val: T) {
        debug_assert_ne!(self.capacity(), self.len());
        if self.capacity() != self.len() {
            self.push(val);
        }
    }

    /// Pure unbloated memcpy with a safety check
    #[inline(always)]
    #[cfg_attr(debug_assertions, track_caller)]
    fn extend_in_cap(&mut self, val: &[T]) where T: Clone {
        let needs_to_grow =  val.len() > self.capacity().wrapping_sub(self.len());
        debug_assert!(!needs_to_grow);
        if !needs_to_grow {
            self.extend_from_slice(val);
        }
    }
}

impl PushString for String {
    #[inline(always)]
    fn push_ascii(&mut self, ch: u8) {
        let ch = char::from(ch);
        if ch.len_utf8() == 1 {
            self.push(ch);
        }
    }

    #[inline(always)]
    #[cfg_attr(debug_assertions, track_caller)]
    fn push_ascii_in_cap(&mut self, ch: u8) {
        let ch = char::from(ch);
        let fits_a_byte = ch.len_utf8() == 1 && self.capacity() != self.len();
        debug_assert!(fits_a_byte, "{ch}");
        if fits_a_byte {
            self.push(ch);
        }
    }

    #[inline(always)]
    #[cfg_attr(debug_assertions, track_caller)]
    fn push_in_cap(&mut self, ch: char) {
        self.push_str_in_cap(ch.encode_utf8(&mut [0; 4]));
    }

    #[inline(always)]
    #[cfg_attr(debug_assertions, track_caller)]
    fn push_str_in_cap(&mut self, s: &str) {
        let needs_to_grow = s.as_bytes().len() > self.capacity().wrapping_sub(self.len());
        debug_assert!(!needs_to_grow);
        if !needs_to_grow {
            self.push_str(s);
        }
    }
}
