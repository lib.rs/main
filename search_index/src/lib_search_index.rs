use crate::stemmer::CustomNormalizer;
use rustc_hash::{FxHashMap as HashMap, FxHashSet as HashSet};
use categories::Synonyms;
use itertools::Itertools;
use levenshtein_automata::{Distance, LevenshteinAutomatonBuilder};
use log::{debug, error, info, warn};
use rich_crate::Origin;
use std::borrow::Cow;
use std::fs;
use std::path::Path;
use std::sync::Arc;
use tantivy::collector::TopDocs;
use tantivy::query::{FuzzyTermQuery, MoreLikeThisQuery, Query, QueryParser, QueryParserError, TermQuery};
use tantivy::schema::{TantivyDocument as Document, OwnedValue as Value, FAST, Field, IndexRecordOption, STORED, STRING, SchemaBuilder, Term, TextFieldIndexing, TextOptions};
use tantivy::tokenizer::{SimpleTokenizer, TextAnalyzer, Token, TokenStream, Tokenizer};
use tantivy::{Index, IndexWriter, SegmentReader, TantivyError};
use util::{crate_name_fuzzy_eq, sort_top_n_unstable_by, pick_top_n_unstable_by, smol_fmt, CowAscii, SmolStr};

mod stemmer;

#[derive(Debug, Clone, Default)]
pub struct SearchResults {
    pub crates: Vec<CrateFound>,
    /// Did you mean? queries.
    pub dym: Vec<SmolStr>,
    /// Summary
    pub top_keywords: Vec<SmolStr>,
    pub nothing_found_without_dym: bool,
    pub dym_added_fuzzy: bool,
    /// Various groups
    pub dividing_keywords: Vec<SmolStr>,
}

pub struct CrateSearchIndex {
    /// `Origin.to_str`
    origin_pkey: Field,
    /// as-is
    crate_name_field: Field,
    /// space-separated
    keywords_field: Field,
    description_field: Field,
    readme_field: Field,
    /// invisible keywords
    extra_field: Field,
    /// raw number
    monthly_downloads: Field,
    /// semver string
    crate_version: Field,
    /// number in range `0..=SCORE_MAX` denoting desirability of the crate
    crate_score: Field,

    tantivy_index: Index,
    synonyms: Arc<Synonyms>,
}

#[derive(Debug, Clone)]
pub struct CrateFound {
    pub origin: Origin,
    pub crate_name: SmolStr,
    pub description: String,
    /// space-separated
    pub keywords_s: String,
    /// space-separated
    pub keywords_normalized_s: String,
    pub score: f32,
    pub relevance_score: f32,
    pub crate_base_score: f32,
    pub version: SmolStr,
    pub monthly_downloads: u64,
    pub exact_match: bool,
    pub fuzzy_match: bool,
}

impl CrateFound {
    pub fn keywords_normalized(&self) -> impl Iterator<Item=&str> {
        self.keywords_normalized_s.split(' ')
    }

    pub fn keywords(&self) -> impl Iterator<Item=&str> {
        self.keywords_s.split(' ')
    }
}

pub struct Indexer {
    index: CrateSearchIndex,
    writer: IndexWriter,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum QueryType {
    /// Sort by ranking
    Keyword,
    /// Just return whatever matches without polishing the results
    Quick,
    /// Regular search
    Relevance,
}

impl CrateSearchIndex {
    pub fn new(data_dir: impl AsRef<Path>) -> tantivy::Result<Self> {
        let data_dir = data_dir.as_ref();
        let synonyms = Arc::new(Synonyms::new(data_dir)?);

        let dir_name = std::env::var_os("TANTIVY_INDEX_DIR");
        let index_dir = data_dir.join(dir_name.as_deref().unwrap_or("tantivy22".as_ref()));

        if !index_dir.exists() {
            return Self::reset_db_to_empty(&index_dir, synonyms);
        }
        let tantivy_index = Index::open_in_dir(index_dir)?;
        tantivy_index.tokenizers().register("keywords", KewordTokenizer(synonyms.clone()));
        tantivy_index.tokenizers().register("librs_stem",
                TextAnalyzer::builder(SimpleTokenizer::default())
                    .filter(CustomNormalizer::new(synonyms.clone()))
                    .build());
        let schema = tantivy_index.schema();

        Ok(Self {
            tantivy_index,
            origin_pkey: schema.get_field("origin").expect("schema"),
            crate_name_field: schema.get_field("crate_name").expect("schema"),
            keywords_field: schema.get_field("keywords").expect("schema"),
            description_field: schema.get_field("description").expect("schema"),
            readme_field: schema.get_field("readme").expect("schema"),
            extra_field: schema.get_field("extra").expect("schema"),
            monthly_downloads: schema.get_field("monthly_downloads").expect("schema"),
            crate_version: schema.get_field("crate_version").expect("schema"),
            crate_score: schema.get_field("crate_score").expect("schema"),
            synonyms,
        })
    }

    fn reset_db_to_empty(index_dir: &Path, synonyms: Arc<Synonyms>) -> tantivy::Result<Self> {
        let _ = fs::create_dir_all(index_dir);

        let mut schema_builder = SchemaBuilder::default();

        let keyword_field_indexing = TextFieldIndexing::default()
            .set_tokenizer("keywords")
            .set_fieldnorms(false)
            .set_index_option(IndexRecordOption::WithFreqsAndPositions); // negation seems to break when positions are missing
        let keyword_options = TextOptions::default()
            .set_indexing_options(keyword_field_indexing);
        let text_field_indexing = TextFieldIndexing::default()
            .set_fieldnorms(true)
            .set_tokenizer("librs_stem")
            .set_index_option(IndexRecordOption::WithFreqsAndPositions);
        let text_options = TextOptions::default().set_indexing_options(text_field_indexing);

        let origin_pkey = schema_builder.add_text_field("origin", STRING | STORED);
        let crate_name_field = schema_builder.add_text_field("crate_name", STRING | STORED);
        let keywords_field = schema_builder.add_text_field("keywords", keyword_options.clone() | STORED);
        let extra_field = schema_builder.add_text_field("extra", keyword_options);
        let description_field = schema_builder.add_text_field("description", text_options.clone() | STORED);
        let readme_field = schema_builder.add_text_field("readme", text_options);
        let crate_version = schema_builder.add_text_field("crate_version", STORED);
        let monthly_downloads = schema_builder.add_u64_field("monthly_downloads", STORED);
        let crate_score = schema_builder.add_f64_field("crate_score", STORED | FAST);

        let schema = schema_builder.build();
        let tantivy_index = Index::create_in_dir(index_dir, schema)?;

        Ok(Self {
            origin_pkey, crate_name_field, keywords_field, description_field,
            readme_field,
            extra_field,
            monthly_downloads, crate_version, crate_score, tantivy_index, synonyms,
        })
    }

    #[must_use] pub fn normalize_keyword<'a>(&'a self, kw: &'a str) -> &'a str {
        self.synonyms.normalize(kw, 2)
    }

    fn parse_query(&self, query_text: &str) -> Result<Box<dyn Query>, QueryParserError> {
        let mut query_parser = QueryParser::for_index(&self.tantivy_index, vec![
            self.crate_name_field, self.keywords_field, self.description_field, self.readme_field,
            self.extra_field,
        ]);
        query_parser.set_conjunction_by_default();
        query_parser.set_field_boost(self.crate_name_field, 2.0);
        query_parser.set_field_boost(self.keywords_field, 1.5);
        query_parser.set_field_boost(self.description_field, 1.1);
        query_parser.set_field_boost(self.readme_field, 0.4);
        query_parser.set_field_boost(self.extra_field, 0.6);
        query_parser.parse_query(query_text)
            .or_else(|e| {
                warn!("{query_text} did not parse: {e}");
                let mangled_query: String = query_text.chars().map(|ch| {
                    if ch.is_alphanumeric() {ch.to_ascii_lowercase()} else {' '}
                }).collect();
                query_parser.parse_query(mangled_query.trim())
            })
    }

    fn fetch_docs(&self, searcher: &tantivy::Searcher, query: &dyn Query, exact_match_text: &str, limit: usize) -> Result<Vec<CrateFound>, TantivyError> {
        let top_docs = searcher.search(query, &TopDocs::with_limit(limit).tweak_score(move |segment_reader: &SegmentReader| {
            let crate_score_reader = segment_reader.fast_fields().f64("crate_score").unwrap();
                move |doc, score| {
                    let mut crate_score = crate_score_reader.values.get_val(doc);
                    if crate_score > 0.4 { crate_score += 1. }; // if it's decent, relevancy starts to matter more than crate rank
                    (f64::from(score) * crate_score) as f32
                }
        }))?;

        let docs = top_docs.into_iter().map(|(relevance_score, doc_address)| {
            let doc: Document = searcher.doc(doc_address)?;
            let crate_base_score = take_f64(doc.get_first(self.crate_score)) as f32;
            let crate_name = SmolStr::from(get_as_str(doc.get_first(self.crate_name_field)));
            let origin = Origin::from_str(get_as_str(doc.get_first(self.origin_pkey)));

            let mut keywords_s = get_as_str(doc.get_first(self.keywords_field));
            // we fudge the keywords field to have crate name always,
            // but that's not supposed to be visible to users
            if let Some((n, rest)) = keywords_s.split_once(' ') {
                if n == crate_name {
                    keywords_s = rest;
                }
            }

            Ok(CrateFound {
                exact_match: crate_name_fuzzy_eq(&crate_name, exact_match_text),
                fuzzy_match: false,
                crate_base_score,
                relevance_score,
                score: 0.,
                crate_name,
                description: get_as_str(doc.get_first(self.description_field)).into(),
                keywords_s: keywords_s.into(),
                keywords_normalized_s: String::new(),
                version: get_as_str(doc.get_first(self.crate_version)).into(),
                monthly_downloads: if origin.is_crates_io() { take_int(doc.get_first(self.monthly_downloads)) } else { 0 },
                origin,
            })
        })
        .collect::<Result<Vec<CrateFound>, TantivyError>>()?;

        Ok(docs)
    }

    pub fn similar_crates(&self, origin: &Origin) -> tantivy::Result<Vec<(Origin, f32)>> {
        let reader = self.tantivy_index.reader()?;
        let searcher = reader.searcher();
        let crate_name = origin.package_name_icase();
        let pkey = Term::from_field_text(self.crate_name_field, crate_name);
        let query = TermQuery::new(
            pkey,
            IndexRecordOption::Basic,
        );
        let mut the_doc = searcher.search(&query, &TopDocs::with_limit(1))?;
        let Some((_, the_doc_addr)) = the_doc.pop() else { error!("not found {crate_name}"); return Ok(vec![]) };
        let the_doc: Document = searcher.doc(the_doc_addr)?;

        let query = MoreLikeThisQuery::builder()
            .with_min_term_frequency(1)
             .with_min_doc_frequency(2)
             .with_max_doc_frequency(5000) // async is important and popular…
             .with_stop_words(["proc-macro", "implementation", "api", "tool", "parser", "utility", "crate", "subcommand", "rust", "for",
                "macro", "client", "test", "wrapper", "bindings", "no-std"].into_iter().map(String::from).collect())
             .with_document_fields(vec![
                (self.keywords_field, the_doc.get_all(self.crate_name_field).chain(the_doc.get_all(self.keywords_field)).cloned().collect()),
                (self.readme_field, the_doc.get_all(self.readme_field).cloned().collect())
            ]);
             // .with_document(*doc_addr);
        let similar_docs = searcher.search(&query, &TopDocs::with_limit(10).tweak_score(move |segment_reader: &SegmentReader| {
            let crate_score_reader = segment_reader.fast_fields().f64("crate_score").unwrap();
                move |doc, match_score| {
                    let mut crate_score = crate_score_reader.values.get_val(doc);
                    if crate_score > 0.5 { crate_score += 2. };
                    (f64::from(match_score) * crate_score) as f32
                }
        }))?;

        similar_docs.into_iter()
            .filter(move |&(_, addr)| addr != the_doc_addr)
            .map(move |(similarity, addr)| {
                let doc: Document = searcher.doc(addr)?;
                let origin = Origin::from_str(get_as_str(doc.get_first(self.origin_pkey)));
                Ok((origin, similarity))
            }).collect()
    }

    /// Assume the query is garbage, and just show anything at all
    pub fn search_fallback(&self, query_text: &str, limit: usize) -> tantivy::Result<SearchResults> {
        let query_text = query_text.split(' ').map(|w| self.synonyms.normalize(w, 2)).join(" ");
        let results = self.search(&query_text, limit, QueryType::Relevance)?;

        let fudged = if query_text.contains(' ') { query_text.replace(' ', " OR ") }
            else if query_text.contains('-') { query_text.replace('-', " OR ") }
            else { return Ok(results); };

        if results.crates.len() >= limit && results.crates[0].crate_base_score > 0.4 {
            return Ok(results);
        }
        let mut res = self.search(&fudged, limit, QueryType::Relevance)?;
        res.nothing_found_without_dym = true;
        Ok(res)
    }

    /// if `query_type` is Keyword, sorts by internal crate score (relevance is still used to select the crates)
    /// Second arg is distinctive keywords
    pub fn search(&self, query_text: &str, limit: usize, query_type: QueryType) -> tantivy::Result<SearchResults> {
        let query_text = query_text.trim();
        let query = self.parse_query(query_text)?;

        // keywords:"foo" query
        let query_text = query_text.strip_prefix("keywords:").unwrap_or(query_text).trim_matches('"');

        let reader = self.tantivy_index.reader()?;
        let searcher = reader.searcher();
        let expanded_limit = (limit + 50 + limit / 2).max(200); // 200 is a hack for https://github.com/tantivy-search/tantivy/issues/700
        let mut docs = self.fetch_docs(&searcher, &query, query_text, expanded_limit)?;

        if cfg!(debug_assertions) {
            for (nth, d) in docs.iter().take(10).enumerate() {
                debug!("{nth} {}: s={}, r={}, b={}", d.crate_name, d.score, d.relevance_score, d.crate_base_score);
            }
        }

        if query_type != QueryType::Keyword { // keywords have their workaround
            // the query could be a crate name, and tantivy keeps missing exact matches!
            self.fix_missing_exact_match(&mut docs, query_text, &searcher)?;
        }

        let nothing_found_without_dym = docs.is_empty();
        let (dym_added_fuzzy, mut dym) = if query_type != QueryType::Quick && query_type != QueryType::Keyword {
            self.did_you_mean_fuzzy(&mut docs, query_text, &searcher)?
        } else { (false, Vec::new()) };

        assign_doc_score(&mut docs, query_type, query_text);
        sort_results_and_discard_irrelevant(&mut docs);

        self.normalize_keywords(&mut docs);
        let DividingKeywordSet { dividing, too_common } = if query_type != QueryType::Quick && query_type != QueryType::Keyword {
            self.get_dividing_keywords(&docs, query_text, limit)
        } else { Default::default() };
        let mut dividing_keywords: Vec<_> = dividing.into_iter().map(SmolStr::from).collect();

        // If the query is dominated by one keyword, it looks spammy and boring. Repeat with diverse results.
        if let Some(too_common) = too_common {
            info!("Mixing query {query_text}, because keyword {too_common} is too common");
            // TODO: use ast
            if let Ok(query2) = self.parse_query(&format!("{query_text} -\"{too_common}\"")) {
                let mut more_docs = self.fetch_docs(&searcher, &query2, query_text, limit/2)?;
                assign_doc_score(&mut more_docs, query_type, query_text);

                // -"keyword" makes scoring different? needs rescale
                // skip the first result, because it's usually an outlier (could have been exact match boost)
                let old_max_score = docs.iter().skip(1).map(|d| d.score).max_by(f32::total_cmp).unwrap_or(1.);
                let new_max_score = more_docs.iter().map(|d| d.score).max_by(f32::total_cmp).unwrap_or(1.);
                for d in &mut more_docs {
                    d.score *= 0.99 / (new_max_score * old_max_score);
                }

                let mut unique_keyword_sets = HashSet::default();
                let too_common = &*SmolStr::from(too_common);
                for (i, d) in docs.iter_mut().skip(4).filter(|d| d.keywords_normalized().any(move |k| k == too_common)).enumerate() {
                    let factor = (0.9 - (i as f32 / 10_f32)).max(0.33);
                    d.score *= factor;
                    // this is for whole sets, which happen mostly for boilerplate/autogenerated crates
                    if !unique_keyword_sets.insert(d.keywords_normalized_s.as_str()) {
                        d.score *= 0.5;
                    }
                }

                self.normalize_keywords(&mut more_docs);

                docs.append(&mut more_docs); drop(more_docs);
                sort_results_and_discard_irrelevant(&mut docs);

                dividing_keywords = self.get_dividing_keywords(&docs, query_text, limit)
                    .dividing.into_iter().map(SmolStr::from).collect();
            }
        }

        // Dedupe
        let mut unique_crates = HashSet::with_capacity_and_hasher(docs.len(), Default::default());
        docs.retain(|d| unique_crates.insert(d.crate_name.clone()));

        if query_type != QueryType::Quick {
            // Make sure that there's at least one crate ranked high for each of the interesting keywords
            // This truncates to the limit
            docs = move_representative_crates_to_top(&dividing_keywords, docs, limit);

            let mut normalize_changed = false;
            let normalized_query = query_text.split(' ')
                .filter(|&w| w != "OR" && w != "AND")
                .map(|w| w.trim_matches(|c: char| !c.is_ascii_alphanumeric()))
                .filter(|w| !w.is_empty())
                .map(|w| {
                    let n = self.synonyms.normalize(w, 1);
                    if n != w { normalize_changed = true; }
                    n
                }).join(" ");
            if normalize_changed && !dym.iter().any(|d| d == &*normalized_query) { dym.push(normalized_query.into()) };
        }

        let top_keywords = self.find_top_keywords(&docs, query_text);
        if docs.len() > 5 {
            Self::downloads_bubble_sort(&mut docs[2..]);
            Self::downloads_bubble_sort(&mut docs[5..]);
        }

        Ok(SearchResults { crates: docs, dym, dym_added_fuzzy, nothing_found_without_dym, dividing_keywords, top_keywords })
    }

    // users pay too much attention to download numbers "why is this small crate above mine?",
    // so gently locally re-sort by downloads to make it happen less often
    fn downloads_bubble_sort(docs: &mut [CrateFound]) {
        for pair in docs.chunks_exact_mut(2) {
            let [a, b] = pair else { continue };
            // no point re-sorting noisy low download numbers
            // and don't bubble up top biggest crates because if they haven't been the exact match already, they'll probably off-topic
            if b.monthly_downloads > 200 && b.monthly_downloads < 1_000_000 && a.monthly_downloads * 3 < b.monthly_downloads {
                std::mem::swap(a, b);
            }
        }
    }

    fn find_top_keywords(&self, docs: &[CrateFound], query_text: &str) -> Vec<SmolStr> {
        let query = &*query_text.as_ascii_lowercase();
        let query_keywords: HashSet<_> = query.split(|c: char| !c.is_alphanumeric()).filter(|k| !k.is_empty())
            .chain(Some(query))
            .map(|k| self.synonyms.normalize(k, 1))
            .take(20).collect();
        let mut counts = HashMap::with_capacity_and_hasher(64, Default::default());
        for res in docs {
            res.keywords_normalized()
                .map(|k| self.synonyms.normalize(k, 1))
                .filter(|k| !query_keywords.contains(k))
                .for_each(|k| {
                    let cnt = counts.entry(k).or_insert((0u32, 0f32));
                    cnt.0 += 1;
                    cnt.1 += res.score;
                });
        }

        let too_obvious = (docs.len() as u32 * 3 / 4) + 3;
        let mut counts: Vec<_> = counts.into_iter()
            // keep if more than 1 crate has it
            // but don't repeat terms from the query
            .filter(|&(_, (n, _))| n >= 1 && n < too_obvious)
            .map(|(k, (_, v))| (k, v)).collect();
        pick_top_n_unstable_by(&mut counts, 30, |a, b| b.1.total_cmp(&a.1));

        // it's boring to have "foo", "bar", "foo-bar"
        let top_single_words = counts.iter().take(8.min(counts.len()/2))
            .map(|&(k, _)| k).filter(|&k| !k.contains('-'))
            .take(4).collect::<HashSet<_>>();
        counts.retain(|&(k, _)| !k.contains('-') || !k.split('-').all(|w| top_single_words.contains(w)));

        let mut text_len = 0;
        counts.into_iter().map(|(k, _)| k).take_while(|&k| {
            text_len += 2 + k.len();
            text_len < 75
        })
        .map(SmolStr::from)
        .collect()
    }

    fn did_you_mean_fuzzy(&self, docs: &mut Vec<CrateFound>, query_text: &str, searcher: &tantivy::Searcher) -> Result<(bool, Vec<SmolStr>), TantivyError> {
        if query_text == "tokyo" {
            return Ok((false, vec!["tokio".into()]));
        }

        // docs are still sorted by tantivy here, which is pure textual relevance, without quality ranking
        // so sometimes the first result is a pretty bad crate
        let top_results_score = docs.iter().take(3).fold(0f32, |a, b| a.max(b.crate_base_score));
        if top_results_score > 0.5 || (top_results_score > 0.2 && docs.len() > 20) {
            // results seem good enough
            return Ok((false, Vec::new()));
        }

        let max_dist = if query_text.len() > 8 && query_text.len() < 30 { 2 } else { 1 };
        let crate_like_query = query_text.chars().map(|c| if c.is_alphanumeric() || c == '_' { c } else { '-' }).collect::<SmolStr>();
        let fuzzy_crate_name = FuzzyTermQuery::new(Term::from_field_text(self.crate_name_field, &crate_like_query), max_dist, true);
        let fuzzy_keyword = FuzzyTermQuery::new(Term::from_field_text(self.keywords_field, query_text), max_dist, true);
        let mut added = 0;
        for q in [fuzzy_crate_name, fuzzy_keyword] {
            let mut tmp = self.fetch_docs(searcher, &q, query_text, 10)?;
            tmp.retain(|doc| doc.crate_base_score > 0.1); // don't add spam
            tmp.truncate(5);

            self.normalize_keywords(&mut tmp);
            // d.score is not set at this point
            for d in &mut tmp {
                d.fuzzy_match = true;
                d.relevance_score *= 0.6; // reduce relevance, since they're fuzzy
            }
            added += Self::append_few_extra_docs(docs, tmp);
        }

        let mut words = HashMap::default();
        for doc in &docs[docs.len() - added ..] {
            for word in doc.keywords().chain([doc.crate_name.as_str()]) {
                *words.entry(unicase::Ascii::new(word)).or_insert(0.) += doc.relevance_score * doc.crate_base_score;
            }
        }

        fn normalize_char(c: u8) -> u8 {
            if c == b'-' || c == b'_' { b' ' } else { c.to_ascii_lowercase() }
        }

        let mut dym_scores = Vec::new();
        let dfa = LevenshteinAutomatonBuilder::new(max_dist, true).build_dfa(&String::from_utf8(query_text.bytes().map(normalize_char).collect::<Vec<_>>()).unwrap());
        for (&word, &score) in &words {
            let mut state = dfa.initial_state();
            for b in word.bytes() {
                state = dfa.transition(state, normalize_char(b));
            }
            let dist = match dfa.distance(state) {
                Distance::Exact(dist) => dist,
                Distance::AtLeast(_) => continue,
            };

            if dist > 0 && dist < 4 && score > 0.1 {
                dym_scores.push((score / f32::from(dist), word));
            }
        }
        pick_top_n_unstable_by(&mut dym_scores, 3, |a, b| b.0.total_cmp(&a.0));

        Ok((added > 0, dym_scores.into_iter().map(|(_, w)| w.into_inner().into()).collect()))
    }

    /// Pick a few representative keywords that are for different meanings of the query (e.g. oracle db vs padding oracle)
    fn get_dividing_keywords<'a>(&'a self, docs: &'a [CrateFound], query_text: &str, limit: usize) -> DividingKeywordSet<'a> {
        let query_lower = query_text.as_ascii_lowercase();
        let query_as_keyword = query_lower.replace(['_', ' '], "-");
        // "foo bar" == "foo-bar"
        let query_rev = query_as_keyword.split('-').rev().join("-");
        let query_keywords: Vec<_> = query_text.split(|c: char| !c.is_alphanumeric()).filter(|k| !k.is_empty())
            .chain([query_text, &query_lower, &query_as_keyword, &query_rev])
            .map(|k| self.synonyms.normalize(k, 1))
            .collect();
        
        self.dividing_keywords_inner(docs, limit/2, &query_keywords, &query_as_keyword, &["blockchain", "solana", "holochain", "ethereum", "bitcoin", "cryptocurrency"]).unwrap_or_default()
    }

    fn fix_missing_exact_match(&self, docs: &mut Vec<CrateFound>, query_text: &str, searcher: &tantivy::Searcher) -> Result<(), TantivyError> {
        let query_is_crate_name = query_text.bytes().all(|c| c.is_ascii_alphanumeric() || c == b'-' || c == b'_');
        if !query_is_crate_name {
            return Ok(());
        }

        let has_exact_match = docs.iter().any(|doc| doc.exact_match);
        if has_exact_match {
            return Ok(());
        }

        let exact_crate_query = self.parse_query(&format!("crate_name:\"{query_text}\""))?;
        let mut extra_docs = self.fetch_docs(searcher, &exact_crate_query, query_text, 3)?;
        if !extra_docs.is_empty() {
            warn!("Added {} manual exact match for {query_text}", extra_docs.len());
        }
        self.normalize_keywords(&mut extra_docs);
        Self::append_few_extra_docs(docs, extra_docs);
        Ok(())
    }

    fn append_few_extra_docs(docs: &mut Vec<CrateFound>, mut extra_docs: Vec<CrateFound>) -> usize {
        extra_docs.truncate(20); // make sure it's never expensive

        // it's probably quicker to loop once than make a hashtable
        for doc in &*docs {
            extra_docs.retain(|d| d.crate_name != doc.crate_name);
            if extra_docs.is_empty() {
                return 0;
            }
        }
        let added = extra_docs.len();
        debug!("Appended {added} extra docs");
        docs.append(&mut extra_docs);
        added
    }

    fn normalize_keywords(&self, docs: &mut [CrateFound]) {
        let mut kw_dedupe = HashSet::default();
        for doc in docs {
            kw_dedupe.clear();
            doc.keywords_normalized_s = doc.keywords_s.split(' ')
                .map(|orig| {
                    // normalize very aggressively, because this is used for similarity grouping
                    // and SERP shouldn't display many redundant keywords
                    self.synonyms.get(orig)
                        .and_then(|(k,_)| self.synonyms.get(k))
                        .map_or(orig, |(k,_)| k)
                })
                .filter(|&k| kw_dedupe.insert(k))
                .join(" ");
        }
    }

    fn dividing_keywords_inner<'a>(&'a self, results: &'a [CrateFound], limit: usize, query_keywords: &[&str], query_as_keyword: &str, skip_entire_results: &[&str]) -> Option<DividingKeywordSet<'a>> {
        // divide keyword popularity by its global popularity tf-idf, because everything gets api, linux, cargo, parser
        // bonus if keyword pair exists
        let mut dupes = HashSet::with_capacity_and_hasher(results.len(), Default::default());
        let mut keyword_sets = results.iter().enumerate().filter_map(|(i, found)| {
                if !dupes.insert(found.keywords_s.as_str()) { // some crate families have all the same keywords, which is spammy and biases the results
                    return None;
                }
                let mut k_set: HashSet<_> = found.keywords_normalized().collect();
                for &q in query_keywords {
                    k_set.remove(q);
                }

                if skip_entire_results.iter().any(|&dealbreaker| k_set.contains(dealbreaker)) {
                    return None;
                }
                Some((k_set, if i < limit { 2048 } else { 1024 } + (128. * found.score) as i32)) // integer for ease of sorting, unique for sort stability
            }).collect::<Vec<_>>();
        drop(dupes);

        // api/cli are boring and generic
        let d1 = self.popular_dividing_keyword(&keyword_sets, query_as_keyword, &["api", "cli"]);
        let too_common = d1.too_common;
        let most_common = d1.dividing?;
        // The most common co-occurrence may be a synonym, so skip it for now
        let second_most_common = self.popular_dividing_keyword(&keyword_sets, query_as_keyword, &[most_common]).dividing?;

        let mut dividing_keywords = Vec::<&'a str>::with_capacity(10);
        let mut next_keyword = second_most_common;
        for _ in 0..10 {
            if dividing_keywords.iter().any(|&k| k == next_keyword) {
                break; // looped, so nothing interesting left?
            }
            dividing_keywords.push(next_keyword);
            for (k_set, w) in &mut keyword_sets {
                if k_set.remove(&next_keyword) && *w > 0 { *w = -*w/2; }
            }
            if keyword_sets.iter().filter(|&(_, w)| *w > 0).count() < 25 {
                break;
            }
            next_keyword = match self.popular_dividing_keyword(&keyword_sets, query_as_keyword, &["reserved", next_keyword]).dividing {
                None => break,
                Some(another) => another,
            };
        }
        Some(DividingKeywordSet { dividing: dividing_keywords, too_common })
    }

    /// Find a keyword that splits the set into two distinctive groups
    fn popular_dividing_keyword<'a>(&'a self, keyword_sets: &[(HashSet<&'a str>, i32)], query_keyword: &str, ignore_keywords: &[&str]) -> DividingKeywords<'a> {
        if keyword_sets.len() < 25 {
            return Default::default(); // too few results will give odd niche keywords
        }
        let prefix = &*smol_fmt!("{query_keyword}-");
        let suffix = &*smol_fmt!("-{query_keyword}");
        let mut counts: HashMap<&str, (u32, i32)> = HashMap::with_capacity_and_hasher(keyword_sets.len(), Default::default());
        for &(ref k_set, w) in keyword_sets {
            for &k in k_set {
                let n = counts.entry(k).or_default();
                n.0 += 1;
                n.1 += w;
                if w > 0 {
                    // for a query like "http" make "http-client" count as "client" too
                    if let Some(rest) = k.strip_prefix(prefix).map(|k| self.synonyms.normalize(k, 1)) {
                        if !k_set.contains(rest) {
                            let n = counts.entry(rest).or_default();
                            n.0 += 1;
                            n.1 += w * 2; // having prefix/suffix keyword is a good indicator that it's a meaningful distinction rather than a synonym
                        }
                    } else if let Some(rest) = k.strip_suffix(&suffix).map(|k| self.synonyms.normalize(k, 1)) {
                        if !k_set.contains(rest) {
                            let n = counts.entry(rest).or_default();
                            n.0 += 1;
                            n.1 += w * 2;
                        }
                    }
                }
            }
        }
        for stopword in ["api", "cli", "rust"] {
            counts.entry(stopword).and_modify(|e| { e.1 = e.1 * 3/4; });
        }

        let good_pop = (keyword_sets.len() / 3) as u32;
        let too_common_pop = (keyword_sets.len() * 5 / 8) as u32;
        let mut too_common_word = None;
        let mut too_common_word_score = 0;

        let dividing = counts.into_iter()
            .filter(|&(k, (pop, _))| {
                if pop > too_common_pop && pop > too_common_word_score {
                    too_common_word_score = pop;
                    too_common_word = Some(k);
                }
                pop > 2 && pop <= too_common_pop
            })
            .filter(|&(k, _)| !ignore_keywords.iter().any(|&ignore| ignore == k))
            .max_by_key(|&(_, (pop, weight))| if pop >= 10 && pop <= good_pop { weight * 2 } else { weight })
            .map(|(k, _)| k);

        DividingKeywords { dividing, too_common: too_common_word }
    }
}

#[derive(Default)]
struct DividingKeywords<'a> {
    dividing: Option<&'a str>,
    too_common: Option<&'a str>,
}

#[derive(Default)]
struct DividingKeywordSet<'a> {
    dividing: Vec<&'a str>,
    too_common: Option<&'a str>,
}

fn move_representative_crates_to_top(dividing_keywords: &[SmolStr], mut docs: Vec<CrateFound>, limit: usize) -> Vec<CrateFound> {
    if docs.len() < 7 {
        docs.truncate(limit);
        return docs;
    }

    let mut interesting_crate_indices = Vec::new();

    // Boost best-ranking and most-popular crates, because sometimes they lack keywords and their relevance is meh
    let better_half = (docs.len() / 3).min(50);
    if better_half > 5 {
        let take = (limit / 70).clamp(1, 3);
        let better_half = &docs[..better_half];
        let max_downloads_in_top = better_half.iter().map(|c| c.monthly_downloads).max().unwrap_or(0);
        let max_ranking_in_top = better_half.iter().map(|c| c.crate_base_score).max_by(f32::total_cmp).unwrap_or(0.);
        // always include crates well-known enough that users expect to see them,
        // unless their score is so low they're deprecated/obsolete.
        let high_downloads = (max_downloads_in_top * 9 / 10).max(100_000);
        let high_rank_score = (max_ranking_in_top * 0.97).max(0.55);
        interesting_crate_indices.extend(better_half.iter().enumerate().skip(3)
            .filter(|(_, c)| c.crate_base_score >= high_rank_score)
            .map(|(i, c)| {
                // no need to give special handling to exact match, because top 3 results
                // are always kept, and if exact match wasn't in top 3, it wasn't worth keeping anyway!
                debug!("boosting {} by rank (>{high_rank_score})", c.crate_name);
                i
            }).take(take));
        let got_so_far = interesting_crate_indices.len();
        interesting_crate_indices.extend(better_half.iter().enumerate().skip(3)
            .filter(|&(_, c)| {
                // TODO: instead of downloads this should be rev deps, due to regex-syntax or internal winapi crates
                let min_score = if c.monthly_downloads >= 1_000_000 { 0.32 } else { 0.55 };
                c.crate_base_score >= min_score && c.monthly_downloads >= high_downloads
            })
            .map(|(i, c)| {
                debug!("boosting {} by downloads (>{high_downloads})", c.crate_name);
                i
            }).take(take.saturating_sub(got_so_far).max(1)));
    }

    // don't crowd-out crates boosted for ranking/popularity
    let kw_boosts_remaining = 3 + 3usize.saturating_sub(interesting_crate_indices.len());
    // inefficient search to find by best dividing_keywords first
    interesting_crate_indices.extend(dividing_keywords.iter().filter_map(|dk| {
        docs.iter().take(100).position(|k| k.crate_base_score > 0.45 && k.keywords_normalized().any(|k| k == dk))
    }).dedup().take(kw_boosts_remaining));

    let mut top_crates = Vec::with_capacity(docs.len());

    // keep top 3 results anyway
    interesting_crate_indices.extend(0..3);
    interesting_crate_indices.sort_unstable();
    interesting_crate_indices.dedup();
    for idx in interesting_crate_indices.into_iter().rev() {
        top_crates.push(docs.remove(idx));
    }
    // pick top 3 results for popularity, but then sort more by relevance
    sort_top_n_unstable_by(&mut top_crates, 3, |a, b| {
        let a_score = (1. + a.score) * (if a.exact_match && a.crate_base_score > 0.4 { 0.6 } else { 0.4 } + a.crate_base_score.min(0.7)) * ((a.monthly_downloads + 15_000) as f32).sqrt();
        let b_score = (1. + b.score) * (if b.exact_match && b.crate_base_score > 0.4 { 0.6 } else { 0.4 } + b.crate_base_score.min(0.7)) * ((b.monthly_downloads + 15_000) as f32).sqrt();
        b_score.total_cmp(&a_score)
    });
    let mixed_score = |a: &CrateFound, b: &CrateFound| {
        let a_score = (0.3 + a.score) * a.crate_base_score * ((a.monthly_downloads + 25_000) as f32).log2();
        let b_score = (0.3 + b.score) * b.crate_base_score * ((b.monthly_downloads + 25_000) as f32).log2();
        b_score.total_cmp(&a_score)
    };
    // sometimes 1st most popular result is pretty bad, and is just an irrelevant popularity outlier
    top_crates[..3].sort_unstable_by(mixed_score);
    top_crates[3..].sort_unstable_by(mixed_score);

    docs.truncate(limit.saturating_sub(top_crates.len()));
    // search picked a few more results to cut out chaff using crate_score
    if let Some(min_score) = docs.first().map(|f| f.score) {
        // keep score monotonic
        top_crates.iter_mut().for_each(|f| if f.score < min_score { f.score = min_score; });
    }
    docs.splice(..0, top_crates);
    docs
}

fn contains_query(a: &str, b: &str) -> bool {
    let a = a.trim_start_matches("cargo").trim_start_matches("rust").trim_end_matches("rs").trim_matches(['-','_']);
    if a.is_empty() {
        return false;
    }
    let (a, b) = if a.len() >= b.len() { (a, b) } else { (b, a) };
    a.contains(b)
}

fn assign_doc_score(docs: &mut [CrateFound], query_type: QueryType, query_text: &str) {
    if docs.is_empty() {
        return;
    }

    // relevance required for #1 spot
    let (max_relevance, top_4_relevance_score) = if query_type == QueryType::Relevance {
        sort_top_n_unstable_by(&mut *docs, 4, |a,b| b.relevance_score.total_cmp(&a.relevance_score));
        (docs[0].relevance_score, docs[3.min(docs.len()-1)].relevance_score)
    } else { (0., 0.) };

    // if the query is specific and contains _-,
    // it's more likely a crate name search than a keyword search
    let query_is_specific = query_text.contains(['-', '_']) || query_text.len() > 15;

    let mut boosted_matches = 0;
    for doc in docs.iter_mut() {
        doc.score = if query_type == QueryType::Relevance {
            // if it's a highly ranked crate, it more likely was meant to be an exact search
            let quality_crate_bonus = (doc.crate_base_score.mul_add(doc.crate_base_score, 0.25) * 2.).min(1.1);
            let match_multiplier = 1. +
                quality_crate_bonus *
                if query_is_specific { 0.9 } else { 0.15 } *
                2. / (2. + boosted_matches as f32); // boost less if already boosted some

            if doc.exact_match && doc.crate_base_score > 0.15 {
                debug_assert!(doc.relevance_score <= max_relevance);
                debug!("{} is exact {query_text} -> {max_relevance} * {match_multiplier}", doc.crate_name);
                boosted_matches += 2;
                max_relevance * match_multiplier
            } else if boosted_matches < 5 && contains_query(&doc.crate_name, query_text) {
                boosted_matches += 1;
                let boosted_score = doc.relevance_score * match_multiplier;
                let minimal_boost = doc.relevance_score * (1. + (match_multiplier-1.) * 0.1);
                // boosting based just on the name is okay to bring crates into top 10, but top 3 needs better signal than keyword spam
                let capped = if boosted_score > top_4_relevance_score {
                    ((top_4_relevance_score * 3. + boosted_score) * (1./4.)).max(minimal_boost)
                } else {
                    boosted_score
                };

                debug!("{} contains {query_text} -> {} * {match_multiplier} -> {boosted_score}|{capped}|{top_4_relevance_score}", doc.crate_name, doc.relevance_score);
                capped
            } else {
                doc.relevance_score
            }
        } else {
            doc.crate_base_score
        };
    }
}

fn sort_results_and_discard_irrelevant(docs: &mut Vec<CrateFound>) {
    // re-sort using our base score
    docs.sort_unstable_by(|a, b| b.score.total_cmp(&a.score));

    // throw away yanked and spammy crates, unless there nothing else to show
    if docs.len() > 3 {
        let keep_at_least = (docs.len() / 4).max(10);
        let mut n = 0;
        docs.retain(|doc| {
            n += 1;
            n <= keep_at_least || doc.crate_base_score > 0.01
        });
    }
}

#[track_caller]
fn get_as_str(val: Option<&Value>) -> &str {
    match val {
        Some(val) => match val {
            Value::Str(s) => s,
            _ => panic!("invalid value type"),
        },
        _ => "",
    }
}

#[track_caller]
fn take_int(val: Option<&Value>) -> u64 {
    match val {
        Some(val) => match val {
            Value::U64(s) => *s,
            Value::I64(s) => *s as u64,
            _ => panic!("invalid value type"),
        },
        _ => 0,
    }
}

#[track_caller]
fn take_f64(val: Option<&Value>) -> f64 {
    match val {
        Some(val) => match val {
            Value::F64(s) => *s,
            _ => panic!("invalid value type"),
        },
        _ => 0.0,
    }
}

pub struct IndexerData<'a> {
    pub origin: &'a Origin,
    pub crate_name: &'a str,
    pub version: &'a str,
    pub description: &'a str,
    pub keywords: &'a [&'a str],
    pub readme: Option<&'a str>,
    pub monthly_downloads: u64,
    pub extra_keywords: Option<String>,
    pub score: f64,
}

impl Indexer {
    pub fn new(index: CrateSearchIndex) -> tantivy::Result<Self> {
        Ok(Self { writer: index.tantivy_index.writer(250_000_000)?, index })
    }

    /// score is float 0..=1 range
    pub fn add(&mut self, IndexerData {origin, crate_name, version, description, keywords, readme, monthly_downloads, extra_keywords, score}: IndexerData<'_>) -> Result<(), TantivyError> {
        let origin = origin.to_str();
        // delete old doc if any
        let pkey = Term::from_field_text(self.index.origin_pkey, &origin);
        self.writer.delete_term(pkey);

        // bug workaround
        let pkey2 = Term::from_field_text(self.index.crate_name_field, crate_name);
        self.writer.delete_term(pkey2);

        if score > 0.001 {
            // index new one
            let mut doc = Document::default();
            doc.add_text(self.index.origin_pkey, &origin);
            doc.add_text(self.index.crate_name_field, crate_name);
            // a crate should always appear in keywords for its own name
            doc.add_text(self.index.keywords_field, [&*crate_name.as_ascii_lowercase()].into_iter()
                .chain(keywords.iter().copied()).join(" "));
            doc.add_text(self.index.description_field, description);

            let mut readme_text = Cow::Borrowed(readme.unwrap_or_default());
            if !keywords.is_empty() {
                // put keywords in the readme, so they get stemming and partial matches
                readme_text.to_mut().extend(keywords.iter().flat_map(|k| [" ", k]));
            }
            if !readme_text.is_empty() {
                doc.add_text(self.index.readme_field, readme_text.into_owned());
            }
            if let Some(e) = extra_keywords {
                doc.add_text(self.index.extra_field, e);
            }
            doc.add_text(self.index.crate_version, version);
            doc.add_u64(self.index.monthly_downloads, monthly_downloads);
            doc.add_f64(self.index.crate_score, score);
            self.writer.add_document(doc)?;
        }
        Ok(())
    }

    pub fn commit(&mut self) -> tantivy::Result<()> {
        self.writer.commit()?;
        Ok(())
    }

    pub fn bye(self) -> tantivy::Result<CrateSearchIndex> {
        self.writer.wait_merging_threads()?;
        Ok(self.index)
    }

    pub fn delete(&self, origin: &Origin) {
        let origin = origin.to_str();
        let pkey = Term::from_field_text(self.index.origin_pkey, &origin);
        self.writer.delete_term(pkey);
    }
}

#[derive(Clone)]
struct KewordTokenizer(Arc<Synonyms>);

struct KeywordTokenStream<'a> {
    syn: Arc<Synonyms>,
    src: std::str::Split<'a, char>,
    tok: Token,
}

impl TokenStream for KeywordTokenStream<'_> {
    fn token(&self) -> &Token {
        debug_assert!(self.tok.position < isize::MAX as _, "{:?}", self.src);
        &self.tok
    }

    fn token_mut(&mut self) -> &mut Token {
        debug_assert!(self.tok.position < isize::MAX as _, "{:?}", self.src);
        &mut self.tok
    }

    fn advance(&mut self) -> bool {
        for kw in self.src.by_ref() {
            if kw.is_empty() {
                self.tok.offset_to += 1;
                continue;
            }
            let kw = self.syn.normalize(kw, 4);
            let mut text: String = kw
                .trim_matches(|c: char| c.is_ascii_punctuation())
                .chars()
                .filter_map(|c| deunicode::deunicode_char(c.to_ascii_lowercase()))
                .flat_map(|s| s.chars().map(|c| if c.is_ascii_punctuation() || c.is_ascii_whitespace() { '-' } else { c }))
                .collect();
            while text.ends_with('-') {
                text.pop();
            }

            self.tok.offset_from = if self.tok.offset_to != 0 { self.tok.offset_to + 1 } else { 0 };
            self.tok.offset_to = self.tok.offset_from + kw.len();
            self.tok.position = self.tok.position.wrapping_add(1);
            self.tok.text = text;
            return true;
        }
        false
    }
}

impl Tokenizer for KewordTokenizer {
    type TokenStream<'a> = KeywordTokenStream<'a>;

    fn token_stream<'a>(&'a mut self, src: &'a str) -> Self::TokenStream<'a> {
        KeywordTokenStream {
            tok: Token {
                offset_from: 0,
                offset_to: 0,
                position: 0usize.wrapping_sub(1), // so next starts at 0
                text: String::default(),
                position_length: 1,
            },
            syn: self.0.clone(),
            src: src.split(' '),
        }
    }
}

#[test]
fn tokeniz() {
    let mut k = KewordTokenizer(Arc::new(Synonyms::new("../data".as_ref()).unwrap()));
    let mut s = k.token_stream("żółw-TEST    a b!_ -c a_x.! こんにちは1 東京☃️ end");
    assert_eq!(s.next().unwrap().text, "zolw-test");
    assert_eq!(s.next().unwrap().text, "a");
    assert_eq!(s.next().unwrap().text, "b");
    assert_eq!(s.next().unwrap().text, "c");
    assert_eq!(s.next().unwrap().text, "a-x");
    assert_eq!(s.next().unwrap().text, "konnitiha1");
    assert_eq!(s.next().unwrap().text, "Dong-Jing-snowman");
    assert_eq!(s.next().unwrap().text, "end");
    assert_eq!(s.next(), None);
}
