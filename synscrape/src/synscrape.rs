use rustc_hash::{FxHashMap as HashMap, FxHashSet as HashSet};
use log::{debug, warn};
use parse_cfg::Cfg;
use serde::{Deserialize, Serialize};
use syn::visit::Visit;
use syn::{
    Attribute, Expr, Field, File, Ident, ItemConst, ItemEnum, ItemFn, ItemForeignMod, ItemMacro, ItemMod, ItemStatic, ItemStruct, ItemTrait, ItemType, MacroDelimiter, Meta, TraitItemFn, Visibility
};
use util::{smol_fmt, PushString, SmolStr};

#[derive(Debug, Clone)]
pub enum ItemKind {
    Module,
    Fn,
    Const,
    Struct,
    Trait,
    Type,
    Macro,
    Field,
}
#[derive(Debug, Clone)]
pub struct Item {
    pub name: SmolStr,
    pub doc: Option<String>,
    pub public: bool,
    pub is_test: bool,
    pub kind: ItemKind,
    pub features: HashSet<SmolStr>,
}

#[derive(Debug, Clone)]
pub struct ModuleScrape {
    pub file_stem: SmolStr,
    pub items: Vec<Item>,
}

struct ModuleScraper<'ast, 's> {
    inner: &'s mut ModuleScrape,
    in_parent_vis: Vec<&'ast Visibility>,
    in_parent_name: Vec<&'ast Ident>,
}

impl ModuleScraper<'_, '_> {
    fn scrape(&mut self, name: &Ident, attrs: &[Attribute], vis: &Visibility, kind: ItemKind) {
        let mut doc = String::new();
        let mut features = HashSet::default();
        let public = matches!(vis, Visibility::Public(_)) &&
            (self.in_parent_vis.is_empty() || self.in_parent_vis.iter().all(|vis| matches!(vis, Visibility::Public(_))));
        let name = if let Some(parent) = self.in_parent_name.last() {
            smol_fmt!("{parent}.{name}")
        } else {
            smol_fmt!("{name}")
        };
        let mut is_test = false;
        for at in attrs {
            match &at.meta {
                Meta::List(v) if public => if v.path.get_ident().is_some_and(|id| id == "cfg") {
                    fn capture_features(cfg: Cfg<&str>, out: &mut HashSet<SmolStr>, is_test: &mut bool) {
                        match cfg {
                            Cfg::Any(cfgs) | Cfg::All(cfgs) => cfgs.into_iter().for_each(move |c| capture_features(c, out, is_test)),
                            Cfg::Equal("feature", v) => { out.insert(v.into()); },
                            Cfg::Is("test") => {
                                *is_test = true;
                            },
                            Cfg::Is(_) | Cfg::Not(_) | Cfg::Equal(_, _) => {},
                        }
                    }
                    let cfg_expr = v.tokens.to_string();
                    match Cfg::<&str>::parse_inner_expr(&cfg_expr) {
                        Ok(cfg) => {
                            capture_features(cfg, &mut features, &mut is_test);
                        },
                        Err(e) => {
                            warn!("invalid cfg({cfg_expr}) in {}: {e}", self.inner.file_stem);
                            continue;
                        },
                    };
                },
                Meta::Path(v) => if v.get_ident().is_some_and(|id| id == "test" || id == "bench") || v.segments.iter().map(|s| &s.ident).eq(["tokio", "test"]) {
                    is_test = true;
                },
                Meta::NameValue(v) => if v.path.get_ident().is_some_and(|id| id == "doc") {
                    if let Expr::Lit(l) = &v.value {
                        if let syn::Lit::Str(s) = &l.lit {
                            let text = s.value();
                            let text = text.as_str();
                            if !text.trim_start().is_empty() {
                                doc.reserve(1 + text.len());
                                if !doc.is_empty() {
                                    doc.push_ascii_in_cap(b'\n');
                                }
                                doc.push_str_in_cap(text);
                            }
                        }
                    }
                },
                Meta::List(_) => {},
            }
        }

        self.inner.items.push(Item {
            public,
            is_test,
            name,
            doc: Some(doc).filter(|d| !d.is_empty()),
            features,
            kind,
        });
    }
}

impl<'ast> Visit<'ast> for ModuleScraper<'ast, '_> {
    fn visit_item_fn(&mut self, i: &'ast ItemFn) {
        self.scrape(&i.sig.ident, &i.attrs, &i.vis, ItemKind::Fn);
    }

    fn visit_item_foreign_mod(&mut self, i: &'ast ItemForeignMod) {
        for i in &i.items {
            match i {
                syn::ForeignItem::Fn(i) => self.scrape(&i.sig.ident, &i.attrs, &i.vis, ItemKind::Fn),
                syn::ForeignItem::Static(i) => self.scrape(&i.ident, &i.attrs, &i.vis, ItemKind::Const),
                syn::ForeignItem::Type(i) => self.scrape(&i.ident, &i.attrs, &i.vis, ItemKind::Type),
                _ => {},
            }
        }
    }

    fn visit_item_macro(&mut self, i: &'ast ItemMacro) {
        if let Some(ident) = &i.ident {
            // macro_rules def
            let Some(vis) = self.in_parent_vis.last() else { return };
            self.scrape(ident, &i.attrs, vis, ItemKind::Macro);
        } else if matches!(i.mac.delimiter, MacroDelimiter::Brace(_)) { // invoke mac!{}
            let code = i.mac.tokens.to_string();
            match syn::parse_str(&code) {
                Ok(file) => {
                    let mut child = ModuleScraper {
                        inner: &mut *self.inner,
                        in_parent_vis: self.in_parent_vis.clone(),
                        in_parent_name: self.in_parent_name.clone(),
                    };
                    child.visit_file(&file);
                },
                Err(e) => log::debug!("Macro visit failed: {e}; code: {{ {code} }}"),
            }
        }
    }

    fn visit_item_enum(&mut self, i: &'ast ItemEnum) {
        self.scrape(&i.ident, &i.attrs, &i.vis, ItemKind::Type);
    }

    fn visit_item_mod(&mut self, i: &'ast ItemMod) {
        self.scrape(&i.ident, &i.attrs, &i.vis, ItemKind::Module);
        self.in_parent_vis.push(&i.vis);
        self.in_parent_name.push(&i.ident);
        syn::visit::visit_item_mod(self, i);
        self.in_parent_name.pop();
        self.in_parent_vis.pop();
    }

    fn visit_item_struct(&mut self, i: &'ast ItemStruct) {
        self.scrape(&i.ident, &i.attrs, &i.vis, ItemKind::Struct);
        self.in_parent_vis.push(&i.vis);
        self.in_parent_name.push(&i.ident);
        syn::visit::visit_item_struct(self, i);
        self.in_parent_name.pop();
        self.in_parent_vis.pop();
    }

    fn visit_item_trait(&mut self, i: &'ast ItemTrait) {
        self.scrape(&i.ident, &i.attrs, &i.vis, ItemKind::Trait);
        self.in_parent_vis.push(&i.vis);
        self.in_parent_name.push(&i.ident);
        syn::visit::visit_item_trait(self, i);
        self.in_parent_name.pop();
        self.in_parent_vis.pop();
    }

    fn visit_trait_item_fn(&mut self, i: &'ast TraitItemFn) {
        if let Some(vis) = self.in_parent_vis.last() {
            self.scrape(&i.sig.ident, &i.attrs, vis, ItemKind::Fn);
        }
    }

    fn visit_item_const(&mut self, i: &'ast ItemConst) {
        self.scrape(&i.ident, &i.attrs, &i.vis, ItemKind::Const);
    }

    fn visit_item_static(&mut self, i: &'ast ItemStatic) {
        self.scrape(&i.ident, &i.attrs, &i.vis, ItemKind::Const);
    }

    fn visit_field(&mut self, i: &'ast Field) {
        if let Some(ident) = &i.ident {
            self.scrape(ident, &i.attrs, &i.vis, ItemKind::Field);
        }
    }

    fn visit_item_type(&mut self, i: &'ast ItemType) {
        self.scrape(&i.ident, &i.attrs, &i.vis, ItemKind::Type);
    }
}

pub type FeaturesEnableItems = HashMap<SmolStr, Vec<String>>;

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct TextFromSourceCode {
    pub idents: Vec<(u32, SmolStr)>,
    pub doc: Vec<(u32, String)>,
}

impl ModuleScrape {
    pub fn from_file(file_stem: &str, source: &str) -> Result<Self, syn::Error> {
        let syntax_tree: File = syn::parse_str(source)?;
        let mut f = Self { file_stem: file_stem.into(), items: vec![] };
        let mut tmp = ModuleScraper { inner: &mut f, in_parent_vis: vec![], in_parent_name: vec![] };
        tmp.visit_file(&syntax_tree);
        for i in tmp.inner.items.iter().filter(|i| !i.features.is_empty()) {
            debug!("scraped features {} {i:#?}", tmp.inner.file_stem);
        }
        Ok(f)
    }
}

#[must_use]
pub fn extract_idents_enabled_by_features(modules: &[ModuleScrape]) -> FeaturesEnableItems {
    let mut features_enable_by_feature = HashMap::default();
    let mut num_prefixes_by_feature = HashMap::default();

    let mut candidates: Vec<_> = modules.iter().flat_map(|m| {
        m.items.iter().filter(|item| !item.is_test && !item.features.is_empty() && !item.name.starts_with('_'))
        .map(move |item| (m.file_stem.as_str(), item))
    }).take(5000).collect();

    // First try most unique (some items are enabled by any feature)
    candidates.sort_by_key(|(_, item)| (!item.public, item.features.len()));

    candidates.iter().take(2000).for_each(|&(file_stem, item)| {
        item.features.iter().for_each(|feature| {
            let feature = feature.as_str();

            // Ensure the features shown are interesting and diverse, not just CONST_1, CONST_2, CONST_3, x50.
            let num_prefixes = num_prefixes_by_feature.entry((feature, item.public)).or_insert_with(HashMap::default);
            let by_stem = num_prefixes.entry((file_stem, item.public)).or_insert(0u16);
            if *by_stem > 15 {
                return;
            }
            *by_stem += 1;
            let name_fragment = item.name.get(..item.name.len() * 2 / 3).unwrap_or(&item.name);
            let by_name = num_prefixes.entry((name_fragment, item.public)).or_insert(0u16);
            if *by_name > 5 {
                return;
            }
            *by_name += 1;

            let either = features_enable_by_feature.entry(SmolStr::from(feature)).or_insert_with(|| (vec![], vec![]));
            // count separately public vs private
            let f = if item.public && !item.name.starts_with('_') { &mut either.0 } else { &mut either.1 };
            if f.len() < 50 {
                f.push(format!("{}::{}", file_stem, item.name));
            }
        });
    });
    features_enable_by_feature.into_iter().map(|(k, (public, private))| (k, if !public.is_empty() { public } else { private })).collect()
}

#[test]
fn source_items() {
    let _ = env_logger::try_init();

    let s = ModuleScrape::from_file("hi", r#"
        #[tokio::test] fn foo() {}
        /// Hello
        pub trait Tr { #[cfg(feature = "foo")] fn tr(); }

        invoke! {
            #[cfg(any(test, unix))]
            pub mod inmacro;
        }
"#).unwrap();
    assert_eq!("hi", s.file_stem);

    assert_eq!(s.items[0].name, "foo");
    assert!(!s.items[0].public);
    assert!(s.items[0].is_test);
    assert!(s.items[0].doc.is_none());

    assert_eq!(s.items[1].name, "Tr");
    assert!(s.items[1].public);
    assert!(!s.items[1].is_test);
    assert_eq!(s.items[1].doc.as_deref().unwrap().trim(), "Hello");

    assert_eq!(s.items[2].name, "Tr.tr");
    assert!(s.items[2].public);
    assert!(s.items[2].features.iter().eq(["foo"]));
    assert!(s.items[2].doc.is_none());

    assert_eq!(s.items[3].name, "inmacro");
    assert!(s.items[3].public);
    assert!(s.items[3].is_test);
    assert!(s.items[3].features.is_empty());
    assert!(s.items[3].doc.is_none());
}
