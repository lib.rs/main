use crate::github_id_from_avatar_url;
use chrono::{DateTime, Utc};
use log::debug;
use serde::{Deserialize, Serialize};
use util::SmolStr;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CrateOwnersFile {
    pub users: Vec<CrateOwner>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CrateTeamsFile {
    pub teams: Vec<CrateOwner>,
}

#[derive(Debug, Copy, Eq, PartialEq, Clone, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum OwnerKind {
    Team,
    User,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CrateOwner {
    #[serde(rename = "login")]
    pub crates_io_login: SmolStr,          // "github:rust-bus:maintainers",
    pub kind: OwnerKind,        // "team" || "user"
    pub url: Option<Box<str>>,    // "https://github.com/rust-bus",
    pub name: Option<SmolStr>,   // "maintainers",
    pub avatar: Option<Box<str>>, // "https://avatars1.githubusercontent.com/u/38887296?v=4"

    #[serde(default)]
    pub github_id: Option<u32>,

    #[serde(default)]
    pub invited_at: Option<DateTime<Utc>>,

    #[serde(default)]
    pub invited_by_github_id: Option<u32>,

    #[serde(default)]
    pub last_seen_at: Option<DateTime<Utc>>,

    /// not from the API, added later
    #[serde(default)]
    pub contributor_only: bool,

    // rust-bus
    #[serde(default)]
    pub not_a_person: bool,

    #[serde(default, rename = "id")]
    pub crates_io_id: Option<u32>,
}

fn minimized_identifier(name: &str) -> SmolStr {
    name.chars()
        .filter(|c| !c.is_ascii_punctuation() && !c.is_ascii_whitespace())
        .map(|c| c.to_ascii_lowercase())
        .collect()
}

impl CrateOwner {
    #[must_use]
    pub fn name(&self) -> Option<&str> {
        let name = self.name.as_deref()
            .filter(move |&n| !n.is_empty() && n != self.crates_io_login);
        match self.kind {
            OwnerKind::User => name,
            OwnerKind::Team => {
                let (login, team) = self.crates_io_login.strip_prefix("github:")?.split_once(':')?;
                let normalized_login = &*minimized_identifier(login);
                if let Some(name) = name {
                    // teams get crappy names
                    if minimized_identifier(name).contains(normalized_login) {
                        return Some(name);
                    }
                    debug!("{login} [{team}] has crappy team name {name}");
                }
                if team != login && minimized_identifier(team).contains(normalized_login) {
                    return Some(team);
                }
                None
            },
        }
    }

    #[must_use] pub fn invited_at(&self) -> Option<DateTime<Utc>> {
        Some(self.invited_at?.with_timezone(&Utc))
    }

    #[must_use]
    pub fn github_id(&self) -> Option<u32> {
        if let ok @ Some(_) = self.github_id {
            return ok;
        }
        github_id_from_avatar_url(self.avatar.as_ref()?)
    }

    /// Be careful about case-insensitivity
    #[must_use]
    pub fn github_login(&self) -> Option<&str> {
        match self.kind {
            OwnerKind::User => Some(&self.crates_io_login),
            OwnerKind::Team => self.crates_io_login.strip_prefix("github:")?.split(':').next(),
        }
    }
}
