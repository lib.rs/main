use crate::cdn::Cdn;
use crate::writer::writer;
use actix_web::dev::Url;
use actix_web::guard::fn_guard;
use actix_web::http::header::HeaderValue;
use actix_web::http::{header, StatusCode};
use actix_web::{middleware, web, App, HttpRequest, HttpResponse, HttpServer};
use util::FxHashMap as HashMap;
use anyhow::{anyhow, Context};
use arc_swap::ArcSwap;
use blocking::block_in_place;
#[cfg(feature = "cap-alloc")]
use cap::Cap;
use categories::{Category, CATEGORIES};
use chrono::prelude::*;
use futures::future::{try_join_all, Future, FutureExt};
use kitchen_sink::filter::ImageOptimAPIFilter;
use kitchen_sink::{running, stop, stopped, ArcRichCrateVersion, KitchenSink, KitchenSinkErr, Origin, RendCtx, RichCrate, SortOrder, SpawnAbortOnDrop};
use locale::Numeric;
use log::{debug, error, info, warn};
use std::sync::LazyLock as Lazy;
use render_readme::{Highlighter, Markup, Renderer};
use repo_url::SimpleRepo;
use search_index::{CrateSearchIndex, QueryType};
use std::borrow::Cow;
use std::path::{Path, PathBuf};
use std::pin::Pin;
use std::process::ExitCode;
use std::sync::atomic::{AtomicBool, AtomicU32, Ordering};
use std::sync::Arc;
use std::time::{Duration, Instant, SystemTime};
use std::{env, fs, io};
use svg_render::SvgRenderer;
use tokio::runtime::Handle;
use tokio::time::timeout;
use urlencoding::{decode, Encoded};
use util::CowAscii;

mod cdn;
mod writer;
mod cached;
use cached::*;
mod renderlock;
use renderlock::*;
mod error;
use error::*;
mod runtime;
use runtime::*;
mod watchdog;
use watchdog::*;

#[cfg(feature = "cap-alloc")]
const MAX_MEM: usize = 7 * 1024 * 1024 * 1024;

#[cfg(feature = "cap-alloc")]
#[global_allocator]
static ALLOCATOR: Cap<std::alloc::System> = Cap::new(std::alloc::System, MAX_MEM);

static HUP_SIGNAL: AtomicU32 = AtomicU32::new(0);

struct ServerState {
    markup: Renderer,
    index: CrateSearchIndex,
    crates: ArcSwap<KitchenSink>,
    page_cache_dir: PathBuf,
    data_dir: PathBuf,
    rt: Handle,
    background_job: tokio::sync::Semaphore,
    foreground_job: tokio::sync::Semaphore,
    start_time: Instant,
    last_ok_response: AtomicU32,
    cdn: Option<Cdn>,
}

type AServerState = web::Data<ServerState>;

fn main() -> ExitCode {
    let mut b = env_logger::Builder::from_default_env();
    b.filter_module("html5ever", log::LevelFilter::Error);
    b.filter_module("tokei", log::LevelFilter::Error);
    b.filter_module("hyper", log::LevelFilter::Warn);
    b.filter_module("tantivy", log::LevelFilter::Error);
    if cfg!(debug_assertions) {
        b.filter_level(log::LevelFilter::Debug);
    }
    let _ = b.try_init().map_err(|e| eprintln!("log: {e}"));

    // console_subscriber::init();

    let sys = actix_web::rt::System::new();

    let rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .thread_name("server")
        .build()
        .unwrap();

    let img_rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .thread_name("images")
        .build()
        .unwrap();

    let res = sys.block_on(run_server(rt.handle().clone(), img_rt.handle().clone()));

    stop();
    rt.shutdown_timeout(Duration::from_secs(2));

    if let Err(e) = res {
        for c in e.chain() {
            error!("Sevrer Error: {c}\n{c:?}");
        }
        return ExitCode::FAILURE;
    }
    ExitCode::SUCCESS
}

const FOREGROUND_JOB_MAX_PERMITS: usize = 32;

async fn run_server(rt: Handle, img_rt: Handle) -> Result<(), anyhow::Error> {
    unsafe { signal_hook::low_level::register(signal_hook::consts::SIGHUP, || HUP_SIGNAL.store(1, Ordering::SeqCst)) }?;
    unsafe { signal_hook::low_level::register(signal_hook::consts::SIGUSR1, || HUP_SIGNAL.store(1, Ordering::SeqCst)) }?;

    kitchen_sink::dont_hijack_ctrlc();

    let dev_root_path = Path::new(if Path::new("data").exists() { "" } else { ".." });

    let data_dir = env::var_os("CRATE_DATA_DIR").map(PathBuf::from).unwrap_or_else(move || dev_root_path.join("data"));
    let public_document_root = env::var_os("DOCUMENT_ROOT").map(PathBuf::from).unwrap_or_else(move || dev_root_path.join("style/public"));
    let page_cache_dir: PathBuf = "/var/tmp/crates-server".into();
    let github_token = env::var("GITHUB_TOKEN").context("GITHUB_TOKEN missing")?;
    let cdn = env::var("CRATE_CDN_CREDENTIALS").ok().as_ref()
        .and_then(|c| c.trim().split_once(' ')).map(|(a,b)| Cdn::new(a, b)).transpose()?;

    let _ = fs::create_dir_all(&page_cache_dir);
    assert!(page_cache_dir.exists(), "{} does not exist", page_cache_dir.display());
    assert!(public_document_root.exists(), "DOCUMENT_ROOT {} does not exist", public_document_root.display());
    assert!(data_dir.exists(), "CRATE_DATA_DIR {} does not exist", data_dir.display());

    let crates = SpawnAbortOnDrop(rt.spawn({
        let data_dir = data_dir.clone();
        let github_token = github_token.clone();
        async move {
            KitchenSink::new(&data_dir, &github_token).await
        }
    })).await??;
    let image_filter = Arc::new(ImageOptimAPIFilter::new("czjpqfbdkz", crates.main_data_dir().join("images.db"), img_rt).await?);
    let markup = Renderer::new_filter(Some(Highlighter::new()), image_filter);

    let index = CrateSearchIndex::new(&data_dir)?;

    if cdn.is_none() {
        warn!("CRATE_CDN_CREDENTIALS is not set");
    }

    let state = web::Data::new(ServerState {
        markup,
        index,
        crates: ArcSwap::from_pointee(crates),
        page_cache_dir,
        data_dir: data_dir.clone(),
        rt,
        background_job: tokio::sync::Semaphore::new(15),
        foreground_job: tokio::sync::Semaphore::new(FOREGROUND_JOB_MAX_PERMITS),
        start_time: Instant::now(),
        last_ok_response: AtomicU32::new(0),
        cdn,
    });

    #[cfg(feature = "cap-alloc")] {
        debug!("Startup mem is {}MB; churned through {}MB", ALLOCATOR.max_allocated()/1_000_000, ALLOCATOR.total_allocated()/1_000_000);
    }

    let enabled = Arc::new(AtomicBool::new(true));

    // event observer
    state.rt.spawn({
        let state = state.clone(); // not Arc
        async move {
            let mut batch_size = 10;
            loop {
                // must reload subscriber to avoid holding a reference to the old db after crates reload
                let mut subscriber = state.crates.load().event_log().subscribe("server observer").unwrap();
                for _ in 0..30 {
                    let Ok(mut batch) = subscriber.next_batch(batch_size.max(1)).await else { batch_size /= 2; break; };
                    debug!("Got events from the log {:?}", batch);
                    batch.allow_auto_ack(false);

                    let batch_res = try_join_all(batch.by_ref().filter_map(|e| e.ok()).map(|ev| async {
                        use kitchen_sink::SharedEvent::*;
                        match ev {
                            CrateIndexed(origin_str) => {
                                info!("Purging local cache {}", origin_str);
                                let o = Origin::from_str(&origin_str);
                                state.crates.load().clear_local_cache_for_crate(&o);
                                let cache_path = state.page_cache_dir.join(cache_file_name_for_origin(&o));
                                let _ = fs::remove_file(&cache_path);
                                background_refresh(state.clone(), cache_path, Box::pin(render_crate_page(state.clone(), o.clone(), 600)), Purge::Crate(o)).await?;
                            },
                            CrateNeedsReindexing(origin_str) => {
                                info!("Heard about outdated crate {}", origin_str);
                                let o = Origin::from_str(&origin_str);
                                let s2 = state.clone();
                                let _ = rt_run_timeout_bg(&state.rt, "bg reload", 300, async move {
                                    if let Ok(Ok(_s)) = timeout(Duration::from_secs(2), s2.background_job.acquire()).await {
                                        // this kicks off indexing if not cached
                                        let _ = s2.crates.load().rich_crate_version_async(&o).await;
                                    }
                                    Ok(())
                                });
                            },
                            DailyStatsUpdated => {
                                let _ = fs::remove_file(state.page_cache_dir.join("_stats_.html"));
                                let _ = fs::remove_file(state.page_cache_dir.join("_new_.html"));
                            },
                        }
                        Ok::<_, anyhow::Error>(())
                    })).await;
                    if (batch_res.is_ok() || batch_size == 1) && batch.ack().is_ok() {
                        batch_size = (10 + batch_size) / 2;
                    } else {
                        batch_size = 1;
                    }
                    tokio::time::sleep(Duration::from_secs(1)).await;
                }
            }
        }
    });

    let watchdog = start_watchdog_threads(&state, &enabled, data_dir, github_token);

    let server = HttpServer::new({
        let state = state.clone(); // not arc!
        move || {
        App::new()
            .app_data(state.clone())
            .app_data(web::FormConfig::default().limit(10_000_000))
            .wrap(middleware::Compress::default())
            .wrap(middleware::DefaultHeaders::new().add(("x-powered-by", HeaderValue::from_static(concat!("actix-web lib.rs/", env!("CARGO_PKG_VERSION"))))))
            .wrap(middleware::Logger::new("%{r}a \"%r\" %s %b \"%{Referer}i\" \"%{User-Agent}i\" %D"))
            .route("/{any:.*}", web::route()
                .guard(actix_web::guard::Header("host", "libs.rs"))
                .guard(fn_guard(|ctx| !ctx.head().uri.path().contains('.'))) // css and png
                .to(handle_libs_redir))
            .route("/{any:.*}", web::route()
                .guard(actix_web::guard::Header("host", "crates.rs"))
                .guard(fn_guard(|ctx| !ctx.head().uri.path().contains('.')))
                .to(handle_crates_rs_redir))
            .route("/", web::get().to(handle_home))
            .route("/search", web::get().to(handle_search))
            .route("/game-engines", web::get().to(handle_game_redirect))
            .route("//", web::get().to(handle_search)) // odd dir listing?
            .route("/index", web::get().to(handle_search)) // old crates.rs/index url
            .route("/categories/{rest:.*}", web::get().to(handle_redirect))
            .route("/new", web::get().to(handle_new_trending))
            .route("/stats", web::get().to(handle_global_stats))
            .route("/keywords/{keyword}", web::get().to(handle_keyword))
            .route("/crates/{crate}", web::get().to(handle_crate))
            .route("/crates/{crate}/features", web::get().to(handle_crate_features))
            .route("/crates/{crate}/versions", web::get().to(handle_crate_all_versions))
            .route("/crates/{crate}/source", web::get().to(handle_crate_source_redirect))
            .route("/crates/{crate}/rev", web::get().to(handle_crate_reverse_dependencies))
            .route("/api/crates/{crate}/downloads.json", web::get().to(handle_crate_api_downloads))
            .route("/crates/{crate}/reverse_dependencies", web::get().to(handle_crate_reverse_dependencies_redir))
            .route("/crates/{crate}/crev", web::get().to(handle_crate_reviews_redirect))
            .route("/crates/{crate}/audit", web::get().to(handle_crate_audit_reviews))
            .route("/~{author}", web::get().to(handle_author))
            .route("/~{author}/dash", web::get().to(handle_maintainer_dashboard_html))
            .route("/~{author}/dash.xml", web::get().to(handle_maintainer_dashboard_xml))
            .route("/~", web::get().to(handle_maintainer_form))
            .route("/dash", web::get().to(handle_maintainer_form))
            .route("/users/{author}", web::get().to(handle_author_redirect))
            .route("/install/{crate:.*}", web::get().to(handle_install))
            .route("/og/{kind}/{crate}.svg", web::get().to(handle_og_svg))
            .route("/og/{kind}/{crate}.png", web::get().to(handle_og_png))
            .route("/compat/{crate:.*}", web::get().to(handle_compat))
            .route("/debug/{crate:.*}", web::get().to(handle_debug))
            .route("/_status", web::get().to(handle_status))
            .route("/{host}/{owner}/{repo}/{crate}", web::get().to(handle_repo_crate))
            .route("/{host}/{owner}/{repo}/{crate}/versions", web::get().to(handle_repo_crate_all_versions))
            .route("/atom.xml", web::get().to(handle_feed))
            .route("/sitemap.xml", web::get().to(handle_sitemap))
            .route("/{crate}/info/refs", web::get().to(handle_git_clone))
            .route("/crates/{crate}/info/refs", web::get().to(handle_git_clone))
            .service(actix_files::Files::new("/", &public_document_root).disable_content_disposition())
            .default_service(web::route().to(default_handler))
    }})
    .bind("127.0.0.1:32531")
    .expect("Can not bind to 127.0.0.1:32531")
    .shutdown_timeout(1);

    // handler installed late after actix hopefully is ready for it
    let _stopme = SpawnAbortOnDrop(state.rt.spawn(async {
        let _ = actix_web::rt::signal::ctrl_c().await;
        info!("Actix got ctrl-c");
        tokio::time::sleep(Duration::from_millis(100)).await;
        stop();
    }));

    info!("Starting HTTP server {} on http://127.0.0.1:32531", env!("CARGO_PKG_VERSION"));
    server.run().await?;

    info!("bye!");

    enabled.store(false, Ordering::SeqCst);
    watchdog.abort();

    Ok(())
}

fn periodic_new_page_refresh(state: AServerState) {
    let new_page = render_new_trending(state.clone(), 600);
    let cache_file = state.page_cache_dir.join("_new_.html");
    let _ = background_refresh(state, cache_file, new_page, Purge::Trending);
}

fn mark_server_still_alive(state: &ServerState) {
    let elapsed = state.start_time.elapsed().as_secs() as u32;
    state.last_ok_response.store(elapsed, Ordering::SeqCst);
}

fn find_category<'a>(slugs: impl Iterator<Item = &'a str>) -> Option<&'static Category> {
    let mut found = None;
    let mut current_sub = &CATEGORIES.root;
    for slug in slugs {
        if let Some(cat) = current_sub.get(slug) {
            found = Some(cat);
            current_sub = &cat.sub;
        } else {
            return None;
        }
    }
    found
}

fn handle_static_page(state: &ServerState, path: &str) -> Result<Option<HttpResponse>, ServerError> {
    if !is_alnum(path) {
        return Ok(None);
    }

    let md_path = state.data_dir.as_path().join(format!("page/{path}.md"));
    if !md_path.exists() {
        return Ok(None);
    }

    let mut chars = path.chars();
    let path_capitalized = chars.next().into_iter().flat_map(char::to_uppercase).chain(chars)
        .map(|c| if c == '-' {' '} else {c}).collect();
    let crates = state.crates.load();
    let index = crates.index().context("crates index")?;
    let crate_num = index.crates_io_crates().context("crates index")?.len();
    let total_crate_num = index.number_of_all_crates().context("crates index")?;

    let md = fs::read_to_string(md_path).context("reading static page")?
        .replace("$CRATE_NUM", &Numeric::english().format_int(crate_num))
        .replace("$TOTAL_CRATE_NUM", &Numeric::english().format_int(total_crate_num));
    let mut page = Vec::with_capacity(md.len() * 2);
    front_end::render_static_page(&mut page, path_capitalized, &Markup::Markdown(md), &state.markup)?;
    minify_html(&mut page);

    mark_server_still_alive(state);
    Ok(Some(HttpResponse::Ok()
        .content_type("text/html;charset=UTF-8")
        .insert_header((header::CACHE_CONTROL, "public, max-age=7200, stale-while-revalidate=604800, stale-if-error=86400"))
        .no_chunking(page.len() as u64)
        .body(page)))
}

async fn handle_libs_redir(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let path = req.path();
    if path.as_bytes().iter().any(|&b| b == b'<' || b == b'"' || b == b':' || b == b'>' || b == b'\'' || b.is_ascii_whitespace()) {
        return Ok(HttpResponse::BadRequest().body(vec![]));
    }

    Ok(HttpResponse::PermanentRedirect()
        .append_header((header::LOCATION, format!("https://lib.rs{path}")))
        .body(vec![]))
}

async fn handle_crates_rs_redir(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let path = req.path();
    if path.as_bytes().iter().any(|&b| b == b'<' || b == b'"' || b == b':' || b == b'>' || b == b'\'' || b.is_ascii_whitespace()) {
        return Ok(HttpResponse::BadRequest().body(vec![]));
    }

    let referrer = req.headers().get(header::REFERER).and_then(|h| h.to_str().ok());
    info!("crates.rs from {}", referrer.unwrap_or("no-referrer"));

    // The warning is meant only for people typing the URL. Links from web pages should be intentional.
    if referrer.is_some() {
        return Ok(HttpResponse::PermanentRedirect()
            .insert_header((header::LOCATION, format!("https://lib.rs{path}")))
            .body(vec![]));
    }

    let mut page = Vec::with_capacity(5000);
    front_end::render_static_trusted_html(&mut page, "Unofficial".into(), format!(r##"
        <h1>crates.<em>rs</em> is unofficial</h1>
        <p>The official crate repository is <a rel="alternate" href="https://crates.io{path}">crates.<b>io</b></a>.</p>
        <p><a rel="canonical" href="https://lib.rs{path}">Continue to the lib.rs website</a>.</p>
    "##))?;

    Ok(HttpResponse::Ok()
        .content_type("text/html;charset=UTF-8")
        .insert_header((header::CACHE_CONTROL, "public, max-age=60"))
        .append_header((header::LINK, format!("<https://lib.rs{path}>; rel=\"canonical\"")))
        .insert_header((header::CONTENT_LOCATION, format!("https://lib.rs{path}")))
        .insert_header((header::REFRESH, format!("10;url=https://lib.rs{path}")))
        .no_chunking(page.len() as u64)
        .body(page))
}

async fn handle_maintainer_form(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let query = req.query_string().trim_start_matches('?');
    if let Some(u) = query.strip_prefix("username=") {
        return Ok(HttpResponse::PermanentRedirect().insert_header((header::LOCATION, format!("/~{u}/dash"))).finish());
    }

    let mut page = Vec::with_capacity(5000);
    front_end::render_static_trusted_html(&mut page, "Maintainer dashboard".into(), r#"
        <h1>Hello Rustaceans</h1>
        <p>Have you published any Rust crates? Find your profile page and dashboard on lib.rs. The dashboard lets you validate your crates' metadata and dependencies.</p>
        <form>
        <p><label>Your <strong>GitHub username</strong>: <input type="text" name="username"></label><p>
        <p><button type="submit">Go to the Dashboard</button></p>
        </form>
    "#.into())?;

    Ok(HttpResponse::Ok()
        .content_type("text/html;charset=UTF-8")
        .insert_header((header::CACHE_CONTROL, "public, max-age=604800"))
        .no_chunking(page.len() as u64)
        .body(page))
}

async fn default_handler(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    if stopped() {
        return Ok(HttpResponse::ServiceUnavailable()
            .insert_header((header::REFRESH, "5"))
            .insert_header((header::CACHE_CONTROL, "must-revalidate"))
            .content_type("text/plain;charset=UTF-8")
            .body("ERROR: lib.rs server is restarting, please try again in a second"));
    }

    let state: &AServerState = req.app_data().expect("appdata");
    let path = req.uri().path();
    if let Some(path_trimmed) = path.strip_suffix('/').or_else(|| if path.starts_with("//") { path.strip_prefix('/') } else { None }) {
        return Ok(HttpResponse::PermanentRedirect().insert_header((header::LOCATION, path_trimmed.trim_end_matches('/'))).finish());
    }

    let mut path = path.strip_prefix('/').unwrap_or(path);
    if path.starts_with('.') || path.starts_with("wp-content/") || path.starts_with("wp-admin/") || path.ends_with(".php") {
        return Ok(HttpResponse::Forbidden().finish());
    }
    let path_no_feed_suffix = path.strip_suffix(".atom").unwrap_or(path);

    if let Some(cat) = find_category(path_no_feed_suffix.split('/')) {
        let atom = path_no_feed_suffix.len() != path.len();
        return Box::pin(handle_category(req, cat, atom)).await;
    }

    if let Some(page) = handle_static_page(state, path)? {
        return Ok(page);
    }

    let subpage_suffix = path.rsplit_once('/').and_then(|(rest, suffix)| {
        if matches!(suffix, "features" | "versions" | "source" | "rev" | "reverse_dependencies" | "crev" | "audit") {
            path = rest;
            Some(suffix)
        } else {
            None
        }
    });

    let mut potential_name = path.trim_matches('/').to_owned();
    if potential_name.len() < 3 {
        let qs = req.query_string().trim_start_matches(|c: char| !c.is_ascii_alphanumeric()).split('&').next().unwrap();
        if !qs.is_empty() {
            potential_name.push('-');
            potential_name.push_str(qs);
        }
    }
    let fallback_query = query_from_rawtext(&path_to_rawtext(&potential_name));

    let crates = state.crates.load();

    let (found_crate, found_keyword, found_author, timed_out) = rt_run_timeout(&state.rt, "url 404 fallback", 3, async move {
        let fragments = potential_name.split(|c: char| !c.is_ascii_alphanumeric() && c != '_' && c != '-')
            .map(|n| n.trim_matches(|c: char| !c.is_ascii_alphanumeric()))
            .filter(|n| !n.is_empty() && (n.len() > 1 || potential_name.len() < 5));
        for candidate_name in fragments.take(5) {
            let Some(o) = Origin::try_from_crates_io_name(candidate_name) else { continue };

            let mut crate_maybe = Box::pin(crates.rich_crate_async(&o)).await.ok();
            // also lowercases
            let inverted_hyphens = invert_hyphens(candidate_name);
            if crate_maybe.is_none() {
                if let Some(o) = Origin::try_from_crates_io_name(&inverted_hyphens) {
                    crate_maybe = Box::pin(crates.rich_crate_async(&o)).await.ok();
                }
            }

            if let Some(k) = crate_maybe {
                // lib.rs/git isn't a real crate, so it's better to search for git instead of showing the crate
                let is_good = crates.is_crate_good_enough_for_short_url(k.origin()).await.unwrap_or(true); // new not-indexed-yet won't be found
                return Ok((Some((k, is_good)), None, None, false));
            }

            if crates.is_it_a_keyword(&inverted_hyphens).await {
                return Ok((None, Some(inverted_hyphens), None, false));
            }

            if let Ok(author) = crates.author_by_login(candidate_name).await {
                if let Ok(rows) = crates.all_crates_of_author(&author).await {
                    return Ok((None, None, Some(author.github.login).filter(|_| !rows.is_empty()), false));
                }
            }
        }
        Ok((None, None, None, false))
    }).await.unwrap_or((None, None, None, true));

    if let Some((k, is_good)) = found_crate {
        let show_crate = if is_good { true } else {
            let name = k.name().to_owned();
            let state = state.clone();
            tokio::task::spawn_blocking(blocking::closure("defsearch", move || {
                // if search just finds this crate (+ its -derive or -core or such), there's no point showing the search
                state.index.search(&name, 4, QueryType::Quick).unwrap_or_default().crates.len() <= 2
            })).await.unwrap()
        };
        return Ok(if show_crate {
            let url = format!("/crates/{}{}{}", Encoded(k.name()), if subpage_suffix.is_some() {"/"} else {""}, subpage_suffix.unwrap_or_default());
            HttpResponse::PermanentRedirect().insert_header((header::LOCATION, url)).finish()
        } else {
            HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, format!("/search?q={}&f=1", Encoded(fallback_query)))).finish()
        })
    }
    if let Some(keyword) = found_keyword {
        return Ok(HttpResponse::SeeOther().insert_header((header::LOCATION, format!("/keywords/{}", Encoded(&keyword)))).finish());
    }
    if let Some(author) = found_author {
        return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, format!("/~{}", Encoded(&author)))).finish());
    }
    if timed_out {
        return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, format!("/search?q={}", Encoded(fallback_query)))).finish());
    }
    render_404_page(state, path, "crate or category").await
}

fn invert_hyphens(name: &str) -> String {
    name.chars().map(|c| if c == '-' {'_'} else if c == '_' {'-'} else { c.to_ascii_lowercase() }).collect()
}

fn path_to_rawtext(path: &str) -> Cow<'_, str> {
    decode(path).unwrap_or(Cow::Borrowed(path))
}

fn query_from_rawtext(rawtext: &str) -> String {
    rawtext.split(|c: char| !c.is_alphanumeric())
        .filter(|&p| !p.is_empty() && p != "crate" && p != "crates" && p != "lib" && p != "gh" && p != "lab")
        .take(10).collect::<Vec<_>>().join(" ")
}

async fn render_404_page(state: &AServerState, path: &str, item_name: &str) -> Result<HttpResponse, ServerError> {
    let page = render_404_page_int(state, path, item_name).await?;
    Ok(serve_page(None, page))
}

fn render_404_page_int(state: &AServerState, path: &str, item_label: &str) -> Pin<Box<dyn Future<Output = Result<Rendered, anyhow::Error>> + Send>> {
    let item_label = item_label.to_owned();
    let state = state.clone();
    let rawtext = path_to_rawtext(path).trim_matches('/').replace('/', " ");
    let fallback_query = query_from_rawtext(&rawtext);

    Box::pin(tokio::task::spawn_blocking(blocking::closure("404", move || {
        let results = state.index.search_fallback(&fallback_query, 7).unwrap_or_default();
        let mut page: Vec<u8> = Vec::with_capacity(32000);
        front_end::render_404_page(&mut page, &fallback_query, &item_label, &rawtext, &results.crates, &state.markup)?;
        Ok(Rendered { page: Page::Html(StatusCode::NOT_FOUND, page), cache_time: 0, cdn_cache: false, enc: None, refresh: false, last_modified: None })
    })).map(|res_res| res_res.map_err(KitchenSinkErr::from)?))
}

fn is_bot(req: &HttpRequest) -> bool {
    req.headers().get(header::USER_AGENT)
        .and_then(|ua| ua.to_str().ok())
        .map_or(true, |ua| {
            ua.contains("+http") ||
            ua.starts_with("python") ||
            ua.bytes().any(|c| c == b'@') ||
            ua.contains("https://") ||
            ua.contains("Headless") ||
            ua.contains("Bot/") ||
            ua.contains("feed/")
        })
}

async fn handle_category(req: HttpRequest, cat: &'static Category, atom_feed: bool) -> Result<HttpResponse, ServerError> {
    let state: &AServerState = req.app_data().expect("appdata");
    let crates = state.crates.load();
    let sort_order = match qstring::QString::from(req.query_string()).get("sort").unwrap_or_default() {
        "popular" => SortOrder::Popularity,
        "new" => SortOrder::Recency,
        _ => SortOrder::Quality,
    };
    let sort_key = match sort_order {
        SortOrder::Quality => "",
        SortOrder::Popularity => "_pop",
        SortOrder::Recency => "_new",
    };
    let kind = if !atom_feed {"html"} else {"xml"};

    let stale = is_bot(&req);
    let mut cache_time = if atom_feed { 3600 } else { 1800 };
    let purge = Purge::Category(cat);
    Ok(serve_page(Some(&req),
        with_file_cache(state, &format!("_{}{sort_key}.{kind}", cat.slug), cache_time, stale, {
            let state = state.clone();
            let deadline = Instant::now() + Duration::from_secs(29);
            rt_run_timeout(&state.clone().rt, "catrender", 30, async move {
                let mut page: Vec<u8> = Vec::with_capacity(150_000);
                let rend = RendCtx::new(deadline);
                front_end::render_category(&mut page, cat, &crates, &state.markup, sort_order, atom_feed, &rend).await?;
                if rend.is_incomplete() {
                    if atom_feed {
                        anyhow::bail!("The feed is not ready yet. Please try again in a few minutes.");
                    } else {
                        cache_time /= 8;
                    }
                }
                let page = if !atom_feed {
                    minify_html(&mut page);
                    Page::Html(StatusCode::OK, page)
                } else {
                    Page::Feed(StatusCode::OK, page)
                };
                mark_server_still_alive(&state);
                Ok::<_, anyhow::Error>(Rendered { page, cdn_cache: false, enc: None, cache_time, refresh: false, last_modified: None })
            })
        }, purge)
        .await?,
    ))
}

async fn handle_home(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let query = req.query_string().trim_start_matches('?');
    if !query.is_empty() && query.find('=').is_none() {
        return Ok(HttpResponse::SeeOther().insert_header((header::LOCATION, format!("/search?q={query}"))).finish());
    }

    let state: &AServerState = req.app_data().expect("appdata");
    let mut cache_time = 3600;
    Ok(serve_page(Some(&req),
        with_file_cache(state, "_.html", cache_time, false, {
            let state = state.clone();
            let deadline = Instant::now() + Duration::from_secs(290);
            run_timeout("homepage", 300, async move {
                let crates = state.crates.load();
                let mut page: Vec<u8> = Vec::with_capacity(32000);
                let rend = RendCtx::new(deadline);
                front_end::render_homepage(&mut page, &crates, &rend).await?;
                minify_html(&mut page);
                mark_server_still_alive(&state);
                if rend.is_incomplete() {
                    cache_time /= 8;
                }
                Ok::<_, anyhow::Error>(Rendered { page: Page::Html(StatusCode::OK, page), cache_time, refresh: false, cdn_cache: false, enc: None, last_modified: Some(Utc::now())})
            })
        }, Purge::Nothing)
        .await?,
    ))
}

async fn handle_redirect(req: HttpRequest) -> HttpResponse {
    let inf = req.match_info();
    let rest = inf.query("rest");
    HttpResponse::PermanentRedirect().insert_header((header::LOCATION, format!("/{rest}"))).finish()
}

async fn handle_git_clone(req: HttpRequest) -> HttpResponse {
    let inf = req.match_info();
    let crate_name = inf.query("crate");
    if let Some(o) = Origin::try_from_crates_io_name(crate_name) {
        let state2: &AServerState = req.app_data().expect("appdata");
        let state = state2.clone();
        if let Ok(url) = rt_run_timeout(&state2.rt, "gc", 60, async move {
            let crates = state.crates.load();
            let k = if let Ok(k) = crates.rich_crate_version_stale_is_ok(&o).await { k } else {
                let o = Origin::from_crates_io_name(&invert_hyphens(o.package_name_icase()));
                crates.rich_crate_version_stale_is_ok(&o).await?
            };
            let r = k.repository().context("no repo")?;

            let mut url = r.canonical_git_url();
            url.truncate(url.trim_end_matches('/').len());
            if !url.ends_with(".git") {
                url.push_str(".git");
            }
            url.push_str("/info/refs?service=git-upload-pack");

            Ok(url)
        }).await {
            return HttpResponse::PermanentRedirect()
                .insert_header(("X-Robots-Tag", "noindex, nofollow"))
                .insert_header((header::LOCATION, url))
                .body("");
        }
    }
    HttpResponse::NotFound().body("Crate not found")
}

async fn handle_crate_reverse_dependencies_redir(req: HttpRequest) -> HttpResponse {
    let inf = req.match_info();
    let rest = inf.query("crate");
    HttpResponse::PermanentRedirect().insert_header((header::LOCATION, format!("/crates/{rest}/rev"))).finish()
}

async fn handle_author_redirect(req: HttpRequest) -> HttpResponse {
    let inf = req.match_info();
    let rest = inf.query("author");
    HttpResponse::PermanentRedirect().insert_header((header::LOCATION, format!("/~{rest}"))).finish()
}

async fn handle_game_redirect(_: HttpRequest) -> HttpResponse {
    HttpResponse::PermanentRedirect().insert_header((header::LOCATION, "/game-development")).finish()
}

async fn handle_repo_crate(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let state: &AServerState = req.app_data().expect("appdata");
    let origin = match get_origin_from_req_match(&req) {
        Ok(res) => res,
        Err(crate_name) => return render_404_page(state, crate_name, "git crate").await,
    };

    if !state.crates.load().crate_exists(&origin) {
        let (repo, _) = origin.into_repo().expect("repohost");
        let url = repo.canonical_http_url("", None);
        return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, url)).finish());
    }

    let stale = is_bot(&req);
    let cache_time = 86400;
    let purge = Purge::Crate(origin.clone());
    Ok(serve_page(Some(&req), with_file_cache(state, &cache_file_name_for_origin(&origin), cache_time, stale, {
        Box::pin(render_crate_page(state.clone(), origin, cache_time))
    }, purge).await?))
}

fn get_origin_from_req_match(req: &HttpRequest) -> Result<Origin, &str> {
    let inf = req.match_info();
    let slug = inf.query("host");
    let owner = inf.query("owner");
    let repo = inf.query("repo");
    let crate_name = inf.query("crate");
    debug!("{} crate {}/{}/{}", slug, owner, repo, crate_name);
    if !is_alnum_dot(owner) || !is_alnum_dot(repo) || !is_alnum(crate_name) {
        return Err(crate_name);
    }

    let origin = match slug {
        "gh" => Origin::from_github(SimpleRepo::new(owner, repo), crate_name),
        "lab" => Origin::from_gitlab(SimpleRepo::new(owner, repo), crate_name),
        _ => return Err(crate_name),
    };
    Ok(origin)
}

fn get_origin_from_subpath(q: &actix_web::dev::Path<Url>) -> Option<Origin> {
    let parts = q.query("crate");
    let mut parts = parts.splitn(4, '/');
    let first = parts.next()?;
    match parts.next() {
        None => Origin::try_from_crates_io_name(first),
        Some(owner) => {
            let repo = parts.next()?;
            let package = parts.next()?;
            match first {
                "github" | "gh" => Some(Origin::from_github(SimpleRepo::new(owner, repo), package)),
                "gitlab" | "lab" => Some(Origin::from_gitlab(SimpleRepo::new(owner, repo), package)),
                _ => None,
            }
        },
    }
}

static SVG_RENDER: Lazy<SvgRenderer> = Lazy::new(SvgRenderer::new);

async fn handle_og_png(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    if req.headers().get(header::ACCEPT).and_then(|h| h.to_str().ok()).is_some_and(|a| a.contains("image/svg")) {
        return handle_og_svg_inner(req, true).await;
    }
    let ((l1, l2), svg) = make_og_svg(req).await?;
    let png = SVG_RENDER.render_to_png(std::str::from_utf8(&svg).context("SVG parsing failed")?)?;
    Ok(HttpResponse::Ok()
        .content_type("image/png")
        .insert_header((header::CACHE_CONTROL, "max-age=604800"))
        .append_header((header::LINK, l1))
        .append_header((header::LINK, l2))
        .insert_header((header::VARY, "accept"))
        .no_chunking(png.len() as u64)
        .body(png))
}

async fn handle_og_svg(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    handle_og_svg_inner(req, false).await
}

async fn handle_og_svg_inner(req: HttpRequest, vary: bool) -> Result<HttpResponse, ServerError> {
    let ((l1, l2), svg) = make_og_svg(req).await?;
    let mut res = HttpResponse::Ok();
    res.content_type("image/svg+xml;charset=UTF-8")
        .insert_header((header::CACHE_CONTROL, "max-age=259200"))
        .append_header((header::LINK, l1))
        .append_header((header::LINK, l2));
    if vary {
        res.insert_header((header::VARY, "accept"));
    }
    Ok(res.no_chunking(svg.len() as u64).body(svg))
}

/// (link_header header, svg)
async fn make_og_svg(req: HttpRequest) -> Result<((String, String), Vec<u8>), ServerError> {
    let state: &AServerState = req.app_data().expect("appdata");
    let crate_name = req.match_info().query("crate");
    let origin = Origin::try_from_crates_io_name(crate_name).context("bad crate name")?;
    let crates = state.crates.load();
    let svg = rt_run_timeout(&state.rt, "preview", 15, async move {
        let k = crates.rich_crate_version_stale_is_ok(&origin).await?;
        let deadline = Instant::now() + Duration::from_secs(6);
        let (mut owners, passive_owners, _) = crates.crate_owners_and_contributors(&k, deadline.into()).await.unwrap_or_default();
        owners.retain(|a| a.owner);
        if owners.is_empty() {
            owners = passive_owners;
        }

        let mut svg = Vec::with_capacity(2048);
        front_end::render_svg_preview(&mut svg, &k, &owners)?;
        Ok(svg)
    }).await?;

    let link_header1 = format!("<https://lib.rs/crates/{}>; rel=\"alternate\"", Encoded(crate_name));
    let link_header2 = format!("<https://lib.rs/og/c/{}.svg>; rel=\"canonical\"", Encoded(crate_name));
    Ok(((link_header1, link_header2), svg))
}

async fn handle_compat(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let origin = get_origin_from_subpath(req.match_info()).ok_or_else(|| anyhow!("boo"))?;
    let state: &AServerState = req.app_data().expect("appdata");
    let crates = state.crates.load();
    crates.clear_local_cache_for_crate(&origin);
    let page = rt_run_timeout(&state.rt, "dbgcrate", 60, async move {
        let all = crates.rich_crate_async(&origin).await?;
        let mut page: Vec<u8> = Vec::with_capacity(32000);
        front_end::render_compat_page(&mut page, all, &crates).await?;
        Ok(page)
    }).await?;
    Ok(HttpResponse::Ok()
        .content_type("text/html;charset=UTF-8")
        .insert_header((header::CACHE_CONTROL, "no-cache"))
        .no_chunking(page.len() as u64)
        .body(page))
}

async fn handle_debug(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let origin = get_origin_from_subpath(req.match_info()).ok_or_else(|| anyhow!("boo"))?;
    let state: &AServerState = req.app_data().expect("appdata");
    let crates = state.crates.load();
    let page = rt_run_timeout(&state.rt, "dbgcrate", 60, async move {
        let mut page: Vec<u8> = Vec::with_capacity(32000);
        front_end::render_debug_page(&mut page, &crates, &origin).await?;
        Ok(page)
    }).await?;
    Ok(HttpResponse::Ok()
        .content_type("text/html;charset=UTF-8")
        .insert_header((header::CACHE_CONTROL, "no-cache"))
        .no_chunking(page.len() as u64)
        .body(page))
}

async fn handle_status(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let state: &AServerState = req.app_data().expect("appdata");
    #[cfg(feature = "cap-alloc")]
    let allocated = ALLOCATOR.allocated()/1_000_000;
    #[cfg(not(feature = "cap-alloc"))]
    let allocated = 0;

    let page = format!("
up: {} min
bg: {}
fg: {}
mem: {}MB",
    state.start_time.elapsed().as_secs()/60,
    state.background_job.available_permits(),
    state.foreground_job.available_permits(),
    allocated);

    let page: Vec<_> = page.into();
    Ok(HttpResponse::Ok()
        .content_type("text/plain;charset=UTF-8")
        .insert_header((header::CACHE_CONTROL, "no-cache"))
        .no_chunking(page.len() as u64)
        .body(page))
}

async fn handle_install(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let state2: &AServerState = req.app_data().expect("appdata");
    let Some(origin) = get_origin_from_subpath(req.match_info()) else {
        return render_404_page(state2, req.path().trim_start_matches("/install"), "crate").await;
    };

    let state = state2.clone();
    let rendered = rt_run_timeout(&state2.rt, "instpage", 30, async move {
        let crates = state.crates.load();
        let ver = blocking::awatch(format!("get {origin:?}"), crates.rich_crate_version_async(&origin)).await?;
        let mut page: Vec<u8> = Vec::with_capacity(32000);
        blocking::awatch(format!("inst for {origin:?}"), front_end::render_install_page(&mut page, &ver, &crates, &state.markup)).await?;
        minify_html(&mut page);
        mark_server_still_alive(&state);
        Ok::<_, anyhow::Error>(Rendered { page: Page::Html(StatusCode::OK, page), cache_time: 24 * 3600, refresh: false, cdn_cache: false, enc: None, last_modified: None })
    }).await?;
    Ok(serve_page(Some(&req), rendered))
}

async fn handle_author(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let login = req.match_info().query("author");
    let state: &AServerState = req.app_data().expect("appdata");

    let aut = match rt_run_timeout(&state.rt, "aut1", 5, {
        let login = login.to_owned();
        let crates = state.crates.load();
        async move { crates.author_by_login(&login).await.map_err(anyhow::Error::from) }
    }).await {
        Ok(aut) => aut,
        Err(e) => {
            debug!("user fetch {} failed: {}", login, e);
            return render_404_page(state, login, "user").await;
        }
    };
    debug!("author page for {login:?}");
    if !aut.github.login.eq_ignore_ascii_case(login) {
        return Ok(HttpResponse::PermanentRedirect().insert_header((header::LOCATION, format!("/~{}", Encoded(&aut.github.login)))).finish());
    }

    let crates = state.crates.load();
    let aut2 = aut.clone();
    let mut rows = rt_run_timeout(&state.rt, "authorpage1", 60, async move { crates.all_crates_of_author(&aut2).await.map_err(anyhow::Error::from) }).await?;
    rows.retain(|c| c.deleted_at.is_none());

    if rows.is_empty() {
        debug!("author {:?} has 0 crates", aut.github.login);
        return Ok(HttpResponse::TemporaryRedirect()
            .insert_header((header::CACHE_CONTROL, "no-cache"))
            .insert_header(("X-Robots-Tag", "noindex, nofollow"))
            .insert_header((header::LOCATION, format!("https://github.com/{}", Encoded(&aut.github.login)))).finish());
    }

    let stale = is_bot(&req);
    let cache_time = 3600;
    Ok(serve_page(Some(&req),
        with_file_cache(state, &format!("@{}.html", aut.github.login.as_ascii_lowercase()), cache_time, stale, {
            let state = state.clone();
            run_timeout(format!("authorpage {}", aut.github.login), 60, async move {
                let crates = state.crates.load();
                let mut page: Vec<u8> = Vec::with_capacity(32000);
                front_end::render_author_page(&mut page, rows, &aut, &crates).await?;
                minify_html(&mut page);
                mark_server_still_alive(&state);
                Ok::<_, anyhow::Error>(Rendered { page: Page::Html(StatusCode::OK, page), refresh: false, cdn_cache: false, enc: None, cache_time, last_modified: None })
            })
        }, Purge::Nothing)
        .await?,
    ))
}

async fn handle_maintainer_dashboard_html(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    handle_maintainer_dashboard(req, false).await
}

async fn handle_maintainer_dashboard_xml(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    handle_maintainer_dashboard(req, true).await
}

/// did you put crate name by accident?
async fn author_by_crate_name(crate_name: &str, state: &ServerState) -> Option<String> {
    let origin = Origin::try_from_crates_io_name(crate_name)?;
    let crate_name = crate_name.to_owned();
    let crates = state.crates.load();
    rt_run_timeout(&state.rt, "chk", 5, async move {
        let owners = crates.crate_owners(&origin, kitchen_sink::CrateOwners::Strict).await?;
        if let Some(gh) = owners.iter().find_map(|o| o.github_login()) {
            if !gh.eq_ignore_ascii_case(&crate_name) {
                return Ok(gh.to_owned());
            }
        }
        anyhow::bail!("none")
    }).await.ok()
}

async fn handle_maintainer_dashboard(req: HttpRequest, atom_feed: bool) -> Result<HttpResponse, ServerError> {
    let login = req.match_info().query("author").trim();
    debug!("maintainer_dashboard for {:?}", login);
    let state: &AServerState = req.app_data().expect("appdata");

    let aut = match rt_run_timeout(&state.rt, "aut1", 5, {
        let login = login.to_owned();
        let crates = state.crates.load();
        async move { crates.author_by_login(&login).await.map_err(anyhow::Error::from) }
    }).await {
        Ok(aut) => aut,
        Err(e) => {
            if let Some(gh) = author_by_crate_name(login, state).await {
                return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, format!("/~{}/dash", Encoded(gh)))).finish());
            }
            debug!("user fetch {} failed: {}", login, e);
            return render_404_page(state, login, "user").await;
        },
    };
    if !atom_feed && !aut.github.login.eq_ignore_ascii_case(login) {
        return Ok(HttpResponse::PermanentRedirect().insert_header((header::LOCATION, format!("/~{}/dash", Encoded(&aut.github.login)))).finish());
    }

    let crates = state.crates.load();
    if crates.crates_io_login_on_blocklist(&aut.github.login, Some(aut.github.id)).is_some() {
        return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, format!("/~{}", Encoded(login)))).finish());
    }

    let aut2 = aut.clone();
    let mut rows = rt_run_timeout(&state.rt, "maintainer_dashboard1", 60, async move { crates.all_crates_of_author(&aut2).await.map_err(anyhow::Error::from) }).await?;
    rows.retain(|c| c.deleted_at.is_none());

    if rows.is_empty() {
        if let Some(gh) = author_by_crate_name(login, state).await {
            return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, format!("/~{}/dash", Encoded(gh)))).finish());
        }
        return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, format!("/~{}", Encoded(&aut.github.login)))).finish());
    }

    let cache_file_name = format!("@{login}.dash{}.html", if atom_feed { "xml" } else { "" });
    let mut cache_time = if atom_feed { 3 * 3600 } else { 600 };
    let rendered = with_file_cache(state, &cache_file_name, cache_time, false, {
        let state = state.clone();
        let deadline = Instant::now() + Duration::from_secs(59);
        run_timeout(format!("maintainer_dashboard2 {} {atom_feed}", aut.github.login), 60, async move {
            let crates = state.crates.load();
            let mut page: Vec<u8> = Vec::with_capacity(32000);
            let rend = RendCtx::new(deadline);
            front_end::render_maintainer_dashboard(&mut page, atom_feed, rows, &aut, &crates, &state.markup, &rend).await?;
            if !atom_feed { minify_html(&mut page); }
            mark_server_still_alive(&state);
            if rend.is_incomplete() {
                cache_time = 1;
            }
            let page = if atom_feed { Page::Feed(StatusCode::OK, page) } else { Page::Html(StatusCode::OK, page) };
            Ok::<_, anyhow::Error>(Rendered { page, refresh: false, cache_time, cdn_cache: false, enc: None, last_modified: None })
        })
    }, Purge::Nothing)
    .await?;
    Ok(serve_page(Some(&req), rendered))
}

async fn handle_crate(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let crate_name = req.match_info().query("crate");
    debug!("crate page for {:?}", crate_name);
    let state: &AServerState = req.app_data().expect("appdata");

    let Some(origin) = Origin::try_from_crates_io_name(crate_name) else {
        return render_404_page(state, crate_name, "crate").await
    };
    let bot = is_bot(&req);
    let stale = bot || req.headers().contains_key(header::IF_NONE_MATCH);
    let cache_time = 600;
    let purge = Purge::Crate(origin.clone());
    let state2 = state.clone();
    Ok(serve_page(Some(&req), with_file_cache(state, &cache_file_name_for_origin(&origin), cache_time, stale, Box::pin(async move {
        if bot {
            tokio::time::sleep(Duration::from_millis(60)).await; // throttle
        }
        render_crate_page(state2, origin, cache_time).await
    }), purge).await?))
}

async fn handle_repo_crate_all_versions(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let state: &AServerState = req.app_data().expect("appdata");
    let origin = match get_origin_from_req_match(&req) {
        Ok(res) => res,
        Err(crate_name) => return render_404_page(state, crate_name, "git crate").await,
    };

    Ok(serve_page(Some(&req), Box::pin(render_crate_all_versions(state.clone(), origin)).await?))
}

async fn handle_crate_all_versions(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let crate_name = req.match_info().query("crate");
    debug!("allver for {:?}", crate_name);
    let state: &AServerState = req.app_data().expect("appdata");
    let Some(origin) = Origin::try_from_crates_io_name(crate_name) else {
        return render_404_page(state, crate_name, "crate").await
    };
    Ok(serve_page(Some(&req), Box::pin(render_crate_all_versions(state.clone(), origin)).await?))
}

async fn handle_crate_features(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let crate_name = req.match_info().query("crate");
    let state: &AServerState = req.app_data().expect("appdata");
    let Some(origin) = Origin::try_from_crates_io_name(crate_name) else {
        return render_404_page(state, crate_name, "crate").await
    };
    Ok(serve_page(Some(&req), Box::pin(render_crate_features(state.clone(), origin)).await?))
}

async fn handle_crate_source_redirect(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let crate_name = req.match_info().query("crate");
    let qs = qstring::QString::from(req.query_string());
    let at_ver = qs.get("at").unwrap_or_default();
    let new_check = qs.get("nofollow").unwrap_or_default();

    // Bots force me to fetch too many crate tarballs
    if new_check.is_empty() || is_bot(&req) {
        let url = format!("https://docs.rs/crate/{crate_name}/{at_ver}/source/");
        return Ok(HttpResponse::TemporaryRedirect()
            .insert_header(("X-Robots-Tag", "noindex, nofollow"))
            .insert_header((header::LOCATION, url))
            .finish());
    }

    if at_ver.is_empty() {
        return Ok(HttpResponse::Forbidden().finish());
    }
    debug!("git redirect for {:?} {:?}", crate_name, at_ver);

    let state: &AServerState = req.app_data().expect("appdata");
    let Some(origin) = Origin::try_from_crates_io_name(crate_name) else {
        return render_404_page(state, crate_name, "git crate").await
    };

    let at_ver = at_ver.to_owned();
    let state = Arc::clone(state);
    let git_url = rt_run_timeout(&state.clone().rt, "git revision lookup", 15, async move {
        let crates = state.crates.load();
        crates.canonical_http_of_crate_at_version(&origin, &at_ver).await.map_err(anyhow::Error::from)
    })
    .await?;
    return Ok(HttpResponse::PermanentRedirect()
        .insert_header(("X-Robots-Tag", "noindex, nofollow"))
        .insert_header((header::CACHE_CONTROL, "public, immutable"))
        .insert_header((header::LOCATION, git_url))
        .finish());
}

async fn handle_crate_api_downloads(crate_name: web::Path<String>, state: web::Data<ServerState>) -> Result<web::Json<HashMap<String, u32>>, ServerError> {
    let origin = Origin::try_from_crates_io_name(&crate_name).context("bad crate name")?;
    let dl = rt_run_timeout(&state.clone().rt, "api dl", 30, async move {
        let kitchen_sink = state.crates.load();
        kitchen_sink.recent_downloads_by_version(&origin).await.map_err(anyhow::Error::from)
    }).await?;
    let dl = dl.into_iter().map(|(k, v)| (k.to_semver().to_string(), v)).collect();
    Ok(web::Json(dl))
}

async fn handle_crate_reverse_dependencies(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let crate_name = req.match_info().query("crate");
    debug!("rev deps for {:?}", crate_name);
    let state: &AServerState = req.app_data().expect("appdata");
    let Some(origin) = Origin::try_from_crates_io_name(crate_name) else {
        return render_404_page(state, crate_name, "crate").await
    };
    Ok(serve_page(Some(&req), Box::pin(render_crate_reverse_dependencies(state.clone(), origin)).await?))
}

async fn handle_crate_reviews_redirect(req: HttpRequest) -> HttpResponse {
    let c = req.match_info().query("crate");
    HttpResponse::PermanentRedirect().insert_header((header::LOCATION, format!("/crates/{}/audit", Encoded(c)))).body(())
}

async fn handle_crate_audit_reviews(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let crate_name = req.match_info().query("crate");
    debug!("crev for {:?}", crate_name);
    let state: &AServerState = req.app_data().expect("appdata");
    let Some(origin) = Origin::try_from_crates_io_name(crate_name) else {
        return render_404_page(state, crate_name, "crate").await
    };
    let state = state.clone();
    Ok(serve_page(Some(&req),
        rt_run_timeout(&state.clone().rt, "revpage", 30, async move {
            let crates = state.crates.load();
            let ver = crates.rich_crate_version_async(&origin).await?;
            let mut page: Vec<u8> = Vec::with_capacity(32000);
            let reviews = crates.reviews_for_crate(ver.origin());
            let vreviews = crates.vets_for_crate(ver.origin()).await.map_err(|e| {
                use std::error::Error;
                error!("cargo vet fail: {e} {:?}", e.source());
            }).unwrap_or_default();
            front_end::render_crate_audit_reviews(&mut page, &reviews, &vreviews, &ver, &crates, &state.markup).await?;
            minify_html(&mut page);
            mark_server_still_alive(&state);
            Ok::<_, anyhow::Error>(Rendered { page: Page::Html(StatusCode::OK, page), cdn_cache: true, enc: None, cache_time: 24 * 3600, refresh: false, last_modified: None })
        })
        .await?,
    ))
}

async fn handle_new_trending(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let state: &AServerState = req.app_data().expect("appdata");

    Ok(serve_page(Some(&req), with_file_cache(state, "_new_.html", 600, false, render_new_trending(state.clone(), 600), Purge::Trending).await?))
}

fn render_new_trending(state: AServerState, mut cache_time: u32) -> Pin<Box<dyn Future<Output=Result<Rendered, anyhow::Error>> + Send + 'static>> {
    let deadline = Instant::now() + Duration::from_secs(59);
    run_timeout("trendpage", 60, async move {
        let crates = state.crates.load();
        let mut page: Vec<u8> = Vec::with_capacity(32000);
        let rend = RendCtx::new(deadline);
        front_end::render_trending_crates(&mut page, Arc::clone(&crates), &state.markup, &rend).await?;
        minify_html(&mut page);
        if rend.is_incomplete() {
            cache_time /= 8;
        }
        Ok::<_, anyhow::Error>(Rendered { page: Page::Html(StatusCode::OK, page), cdn_cache: true, enc: None, refresh: false, cache_time, last_modified: None })
    })
}

async fn handle_global_stats(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let state: &AServerState = req.app_data().expect("appdata");
    let mut cache_time = 6 * 3600;
    Ok(serve_page(Some(&req),
        with_file_cache(state, "_stats_.html", cache_time, true, {
            let state = state.clone();
            let deadline = Instant::now() + Duration::from_secs(200);
            run_timeout("globstats", 300, async move {
                let crates = state.crates.load();
                let mut page: Vec<u8> = Vec::with_capacity(64000);
                let rend = RendCtx::new(deadline);
                front_end::render_global_stats(&mut page, &crates, &state.markup, &rend).await?;
                minify_html(&mut page);
                if rend.is_incomplete() {
                    cache_time /= 10;
                }
                Ok::<_, anyhow::Error>(Rendered { page: Page::Html(StatusCode::OK, page), cdn_cache: false, enc: None, refresh: false, cache_time, last_modified: None })
            })
        }, Purge::Stats)
        .await?,
    ))
}

/// takes path to storage, freshness in seconds, and a function to call on cache miss
/// returns (page, fresh in seconds)
#[inline(never)]
async fn with_file_cache(state: &AServerState, cache_file_name: &str, cache_time: u32, allow_stale: bool, generate: Pin<Box<dyn Future<Output=Result<Rendered, anyhow::Error>> + Send + 'static>>, purge: Purge) -> Result<Rendered, anyhow::Error> {
    let l = RenderLock::new(cache_file_name);
    let _g = blocking::awatch(format_args!("render lock {cache_file_name}"), l.lock()).await;

    let cache_file = state.page_cache_dir.join(cache_file_name);
    if let Ok(modified) = fs::metadata(&cache_file).and_then(|m| m.modified()) {
        let now = SystemTime::now();
        let cache_time = u64::from(cache_time); // TODO: read it from the cache file
        // rebuild in debug always
        let is_fresh = !cfg!(debug_assertions) && modified > (now - Duration::from_secs(cache_time / 20 + 5));
        let is_acceptable = allow_stale || (modified > (now - Duration::from_secs(3600 * 24 * 7 + cache_time * 8)));

        let age_secs = now.duration_since(modified).ok().map_or(0, |age| age.as_secs());

        if let Ok(page) = CachedPage::get_cached_page(&cache_file, is_acceptable) {
            let cache_time_remaining = cache_time.saturating_sub(age_secs);

            debug!("Using cached page {} {}s fresh={:?} acc={:?} bot={allow_stale}", cache_file.display(), cache_time_remaining, is_fresh, is_acceptable);

            if !is_fresh && !allow_stale && !stopped() {
                let _ = background_refresh(state.clone(), cache_file, generate, purge);
            }
            let body = page.html.into_owned();
            return Ok(Rendered {
                enc: page.content_encoding,
                page: Page::Html(StatusCode::OK, body),
                cache_time: if !is_fresh { cache_time_remaining / 4 } else { cache_time_remaining }.max(2) as u32,
                refresh: !is_acceptable,
                cdn_cache: false, // TODO: preserve?
                last_modified: page.last_modified,
            });
        }

        debug!("Cache miss {} {}", cache_file.display(), age_secs);
    } else {
        debug!("Cache miss {} no file", cache_file.display());
    }

    let generate_spawned = state.rt.spawn({
        let state = state.clone();
        let cache_file_name2 = cache_file_name.to_string();
        async move {
            let _s = timeout(Duration::from_secs(10), state.foreground_job.acquire()).await
                .context("The server is too busy to generate this page")
                .inspect_err(|_| warn!("too busy for {cache_file_name2}"))?;
            running()?;
            blocking::awatch(format!("page miss {cache_file_name2}"), generate).await
        }
    });

    let rendered = match timeout(Duration::from_secs(if allow_stale {13} else {8}), generate_spawned).await {
        Ok(res) => match res.map_err(KitchenSinkErr::from)? {
            Ok(rendered) => rendered,
            Err(err) => {
                let kerr = err.downcast::<KitchenSinkErr>()?;
                log::warn!("failure when generating {cache_file_name}: {kerr}");
                let Some((query, item_name)) = not_found_from_err(&kerr) else { return Err(kerr.into()) };
                render_404_page_int(state, query, item_name).await?
            },
        },
        Err(elapsed) => {
            warn!("{cache_file_name} is slow to generate {elapsed:?}");
            return Err(WorkInProgressError(FOREGROUND_JOB_MAX_PERMITS - state.foreground_job.available_permits()).into())
        },
    };

    if let Page::Html(StatusCode::OK, _) = rendered.page {
        running()?;
        let mut rendered = rendered.clone();
        std::thread::spawn(move || {
            let _ = rendered.compress();
            let Page::Html(StatusCode::OK, body) = rendered.page else {
                return;
            };
            let _ = CachedPage {
                html: body.into(),
                cache_time: Some(rendered.cache_time),
                last_modified: rendered.last_modified,
                content_encoding: rendered.enc,
            }.write_serialized_cache(&cache_file);
        });
    }

    Ok(rendered)
}

fn not_found_from_err(kerr: &KitchenSinkErr) -> Option<(&str, &str)> {
    Some(match kerr {
        KitchenSinkErr::CategoryNotFound(cat) => (cat.as_str(), "category"),
        KitchenSinkErr::CrateNotFound(origin) | KitchenSinkErr::NotAPackage(origin) => (origin.package_name_icase(), "crate"),
        KitchenSinkErr::AuthorNotFound(a) => (a.as_str(), "author"),
        KitchenSinkErr::NoVersions(c) => (c.as_str(), "releases of crate"),
        KitchenSinkErr::CrateNotFoundInRepo(c, _) => (&**c, "git crate"),
        KitchenSinkErr::GitCrateNotAllowed(c) => (c.package_name_icase(), "git crate"),
        KitchenSinkErr::GitCheckoutFailed(p, _) => (p.as_str(), "repo"),
        KitchenSinkErr::PageRenderError(e) => return not_found_from_err(e.downcast_ref::<KitchenSinkErr>()?),
        KitchenSinkErr::Context(_, e) => return not_found_from_err(e),
        _other => return None,
    })
}

impl ServerState {
    async fn purge(&self, purge: &Purge) {
        let Some(cdn) = &self.cdn else { return };
        let url = front_end::Urler::new(None);
        let files = match purge {
            Purge::Nothing => return,
            Purge::Trending => vec!["https://lib.rs/new".into()],
            Purge::Category(cat) => vec![
                format!("https://lib.rs{}", url.category(cat, SortOrder::Quality)),
                format!("https://lib.rs{}", url.category(cat, SortOrder::Popularity)),
                format!("https://lib.rs{}", url.category(cat, SortOrder::Recency)),
            ],
            Purge::Stats => vec!["https://lib.rs/stats".into()],
            Purge::Crate(o) => {
                let mut urls = vec![
                    format!("https://lib.rs{}", url.crate_by_origin(o)),
                    format!("https://lib.rs{}", url.install(o)),
                    format!("https://lib.rs{}", url.features(o, None)),
                ];
                if let Some(p) = url.reviews(o) { urls.push(format!("https://lib.rs{p}")); }
                if let Some(p) = url.all_versions(o) { urls.push(format!("https://lib.rs{p}")); }
                urls
            }
        };
        let _ = cdn.purge(&files).await.map_err(|e| log::error!("{e}"));
    }
}

#[inline(never)]
// It must not be an `async fn`!
fn background_refresh(state: AServerState, cache_file: PathBuf, generate: Pin<Box<dyn Future<Output=Result<Rendered, anyhow::Error>> + Send + 'static>>, mut purge: Purge) -> impl Future<Output=Result<(), anyhow::Error>> + 'static + Send {
    debug_assert!(cache_file.is_absolute());
    rt_run_timeout_bg(&state.clone().rt, "refreshbg", 300, async move {
        running()?;
        let mut should_purge_now = true;

        // works like a closure
        let do_refresh = async {
            let start = Instant::now();
            debug!("Bg refresh of {}", cache_file.display());
            if let Ok(_s) = state.background_job.try_acquire() {
                match blocking::awatch(format!("bg {}", cache_file.display()), generate).await {
                    Ok(mut p @ Rendered { page: Page::Html(StatusCode::OK, _), .. }) => {
                        running()?;
                        p.compress()?;
                        let Page::Html(_, body) = p.page else {
                            unreachable!();
                        };

                        // refresh means it's not ready yet, so don't purge cdn yet either
                        if p.refresh {
                            should_purge_now = false;
                        }
                        // if it's very old, nothing important has changed
                        if p.last_modified.is_some_and(|l| {
                            let age = Utc::now().signed_duration_since(l);
                            age.num_weeks().abs() > 10
                        }) {
                            purge = Purge::Nothing;
                        }

                        info!("Done refresh of {} in {}ms <{purge:?} {should_purge_now}>", cache_file.display(), start.elapsed().as_millis() as u32);

                        CachedPage {
                            html: body.into(),
                            last_modified: p.last_modified,
                            cache_time: Some(p.cache_time),
                            content_encoding: p.enc,
                        }.write_serialized_cache(&cache_file)?;
                    }
                    Ok(Rendered { .. }) => {
                        warn!("Refresh of {} bailed out after {}ms  <{purge:?}>", cache_file.display(), start.elapsed().as_millis() as u32);
                        should_purge_now = false;
                        let _ = fs::remove_file(&cache_file);
                    },
                    Err(e) => {
                        should_purge_now = false;
                        error!("Refresh err: {} {}", e.chain().map(|e| e.to_string()).collect::<Vec<_>>().join("; "), cache_file.display());
                    },
                }
            } else {
                should_purge_now = false;
                info!("Too busy to refresh {} <{purge:?}>", cache_file.display());
            }
            Ok(())
        };

        let res = do_refresh.await;

        if !matches!(purge, Purge::Nothing) {
            // we shouldn't be dropping any purges here. The BG refresh may be in response to a crate change event.
            // TODO: this is not good enough for restarts, so there may need to be a more permanent purge-queue
            if !should_purge_now {
                tokio::time::sleep(Duration::from_secs(15)).await;
            }
            state.purge(&purge).await;
        }
        res
    })
}

#[inline(never)]
async fn load_crate(crates: &KitchenSink, origin: &Origin) -> Result<(bool, bool, RichCrate, ArcRichCrateVersion), KitchenSinkErr> {
    let ((bans, all), ver) = futures::try_join!(async move {
        let all = crates.rich_crate_async(origin).await?;
        Ok((crates.crate_blocklist_actions(all.origin()).await, all))
    }, crates.rich_crate_version_async(origin))?;

    let (matches, actions) = bans;
    let redir = matches && actions.iter().all(|a| a.is_redirect());
    Ok((matches, redir, all, ver))
}

#[inline(never)]
fn render_crate_page(state: AServerState, origin: Origin, mut cache_time: u32) -> Pin<Box<dyn Future<Output = Result<Rendered, anyhow::Error>> + Send + 'static>> {
    let deadline = Instant::now() + Duration::from_secs(59);
    run_timeout(format!("cratepage {origin:?}"), 60, async move {
        let crates = state.crates.load();
        let (_, redir, all, ver) = load_crate(&crates, &origin).await?;

        if redir {
            // if there's no message to print, do not collect $200, go to crates.io
            return Ok(Rendered { page: Page::Redirect(format!("https://crates.io/crates/{}", Encoded(all.name()))), cdn_cache: false, enc: None, cache_time: 0, refresh: false, last_modified: None });
        }

        if ver.is_hidden() || ver.is_spam() {
            return render_404_page_int(&state, all.name(), "displayable crate").await;
        }

        let mut page: Vec<u8> = Vec::with_capacity(32000);
        let rend = RendCtx::new(deadline);
        let similar = block_in_place("similarc", || state.index.similar_crates(&origin))
            .map_err(|e| error!("similar: {e}")).unwrap_or_default();
        let last_modified = Box::pin(blocking::awatch(format!("fcp {}", all.origin().package_name_icase()),
            front_end::render_crate_page(&mut page, &all, &ver, similar, &crates, &state.markup, &rend))).await?;
        minify_html(&mut page);
        mark_server_still_alive(&state);
        if rend.is_incomplete() {
            cache_time /= 8;
        }
        if let Some(l) = last_modified {
            let age_days = Utc::now().signed_duration_since(l).num_days().max(0) as u32;
            if age_days < 7 {
                cache_time /= 2;
            } else if age_days > 360 {
                cache_time *= 2;
            }
        }
        Ok::<_, anyhow::Error>(Rendered { page: Page::Html(StatusCode::OK, page), refresh: false, cdn_cache: true, enc: None, last_modified, cache_time })
    })
}

async fn render_crate_all_versions(state: AServerState, origin: Origin) -> Result<Rendered, anyhow::Error> {
    let s = state.clone();
    rt_run_timeout(&s.rt, "allver", 60, async move {
        let crates = state.crates.load();
        let (redir, _, all, ver) = load_crate(&crates, &origin).await?;
        if redir {
            return Ok(Rendered { page: Page::Redirect(format!("https://crates.io/crates/{}/versions", all.name())), cdn_cache: false, enc: None, cache_time: 0, refresh: false, last_modified: None });
        }

        let last_modified = Some(all.most_recent_release());
        let mut page: Vec<u8> = Vec::with_capacity(60000);
        front_end::render_all_versions_page(&mut page, &all, &ver, &crates).await?;
        minify_html(&mut page);
        mark_server_still_alive(&state);
        Ok::<_, anyhow::Error>(Rendered { page: Page::Html(StatusCode::OK, page), cache_time: 24*3600, refresh: false, cdn_cache: false, enc: None, last_modified})
    }).await
}

async fn render_crate_features(state: AServerState, origin: Origin) -> Result<Rendered, anyhow::Error> {
    let s = state.clone();
    rt_run_timeout(&s.rt, "deps", 60, async move {
        let crates = state.crates.load();
        let (redir, _, all, ver) = load_crate(&crates, &origin).await?;
        if redir {
            return Ok(Rendered { page: Page::Redirect(format!("https://docs.rs/crate/{}/{}/features", all.name(), all.most_recent_version())), cdn_cache: false, enc: None, cache_time: 0, refresh: false, last_modified: None });
        }

        let last_modified = Some(all.most_recent_release()); drop(all);
        let mut page: Vec<u8> = Vec::with_capacity(60000);
        front_end::render_features_page(&mut page, &state.markup, &ver, &crates).await?;
        minify_html(&mut page);
        mark_server_still_alive(&state);
        Ok::<_, anyhow::Error>(Rendered { page: Page::Html(StatusCode::OK, page), cache_time: 3*24*3600, cdn_cache: false, enc: None, refresh: false, last_modified})
    }).await
}

async fn render_crate_reverse_dependencies(state: AServerState, origin: Origin) -> Result<Rendered, anyhow::Error> {
    let s = state.clone();
    rt_run_timeout(&s.rt, "revpage2", 30, async move {
        let crates = state.crates.load();
        let ver = crates.rich_crate_version_async(&origin).await?;
        let mut page: Vec<u8> = Vec::with_capacity(32000);
        front_end::render_crate_reverse_dependencies(&mut page, &ver, &crates, &state.markup).await?;
        minify_html(&mut page);
        mark_server_still_alive(&state);
        Ok::<_, anyhow::Error>(Rendered { page: Page::Html(StatusCode::OK, page), cache_time: 24*3600, cdn_cache: false, enc: None, refresh: false, last_modified: None })
    }).await
}

async fn handle_keyword(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let query = match decode(req.match_info().query("keyword")) {
        Ok(q) if !q.is_empty() => q,
        _ => return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, "/")).finish()),
    };

    let state: &AServerState = req.app_data().expect("appdata");
    let query = query.as_ascii_lowercase();
    let norm = state.index.normalize_keyword(&query);
    if norm != query {
        return Ok(HttpResponse::SeeOther().insert_header((header::LOCATION, format!("/keywords/{norm}"))).finish());
    }

    // keywords equal categories should be categories
    if find_category(norm.split('/')).is_some() {
        return Ok(HttpResponse::PermanentRedirect().insert_header((header::LOCATION, format!("/{norm}"))).finish());
    }

    let query = query.into_owned();
    let state2 = state.clone();
    let (query, page) = tokio::task::spawn_blocking(blocking::closure("hkw", move || {
        if !is_alnum(&query) {
            return Ok::<_, anyhow::Error>((query, None));
        }
        let keyword_query = format!("keywords:\"{query}\"");
        let mut results = state2.index.search(&keyword_query, 250, QueryType::Keyword).context("Search index failure")?;

        let crates = state2.crates.load();
        results.crates.retain(|res| crates.crate_exists(&res.origin)); // search index can contain stale items

        if !results.crates.is_empty() && !results.nothing_found_without_dym {
            let mut page: Vec<u8> = Vec::with_capacity(60_000);
            front_end::render_keyword_page(&mut page, &query, &results, &state2.markup)?;
            minify_html(&mut page);
            Ok((query, Some(page)))
        } else {
            Ok((query, None))
        }
    })).await??;

    Ok(if let Some(page) = page {
        HttpResponse::Ok()
            .content_type("text/html;charset=UTF-8")
            .insert_header((header::CACHE_CONTROL, "public, max-age=172800, stale-while-revalidate=604800, stale-if-error=86400"))
            .no_chunking(page.len() as u64)
            .body(page)
    } else {
        HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, format!("/search?q={}", Encoded(&query)))).finish()
    })
}

#[inline(never)]
fn serve_page(req: Option<&HttpRequest>, mut rendered: Rendered) -> HttpResponse {
    let if_none_match = req.and_then(|r| r.headers().get(header::IF_NONE_MATCH).and_then(|h| h.to_str().ok()));
    let expects_br = if_none_match.is_some_and(|i| i.starts_with("\"br"));
    if expects_br {
        debug!("compressing to match 304 {if_none_match:?}");
        let _ = rendered.compress().map_err(|e| error!("Failed to compress: {e} {if_none_match:?}"));
    }

    let Rendered { page, cache_time, refresh, enc, cdn_cache, last_modified } = rendered;

    let (mut status, body, content_type) = match page {
        Page::Html(status, html) => (status, html, "text/html;charset=utf-8"),
        Page::Feed(status, xml) => (status, xml, "application/atom+xml;charset=UTF-8"),
        Page::Redirect(url) => return HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, url)).finish(),
    };

    let err_max = (cache_time * 10).max(3600 * 24 * 2);

    let last_modified_secs = last_modified.map_or(0, |l| Utc::now().signed_duration_since(l).num_seconds().max(0) as u32);
    // if no updates for a year, don't expect more, and keep old page cached for longer
    let extra_time = if last_modified_secs < 3600*24*365 { last_modified_secs / 300 } else { last_modified_secs / 40 };
    let cache_time = cache_time.max(extra_time);

    let etag = status.is_success().then(|| {
        // last-modified is ambiguous, because it's modification of the content, not the whole state
        let mut hasher = blake3::Hasher::new();
        hasher.update(env!("CARGO_PKG_VERSION").as_bytes());
        hasher.update(&body);
        let etag = format!("\"{}{}{}{:.12}\"",
            body.len(),
            if refresh { "tmp" } else { "" },
            enc.as_deref().unwrap_or_default(),
            base64::encode(hasher.finalize().as_bytes()),
        );

        if !refresh && if_none_match.is_some_and(|i| i.contains(&etag)) {
            status = StatusCode::NOT_MODIFIED;
        }

        etag
    });

    let mut h = HttpResponse::build(status);

    if let Some(etag) = etag {
        h.insert_header((header::ETAG, etag));
    }

    if let Some(enc) = enc {
        log::debug!("Serving precompressed with {enc}");
        h.insert_header((header::CONTENT_ENCODING, enc));
    }

    h.content_type(content_type);
    if !status.is_success() {
        h.insert_header((header::CACHE_CONTROL, "max-age=1")); // this is a DoS danger, so be sure it's used only for errors that happen for everyone
    } else if !refresh {
        h.insert_header((header::CACHE_CONTROL, format!("public, max-age={cache_time}, stale-while-revalidate={}, stale-if-error={err_max}", cache_time * 3)));
        if cdn_cache {
            h.insert_header((header::CDN_CACHE_CONTROL, format!("public, max-age={}, stale-while-revalidate={}, stale-if-error={err_max}", 600 + cache_time * 5, cache_time * 7)));
        }
    } else if refresh {
        h.insert_header((header::REFRESH, "2"));
        h.insert_header((header::CACHE_CONTROL, "no-cache, s-maxage=4, must-revalidate"));
    }
    if let Some(l) = last_modified {
        // can't give validator, because then 304 leaves refresh
        if !refresh {
            h.insert_header((header::LAST_MODIFIED, l.to_rfc2822()));
        }
    }

    if status == StatusCode::NOT_MODIFIED {
        h.body(Vec::new())
    } else {
        h.no_chunking(body.len() as u64).body(body)
    }
}

fn is_alnum(q: &str) -> bool {
    q.as_bytes().iter().copied().all(|c| c.is_ascii_alphanumeric() || c == b'_' || c == b'-')
}

fn is_alnum_dot(q: &str) -> bool {
    let mut chars = q.as_bytes().iter().copied();
    if !chars.next().is_some_and(|first| first.is_ascii_alphanumeric() || first == b'_') {
        return false;
    }
    chars.all(|c| c.is_ascii_alphanumeric() || c == b'_' || c == b'-' || c == b'.')
}

#[inline(never)]
async fn handle_search(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let qs = req.query_string().replace('+', "%20");
    let qs = qstring::QString::from(qs.as_str());
    let q = qs.get("q").unwrap_or_default().trim_start();
    if q.is_empty() {
        return Ok(HttpResponse::PermanentRedirect().insert_header((header::LOCATION, "/")).finish());
    }

    // This was unambiguous, and blocking his personal propaganda outlet is the least I can do
    // to oppose the trumpism.
    //
    // https://www.404media.co/hundreds-of-subreddits-are-considering-banning-all-links-to-x/
    //
    // If you come to me with some devil advocacy whataboutism or performative defense
    // of free speech hypotheticals, instead of actually opposing a real fucking neo-nazi
    // who already uses his enormous power and influence to silence vulnerable people,
    // spread harmful lies, and fuel hate, I will know you're a sympathizer.
    // This debate was already concluded at the Nuremberg Trials.
    // https://www.youtube.com/watch?v=IKICKcMU3MU
    if q.starts_with("twitter") {
        return Ok(HttpResponse::PermanentRedirect()
            .insert_header((header::LOCATION, "https://www.wired.com/story/neo-nazis-love-elon-musk-nazi-like-salutes-trumps-inauguration")).finish());
    }

    let is_fallback_query = !qs.get("f").unwrap_or_default().is_empty();
    let is_dym_query = !qs.get("dym").unwrap_or_default().is_empty();

    // robots.txt blocks crawling search results (single-word ones may be ok, but long ones are just
    // bots being lost in the see-also links)
    if (is_fallback_query || is_dym_query || q.contains(' ')) && is_bot(&req) {
        return Ok(HttpResponse::Forbidden().append_header((header::VARY, "user-agent")).finish());
    }

    let state: &AServerState = req.app_data().expect("appdata");
    let query = q.to_owned();
    Ok(serve_page(Some(&req), tokio::task::spawn_blocking({
        let state = state.clone();
        blocking::closure(format!("search {q}"), move || {
            let mut results = if !is_fallback_query {
                state.index.search(&query, 150, QueryType::Relevance)
            } else {
                state.index.search_fallback(&query, 50)
            }.context("Search index failure")?;
            let crates = state.crates.load();
            results.crates.retain(|res| crates.crate_exists(&res.origin)); // search index can contain stale items

            if results.crates.len() == 1 && results.crates[0].crate_base_score > 0.5 {
                return Ok(Rendered {
                    page: Page::Redirect(format!("/crates/{}", Encoded(results.crates.remove(0).crate_name))),
                    cache_time: 60, refresh: false, last_modified: None, cdn_cache: false, enc: None,
                });
            }

            let mut page = Vec::with_capacity(32000);
            front_end::render_serp_page(&mut page, &query, &results, &state.markup)?;
            minify_html(&mut page);
            Ok::<_, anyhow::Error>(Rendered { page: Page::Html(StatusCode::OK, page), cdn_cache: false, enc: None, cache_time: 600, refresh: false, last_modified: None })
        })
    }).await??))
}

#[inline(never)]
async fn handle_sitemap(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let (w, page) = writer::<_, ServerError>().await;
    let state: &AServerState = req.app_data().expect("appdata");
    let _runs_in_bg = state.rt.spawn({
        let state = state.clone();
        async move {
            let mut w = io::BufWriter::with_capacity(16000, w);
            let crates = state.crates.load();
            if let Err(e) = front_end::render_sitemap(&mut w, &crates).await {
                if let Ok(mut w) = w.into_inner() {
                    w.fail(e.into()).await;
                }
            }
        }
    });
    Ok(HttpResponse::Ok()
        .content_type("application/xml;charset=UTF-8")
        .insert_header((header::CACHE_CONTROL, "public, max-age=259200, stale-while-revalidate=72000, stale-if-error=72000"))
        .streaming::<_, ServerError>(page))
}

#[inline(never)]
async fn handle_feed(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let state: &AServerState = req.app_data().expect("appdata");
    let state2 = state.clone();
    let deadline = Instant::now() + Duration::from_secs(59);
    let page = rt_run_timeout(&state.rt, "feed", 60, async move {
        let crates = state2.crates.load();
        let mut page: Vec<u8> = Vec::with_capacity(32000);
        let rend = RendCtx::new(deadline);
        front_end::render_feed(&mut page, &crates, &rend).await?;
        Ok::<_, anyhow::Error>(page)
    }).await?;
    Ok(serve_page(Some(&req), Rendered {
        page: Page::Feed(StatusCode::OK, page),
        cdn_cache: false, enc: None, cache_time: 10800, refresh: false, last_modified: None
    }))
}

#[inline(never)]
fn minify_html(page: &mut Vec<u8>) {
    blocking::watch("minify", || {
        let mut m = html_minifier::HTMLMinifier::new();
        // digest wants bytes anyway
        if matches!(m.digest(&page), Ok(())) {
            let out = m.get_html();
            page.clear();
            page.extend_from_slice(out);
        }
    });
}
