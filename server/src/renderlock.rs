use crate::ServerError;
use util::FxHashMap as HashMap;
use anyhow::anyhow;
use log::debug;
use std::sync::LazyLock as Lazy;
use std::sync::Arc;
use std::time::Duration;
use tokio::time::timeout;
use std::fmt;

/// Makes sure that the same page isn't rendered multiple times in parallel
static PAGES_RENDER: Lazy<std::sync::Mutex<HashMap<Box<str>, Arc<tokio::sync::Mutex<()>>>>> = Lazy::new(|| std::sync::Mutex::new(HashMap::default()));

pub(crate) struct RenderLock<'k> {
    key: &'k str,
    lock: Arc<tokio::sync::Mutex<()>>,
}

pub(crate) struct RenderLockGuard<'k, 'l> {
    key: &'k str,
    _guard: tokio::sync::MutexGuard<'l, ()>,
}

impl Drop for RenderLockGuard<'_, '_> {
    fn drop(&mut self) {
        // this is a bit crap, it should check refcount?
        PAGES_RENDER.lock().unwrap().remove(self.key);
    }
}

impl<'k> RenderLock<'k> {
    pub fn new(key: &'k str) -> Self {
        let lock = PAGES_RENDER.lock().unwrap().entry(key.into()).or_default().clone();
        Self { key, lock }
    }

    pub async fn lock(&self) -> Result<RenderLockGuard<'k, '_>, ServerError> {
        let guard = if let Ok(g) = self.lock.try_lock() {g} else {
            debug!("throttling rendering of {}, because one is already running", self.key);
            timeout(Duration::from_secs(125), self.lock.lock()).await
                .map_err(|_| {
                    PAGES_RENDER.lock().unwrap().remove(self.key);
                    anyhow!("Timed out waiting for cache lock of {}", self.key)
                })?
        };
        Ok(RenderLockGuard {
            key: self.key,
            _guard: guard,
        })
    }
}


/// jobs in queue
#[derive(Debug)]
pub(crate) struct WorkInProgressError(pub usize);


impl std::error::Error for WorkInProgressError {
}


impl fmt::Display for WorkInProgressError {
    #[cold]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "The page is being generated, and should be ready in a few seconds. Lib.rs must fetch and process new data.\nThere {} {} job{} in the queue. Please refresh the page.",
            if self.0 == 1 { "is" } else { "are" },
            self.0,
            if self.0 == 1 { "" } else { "s" },
        )
    }
}
