use crev_data::proof::content::CommonOps;
use crev_data::proof::ContentExt;
pub use crev_data::proof::Date;
use crev_data::Id;
pub use crev_data::{Level, Rating, Version};
pub use crev_lib::Error;
use crev_lib::{Local, ProofStore};
use crev_wot::ProofDB;
use log::{debug, error, warn};
pub use mini_vet as vet;
use parking_lot::{MappedRwLockReadGuard, RwLock, RwLockReadGuard};
use std::time::Duration;

pub mod security;

pub struct Creviews {
    crev_local: Local,
    /// DB load is expensive
    proof_db: RwLock<Option<ProofDB>>,
}

impl Creviews {
    pub fn new() -> Result<Self, Error> {
        let local = Local::auto_create_or_open()
            .map_err(|e| { error!("crev fail {e}"); e})?;
        debug!("crev opened at u={} p={}", local.user_dir_path().display(), local.user_proofs_path().display());
        Ok(Self { crev_local: local, proof_db: Default::default() })
    }

    pub fn setup(&self) -> Result<(), Error> {
        if self.crev_local.get_current_userid().is_err() {
            warn!("no identity in crev, making new");
            let mut warnings = Vec::new();
            let locked_id = self.crev_local.generate_id(None, false, || Ok(String::new()), &mut warnings)?;
            for w in warnings { warn!("crev: {w}"); }
            let unlocked = locked_id.to_unlocked("")?;
            let draft = self.crev_local.build_trust_proof(&unlocked.id, vec![
                Id::crevid_from_str("u29pQAWxKpw1aLrmramSI9CiN8MQflnsFwo_b3leURY")?,
                Id::crevid_from_str("Cp62JH-iG2VWsrPVHauh-zKH-S1lv9WJmPd_m7KFjUI")?,
                Id::crevid_from_str("vfFGLngrNij8Mtla7J0RfdSmQqM4meJFZ128_04L780")?,
            ], crev_data::TrustLevel::High, vec![])?;
            debug!("crev adding root trust {draft:?}");
            self.crev_local.insert(&draft.sign_by(&unlocked)?)?;
            self.update()?;
        }
        Ok(())
    }

    pub fn cleanup(&self) {
        *self.proof_db.try_write_for(Duration::from_secs(30)).unwrap() = None;
    }

    pub fn update(&self) -> Result<(), Error> {
        debug!("fetching all creviews");
        let mut warnings = Vec::new();
        self.crev_local.fetch_all(&mut warnings)
            .map_err(|e| {error!("crev {e}"); e})?;
        for w in warnings { warn!("crev: {w}"); }
        self.cleanup();
        Ok(())
    }

    fn get_loaded_db(&self) -> Option<MappedRwLockReadGuard<'_, ProofDB>> {
        let locked_db = self.proof_db.read();
        RwLockReadGuard::try_map(locked_db, |db: &Option<ProofDB>| db.as_ref()).ok()
    }

    fn get_db(&self) -> Result<MappedRwLockReadGuard<'_, ProofDB>, Error> {
        if let Some(db) = self.get_loaded_db() {
            return Ok(db);
        }

        loop {
            let db = self.crev_local.load_db()?;
            *self.proof_db.write() = Some(db);
            if let Some(db) = self.get_loaded_db() {
                debug!("crev db loaded, has unique_package_review_proof_count={}", db.unique_package_review_proof_count());
                return Ok(db);
            }
            error!("race condition loading proof db"); // cleanup could be called anytime
        }
    }

    pub fn reviews_for_crate(&self, crate_name: &str) -> Result<Vec<Review>, Error> {
        let db = self.get_db()?;

        let mut reviews: Vec<_> = db.get_pkg_reviews_for_name("https://crates.io", crate_name).map(|r| {
            let (thoroughness, understanding, rating) = r
                .review().map_or_else(|| (Level::None, Level::None, Rating::Neutral), |r| (r.thoroughness, r.understanding, r.rating));

            let from = r.from();
            let mut issues = Vec::new();
            for a in &r.advisories {
                issues.push(ReviewIssue {
                    ids: a.ids.clone(),
                    comment_markdown: a.comment.clone(),
                    severity: a.severity,
                });
            }
            for a in &r.issues {
                issues.push(ReviewIssue {
                    ids: vec![a.id.clone()],
                    comment_markdown: a.comment.clone(),
                    severity: a.severity,
                });
            }

            Review {
                author_id: from.id.to_string(),
                author_url: db.lookup_url(&from.id).verified().map(|u| u.url.to_string()),
                unmaintained: r.flags.unmaintained,
                version: r.package.id.version.clone(),
                thoroughness,
                understanding,
                rating,
                comment_markdown: r.comment.clone(),
                date: r.common.date,
                issues,
            }
        }).collect();

        reviews.sort_unstable_by(|a, b| b.author_url.is_some().cmp(&a.author_url.is_some())
            .then(b.version.cmp(&a.version))
            .then_with(|| b.date.cmp(&a.date)));

        Ok(reviews)
    }
}

pub struct Review {
    pub author_id: String,
    pub author_url: Option<String>,
    pub unmaintained: bool,
    pub version: Version,
    pub thoroughness: Level,
    pub understanding: Level,
    pub rating: Rating,
    pub comment_markdown: String,
    pub date: Date,
    pub issues: Vec<ReviewIssue>,
}

pub struct ReviewIssue {
    pub ids: Vec<String>,
    pub severity: Level,
    pub comment_markdown: String,
}

#[test]
fn smoke_test() {
    let c = Creviews::new().unwrap();
    let r = c.reviews_for_crate("rgb").unwrap();
    assert!(r.len() > 1);
}
