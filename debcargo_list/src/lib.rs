use rustc_hash::FxHashSet as HashSet;
use index_debcargo::Index;
use quick_error::quick_error;
use smartstring::alias::String as SmolStr;
use std::path::Path;
use std::sync::RwLock;

quick_error! {
    #[derive(Debug)]
    pub enum Error {
        Git(err: index_debcargo::Error) {
            display("{}", err)
            from()
            source(err)
        }
        Other(s: &'static str) {
            display("{}", s)
            from()
        }
    }
}

struct DebcargoListInner {
    index: Index,
    list: HashSet<SmolStr>,
}

pub struct DebcargoList {
    inner: RwLock<DebcargoListInner>,
}

// this is problematic, because gix::ThreadSafeRepository is not thread safe?
unsafe impl Sync for DebcargoList {}
unsafe impl Send for DebcargoList {}

impl DebcargoList {
    pub fn new(cache_dir: &Path) -> Result<Self, Error> {
        let index = if let Ok(i) = Index::new(cache_dir) { i } else {
            let git_dir = cache_dir.join("debcargo.git");
            // gix picks the wrong branch name?
            let _ = std::process::Command::new("git")
                .args(["fetch", "--depth=1", "-t", "origin", "master:refs/heads/main"])
                .current_dir(&git_dir)
                .status();
            if let Ok(i) = Index::new(cache_dir) { i } else {
                let _ = std::fs::remove_dir_all(&git_dir);
                Index::new(cache_dir)?
            }
        };

        Ok(Self {
            inner: RwLock::new(DebcargoListInner { index, list: HashSet::default() })
        })
    }

    pub fn update(&self) -> Result<(), Error> {
        let mut inner = self.inner.write().unwrap();
        inner.index.update(None)?;
        inner.list.clear();
        inner.fill_list()?;
        Ok(())
    }

    pub fn has(&self, crate_name: &str) -> Result<bool, Error> {
        let mut l = self.inner.read().unwrap();
        if l.list.is_empty() {
            drop(l);
            self.inner.write().unwrap().fill_list()?;
            l = self.inner.read().unwrap();
        }
        Ok(l.list.contains(crate_name))
    }
}

impl DebcargoListInner {
    fn fill_list(&mut self) -> Result<(), Error> {
        if !self.list.is_empty() {
            return Ok(());
        }

        self.list.extend(self.index.list_all()
            .into_iter()
            .filter_map(|res| {
                res.inspect_err(|e| log::warn!("debcargo: {e}")).ok()
            })
            .map(|p| SmolStr::from(p.name)));

        if self.list.is_empty() {
            return Err("unexpectedly empty".into());
        }
        Ok(())
    }
}

#[test]
fn is_send() {
    const fn check<T: Send + Sync>() {}
    check::<DebcargoList>();
}

#[test]
fn debcargo_test() {
    std::fs::create_dir_all("/tmp/debcargo-conf-test").unwrap();
    let l = DebcargoList::new(Path::new("/tmp/debcargo-conf-test")).unwrap();
    assert!(l.has("rand").unwrap());
    assert!(!l.has("").unwrap());
    assert!(!l.has("/").unwrap());
    assert!(!l.has("..").unwrap());
    assert!(!l.has("borkedcratespam").unwrap());
    assert!(l.has("rgb").unwrap());
}
